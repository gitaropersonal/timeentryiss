﻿namespace TimeEntryClient.Const {
    public static class ClientMessages {
        public const string _MSG_ERROR_LOGIN_EMPTY              = "社員番号とパスワードを入力してください";
        public const string _MSG_ERROR_LOGIN_MISS               = "社員番号に誤りがあります";
        public const string _MSG_ERROR_DEL_FLG_ON               = "マスタに存在しないIDが入力されています";
        public const string _MSG_ERROR_FOLDER_PATH_EMPTY        = "フォルダを選択してください";
        public const string _MSG_CONFIRM_CREATE_ROSTER          = "作業・時間外報告書を作成しますか？";
        public const string _MSG_CONFIRM_CREATE_ANALYSIS        = "工数分析Excelを作成しますか？";
        public const string _MSG_CONFIRM_RECREATE               = "既にある作業・時間外報告書を一旦削除します\r\nよろしいですか？";
        public const string _MSG_FINISH_CREATE_ROSTER           = "作成しました\r\n作業・時間外報告書を開きますか？";
        public const string _MSG_FINISH_CREATE_ANALYSIS         = "作成しました\r\n工数分析Excelを開きますか？";
        public const string _MSG_ASK_CLEAR                      = "クリアしますか？";
        public const string _MSG_ASK_CANCEL                     = "キャンセルしますか？";
        public const string _MSG_ASK_SAVE_FORCING_LOGIN         = "保存しますか？\r\n※保存後は再ログインが必要です";
        public const string _MSG_ASK_SAVE                       = "保存しますか？";
        public const string _MSG_ASK_UPDATE                     = "更新しますか？";
        public const string _MSG_ASK_CLOSE                      = "終了しますか？";
        public const string _MSG_ERROR_EMPTY                    = "{0}を入力してください";
        public const string _MSG_ERROR_ILLEGAL                  = "{0}を正しく入力してください";
        public const string _MSG_ERROR_DOUPLE                   = "{0}が重複しています";
        public const string _MSG_ERROR_SELECTED_ROW_NOTHING     = "行を選択してください。";
        public const string _MSG_ERROR_NOT_EXIST_DATA           = "対象のデータがありません";
        public const string _MSG_ERROR_REST_COUNT_ILEEGAL       = "休暇用のチェックは同じ年度につき1つだけ登録してください";
        public const string _MSG_ERROR_CREATE_EXCEL_FAILED      = "Excel作成処理に失敗しました";
        public const string _MSG_ERROR_ASKING_CORRECT_PASS      = "パスワードは1文字以上の半角英数を入力してください";
        public const string _MSG_ERROR_NOT_MUTCH_BU_GROUP_CODE  = "グループコードの前3桁は部署コードと一致させてください";
        public const string _MSG_ERROR_FILE_ALREADY_OPENDE      = "ファイルがすでに開かれています。";
        public const string _MSG_ERROR_WORK_ITEM_ID_FAILED      = "ワークアイテムIDが取得できませんでした。\r\n・プロジェクトID：{0}\r\n・ユーザ：{1}\r\n・対象月：{2}";
        public const string _MSG_FINISH_UPDATE                  = "更新しました";
        public const string _MSG_FINISH_UPDATE_DELAY            = "更新結果がこの画面に反映されるまで、時間を要する場合があります。\r\n数分経ってから再検索してください。";
        public const string _MSG_ERROR_DOUPLE_BOOT              = "多重起動はできません";
        public const string _MSG_ASK_POSITION_MST_CHANGE        = "更新すると作業報告書の出力先が変更されます。\r\nよろしいですか？";

        public const string _LITERAL_CNFIRM              = "確認";
        public const string _LITERAL_EXCRAMATION         = "警告";
        public const string _DIALOG_TITLE_ERROR          = "エラー";
        public const char _PASS_WORD_CHAR                = '*';
    }
}
