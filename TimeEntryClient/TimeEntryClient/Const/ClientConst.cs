﻿namespace TimeEntryClient.Const {
    public static class ClientConst {
        public enum ScreenMode {
            INSERT = 0,
            UPDATE = 10,
        }
        public enum EditStatus {
            NONE = 0,
            INSERT = 1,
            UPDATE = 2,
        }
        public const string _MUTEX_NAME = "TimeEntryClient";
    }
}
