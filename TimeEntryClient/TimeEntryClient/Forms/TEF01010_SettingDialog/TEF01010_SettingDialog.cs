﻿using System.Windows.Forms;
using TimeEntryClient.Const;
using TimeEntryShared.Entity;

namespace TimeEntryClient.Forms {
    public partial class TEF01010_SettingDialog : Form {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="tgtEmploeeInfo"></param>
        public TEF01010_SettingDialog(MstEmploeeEntity emp, ClientConst.ScreenMode ScreenMode) {
            InitializeComponent();
            var fl = new TEF01010_SettingDialogFormLogic(this, emp, ScreenMode);
        }
    }
}
