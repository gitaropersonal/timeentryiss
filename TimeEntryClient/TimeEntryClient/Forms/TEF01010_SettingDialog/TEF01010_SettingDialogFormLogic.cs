﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TimeEntryClient.Const;
using TimeEntryShared.Const;
using TimeEntryShared.Entity;
using TimeEntryShared.Util;
using TimeEntryClient.Util;

namespace TimeEntryClient.Forms {
    public class TEF01010_SettingDialogFormLogic {
        private const string _PARAM_CHARGE_NO = "チャージ№";
        private const string _PARAM_WORK_NAME = "作業名";
        private const string _PARAM_WORK_TIME = "作業時間";
        /// <summary>
        /// ボディパネル
        /// </summary>
        private TEF01010_SettingDialog _Body;
        /// <summary>
        /// Config情報
        /// </summary>
        private TimeEntryConfigEntity _ConfigInfo;
        /// <summary>
        /// 従業員情報
        /// </summary>
        private List<MstEmploeeEntity> _EmploeeInfos;
        /// <summary>
        /// 設定対象の従業員情報
        /// </summary>
        private MstEmploeeEntity _Emp;
        /// <summary>
        /// 画面モード
        /// </summary>
        private ClientConst.ScreenMode _ScreenMode;
        /// <summary>
        /// グループコンボボックスのデータソース用リスト
        /// </summary>
        private Dictionary<string, string> _CmbBuSourceList = new Dictionary<string, string>();
        /// <summary>
        /// グループコンボボックスのデータソース用リスト
        /// </summary>
        private Dictionary<string, string> _CmbGroupSourceList = new Dictionary<string, string>();
        private List<MstGroupEntity> _GroupList = new List<MstGroupEntity>();
        private List<MstBuEntity> _BuList = new List<MstBuEntity>();
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        /// <param name="emp"></param>
        /// <param name="screenMode"></param>
        public TEF01010_SettingDialogFormLogic(TEF01010_SettingDialog body, MstEmploeeEntity emp, ClientConst.ScreenMode screenMode) {
            _Body = body;
            _Emp = emp;
            _ScreenMode = screenMode;

            // 初期化
            Init();
            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.H:
                        if (_Body.linkLblEmploeeNum.Focused) {
                            MessageDialogUtil.ShowExplainEmploeeNum();
                        }
                        break;
                    case Keys.C:
                        _Body.btnCancel.Focus();
                        _Body.btnCancel.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSave.Focus();
                        _Body.btnSave.PerformClick();
                        break;
                }
            };
            // 社員番号とは？ラベルクリック
            _Body.linkLblEmploeeNum.Click += (s, e) => {
                MessageDialogUtil.ShowExplainEmploeeNum();
            };
            // キャンセルボタン押下
            _Body.btnCancel.Click += (s, e) => {
                _Body.Close();
            };
            // 保存ボタン押下
            _Body.btnSave.Click += (s, e) => {
                // エラーチェック
                if (!SaveValidate()) {
                    return;
                }
                // 確認ダイアログ
                if (MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_SAVE) != DialogResult.OK) {
                    return;
                }
                // 保存処理
                try {
                    UpdateEmploeeInfo();
                } catch (Exception ex) {
                    MessageDialogUtil.ShowErroMsg(ex);
                    return;
                }
                _Body.DialogResult = DialogResult.OK;
                _Body.Close();
            };
            // デフォルト作業チェック変更
            _Body.chkDefaultWork.CheckedChanged += (s, e) => {
                SwitchEnableDefaultWork();
            };
            // テキストボックスフォーカスイン
            _Body.txtName.Enter += (s, e) => {
                _Body.txtName.SelectAll();
            };
            // テキストボックスvalidate
            _Body.txtEmploeeNum.Validated += (s, e) => {
                if (string.IsNullOrEmpty(_Body.txtEmploeeNum.Text)) {
                    return;
                }
                _Body.txtEmploeeNum.Text = CommonUtil.CutSpace(StrUtil.CutHalfMarkChars(_Body.txtEmploeeNum.Text)).PadLeft(6, CommonLiteral._CHAR_PAD_ZERO);
            };
            _Body.txtName.Validated += (s, e) => {
                _Body.txtName.Text = StrUtil.CutHalfMarkChars(_Body.txtName.Text);
            };
            _Body.txtDefaultChargeNo1.Validated += (s, e) => {
                _Body.txtDefaultChargeNo1.Text = StrUtil.CutHalfMarkCharsEx(_Body.txtDefaultChargeNo1.Text, CommonLiteral._LITERAL_HYPHEN);
            };
            _Body.txtDefaultChargeNo2.Validated += (s, e) => {
                _Body.txtDefaultChargeNo2.Text = StrUtil.CutHalfMarkCharsEx(_Body.txtDefaultChargeNo2.Text, CommonLiteral._LITERAL_HYPHEN);
            };
            _Body.txtDefaultChargeNo3.Validated += (s, e) => {
                _Body.txtDefaultChargeNo3.Text = StrUtil.CutHalfMarkCharsEx(_Body.txtDefaultChargeNo3.Text, CommonLiteral._LITERAL_HYPHEN);
            };
            _Body.txtDefaultWorkName1.Validated += (s, e) => {
                _Body.txtDefaultWorkName1.Text = StrUtil.CutHalfMarkChars(_Body.txtDefaultWorkName1.Text);
            };
            _Body.txtDefaultWorkName2.Validated += (s, e) => {
                _Body.txtDefaultWorkName2.Text = StrUtil.CutHalfMarkChars(_Body.txtDefaultWorkName2.Text);
            };
            _Body.txtDefaultWorkName3.Validated += (s, e) => {
                _Body.txtDefaultWorkName3.Text = StrUtil.CutHalfMarkChars(_Body.txtDefaultWorkName3.Text);
            };
            // 所属コンボボックスSelectionChangeCommitted
            _Body.cmbBuName.SelectionChangeCommitted += (s, e) => {
                ControlUtil.InitGroupCombo(_GroupList, ref _CmbGroupSourceList, _Body.cmbGroup, _Body.cmbBuName.Text, false);
            };
        }
        /// <summary>
        /// 保存時Validate
        /// </summary>
        private bool SaveValidate() {
            // 背景色初期化
            _Body.txtEmploeeNum.BackColor = Color.Empty;
            _Body.txtName.BackColor = Color.Empty;

            // エラーチェック
            if (_ScreenMode == ClientConst.ScreenMode.INSERT) {
                if (string.IsNullOrEmpty(_Body.txtEmploeeNum.Text)) {
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _Body.lblEmploeeNum.Text));
                    _Body.txtEmploeeNum.BackColor = Color.Red;
                    _Body.txtEmploeeNum.Focus();
                    return false;
                }
                if (_EmploeeInfos.Exists(n => n.EmploeeNum == _Body.txtEmploeeNum.Text)) {
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_DOUPLE, _Body.lblEmploeeNum.Text));
                    _Body.txtEmploeeNum.BackColor = Color.Red;
                    _Body.txtEmploeeNum.Focus();
                    return false;
                }
            }
            if (string.IsNullOrEmpty(_Body.txtName.Text)) {
                MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _Body.lblEmploeeName.Text));
                _Body.txtName.BackColor = Color.Red;
                _Body.txtName.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(_Body.cmbBuName.Text)) {
                MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _Body.lblCmbBu.Text));
                _Body.cmbBuName.BackColor = Color.Red;
                _Body.cmbBuName.Focus();
                return false;
            }
            if (_Body.chkDefaultWork.Checked) {
                if (string.IsNullOrEmpty(_Body.txtDefaultChargeNo1.Text)
                 && string.IsNullOrEmpty(_Body.txtDefaultChargeNo2.Text)
                 && string.IsNullOrEmpty(_Body.txtDefaultChargeNo3.Text)) {
                    // チェックがついているのにデフォ作業が1つも登録されていなければNG
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _PARAM_CHARGE_NO));
                    _Body.txtDefaultChargeNo1.BackColor = Color.Red;
                    _Body.txtDefaultChargeNo1.Focus();
                    return false;
                }
                // デフォルト作業１
                if (!ValidateDefaultWorkCommon(_Body.txtDefaultChargeNo1
                                             , _Body.txtDefaultWorkName1
                                             , _Body.cmbDefaultWorkHH1
                                             , _Body.cmbDefaultWorkMM1)) {
                    return false;
                }
                // デフォルト作業２
                if (!ValidateDefaultWorkCommon(_Body.txtDefaultChargeNo2
                                             , _Body.txtDefaultWorkName2
                                             , _Body.cmbDefaultWorkHH2
                                             , _Body.cmbDefaultWorkMM2)) {
                    return false;
                }
                // デフォルト作業３
                if (!ValidateDefaultWorkCommon(_Body.txtDefaultChargeNo3
                                             , _Body.txtDefaultWorkName3
                                             , _Body.cmbDefaultWorkHH3
                                             , _Body.cmbDefaultWorkMM3)) {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// デフォルト作業Validate（共通）
        /// </summary>
        /// <param name="txtChargeNo"></param>
        /// <param name="txtWorkName"></param>
        /// <param name="cmbWorkHH"></param>
        /// <param name="cmbWorkMM"></param>
        /// <returns></returns>
        private bool ValidateDefaultWorkCommon(TextBox txtChargeNo, TextBox txtWorkName, ComboBox cmbWorkHH, ComboBox cmbWorkMM) {
            txtChargeNo.BackColor = Color.Empty;
            txtWorkName.BackColor = Color.Empty;
            cmbWorkHH.BackColor = Color.Empty;
            cmbWorkMM.BackColor = Color.Empty;

            if (!string.IsNullOrEmpty(txtChargeNo.Text)
             || !string.IsNullOrEmpty(txtWorkName.Text)
             || cmbWorkHH.Text != StrUtil.StrIntToStrPreZero2(decimal.Zero.ToString())
             || cmbWorkMM.Text != StrUtil.StrIntToStrPreZero2(decimal.Zero.ToString())) {
                if (string.IsNullOrEmpty(txtChargeNo.Text)) {
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _PARAM_CHARGE_NO));
                    txtChargeNo.BackColor = Color.Red;
                    txtChargeNo.Focus();
                    return false;
                }
                if (string.IsNullOrEmpty(txtWorkName.Text)) {
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _PARAM_WORK_NAME));
                    txtWorkName.BackColor = Color.Red;
                    txtWorkName.Focus();
                    return false;
                }
                if (cmbWorkHH.Text == StrUtil.StrIntToStrPreZero2(decimal.Zero.ToString())
                 && cmbWorkMM.Text == StrUtil.StrIntToStrPreZero2(decimal.Zero.ToString())) {
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _PARAM_WORK_TIME));
                    cmbWorkHH.BackColor = Color.Red;
                    cmbWorkMM.BackColor = Color.Red;
                    cmbWorkHH.Focus();
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 従業員情報更新
        /// </summary>
        private void UpdateEmploeeInfo() {
            // TimeTrackerユーザ情報ロード
            var ConfigInfo = ModelUtil.LoadConfig();

            // 従業員情報csvロード
            var EmploeeInfos = ModelUtil.LoadEmploeeMst(ConfigInfo);

            // 従業員情報更新
            bool isUpd = true;
            var tgtEmp = EmploeeInfos.Where(n => n.EmploeeNum == _Emp.EmploeeNum).FirstOrDefault();
            if (tgtEmp == null) {
                tgtEmp = new MstEmploeeEntity();
                isUpd = false;
            }
            // 画面から保存情報取得
            tgtEmp = GetSaveEntityFromScreen(tgtEmp);

            if (isUpd) {
                // 更新
                ModelUtil.UpdEmploeeMst(ConfigInfo.ConnStrTimeEntryDB, tgtEmp);
                ModelUtil.UpdDefaultWorkMst(ConfigInfo.ConnStrTimeEntryDB, tgtEmp);
                return;
            }
            // 登録
            ModelUtil.InsertEmploeeMst(ConfigInfo.ConnStrTimeEntryDB, tgtEmp);
            ModelUtil.InsertDefaultWorkMst(ConfigInfo.ConnStrTimeEntryDB, tgtEmp);
        }
        /// <summary>
        /// 画面から保存情報取得
        /// </summary>
        /// <param name="ret"></param>
        /// <returns></returns>
        private MstEmploeeEntity GetSaveEntityFromScreen(MstEmploeeEntity ret) {
            ret.EmploeeNum           = _Body.txtEmploeeNum.Text;
            ret.Name                 = _Body.txtName.Text;
            ret.BuCode               = _Body.cmbBuName.Text.Substring(0, 3);
            ret.GroupCode            = string.IsNullOrEmpty(_Body.cmbGroup.Text) ? string.Empty : _Body.cmbGroup.Text.Substring(0, 6);
            ret.DeleteFlg            = GetFlgVal(_Body.chkDelFlg.Checked);
            ret.DefaultWorkFlg       = GetFlgVal(_Body.chkDefaultWork.Checked);
            ret.MhscFlg              = GetFlgVal(_Body.chkMHSC.Checked);
            ret.ForcingShiftBFlg     = GetFlgVal(_Body.chkForcingBshift.Checked);
            ret.NightShiftFlg        = GetFlgVal(_Body.chkNightShift.Checked);
            ret.DefaultWorkChargeNo1 = _Body.txtDefaultChargeNo1.Text;
            ret.DefaultWorkChargeNo2 = _Body.txtDefaultChargeNo2.Text;
            ret.DefaultWorkChargeNo3 = _Body.txtDefaultChargeNo3.Text;
            ret.DefaultWorkName1     = _Body.txtDefaultWorkName1.Text;
            ret.DefaultWorkName2     = _Body.txtDefaultWorkName2.Text;
            ret.DefaultWorkName3     = _Body.txtDefaultWorkName3.Text;
            ret.DefaultWorkTimeH1    = StrUtil.strToIntToString(_Body.cmbDefaultWorkHH1.Text);
            ret.DefaultWorkTimeH2    = StrUtil.strToIntToString(_Body.cmbDefaultWorkHH2.Text);
            ret.DefaultWorkTimeH3    = StrUtil.strToIntToString(_Body.cmbDefaultWorkHH3.Text);
            ret.DefaultWorkTimeM1    = StrUtil.strToIntToString(_Body.cmbDefaultWorkMM1.Text);
            ret.DefaultWorkTimeM2    = StrUtil.strToIntToString(_Body.cmbDefaultWorkMM2.Text);
            ret.DefaultWorkTimeM3    = StrUtil.strToIntToString(_Body.cmbDefaultWorkMM3.Text);
            return ret;
        }
        /// <summary>
        /// 初期化
        /// </summary>
        private void Init() {
            // Configロード
            _ConfigInfo = ModelUtil.LoadConfig();
            _BuList = ModelUtil.LoadBuMst(_ConfigInfo);
            _GroupList = ModelUtil.LoadGroupMst(_ConfigInfo);

            // 従業員情報ロード
            _EmploeeInfos = ModelUtil.LoadEmploeeMst(_ConfigInfo);

            ControlUtil.InitBuCombo(_BuList, ref _CmbBuSourceList, _Body.cmbBuName, false);
            string buCode = string.Empty;
            if (!string.IsNullOrEmpty(_Emp.BuCode) && _BuList.Exists(n => n.BuCode == _Emp.BuCode)) {
                buCode = string.Format(FormatConst._FORMAT_CMB_BU_FORMAT, _Emp.BuCode, _BuList.Where(n => n.BuCode == _Emp.BuCode).FirstOrDefault().BuName);
            }
            string groupCode = string.Empty;
            if (!string.IsNullOrEmpty(_Emp.GroupCode) && _GroupList.Exists(n => n.GroupCode == _Emp.GroupCode)) {
                groupCode = string.Format(FormatConst._FORMAT_CMB_GROUP_FORMAT, _Emp.GroupCode, _GroupList.Where(n => n.GroupCode == _Emp.GroupCode).FirstOrDefault().GroupName);
            }
            _Body.txtEmploeeNum.Text       = _Emp.EmploeeNum;
            _Body.txtName.Text             = _Emp.Name;
            _Body.cmbBuName.Text           = buCode;
            ControlUtil.InitGroupCombo(_GroupList, ref _CmbGroupSourceList, _Body.cmbGroup, _Body.cmbBuName.Text, false);
            _Body.cmbGroup.Text            = groupCode;
            _Body.chkDelFlg.Checked        = GetChkVal(_Emp.DeleteFlg);
            _Body.chkDefaultWork.Checked   = GetChkVal(_Emp.DefaultWorkFlg);
            _Body.chkMHSC.Checked          = GetChkVal(_Emp.MhscFlg);
            _Body.chkNightShift.Checked    = GetChkVal(_Emp.NightShiftFlg);
            _Body.chkForcingBshift.Checked = GetChkVal(_Emp.ForcingShiftBFlg);
            _Body.txtDefaultChargeNo1.Text = _Emp.DefaultWorkChargeNo1;
            _Body.txtDefaultChargeNo2.Text = _Emp.DefaultWorkChargeNo2;
            _Body.txtDefaultChargeNo3.Text = _Emp.DefaultWorkChargeNo3;
            _Body.txtDefaultWorkName1.Text = _Emp.DefaultWorkName1;
            _Body.txtDefaultWorkName2.Text = _Emp.DefaultWorkName2;
            _Body.txtDefaultWorkName3.Text = _Emp.DefaultWorkName3;

            _Body.cmbDefaultWorkHH1.DataSource = ControlUtil.GetCmbHHList();
            _Body.cmbDefaultWorkHH2.DataSource = ControlUtil.GetCmbHHList();
            _Body.cmbDefaultWorkHH3.DataSource = ControlUtil.GetCmbHHList();
            _Body.cmbDefaultWorkMM1.DataSource = ControlUtil.GetCmbMMList();
            _Body.cmbDefaultWorkMM2.DataSource = ControlUtil.GetCmbMMList();
            _Body.cmbDefaultWorkMM3.DataSource = ControlUtil.GetCmbMMList();

            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkHH1, StrUtil.StrIntToStrPreZero2(_Emp.DefaultWorkTimeH1));
            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkHH2, StrUtil.StrIntToStrPreZero2(_Emp.DefaultWorkTimeH2));
            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkHH3, StrUtil.StrIntToStrPreZero2(_Emp.DefaultWorkTimeH3));
            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkMM1, StrUtil.StrIntToStrPreZero2(_Emp.DefaultWorkTimeM1));
            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkMM2, StrUtil.StrIntToStrPreZero2(_Emp.DefaultWorkTimeM2));
            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkMM3, StrUtil.StrIntToStrPreZero2(_Emp.DefaultWorkTimeM3));

            SwitchEnableDefaultWork();
            _Body.txtEmploeeNum.Focus();
            _Body.txtEmploeeNum.ReadOnly = !(_ScreenMode == ClientConst.ScreenMode.INSERT);
        }
        /// <summary>
        /// 画面のチェックボックスの値からフラグの値を返す
        /// </summary>
        /// <param name="chkVal"></param>
        /// <returns></returns>
        private string GetFlgVal(bool chkVal) {
            if (chkVal) {
                return FlgValConst._STR_FLG_ON;
            }
            return FlgValConst._STR_FLG_OFF;
        }
        /// <summary>
        /// フラグから画面のチェックボックスのチェックの値を返す
        /// </summary>
        /// <param name="strDelFlg"></param>
        /// <returns></returns>
        private bool GetChkVal(string strDelFlg) {
            return strDelFlg == FlgValConst._STR_FLG_ON;
        }
        /// <summary>
        /// デフォルト作業関連のコントロール使用可否切り替え
        /// </summary>
        private void SwitchEnableDefaultWork() {
            bool isDefaultWork = _Body.chkDefaultWork.Checked;
            _Body.txtDefaultChargeNo1.Enabled = isDefaultWork;
            _Body.txtDefaultChargeNo2.Enabled = isDefaultWork;
            _Body.txtDefaultChargeNo3.Enabled = isDefaultWork;
            _Body.txtDefaultWorkName1.Enabled = isDefaultWork;
            _Body.txtDefaultWorkName2.Enabled = isDefaultWork;
            _Body.txtDefaultWorkName3.Enabled = isDefaultWork;
            _Body.cmbDefaultWorkHH1.Enabled = isDefaultWork;
            _Body.cmbDefaultWorkHH2.Enabled = isDefaultWork;
            _Body.cmbDefaultWorkHH3.Enabled = isDefaultWork;
            _Body.cmbDefaultWorkMM1.Enabled = isDefaultWork;
            _Body.cmbDefaultWorkMM2.Enabled = isDefaultWork;
            _Body.cmbDefaultWorkMM3.Enabled = isDefaultWork;
        }
    }
}
