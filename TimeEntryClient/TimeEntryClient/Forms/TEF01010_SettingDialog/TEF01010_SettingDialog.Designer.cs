﻿namespace TimeEntryClient.Forms {
    partial class TEF01010_SettingDialog {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TEF01010_SettingDialog));
            this.grpLoginCondition = new System.Windows.Forms.GroupBox();
            this.linkLblEmploeeNum = new System.Windows.Forms.LinkLabel();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.cmbBuName = new System.Windows.Forms.ComboBox();
            this.lblCmbGroup = new System.Windows.Forms.Label();
            this.lblCmbBu = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkNightShift = new System.Windows.Forms.CheckBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblEmploeeName = new System.Windows.Forms.Label();
            this.txtEmploeeNum = new System.Windows.Forms.TextBox();
            this.lblEmploeeNum = new System.Windows.Forms.Label();
            this.cmbDefaultWorkMM2 = new System.Windows.Forms.ComboBox();
            this.cmbDefaultWorkMM3 = new System.Windows.Forms.ComboBox();
            this.cmbDefaultWorkMM1 = new System.Windows.Forms.ComboBox();
            this.cmbDefaultWorkHH2 = new System.Windows.Forms.ComboBox();
            this.cmbDefaultWorkHH3 = new System.Windows.Forms.ComboBox();
            this.cmbDefaultWorkHH1 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chkForcingBshift = new System.Windows.Forms.CheckBox();
            this.chkMHSC = new System.Windows.Forms.CheckBox();
            this.chkDefaultWork = new System.Windows.Forms.CheckBox();
            this.txtDefaultWorkName3 = new System.Windows.Forms.TextBox();
            this.txtDefaultChargeNo3 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDefaultWorkName2 = new System.Windows.Forms.TextBox();
            this.txtDefaultChargeNo2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDefaultWorkName1 = new System.Windows.Forms.TextBox();
            this.txtDefaultChargeNo1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.chkDelFlg = new System.Windows.Forms.CheckBox();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.grpLoginCondition.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpLoginCondition
            // 
            this.grpLoginCondition.Controls.Add(this.linkLblEmploeeNum);
            this.grpLoginCondition.Controls.Add(this.cmbGroup);
            this.grpLoginCondition.Controls.Add(this.cmbBuName);
            this.grpLoginCondition.Controls.Add(this.lblCmbGroup);
            this.grpLoginCondition.Controls.Add(this.lblCmbBu);
            this.grpLoginCondition.Controls.Add(this.label1);
            this.grpLoginCondition.Controls.Add(this.chkNightShift);
            this.grpLoginCondition.Controls.Add(this.txtName);
            this.grpLoginCondition.Controls.Add(this.lblEmploeeName);
            this.grpLoginCondition.Controls.Add(this.txtEmploeeNum);
            this.grpLoginCondition.Controls.Add(this.lblEmploeeNum);
            this.grpLoginCondition.Controls.Add(this.cmbDefaultWorkMM2);
            this.grpLoginCondition.Controls.Add(this.cmbDefaultWorkMM3);
            this.grpLoginCondition.Controls.Add(this.cmbDefaultWorkMM1);
            this.grpLoginCondition.Controls.Add(this.cmbDefaultWorkHH2);
            this.grpLoginCondition.Controls.Add(this.cmbDefaultWorkHH3);
            this.grpLoginCondition.Controls.Add(this.cmbDefaultWorkHH1);
            this.grpLoginCondition.Controls.Add(this.label9);
            this.grpLoginCondition.Controls.Add(this.label8);
            this.grpLoginCondition.Controls.Add(this.label7);
            this.grpLoginCondition.Controls.Add(this.chkForcingBshift);
            this.grpLoginCondition.Controls.Add(this.chkMHSC);
            this.grpLoginCondition.Controls.Add(this.chkDefaultWork);
            this.grpLoginCondition.Controls.Add(this.txtDefaultWorkName3);
            this.grpLoginCondition.Controls.Add(this.txtDefaultChargeNo3);
            this.grpLoginCondition.Controls.Add(this.label6);
            this.grpLoginCondition.Controls.Add(this.txtDefaultWorkName2);
            this.grpLoginCondition.Controls.Add(this.txtDefaultChargeNo2);
            this.grpLoginCondition.Controls.Add(this.label4);
            this.grpLoginCondition.Controls.Add(this.txtDefaultWorkName1);
            this.grpLoginCondition.Controls.Add(this.txtDefaultChargeNo1);
            this.grpLoginCondition.Controls.Add(this.label3);
            this.grpLoginCondition.Controls.Add(this.chkDelFlg);
            this.grpLoginCondition.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpLoginCondition.Location = new System.Drawing.Point(12, 24);
            this.grpLoginCondition.Name = "grpLoginCondition";
            this.grpLoginCondition.Size = new System.Drawing.Size(786, 325);
            this.grpLoginCondition.TabIndex = 1;
            this.grpLoginCondition.TabStop = false;
            this.grpLoginCondition.Text = "社員情報";
            // 
            // linkLblEmploeeNum
            // 
            this.linkLblEmploeeNum.AutoSize = true;
            this.linkLblEmploeeNum.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.linkLblEmploeeNum.Location = new System.Drawing.Point(25, 60);
            this.linkLblEmploeeNum.Name = "linkLblEmploeeNum";
            this.linkLblEmploeeNum.Size = new System.Drawing.Size(126, 13);
            this.linkLblEmploeeNum.TabIndex = 1;
            this.linkLblEmploeeNum.TabStop = true;
            this.linkLblEmploeeNum.Text = "※社員番号とは？（H）";
            // 
            // cmbGroup
            // 
            this.cmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroup.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(142, 144);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(466, 24);
            this.cmbGroup.TabIndex = 4;
            // 
            // cmbBuName
            // 
            this.cmbBuName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBuName.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.cmbBuName.FormattingEnabled = true;
            this.cmbBuName.Location = new System.Drawing.Point(142, 117);
            this.cmbBuName.Name = "cmbBuName";
            this.cmbBuName.Size = new System.Drawing.Size(466, 24);
            this.cmbBuName.TabIndex = 3;
            // 
            // lblCmbGroup
            // 
            this.lblCmbGroup.AutoSize = true;
            this.lblCmbGroup.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCmbGroup.Location = new System.Drawing.Point(23, 147);
            this.lblCmbGroup.Name = "lblCmbGroup";
            this.lblCmbGroup.Size = new System.Drawing.Size(58, 16);
            this.lblCmbGroup.TabIndex = 50;
            this.lblCmbGroup.Text = "グループ";
            // 
            // lblCmbBu
            // 
            this.lblCmbBu.AutoSize = true;
            this.lblCmbBu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCmbBu.Location = new System.Drawing.Point(24, 120);
            this.lblCmbBu.Name = "lblCmbBu";
            this.lblCmbBu.Size = new System.Drawing.Size(40, 16);
            this.lblCmbBu.TabIndex = 49;
            this.lblCmbBu.Text = "部署";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(23, 180);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 16);
            this.label1.TabIndex = 46;
            this.label1.Text = "設定フラグ";
            // 
            // chkNightShift
            // 
            this.chkNightShift.AutoSize = true;
            this.chkNightShift.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkNightShift.Location = new System.Drawing.Point(516, 180);
            this.chkNightShift.Name = "chkNightShift";
            this.chkNightShift.Size = new System.Drawing.Size(107, 20);
            this.chkNightShift.TabIndex = 13;
            this.chkNightShift.Text = "夜勤担当者";
            this.chkNightShift.UseVisualStyleBackColor = true;
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtName.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtName.Location = new System.Drawing.Point(142, 91);
            this.txtName.MaxLength = 10;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(139, 23);
            this.txtName.TabIndex = 2;
            this.txtName.Text = "あいうえおかきくけこ";
            // 
            // lblEmploeeName
            // 
            this.lblEmploeeName.AutoSize = true;
            this.lblEmploeeName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEmploeeName.Location = new System.Drawing.Point(23, 94);
            this.lblEmploeeName.Name = "lblEmploeeName";
            this.lblEmploeeName.Size = new System.Drawing.Size(56, 16);
            this.lblEmploeeName.TabIndex = 45;
            this.lblEmploeeName.Text = "社員名";
            // 
            // txtEmploeeNum
            // 
            this.txtEmploeeNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtEmploeeNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtEmploeeNum.Location = new System.Drawing.Point(142, 32);
            this.txtEmploeeNum.MaxLength = 6;
            this.txtEmploeeNum.Name = "txtEmploeeNum";
            this.txtEmploeeNum.Size = new System.Drawing.Size(56, 23);
            this.txtEmploeeNum.TabIndex = 0;
            this.txtEmploeeNum.Text = "012345";
            this.txtEmploeeNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblEmploeeNum
            // 
            this.lblEmploeeNum.AutoSize = true;
            this.lblEmploeeNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEmploeeNum.Location = new System.Drawing.Point(23, 35);
            this.lblEmploeeNum.Name = "lblEmploeeNum";
            this.lblEmploeeNum.Size = new System.Drawing.Size(72, 16);
            this.lblEmploeeNum.TabIndex = 44;
            this.lblEmploeeNum.Text = "社員番号";
            // 
            // cmbDefaultWorkMM2
            // 
            this.cmbDefaultWorkMM2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkMM2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkMM2.FormattingEnabled = true;
            this.cmbDefaultWorkMM2.Location = new System.Drawing.Point(712, 237);
            this.cmbDefaultWorkMM2.Name = "cmbDefaultWorkMM2";
            this.cmbDefaultWorkMM2.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkMM2.TabIndex = 33;
            // 
            // cmbDefaultWorkMM3
            // 
            this.cmbDefaultWorkMM3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkMM3.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkMM3.FormattingEnabled = true;
            this.cmbDefaultWorkMM3.Location = new System.Drawing.Point(712, 262);
            this.cmbDefaultWorkMM3.Name = "cmbDefaultWorkMM3";
            this.cmbDefaultWorkMM3.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkMM3.TabIndex = 43;
            // 
            // cmbDefaultWorkMM1
            // 
            this.cmbDefaultWorkMM1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkMM1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkMM1.FormattingEnabled = true;
            this.cmbDefaultWorkMM1.Location = new System.Drawing.Point(712, 212);
            this.cmbDefaultWorkMM1.Name = "cmbDefaultWorkMM1";
            this.cmbDefaultWorkMM1.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkMM1.TabIndex = 23;
            // 
            // cmbDefaultWorkHH2
            // 
            this.cmbDefaultWorkHH2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkHH2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkHH2.FormattingEnabled = true;
            this.cmbDefaultWorkHH2.Location = new System.Drawing.Point(656, 237);
            this.cmbDefaultWorkHH2.Name = "cmbDefaultWorkHH2";
            this.cmbDefaultWorkHH2.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkHH2.TabIndex = 32;
            // 
            // cmbDefaultWorkHH3
            // 
            this.cmbDefaultWorkHH3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkHH3.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkHH3.FormattingEnabled = true;
            this.cmbDefaultWorkHH3.Location = new System.Drawing.Point(656, 262);
            this.cmbDefaultWorkHH3.Name = "cmbDefaultWorkHH3";
            this.cmbDefaultWorkHH3.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkHH3.TabIndex = 42;
            // 
            // cmbDefaultWorkHH1
            // 
            this.cmbDefaultWorkHH1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkHH1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkHH1.FormattingEnabled = true;
            this.cmbDefaultWorkHH1.ItemHeight = 15;
            this.cmbDefaultWorkHH1.Location = new System.Drawing.Point(656, 212);
            this.cmbDefaultWorkHH1.Name = "cmbDefaultWorkHH1";
            this.cmbDefaultWorkHH1.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkHH1.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(698, 265);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 16);
            this.label9.TabIndex = 41;
            this.label9.Text = "：";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(698, 240);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 16);
            this.label8.TabIndex = 38;
            this.label8.Text = "：";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(698, 215);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(16, 16);
            this.label7.TabIndex = 35;
            this.label7.Text = "：";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkForcingBshift
            // 
            this.chkForcingBshift.AutoSize = true;
            this.chkForcingBshift.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkForcingBshift.Location = new System.Drawing.Point(397, 180);
            this.chkForcingBshift.Name = "chkForcingBshift";
            this.chkForcingBshift.Size = new System.Drawing.Size(104, 20);
            this.chkForcingBshift.TabIndex = 12;
            this.chkForcingBshift.Text = "強制Bシフト";
            this.chkForcingBshift.UseVisualStyleBackColor = true;
            // 
            // chkMHSC
            // 
            this.chkMHSC.AutoSize = true;
            this.chkMHSC.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkMHSC.Location = new System.Drawing.Point(274, 180);
            this.chkMHSC.Name = "chkMHSC";
            this.chkMHSC.Size = new System.Drawing.Size(102, 20);
            this.chkMHSC.TabIndex = 11;
            this.chkMHSC.Text = "MHSC所属";
            this.chkMHSC.UseVisualStyleBackColor = true;
            // 
            // chkDefaultWork
            // 
            this.chkDefaultWork.AutoSize = true;
            this.chkDefaultWork.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkDefaultWork.Location = new System.Drawing.Point(142, 180);
            this.chkDefaultWork.Name = "chkDefaultWork";
            this.chkDefaultWork.Size = new System.Drawing.Size(126, 20);
            this.chkDefaultWork.TabIndex = 10;
            this.chkDefaultWork.Text = "デフォ作業使用";
            this.chkDefaultWork.UseVisualStyleBackColor = true;
            // 
            // txtDefaultWorkName3
            // 
            this.txtDefaultWorkName3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultWorkName3.Location = new System.Drawing.Point(249, 262);
            this.txtDefaultWorkName3.MaxLength = 30;
            this.txtDefaultWorkName3.Name = "txtDefaultWorkName3";
            this.txtDefaultWorkName3.Size = new System.Drawing.Size(403, 23);
            this.txtDefaultWorkName3.TabIndex = 41;
            this.txtDefaultWorkName3.Text = "ああああああああああああああああああああああああああああああ";
            // 
            // txtDefaultChargeNo3
            // 
            this.txtDefaultChargeNo3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultChargeNo3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDefaultChargeNo3.Location = new System.Drawing.Point(142, 262);
            this.txtDefaultChargeNo3.MaxLength = 9;
            this.txtDefaultChargeNo3.Name = "txtDefaultChargeNo3";
            this.txtDefaultChargeNo3.Size = new System.Drawing.Size(101, 23);
            this.txtDefaultChargeNo3.TabIndex = 40;
            this.txtDefaultChargeNo3.Text = "123456789";
            this.txtDefaultChargeNo3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(24, 265);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(110, 16);
            this.label6.TabIndex = 32;
            this.label6.Text = "デフォルト作業３";
            // 
            // txtDefaultWorkName2
            // 
            this.txtDefaultWorkName2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultWorkName2.Location = new System.Drawing.Point(249, 237);
            this.txtDefaultWorkName2.MaxLength = 30;
            this.txtDefaultWorkName2.Name = "txtDefaultWorkName2";
            this.txtDefaultWorkName2.Size = new System.Drawing.Size(403, 23);
            this.txtDefaultWorkName2.TabIndex = 31;
            this.txtDefaultWorkName2.Text = "ああああああああああああああああああああああああああああああ";
            // 
            // txtDefaultChargeNo2
            // 
            this.txtDefaultChargeNo2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultChargeNo2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDefaultChargeNo2.Location = new System.Drawing.Point(142, 237);
            this.txtDefaultChargeNo2.MaxLength = 9;
            this.txtDefaultChargeNo2.Name = "txtDefaultChargeNo2";
            this.txtDefaultChargeNo2.Size = new System.Drawing.Size(101, 23);
            this.txtDefaultChargeNo2.TabIndex = 30;
            this.txtDefaultChargeNo2.Text = "123456789";
            this.txtDefaultChargeNo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(24, 240);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 16);
            this.label4.TabIndex = 29;
            this.label4.Text = "デフォルト作業２";
            // 
            // txtDefaultWorkName1
            // 
            this.txtDefaultWorkName1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultWorkName1.Location = new System.Drawing.Point(249, 212);
            this.txtDefaultWorkName1.MaxLength = 30;
            this.txtDefaultWorkName1.Name = "txtDefaultWorkName1";
            this.txtDefaultWorkName1.Size = new System.Drawing.Size(403, 23);
            this.txtDefaultWorkName1.TabIndex = 21;
            this.txtDefaultWorkName1.Text = "ああああああああああああああああああああああああああああああ";
            // 
            // txtDefaultChargeNo1
            // 
            this.txtDefaultChargeNo1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultChargeNo1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDefaultChargeNo1.Location = new System.Drawing.Point(142, 212);
            this.txtDefaultChargeNo1.MaxLength = 9;
            this.txtDefaultChargeNo1.Name = "txtDefaultChargeNo1";
            this.txtDefaultChargeNo1.Size = new System.Drawing.Size(101, 23);
            this.txtDefaultChargeNo1.TabIndex = 20;
            this.txtDefaultChargeNo1.Text = "123456789";
            this.txtDefaultChargeNo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(24, 215);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(110, 16);
            this.label3.TabIndex = 26;
            this.label3.Text = "デフォルト作業１";
            // 
            // chkDelFlg
            // 
            this.chkDelFlg.AutoSize = true;
            this.chkDelFlg.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkDelFlg.Location = new System.Drawing.Point(639, 180);
            this.chkDelFlg.Name = "chkDelFlg";
            this.chkDelFlg.Size = new System.Drawing.Size(59, 20);
            this.chkDelFlg.TabIndex = 14;
            this.chkDelFlg.Text = "削除";
            this.chkDelFlg.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCancel.Location = new System.Drawing.Point(675, 355);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(123, 40);
            this.btnCancel.TabIndex = 81;
            this.btnCancel.Text = "キャンセル（C）";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSave.Location = new System.Drawing.Point(546, 355);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(123, 40);
            this.btnSave.TabIndex = 80;
            this.btnSave.Text = "保存（S）";
            this.btnSave.UseVisualStyleBackColor = true;
            // 
            // TEF01010_SettingDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(810, 407);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.grpLoginCondition);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(830, 450);
            this.MinimumSize = new System.Drawing.Size(830, 450);
            this.Name = "TEF01010_SettingDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "設定";
            this.grpLoginCondition.ResumeLayout(false);
            this.grpLoginCondition.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.GroupBox grpLoginCondition;
        public System.Windows.Forms.Button btnCancel;
        public System.Windows.Forms.Button btnSave;
        public System.Windows.Forms.CheckBox chkDelFlg;
        public System.Windows.Forms.TextBox txtDefaultWorkName3;
        public System.Windows.Forms.TextBox txtDefaultChargeNo3;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtDefaultWorkName2;
        public System.Windows.Forms.TextBox txtDefaultChargeNo2;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtDefaultWorkName1;
        public System.Windows.Forms.TextBox txtDefaultChargeNo1;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.CheckBox chkMHSC;
        public System.Windows.Forms.CheckBox chkDefaultWork;
        public System.Windows.Forms.CheckBox chkForcingBshift;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cmbDefaultWorkMM2;
        public System.Windows.Forms.ComboBox cmbDefaultWorkMM3;
        public System.Windows.Forms.ComboBox cmbDefaultWorkMM1;
        public System.Windows.Forms.ComboBox cmbDefaultWorkHH2;
        public System.Windows.Forms.ComboBox cmbDefaultWorkHH3;
        public System.Windows.Forms.ComboBox cmbDefaultWorkHH1;
        public System.Windows.Forms.TextBox txtName;
        public System.Windows.Forms.Label lblEmploeeName;
        public System.Windows.Forms.TextBox txtEmploeeNum;
        public System.Windows.Forms.Label lblEmploeeNum;
        public System.Windows.Forms.CheckBox chkNightShift;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.ComboBox cmbGroup;
        public System.Windows.Forms.ComboBox cmbBuName;
        public System.Windows.Forms.Label lblCmbGroup;
        public System.Windows.Forms.Label lblCmbBu;
        public System.Windows.Forms.LinkLabel linkLblEmploeeNum;
    }
}

