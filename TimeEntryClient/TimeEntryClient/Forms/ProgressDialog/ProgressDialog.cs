﻿using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace TimeEntryClient.Forms {
    public partial class ProgressDialog : Form {
        /// <summary>
        /// 実行関数
        /// </summary>
        public Delegate Method { get; set; }
        public string ScreenId { get; set; }
        public string ScreenName { get; set; }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ProgressDialog() {
            InitializeComponent();
            this.ShowInTaskbar = false;
            this.ControlBox = false;
            BackgroundWorker bgw = new BackgroundWorker();

            this.Load += (s, e) => {
                progressBar.Style = ProgressBarStyle.Marquee;

                bgw.WorkerSupportsCancellation = true;
                bgw.DoWork += bw_DoWork;

                // 処理実行
                bgw.RunWorkerAsync();
            };

            bgw.RunWorkerCompleted += (s, e) => {
                this.Close();
            };

        }

        /// <summary>
        /// バックグラウンド処理
        /// </summary>
        private void bw_DoWork(object sender, DoWorkEventArgs e) {
            // Formを閉じる
            Action EndForm = () => this.Close();

            Method.DynamicInvoke();
            // 処理が終了すると画面が閉じられる
            this.Invoke(EndForm);
        }

        public void Init() { }

        public void ToDefaultCursor() { }

        public void ToWaitCursor() { }
    }
}
