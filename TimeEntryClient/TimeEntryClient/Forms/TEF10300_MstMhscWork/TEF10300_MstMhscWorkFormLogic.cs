﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TimeEntryClient.Const;
using TimeEntryClient.Dto;
using TimeEntryClient.Util;
using TimeEntryShared.Const;
using TimeEntryShared.Entity;
using TimeEntryShared.Util;

namespace TimeEntryClient.Forms {
    public class TEF10300_MstMhscWorkFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private TEF10300_MstMhscWorkBody _Body;
        /// <summary>
        /// Config情報
        /// </summary>
        private TimeEntryConfigEntity _ConfigInfo = new TimeEntryConfigEntity();
        /// <summary>
        /// グループコンボボックスのデータソース用リスト
        /// </summary>
        private Dictionary<string, string> _CmbBuSourceList = new Dictionary<string, string>();
        /// <summary>
        /// グリッドデータソース
        /// </summary>
        private List<MhscMstEntity> _MhscWorkList = new List<MhscMstEntity>();
        /// <summary>
        /// グリッドセル名
        /// </summary>
        private const string _GRID_CELL_NENDO     = "Nendo";
        private const string _GRID_CELL_CHARGE_NO = "ChargeNo";
        private const string _GRID_CELL_WORK_NAME = "WorkName";
        private const string _GRID_CELL_REST_FLG  = "RestFlg";
        private const string _GRID_CELL_DEL_FLG   = "DelFlg";
        private const string _GRID_CELL_EDITED    = "EditedFlg";
        private string[] _EditableGridCells = new string[] { _GRID_CELL_NENDO, _GRID_CELL_CHARGE_NO, _GRID_CELL_WORK_NAME, _GRID_CELL_REST_FLG, _GRID_CELL_DEL_FLG };
        /// <summary>
        /// 検索時行数
        /// </summary>
        private int _SEARCHED_ROW_COUNT = 0;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        public TEF10300_MstMhscWorkFormLogic(TEF10300_MstMhscWorkBody body) {
            // 初期化
            _Body = body;
            Init();
            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                }
            };
            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                ShowGrid();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var result = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLEAR);
                if (result == DialogResult.OK) {
                    Init();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                Update();
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // グリッド行番号描画
            _Body.dgvMstMhscWork.RowPostPaint += (s, e) => {
                GridRowUtil<MstEmploeeDto>.SetRowNum(s, e);
            };
            // グリッドCellValueChanged
            _Body.dgvMstMhscWork.CellValueChanged += GridCellValueChanged;

            // グリッドCellEnter
            _Body.dgvMstMhscWork.CellEnter += (s, e) => {
                GridCelEnter(e.RowIndex, e.ColumnIndex);
            };
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="RowIdx"></param>
        /// <param name="ColIdx"></param>
        private void GridCelEnter(int RowIdx, int ColIdx) {
            if (RowIdx < 0 || ColIdx < 0) {
                return;
            }
            var tgtCol = _Body.dgvMstMhscWork.Columns[ColIdx];
            if (tgtCol == null) {
                return;
            }
            // IMEモードを設定
            string tgtColName = tgtCol.Name;
            switch (tgtColName) {
                case _GRID_CELL_NENDO:
                case _GRID_CELL_CHARGE_NO:
                    _Body.dgvMstMhscWork.ImeMode = ImeMode.Disable;
                    break;
                case _GRID_CELL_WORK_NAME:
                    _Body.dgvMstMhscWork.ImeMode = ImeMode.Hiragana;
                    break;
                default:
                    _Body.dgvMstMhscWork.ImeMode = ImeMode.NoControl;
                    break;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(object s, DataGridViewCellEventArgs e) {

            // イベント退避
            _Body.dgvMstMhscWork.CellValueChanged -= GridCellValueChanged;

            var dto = new MstMhscDto();
            string tgtCellName = _Body.dgvMstMhscWork.Columns[e.ColumnIndex].Name;
            switch (tgtCellName) {
                case _GRID_CELL_NENDO:     // 年度
                    CellValueChangNendo(e.RowIndex);
                    break;
                case _GRID_CELL_CHARGE_NO: // チャージ№
                    CellValueChangChargeNo(e.RowIndex);
                    break;
                case _GRID_CELL_WORK_NAME: // 作業名
                    CellValueChangWorkName(e.RowIndex);
                    break;
            }
            if (0 <= e.ColumnIndex && 0 <= e.RowIndex && _EditableGridCells.Contains(tgtCellName)) {

                // セル背景色
                _Body.dgvMstMhscWork.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;

                // 編集ステータス
                if (e.RowIndex < _SEARCHED_ROW_COUNT - 1) {
                    _Body.dgvMstMhscWork.Rows[e.RowIndex].Cells[_GRID_CELL_EDITED].Value = ClientConst.EditStatus.UPDATE;
                } else {
                    _Body.dgvMstMhscWork.Rows[e.RowIndex].Cells[_GRID_CELL_EDITED].Value = ClientConst.EditStatus.INSERT;
                }
            }
            // イベント回復
            _Body.dgvMstMhscWork.CellValueChanged += GridCellValueChanged;
        }
        /// <summary>
        /// グリッドCellValueChanged（年度）
        /// </summary>
        /// <param name="RowIdx"></param>
        private void CellValueChangNendo(int RowIdx) {
            var val = _Body.dgvMstMhscWork.Rows[RowIdx].Cells[_GRID_CELL_NENDO].Value;
            if (val == null) {
                _Body.dgvMstMhscWork.Rows[RowIdx].Cells[_GRID_CELL_NENDO].Value = string.Empty;
                return;
            }
            int intNendo;
            if (!int.TryParse(val.ToString(), out intNendo)) {
                _Body.dgvMstMhscWork.Rows[RowIdx].Cells[_GRID_CELL_NENDO].Value = string.Empty;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged（チャージ№）
        /// </summary>
        /// <param name="RowIdx"></param>
        private void CellValueChangChargeNo(int RowIdx) {
            var val = _Body.dgvMstMhscWork.Rows[RowIdx].Cells[_GRID_CELL_CHARGE_NO].Value;
            if (val == null) {
                // nullなら不正
                _Body.dgvMstMhscWork.Rows[RowIdx].Cells[_GRID_CELL_CHARGE_NO].Value = string.Empty;
                return;
            }
            string strVal = val.ToString();
            if (strVal.Length != 9) {
                // 文字数！＝9なら不正
                _Body.dgvMstMhscWork.Rows[RowIdx].Cells[_GRID_CELL_CHARGE_NO].Value = string.Empty;
                return;
            }
            if (!strVal.Contains(CommonLiteral._LITERAL_HYPHEN) || strVal.IndexOf(CommonLiteral._LITERAL_HYPHEN) == strVal.LastIndexOf(CommonLiteral._LITERAL_HYPHEN)) {
                // ハイフンが２つ含まれない場合は不正
                _Body.dgvMstMhscWork.Rows[RowIdx].Cells[_GRID_CELL_CHARGE_NO].Value = string.Empty;
                return;
            }
            int firstIdxH = strVal.IndexOf(CommonLiteral._LITERAL_HYPHEN);
            int lastIdxH = strVal.LastIndexOf(CommonLiteral._LITERAL_HYPHEN);
            if (lastIdxH - firstIdxH != 3) {
                // XXX-XX-XXの形式でなければ不正
                _Body.dgvMstMhscWork.Rows[RowIdx].Cells[_GRID_CELL_CHARGE_NO].Value = string.Empty;
                return;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged（作業名）
        /// </summary>
        /// <param name="RowIdx"></param>
        private void CellValueChangWorkName(int RowIdx) {
            var WorkName = _Body.dgvMstMhscWork.Rows[RowIdx].Cells[_GRID_CELL_WORK_NAME].Value;
            if (WorkName == null || string.IsNullOrEmpty(WorkName.ToString())) {
                _Body.dgvMstMhscWork.Rows[RowIdx].Cells[_GRID_CELL_WORK_NAME].Value = string.Empty;
                return;
            }
            _Body.dgvMstMhscWork.Rows[RowIdx].Cells[_GRID_CELL_WORK_NAME].Value = StrUtil.CutHalfMarkChars(WorkName.ToString());
        }
        /// <summary>
        /// 更新処理
        /// </summary>
        private void Update() {
            // チェック処理
            if (!UpdValidate()) {
                return;
            }
            // 確認ダイアログ
            if (MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_UPDATE) != DialogResult.OK) {
                return;
            }
            // 更新データ作成
            var addInfos = new List<MhscMstEntity>();
            var updInfos = new List<MhscMstEntity>();
            foreach (DataGridViewRow r in _Body.dgvMstMhscWork.Rows) {
                var rowDto = GridRowUtil<MstMhscDto>.GetRowModel(r);
                switch (rowDto.EditedFlg) {
                    case ClientConst.EditStatus.INSERT:
                        // 新規登録
                        var newEntity = new MhscMstEntity() {
                            Nendo = rowDto.Nendo,
                            ChargeNo = rowDto.ChargeNo,
                            WorkName = rowDto.WorkName,
                            RestFlg = rowDto.RestFlg ? FlgValConst._INT_FLG_ON : FlgValConst._INT_FLG_OFF,
                            DelFlg = rowDto.DelFlg ? FlgValConst._INT_FLG_ON : FlgValConst._INT_FLG_OFF,
                        };
                        addInfos.Add(newEntity);
                        break;
                    case ClientConst.EditStatus.UPDATE:
                        // 更新データ
                        var tgtEntity = _MhscWorkList.Where(n => n.Nendo == rowDto.Nendo && n.ChargeNo == rowDto.ChargeNo).FirstOrDefault();
                        var updEntity = new MhscMstEntity() {
                            Nendo = tgtEntity.Nendo,
                            ChargeNo = tgtEntity.ChargeNo,
                            WorkName = rowDto.WorkName,
                            RestFlg = rowDto.RestFlg ? FlgValConst._INT_FLG_ON : FlgValConst._INT_FLG_OFF,
                            DelFlg = rowDto.DelFlg ? FlgValConst._INT_FLG_ON : FlgValConst._INT_FLG_OFF,
                        };
                        updInfos.Add(updEntity);
                        break;
                }
            }
            // 登録
            ModelUtil.InsMhscMst(_ConfigInfo.ConnStrTimeEntryDB, addInfos);

            // 更新
            ModelUtil.UpdMhscMst(_ConfigInfo.ConnStrTimeEntryDB, updInfos);

            // グリッド描画
            ShowGrid();

            // メッセージ表示
            MessageDialogUtil.ShowInfolMsgOK(ClientMessages._MSG_FINISH_UPDATE);
        }
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <returns></returns>
        private bool UpdValidate() {

            // マスタ再ロード
            _MhscWorkList = ModelUtil.LoadMhscMst(_ConfigInfo.ConnStrTimeEntryDB, 0, true);

            // Validate
            int updRowCount = 0;
            var keyInfos = new List<MstMhscDto>();
            foreach (DataGridViewRow r in _Body.dgvMstMhscWork.Rows) {
                var rowDto = GridRowUtil<MstMhscDto>.GetRowModel(r);

                // キー情報取得
                var key = new MstMhscDto() {
                    Nendo = rowDto.Nendo,
                    ChargeNo = StrUtil.TostringNullForbid(rowDto.ChargeNo),
                    RestFlg = rowDto.RestFlg,
                    EditedFlg = rowDto.EditedFlg,
                };
                keyInfos.Add(key);

                // 編集行以外はスルー
                if (rowDto.EditedFlg == ClientConst.EditStatus.NONE) {
                    continue;
                }
                updRowCount++;

                if (string.IsNullOrEmpty(rowDto.Nendo)) {
                    // 年度未入力
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _Body.dgvMstMhscWork.Columns[nameof(rowDto.Nendo)].HeaderText));
                    r.Cells[nameof(rowDto.Nendo)].Style.BackColor = Color.Red;
                    return false;
                }
                int intNendo;
                if (!int.TryParse(rowDto.Nendo, out intNendo)) {
                    // 年度不正入力
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_ILLEGAL, _Body.dgvMstMhscWork.Columns[nameof(rowDto.Nendo)].HeaderText));
                    r.Cells[nameof(rowDto.Nendo)].Style.BackColor = Color.Red;
                    return false;
                }
                if (string.IsNullOrWhiteSpace(rowDto.ChargeNo)) {
                    // チャージ№未入力
                    MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_MUTCH_BU_GROUP_CODE);
                    r.Cells[nameof(rowDto.ChargeNo)].Style.BackColor = Color.Red;
                    return false;
                }
                if (string.IsNullOrWhiteSpace(rowDto.WorkName) || string.IsNullOrEmpty(rowDto.WorkName)) {
                    // 作業名未入力
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _Body.dgvMstMhscWork.Columns[nameof(rowDto.WorkName)].HeaderText));
                    r.Cells[nameof(rowDto.WorkName)].Style.BackColor = Color.Red;
                    return false;
                }
            }
            if (updRowCount == 0) {
                // 更新対象データなし
                MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_EXIST_DATA);
            }
            // 重複チェック
            if (!ValidateDouple(keyInfos)) {
                return false;
            }
            // 休暇フラグチェック
            if (!ValidateRest(keyInfos)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <param name="keyInfos"></param>
        /// <returns></returns>
        private bool ValidateDouple(List<MstMhscDto> keyInfos) {
            foreach (var keyInfo in keyInfos) {
                if (string.IsNullOrEmpty(keyInfo.Nendo)) {
                    continue;
                }
                int doupleCountMst = _MhscWorkList.Where(n => n.Nendo == keyInfo.Nendo && n.ChargeNo == keyInfo.ChargeNo).ToList().Count;
                if ((keyInfo.EditedFlg == ClientConst.EditStatus.INSERT && 0 < doupleCountMst) || 1 < doupleCountMst) {
                    // 重複エラー時イベント
                    DopleErrEvent(keyInfo.Nendo, keyInfo.ChargeNo, keyInfos.IndexOf(keyInfo));
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 重複エラー時イベント
        /// </summary>
        /// <param name="tgtNendo"></param>
        /// <param name="tgtChargeNo"></param>
        /// <param name="rowIdx"></param>
        private void DopleErrEvent(string tgtNendo, string tgtChargeNo, int rowIdx) {
            // キー重複
            var dto = new MstMhscDto();
            string errCols = _Body.dgvMstMhscWork.Columns[nameof(dto.Nendo)].HeaderText;
            errCols += CommonLiteral._LITERAL_PUNCTUATION;
            errCols += _Body.dgvMstMhscWork.Columns[nameof(dto.ChargeNo)].HeaderText;

            // キー重複している行の背景色を着色
            _Body.dgvMstMhscWork.Rows[rowIdx].Cells[_GRID_CELL_NENDO].Style.BackColor = Color.Red;
            _Body.dgvMstMhscWork.Rows[rowIdx].Cells[_GRID_CELL_CHARGE_NO].Style.BackColor = Color.Red;

            // ダイアログ表示
            MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_DOUPLE, errCols));
        }
        /// <summary>
        /// 休暇用フラグチェック
        /// </summary>
        /// <param name="keyInfo"></param>
        /// <returns></returns>
        private bool ValidateRest(List<MstMhscDto> keyInfos) {
            // チェック用Dto作成
            var margedDtos = new List<MstMhscDto>();
            margedDtos.AddRange(keyInfos);
            foreach (var mst in _MhscWorkList) {
                if (!margedDtos.Exists(n => n.Nendo == mst.Nendo && n.ChargeNo == mst.ChargeNo)) {
                    var dto = new MstMhscDto() {
                        Nendo = mst.Nendo,
                        ChargeNo = mst.ChargeNo,
                        RestFlg = mst.RestFlg == FlgValConst._INT_FLG_ON,
                    };
                }
            }
            // チェック
            foreach (var margedDto in margedDtos) {
                if (string.IsNullOrEmpty(margedDto.Nendo)) {
                    continue;
                }
                if (margedDtos.Where(n => n.Nendo == margedDto.Nendo && n.RestFlg).ToList().Count != 1) {
                    // ダイアログ表示
                    MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_REST_COUNT_ILEEGAL);
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// グリッド描画
        /// </summary>
        private void ShowGrid() {
            // 初期化
            _SEARCHED_ROW_COUNT = 0;
            _Body.dgvMstMhscWork.Rows.Clear();
            int tgtNendo = (string.IsNullOrEmpty(_Body.txtNendo.Text)) ? 0 : int.Parse(_Body.txtNendo.Text);

            // マスタ再ロード
            _MhscWorkList = ModelUtil.LoadMhscMst(_ConfigInfo.ConnStrTimeEntryDB, tgtNendo, _Body.chkDelFlg.Checked);

            // ソート
            _MhscWorkList = _MhscWorkList.OrderBy(n => n.Nendo).ThenBy(n => n.ChargeNo).ToList();

            // グリッドオブジェクトを作成
            var gridDataSource = CreateGridDtoList(_Body.txtNendo.Text, _Body.chkDelFlg.Checked);

            // エラーメッセージ
            if (gridDataSource == null || gridDataSource.Count == 0) {
                MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_EXIST_DATA);
                return;
            }
            // データソース割り当て
            _Body.dgvMstMhscWork.DataSource = gridDataSource;

            // グリッドセル背景色編集
            EditGridCellColor();

            // 行数
            _SEARCHED_ROW_COUNT = _Body.dgvMstMhscWork.Rows.Count;

            _Body.dgvMstMhscWork.Show();
            _Body.dgvMstMhscWork.Focus();
        }
        /// <summary>
        /// グリッドオブジェクトを作成
        /// </summary>
        /// <param name="tgtNendo"></param>
        /// <param name="chkDelChecked"></param>
        /// <returns></returns>
        private BindingList<MstMhscDto> CreateGridDtoList(string tgtNendo, bool chkDelChecked) {

            var ret = new BindingList<MstMhscDto>();
            foreach (var rec in _MhscWorkList) {
                if (!chkDelChecked && rec.DelFlg == FlgValConst._INT_FLG_ON) {
                    // 削除含むのチェックがついていない場合、削除データは対象外
                    continue;
                }
                if (!string.IsNullOrWhiteSpace(_Body.txtNendo.Text)) {
                    // 画面の年度を入力している場合、異なる年度のデータは除外
                    if (_Body.txtNendo.Text != rec.Nendo) {
                        continue;
                    }
                }
                var rowDto = new MstMhscDto();
                rowDto.Nendo = rec.Nendo;
                rowDto.ChargeNo = rec.ChargeNo;
                rowDto.WorkName = rec.WorkName;
                rowDto.RestFlg = (rec.RestFlg == FlgValConst._INT_FLG_ON);
                rowDto.DelFlg = (rec.DelFlg == FlgValConst._INT_FLG_ON);
                rowDto.EditedFlg = ClientConst.EditStatus.NONE;
                ret.Add(rowDto);
            }
            return ret;
        }
        /// <summary>
        /// グリッドセル背景色編集
        /// </summary>
        private void EditGridCellColor() {
            foreach (DataGridViewRow r in _Body.dgvMstMhscWork.Rows) {
                var rowDto = GridRowUtil<MstMhscDto>.GetRowModel(r);
                if (rowDto.EditedFlg == ClientConst.EditStatus.NONE && !string.IsNullOrEmpty(rowDto.Nendo)) {
                    // 登録済み行
                    r.Cells[nameof(rowDto.Nendo)].ReadOnly = true;
                    r.Cells[nameof(rowDto.ChargeNo)].ReadOnly = true;
                    r.Cells[nameof(rowDto.Nendo)].Style.BackColor = Color.LightGray;
                    r.Cells[nameof(rowDto.ChargeNo)].Style.BackColor = Color.LightGray;
                }
                if (rowDto.DelFlg) {
                    // 削除行
                    r.Cells[nameof(rowDto.Nendo)].Style.BackColor = Color.LightGray;
                    r.Cells[nameof(rowDto.ChargeNo)].Style.BackColor = Color.LightGray;
                    r.Cells[nameof(rowDto.WorkName)].Style.BackColor = Color.LightGray;
                    r.Cells[nameof(rowDto.RestFlg)].Style.BackColor = Color.LightGray;
                    r.Cells[nameof(rowDto.DelFlg)].Style.BackColor = Color.LightGray;
                }
            }
        }
        /// <summary>
        /// 初期化処理
        /// </summary>
        private void Init() {
            _ConfigInfo = ModelUtil.LoadConfig();
            _Body.txtNendo.Text = ModelUtil.GetNendo(DateTime.Parse(_ConfigInfo.SumStartDate)).ToString();
            _MhscWorkList = ModelUtil.LoadMhscMst(_ConfigInfo.ConnStrTimeEntryDB, 0, true);
            _Body.chkDelFlg.Checked = false;
            _Body.dgvMstMhscWork.Rows.Clear();
            _Body.txtNendo.Focus();
            _SEARCHED_ROW_COUNT = 0;
        }
    }
}
