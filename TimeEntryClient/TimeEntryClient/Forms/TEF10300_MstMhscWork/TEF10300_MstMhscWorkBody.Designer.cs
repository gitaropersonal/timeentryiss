﻿namespace TimeEntryClient.Forms {
    partial class TEF10300_MstMhscWorkBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TEF10300_MstMhscWorkBody));
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvMstMhscWork = new System.Windows.Forms.DataGridView();
            this.Nendo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChargeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RestFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DelFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.EditedFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtNendo = new System.Windows.Forms.TextBox();
            this.chkDelFlg = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMstMhscWork)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvMstMhscWork);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(830, 706);
            this.panel1.TabIndex = 30;
            // 
            // dgvMstMhscWork
            // 
            this.dgvMstMhscWork.AllowUserToDeleteRows = false;
            this.dgvMstMhscWork.AllowUserToOrderColumns = true;
            this.dgvMstMhscWork.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvMstMhscWork.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvMstMhscWork.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMstMhscWork.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Nendo,
            this.ChargeNo,
            this.WorkName,
            this.RestFlg,
            this.DelFlg,
            this.EditedFlg});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvMstMhscWork.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgvMstMhscWork.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvMstMhscWork.Location = new System.Drawing.Point(0, 104);
            this.dgvMstMhscWork.Name = "dgvMstMhscWork";
            this.dgvMstMhscWork.RowTemplate.Height = 21;
            this.dgvMstMhscWork.Size = new System.Drawing.Size(830, 389);
            this.dgvMstMhscWork.TabIndex = 30;
            // 
            // Nendo
            // 
            this.Nendo.DataPropertyName = "Nendo";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Nendo.DefaultCellStyle = dataGridViewCellStyle2;
            this.Nendo.HeaderText = "年度";
            this.Nendo.MaxInputLength = 4;
            this.Nendo.Name = "Nendo";
            this.Nendo.Width = 65;
            // 
            // ChargeNo
            // 
            this.ChargeNo.DataPropertyName = "ChargeNo";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            this.ChargeNo.DefaultCellStyle = dataGridViewCellStyle3;
            this.ChargeNo.HeaderText = "チャージ№";
            this.ChargeNo.MaxInputLength = 9;
            this.ChargeNo.Name = "ChargeNo";
            this.ChargeNo.Width = 110;
            // 
            // WorkName
            // 
            this.WorkName.DataPropertyName = "WorkName";
            this.WorkName.HeaderText = "作業名";
            this.WorkName.MaxInputLength = 40;
            this.WorkName.Name = "WorkName";
            this.WorkName.Width = 400;
            // 
            // RestFlg
            // 
            this.RestFlg.DataPropertyName = "RestFlg";
            this.RestFlg.HeaderText = "休暇用";
            this.RestFlg.IndeterminateValue = "";
            this.RestFlg.Name = "RestFlg";
            this.RestFlg.Width = 75;
            // 
            // DelFlg
            // 
            this.DelFlg.DataPropertyName = "DelFlg";
            this.DelFlg.HeaderText = "削除";
            this.DelFlg.Name = "DelFlg";
            this.DelFlg.Width = 50;
            // 
            // EditedFlg
            // 
            this.EditedFlg.DataPropertyName = "EditedFlg";
            this.EditedFlg.HeaderText = "更新フラグ";
            this.EditedFlg.IndeterminateValue = "";
            this.EditedFlg.Name = "EditedFlg";
            this.EditedFlg.ReadOnly = true;
            this.EditedFlg.Visible = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.txtNendo);
            this.panel5.Controls.Add(this.chkDelFlg);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(830, 104);
            this.panel5.TabIndex = 0;
            // 
            // txtNendo
            // 
            this.txtNendo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtNendo.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtNendo.Location = new System.Drawing.Point(77, 16);
            this.txtNendo.MaxLength = 4;
            this.txtNendo.Name = "txtNendo";
            this.txtNendo.Size = new System.Drawing.Size(45, 23);
            this.txtNendo.TabIndex = 0;
            this.txtNendo.Text = "2020";
            this.txtNendo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chkDelFlg
            // 
            this.chkDelFlg.AutoSize = true;
            this.chkDelFlg.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkDelFlg.Location = new System.Drawing.Point(34, 51);
            this.chkDelFlg.Name = "chkDelFlg";
            this.chkDelFlg.Size = new System.Drawing.Size(88, 20);
            this.chkDelFlg.TabIndex = 2;
            this.chkDelFlg.Text = "削除含む";
            this.chkDelFlg.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(31, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 16);
            this.label1.TabIndex = 24;
            this.label1.Text = "年度";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnSearch);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(683, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(147, 104);
            this.panel6.TabIndex = 20;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(12, 51);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(123, 40);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 499);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(830, 58);
            this.panel2.TabIndex = 80;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnUpdate);
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(547, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(283, 58);
            this.panel4.TabIndex = 90;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(19, 9);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(123, 40);
            this.btnUpdate.TabIndex = 90;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(148, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(123, 40);
            this.btnClose.TabIndex = 92;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnClear);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(419, 58);
            this.panel3.TabIndex = 70;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(12, 9);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(123, 40);
            this.btnClear.TabIndex = 70;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // TEF10300_MstMhscWorkBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 557);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(850, 600);
            this.MinimumSize = new System.Drawing.Size(850, 600);
            this.Name = "TEF10300_MstMhscWorkBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MHSC作業マスタ";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMstMhscWork)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.CheckBox chkDelFlg;
        public System.Windows.Forms.DataGridView dgvMstMhscWork;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.TextBox txtNendo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nendo;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChargeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn RestFlg;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DelFlg;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EditedFlg;
    }
}

