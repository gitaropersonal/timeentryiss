﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using TimeEntryClient.Const;
using TimeEntryShared.Const;
using TimeEntryShared.Dto;
using TimeEntryShared.Entity;
using TimeEntryShared.Service;
using TimeEntryShared.Util;
using TimeEntryClient.Forms.Dialog;
using TimeEntryClient.Util;

namespace TimeEntryClient.Forms {
    public class TEF01000_CreateRosterFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private TEF01000_CreateRosterBody _Body;
        /// <summary>
        /// Config情報
        /// </summary>
        private TimeEntryConfigEntity _ConfigInfo = new TimeEntryConfigEntity();
        /// <summary>
        /// TimeTracker出力情報
        /// </summary>
        private List<UserInfoDto> _TimeTrackerUserInfos = new List<UserInfoDto>();
        /// <summary>
        /// 従業員情報
        /// </summary>
        private List<MstEmploeeEntity> _EmploeeInfos = new List<MstEmploeeEntity>();
        /// <summary>
        /// グループマスタ
        /// </summary>
        private List<MstGroupEntity> _GroupList = new List<MstGroupEntity>();
        /// <summary>
        /// 部署マスタ
        /// </summary>
        private List<MstBuEntity> _BuList = new List<MstBuEntity>();
        /// <summary>
        /// グループコンボボックスのデータソース用リスト
        /// </summary>
        private Dictionary<string, string> _CmbBuSourceList = new Dictionary<string, string>();
        /// <summary>
        /// グループコンボボックスのデータソース用リスト
        /// </summary>
        private Dictionary<string, string> _CmbGroupSourceList = new Dictionary<string, string>();
        private const string _FileName_ExploreExe = "EXPLORER.EXE";
        private string _ERR_MSG_CREATE_KINMUHYO = string.Empty;
        private string _ERR_MSG_NIGHT_SHIFT = string.Empty;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        public TEF01000_CreateRosterFormLogic(TEF01000_CreateRosterBody body) {
            // 初期化
            _Body = body;
            Init();
            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.H:
                        if (_Body.linkLblEmploeeNum.Focused) {
                            MessageDialogUtil.ShowExplainEmploeeNum();
                        }
                        break;
                    case Keys.J:
                        _Body.btnExecute.Focus();
                        _Body.btnExecute.PerformClick();
                        break;
                    case Keys.F:
                        _Body.btnFolderOpen.Focus();
                        _Body.btnFolderOpen.PerformClick();
                        break;
                    case Keys.O:
                        _Body.btnCreate.Focus();
                        _Body.btnCreate.PerformClick();
                        break;
                    case Keys.R:
                        _Body.btnReCreate.Focus();
                        _Body.btnReCreate.PerformClick();
                        break;
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
            // 社員番号とは？ラベルクリック
            _Body.linkLblEmploeeNum.Click += (s, e) => {
                MessageDialogUtil.ShowExplainEmploeeNum();
            };
            // 実行ボタン押下
            _Body.btnExecute.Click += (s, e) => {
                ClickBtnExecute();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var result = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLEAR);
                if (result == DialogResult.OK) {
                    Init();
                }
            };
            // フォルダを開くボタン押下
            _Body.btnFolderOpen.Click += (s, e) => {
                ClickBtnOpenFolder();
            };
            // 作成ボタン押下
            _Body.btnCreate.Click += (s, e) => {
                ClickBtnCreate();
            };
            // 再作成ボタン押下
            _Body.btnReCreate.Click += (s, e) => {
                ClickBtnCreate(true);
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // テキストボックスValidate
            _Body.txtEmploeeNum.Validated += (s, e) => {
                if (string.IsNullOrEmpty(_Body.txtEmploeeNum.Text)) {
                    return;
                }
                _Body.txtEmploeeNum.Text = CommonUtil.CutSpace(StrUtil.CutHalfMarkChars(_Body.txtEmploeeNum.Text)).PadLeft(6, CommonLiteral._CHAR_PAD_ZERO);
            };
            // テキストボックスフォーカスイン
            _Body.txtEmploeeNum.Enter += (s, e) => {
                _Body.txtEmploeeNum.SelectAll();
            };
        }
        /// <summary>
        /// 初期化処理
        /// </summary>
        private void Init() {
            // TimeEntry.configロード
            _ConfigInfo = ModelUtil.LoadConfig();

            // TimeTrackerユーザ情報ロード
            _TimeTrackerUserInfos = TimeTrackerUtil.LoadTimeTrackerUserInfo();
            try {
                // 従業員情報csvロード
                _EmploeeInfos = ModelUtil.LoadEmploeeMst(_ConfigInfo);

                _GroupList = ModelUtil.LoadGroupMst(_ConfigInfo);
                _BuList = ModelUtil.LoadBuMst(_ConfigInfo);

                // コントロール初期化
                _Body.txtEmploeeNum.Text = string.Empty;
                _Body.txtOutputFolder.Text = string.Empty;
                InitSumStartDateCombo();
                _Body.cmbSumSatrtDate.Text = DateTime.Parse(_ConfigInfo.SumStartDate).ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI);
                _Body.txtFileName.Text = string.Empty;
                ControlUtil.InitBuCombo(_BuList, ref _CmbBuSourceList, _Body.cmbBuName, true);
                ControlUtil.InitGroupCombo(_GroupList, ref _CmbGroupSourceList, _Body.cmbGroup, _Body.cmbBuName.Text, false);
                _Body.txtEmploeeNum.Enabled = true;
                _Body.linkLblEmploeeNum.Enabled = true;
                _Body.cmbSumSatrtDate.Enabled = true;
                _Body.btnExecute.Enabled = true;
                _Body.pnlEmpInfo.Enabled = false;

                _Body.chkDelFlg.Checked = false;
                _Body.chkDefaultWork.Checked = false;
                _Body.chkForcingBshift.Checked = false;
                _Body.chkMHSC.Checked = false;
                _Body.chkNightShift.Checked = false;

                _Body.txtDefaultChargeNo1.Text = string.Empty;
                _Body.txtDefaultChargeNo2.Text = string.Empty;
                _Body.txtDefaultChargeNo3.Text = string.Empty;
                _Body.txtDefaultWorkName1.Text = string.Empty;
                _Body.txtDefaultWorkName2.Text = string.Empty;
                _Body.txtDefaultWorkName3.Text = string.Empty;

                _Body.cmbDefaultWorkHH1.DataSource = ControlUtil.GetCmbHHList();
                _Body.cmbDefaultWorkHH2.DataSource = ControlUtil.GetCmbHHList();
                _Body.cmbDefaultWorkHH3.DataSource = ControlUtil.GetCmbHHList();
                _Body.cmbDefaultWorkMM1.DataSource = ControlUtil.GetCmbMMList();
                _Body.cmbDefaultWorkMM2.DataSource = ControlUtil.GetCmbMMList();
                _Body.cmbDefaultWorkMM3.DataSource = ControlUtil.GetCmbMMList();

                _Body.txtEmploeeNum.Focus();

                // 検索条件背景色初期化
                _Body.txtEmploeeNum.BackColor = Color.Empty;
            } catch (Exception ex) {
                MessageDialogUtil.ShowErroMsg(ex);
            }
        }
        /// <summary>
        /// 実行ボタン押下処理
        /// </summary>
        private void ClickBtnExecute() {
            // 検索条件背景色初期化
            _Body.txtEmploeeNum.BackColor = Color.Empty;

            // エラーチェック
            if (string.IsNullOrWhiteSpace(_Body.txtEmploeeNum.Text)) {
                MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _Body.lblEmploeeNum.Text));
                _Body.txtEmploeeNum.BackColor = Color.Red;
                _Body.txtEmploeeNum.Focus();
                return;
            }
            var tgtEmploee = _EmploeeInfos.Where(n => n.EmploeeNum == _Body.txtEmploeeNum.Text).FirstOrDefault();
            if (tgtEmploee == null) {
                MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_DEL_FLG_ON);
                _Body.txtEmploeeNum.BackColor = Color.Red;
                _Body.txtEmploeeNum.Focus();
                return;
            }
            // ログイン情報を画面に表示
            var tgtEmploeeInfo = _EmploeeInfos.Where(n => n.EmploeeNum == _Body.txtEmploeeNum.Text).FirstOrDefault();
            string tgtBuCode = _BuList.Where(n => n.BuCode == tgtEmploeeInfo.BuCode).FirstOrDefault().BuCode;
            string tgtBuName = _BuList.Where(n => n.BuCode == tgtEmploeeInfo.BuCode).FirstOrDefault().BuName;
            string tgtBu = string.Format(FormatConst._FORMAT_CMB_BU_FORMAT, tgtBuCode, tgtBuName);
            var tgtGroupCode = _GroupList.Where(n => n.GroupCode == tgtEmploeeInfo.GroupCode).FirstOrDefault().GroupCode;
            var tgtGroupName = _GroupList.Where(n => n.GroupCode == tgtEmploeeInfo.GroupCode).FirstOrDefault().GroupName;
            string tgtGroup = string.Empty;
            if (tgtGroupCode != null && !string.IsNullOrEmpty(tgtGroupCode.ToString())) {
                tgtGroup = string.Format(FormatConst._FORMAT_CMB_GROUP_FORMAT, tgtGroupCode, tgtGroupName);
            }
            _Body.txtFileName.Text         = KinmuhyoUtil.GetKinmuhyoName(_ConfigInfo, tgtEmploeeInfo.Name, DateTime.Parse(_Body.cmbSumSatrtDate.Text));
            _Body.cmbBuName.Text           = tgtBu;
            ControlUtil.InitGroupCombo(_GroupList, ref _CmbGroupSourceList, _Body.cmbGroup, _Body.cmbBuName.Text, false);
            _Body.cmbGroup.Text            = tgtGroup;
            _Body.txtOutputFolder.Text     = _ConfigInfo.KinmuhyoPath;
            _Body.chkDelFlg.Checked        = (tgtEmploee.DeleteFlg      == FlgValConst._STR_FLG_ON);
            _Body.chkDefaultWork.Checked   = (tgtEmploee.DefaultWorkFlg == FlgValConst._STR_FLG_ON);
            _Body.chkForcingBshift.Checked = (tgtEmploee.ForcingShiftBFlg  == FlgValConst._STR_FLG_ON);
            _Body.chkMHSC.Checked          = (tgtEmploee.MhscFlg        == FlgValConst._STR_FLG_ON);
            _Body.chkNightShift.Checked    = (tgtEmploee.NightShiftFlg  == FlgValConst._STR_FLG_ON);

            _Body.txtDefaultChargeNo1.Text = tgtEmploee.DefaultWorkChargeNo1;
            _Body.txtDefaultChargeNo2.Text = tgtEmploee.DefaultWorkChargeNo2;
            _Body.txtDefaultChargeNo3.Text = tgtEmploee.DefaultWorkChargeNo3;
            _Body.txtDefaultWorkName1.Text = tgtEmploee.DefaultWorkName1;
            _Body.txtDefaultWorkName2.Text = tgtEmploee.DefaultWorkName2;
            _Body.txtDefaultWorkName3.Text = tgtEmploee.DefaultWorkName3;

            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkHH1, StrUtil.StrIntToStrPreZero2(tgtEmploee.DefaultWorkTimeH1));
            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkHH2, StrUtil.StrIntToStrPreZero2(tgtEmploee.DefaultWorkTimeH2));
            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkHH3, StrUtil.StrIntToStrPreZero2(tgtEmploee.DefaultWorkTimeH3));
            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkMM1, StrUtil.StrIntToStrPreZero2(tgtEmploee.DefaultWorkTimeM1));
            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkMM2, StrUtil.StrIntToStrPreZero2(tgtEmploee.DefaultWorkTimeM2));
            ControlUtil.InitCmbHHIdx(_Body.cmbDefaultWorkMM3, StrUtil.StrIntToStrPreZero2(tgtEmploee.DefaultWorkTimeM3));

            _Body.btnCreate.Enabled = (!_Body.chkDelFlg.Checked);
            _Body.btnReCreate.Enabled = (!_Body.chkDelFlg.Checked);
            _Body.btnFolderOpen.Enabled = (!_Body.chkDelFlg.Checked);

            _Body.txtEmploeeNum.Enabled = false;
            _Body.linkLblEmploeeNum.Enabled = false;
            _Body.cmbSumSatrtDate.Enabled = false;
            _Body.btnExecute.Enabled = false;
            _Body.pnlEmpInfo.Enabled = true;

            // フォーカス移動
            if (_Body.btnFolderOpen.Enabled) {
                _Body.btnFolderOpen.Focus();
                return;
            }
            _Body.btnClear.Focus();
        }
        /// <summary>
        /// フォルダを開くボタン押下処理
        /// </summary>
        private void ClickBtnOpenFolder() {
            string folderPath = _Body.txtOutputFolder.Text;
            string position = _BuList.Where(n => n.BuCode == _Body.cmbBuName.Text.Substring(0, 3)).FirstOrDefault().BuName;
            string group = string.Empty;
            if (!string.IsNullOrEmpty(_Body.cmbGroup.Text) && _GroupList.Where(n => n.GroupCode == _Body.cmbGroup.Text.Substring(0, 6)).FirstOrDefault() != null) {
                group = _GroupList.Where(n => n.GroupCode == _Body.cmbGroup.Text.Substring(0, 6)).FirstOrDefault().GroupName;
                position = string.Concat(position, CommonLiteral._LITERAL_YEN_MARK, group);
            }
            folderPath = Path.Combine(folderPath, position);
            folderPath = Path.Combine(folderPath, _Body.cmbSumSatrtDate.Text);
            CommonUtil.CreateFolder(folderPath);
            Process.Start(_FileName_ExploreExe, folderPath);
        }
        /// <summary>
        /// 作成ボタン押下処理
        /// </summary>
        /// <param name="isReCreate"></param>
        private void ClickBtnCreate(bool isReCreate = false) {
            // エラーチェック
            if (string.IsNullOrEmpty(_Body.txtOutputFolder.Text)) {
                MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_FOLDER_PATH_EMPTY);
                return;
            }
            // デフォルト勤務表の存在チェック
            string defaultKinmuhyoPath = Path.Combine(_ConfigInfo.DefaultKinmuhyoPath, _ConfigInfo.DefaultKinmuhyoName);
            if (!File.Exists(defaultKinmuhyoPath)) {
                MessageDialogUtil.ShowErroMsg(SharedMessages._MSG_ERROR_NOT_EXIST_DEF);
            }
            // 勤務表を開きっぱなしにしていないかチェック
            var emp = _EmploeeInfos.Where(n => n.EmploeeNum == _Body.txtEmploeeNum.Text).FirstOrDefault();
            var sumDt = DateTime.Parse(_Body.cmbSumSatrtDate.Text);
            if (!KinmuhyoUtil.ValidateIsKinmuhyoOpened(_ConfigInfo, _BuList, _GroupList, emp, sumDt)) {
                MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_FILE_ALREADY_OPENDE);
                return;
            }
            // 確認メッセージ
            var drConfirm = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_CONFIRM_CREATE_ROSTER);
            if (drConfirm != DialogResult.OK) {
                return;
            }
            // ConfigInfoの値を強制編集
            _ConfigInfo.SumStartDate = sumDt.ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH);
            _ConfigInfo.SumEndDate = DateTime.Parse(_Body.cmbSumSatrtDate.Text).AddMonths(1).AddDays(-1).ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH);
            _ConfigInfo.ForcingReCreateFlg = FlgValConst._INT_FLG_OFF;
            _ConfigInfo.DefaultKinmuhyoName = TimeEntryConfigConst._DEFAULT_KINMUHYO_FORMAT_NAME;
            _ConfigInfo = KinmuhyoUtil.GetKinmuhyoFormatName(_ConfigInfo, DateTime.Parse(_Body.cmbSumSatrtDate.Text));
            _ConfigInfo.WbLookPreMonthCount = ModelUtil.GetWBBackMonthCount(DateTime.Parse(_Body.cmbSumSatrtDate.Text));
            if (isReCreate) {
                drConfirm = MessageDialogUtil.ShowInfoExc(ClientMessages._MSG_CONFIRM_RECREATE);
                if (drConfirm != DialogResult.OK) {
                    return;
                }
                _ConfigInfo.ForcingReCreateFlg = FlgValConst._INT_FLG_ON;
            }
            // 時間外報告書作成
            var preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => CreateKinmuhyo()));
                prgBar.ShowDialog();
            }
            // エラーが発生した場合はダイアログを表示して終了
            if (!string.IsNullOrEmpty(_ERR_MSG_CREATE_KINMUHYO)) {
                MessageDialogUtil.ShowErroMsg(_ERR_MSG_CREATE_KINMUHYO);
                return;
            }
            if (!string.IsNullOrEmpty(_ERR_MSG_NIGHT_SHIFT)) {
                MessageDialogUtil.ShowErroMsg(_ERR_MSG_NIGHT_SHIFT);
                return;
            }
            // 終了メッセージ表示
            var drFinish = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_FINISH_CREATE_ROSTER);
            if (drFinish != DialogResult.OK) {
                Cursor.Current = preCursor;
                return;
            }
            Cursor.Current = Cursors.WaitCursor;

            // 作成した時間外報告書を開く
            OpenCreateRoster(preCursor);
        }
        /// <summary>
        /// 時間外報告書作成
        /// </summary>
        private void CreateKinmuhyo() {
            int tgtNendo = ModelUtil.GetNendo(DateTime.Parse(_ConfigInfo.SumStartDate));
            var service1041 = new EditTimeEntryService();
            var service1050 = new EditNightShiftService();
            var propaties = new ServicePropaties() {
                ConfigInfo = _ConfigInfo,
                TimeTrackerUserInfos = _TimeTrackerUserInfos,
                BuList = _BuList,
                GroupList = _GroupList,
                EmploeeInfos = _EmploeeInfos,
                MhscMstInfos = ModelUtil.LoadMhscMst(_ConfigInfo.ConnStrTimeEntryDB, tgtNendo),
            };
            // 勤怠時刻・作業情報転記
            _ERR_MSG_CREATE_KINMUHYO = service1041.Main(propaties, _Body.txtEmploeeNum.Text, true);

            // 夜勤処理
            _ERR_MSG_NIGHT_SHIFT = service1050.Main(propaties, _Body.txtEmploeeNum.Text);
        }
        /// <summary>
        /// 作成した時間外報告書を開く
        /// </summary>
        /// <param name="preCursor"></param>
        private void OpenCreateRoster(Cursor preCursor) {
            string excelPath = _Body.txtOutputFolder.Text;
            string position = _BuList.Where(n => n.BuCode == _Body.cmbBuName.Text.Substring(0, 3)).FirstOrDefault().BuName;
            string group = string.Empty;
            if (!string.IsNullOrEmpty(_Body.cmbGroup.Text) && _GroupList.Where(n => n.GroupCode == _Body.cmbGroup.Text.Substring(0, 6)).FirstOrDefault() != null) {
                group = _GroupList.Where(n => n.GroupCode == _Body.cmbGroup.Text.Substring(0, 6)).FirstOrDefault().GroupName;
                position = string.Concat(position, CommonLiteral._LITERAL_YEN_MARK, group);
            }
            excelPath = Path.Combine(excelPath, position);
            excelPath = Path.Combine(excelPath, _Body.cmbSumSatrtDate.Text);
            excelPath = Path.Combine(excelPath, _Body.txtFileName.Text);
            Process.Start(excelPath);
            Cursor.Current = preCursor;
        }
        /// <summary>
        /// 集計月コンボボックス初期化
        /// </summary>
        private void InitSumStartDateCombo() {
            _Body.cmbSumSatrtDate.Items.Clear();
            _Body.cmbSumSatrtDate.Text = string.Empty;
            DateTime dtSumEnd = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            DateTime dtSumStart = dtSumEnd.AddMonths(-6);
            for (DateTime dt = dtSumStart; dt <= dtSumEnd; dt = dt.AddMonths(1)) {
                _Body.cmbSumSatrtDate.Items.Add(dt.ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI));
            }
        }
    }
}
