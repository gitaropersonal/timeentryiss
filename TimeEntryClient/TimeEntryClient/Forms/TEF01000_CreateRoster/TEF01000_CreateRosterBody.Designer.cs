﻿namespace TimeEntryClient.Forms {
    partial class TEF01000_CreateRosterBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TEF01000_CreateRosterBody));
            this.label12 = new System.Windows.Forms.Label();
            this.cmbSumSatrtDate = new System.Windows.Forms.ComboBox();
            this.pnlEmpInfo = new System.Windows.Forms.Panel();
            this.grpEmploeeInfo = new System.Windows.Forms.GroupBox();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.cmbBuName = new System.Windows.Forms.ComboBox();
            this.lblCmbGroup = new System.Windows.Forms.Label();
            this.lblCmbBu = new System.Windows.Forms.Label();
            this.cmbDefaultWorkMM2 = new System.Windows.Forms.ComboBox();
            this.cmbDefaultWorkMM3 = new System.Windows.Forms.ComboBox();
            this.cmbDefaultWorkMM1 = new System.Windows.Forms.ComboBox();
            this.cmbDefaultWorkHH2 = new System.Windows.Forms.ComboBox();
            this.cmbDefaultWorkHH3 = new System.Windows.Forms.ComboBox();
            this.cmbDefaultWorkHH1 = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDefaultWorkName3 = new System.Windows.Forms.TextBox();
            this.txtDefaultChargeNo3 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDefaultWorkName2 = new System.Windows.Forms.TextBox();
            this.txtDefaultChargeNo2 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDefaultWorkName1 = new System.Windows.Forms.TextBox();
            this.txtDefaultChargeNo1 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.chkNightShift = new System.Windows.Forms.CheckBox();
            this.chkForcingBshift = new System.Windows.Forms.CheckBox();
            this.chkMHSC = new System.Windows.Forms.CheckBox();
            this.chkDefaultWork = new System.Windows.Forms.CheckBox();
            this.chkDelFlg = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFileName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtOutputFolder = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnFolderOpen = new System.Windows.Forms.Button();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnReCreate = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.linkLblEmploeeNum = new System.Windows.Forms.LinkLabel();
            this.btnExecute = new System.Windows.Forms.Button();
            this.txtEmploeeNum = new System.Windows.Forms.TextBox();
            this.lblEmploeeNum = new System.Windows.Forms.Label();
            this.pnlEmpInfo.SuspendLayout();
            this.grpEmploeeInfo.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label12.Location = new System.Drawing.Point(31, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(56, 16);
            this.label12.TabIndex = 57;
            this.label12.Text = "対象月";
            // 
            // cmbSumSatrtDate
            // 
            this.cmbSumSatrtDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSumSatrtDate.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.cmbSumSatrtDate.FormattingEnabled = true;
            this.cmbSumSatrtDate.Location = new System.Drawing.Point(150, 28);
            this.cmbSumSatrtDate.Name = "cmbSumSatrtDate";
            this.cmbSumSatrtDate.Size = new System.Drawing.Size(103, 24);
            this.cmbSumSatrtDate.TabIndex = 0;
            // 
            // pnlEmpInfo
            // 
            this.pnlEmpInfo.Controls.Add(this.grpEmploeeInfo);
            this.pnlEmpInfo.Controls.Add(this.panel7);
            this.pnlEmpInfo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlEmpInfo.Enabled = false;
            this.pnlEmpInfo.Location = new System.Drawing.Point(0, 125);
            this.pnlEmpInfo.Name = "pnlEmpInfo";
            this.pnlEmpInfo.Size = new System.Drawing.Size(830, 374);
            this.pnlEmpInfo.TabIndex = 80;
            // 
            // grpEmploeeInfo
            // 
            this.grpEmploeeInfo.Controls.Add(this.cmbGroup);
            this.grpEmploeeInfo.Controls.Add(this.cmbBuName);
            this.grpEmploeeInfo.Controls.Add(this.lblCmbGroup);
            this.grpEmploeeInfo.Controls.Add(this.lblCmbBu);
            this.grpEmploeeInfo.Controls.Add(this.cmbDefaultWorkMM2);
            this.grpEmploeeInfo.Controls.Add(this.cmbDefaultWorkMM3);
            this.grpEmploeeInfo.Controls.Add(this.cmbDefaultWorkMM1);
            this.grpEmploeeInfo.Controls.Add(this.cmbDefaultWorkHH2);
            this.grpEmploeeInfo.Controls.Add(this.cmbDefaultWorkHH3);
            this.grpEmploeeInfo.Controls.Add(this.cmbDefaultWorkHH1);
            this.grpEmploeeInfo.Controls.Add(this.label9);
            this.grpEmploeeInfo.Controls.Add(this.label8);
            this.grpEmploeeInfo.Controls.Add(this.label1);
            this.grpEmploeeInfo.Controls.Add(this.txtDefaultWorkName3);
            this.grpEmploeeInfo.Controls.Add(this.txtDefaultChargeNo3);
            this.grpEmploeeInfo.Controls.Add(this.label2);
            this.grpEmploeeInfo.Controls.Add(this.txtDefaultWorkName2);
            this.grpEmploeeInfo.Controls.Add(this.txtDefaultChargeNo2);
            this.grpEmploeeInfo.Controls.Add(this.label10);
            this.grpEmploeeInfo.Controls.Add(this.txtDefaultWorkName1);
            this.grpEmploeeInfo.Controls.Add(this.txtDefaultChargeNo1);
            this.grpEmploeeInfo.Controls.Add(this.label11);
            this.grpEmploeeInfo.Controls.Add(this.label7);
            this.grpEmploeeInfo.Controls.Add(this.chkNightShift);
            this.grpEmploeeInfo.Controls.Add(this.chkForcingBshift);
            this.grpEmploeeInfo.Controls.Add(this.chkMHSC);
            this.grpEmploeeInfo.Controls.Add(this.chkDefaultWork);
            this.grpEmploeeInfo.Controls.Add(this.chkDelFlg);
            this.grpEmploeeInfo.Controls.Add(this.label4);
            this.grpEmploeeInfo.Controls.Add(this.txtFileName);
            this.grpEmploeeInfo.Controls.Add(this.label3);
            this.grpEmploeeInfo.Controls.Add(this.txtOutputFolder);
            this.grpEmploeeInfo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.grpEmploeeInfo.Location = new System.Drawing.Point(24, 20);
            this.grpEmploeeInfo.Name = "grpEmploeeInfo";
            this.grpEmploeeInfo.Size = new System.Drawing.Size(778, 268);
            this.grpEmploeeInfo.TabIndex = 106;
            this.grpEmploeeInfo.TabStop = false;
            this.grpEmploeeInfo.Text = "社員情報";
            // 
            // cmbGroup
            // 
            this.cmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroup.Enabled = false;
            this.cmbGroup.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(136, 88);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(466, 24);
            this.cmbGroup.TabIndex = 108;
            // 
            // cmbBuName
            // 
            this.cmbBuName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBuName.Enabled = false;
            this.cmbBuName.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.cmbBuName.FormattingEnabled = true;
            this.cmbBuName.Location = new System.Drawing.Point(136, 61);
            this.cmbBuName.Name = "cmbBuName";
            this.cmbBuName.Size = new System.Drawing.Size(466, 24);
            this.cmbBuName.TabIndex = 107;
            // 
            // lblCmbGroup
            // 
            this.lblCmbGroup.AutoSize = true;
            this.lblCmbGroup.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCmbGroup.Location = new System.Drawing.Point(17, 91);
            this.lblCmbGroup.Name = "lblCmbGroup";
            this.lblCmbGroup.Size = new System.Drawing.Size(58, 16);
            this.lblCmbGroup.TabIndex = 137;
            this.lblCmbGroup.Text = "グループ";
            // 
            // lblCmbBu
            // 
            this.lblCmbBu.AutoSize = true;
            this.lblCmbBu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCmbBu.Location = new System.Drawing.Point(18, 64);
            this.lblCmbBu.Name = "lblCmbBu";
            this.lblCmbBu.Size = new System.Drawing.Size(40, 16);
            this.lblCmbBu.TabIndex = 136;
            this.lblCmbBu.Text = "部署";
            // 
            // cmbDefaultWorkMM2
            // 
            this.cmbDefaultWorkMM2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkMM2.Enabled = false;
            this.cmbDefaultWorkMM2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkMM2.FormattingEnabled = true;
            this.cmbDefaultWorkMM2.Location = new System.Drawing.Point(710, 198);
            this.cmbDefaultWorkMM2.Name = "cmbDefaultWorkMM2";
            this.cmbDefaultWorkMM2.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkMM2.TabIndex = 123;
            this.cmbDefaultWorkMM2.TabStop = false;
            // 
            // cmbDefaultWorkMM3
            // 
            this.cmbDefaultWorkMM3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkMM3.Enabled = false;
            this.cmbDefaultWorkMM3.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkMM3.FormattingEnabled = true;
            this.cmbDefaultWorkMM3.Location = new System.Drawing.Point(710, 223);
            this.cmbDefaultWorkMM3.Name = "cmbDefaultWorkMM3";
            this.cmbDefaultWorkMM3.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkMM3.TabIndex = 128;
            this.cmbDefaultWorkMM3.TabStop = false;
            // 
            // cmbDefaultWorkMM1
            // 
            this.cmbDefaultWorkMM1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkMM1.Enabled = false;
            this.cmbDefaultWorkMM1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkMM1.FormattingEnabled = true;
            this.cmbDefaultWorkMM1.Location = new System.Drawing.Point(710, 173);
            this.cmbDefaultWorkMM1.Name = "cmbDefaultWorkMM1";
            this.cmbDefaultWorkMM1.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkMM1.TabIndex = 118;
            this.cmbDefaultWorkMM1.TabStop = false;
            // 
            // cmbDefaultWorkHH2
            // 
            this.cmbDefaultWorkHH2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkHH2.Enabled = false;
            this.cmbDefaultWorkHH2.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkHH2.FormattingEnabled = true;
            this.cmbDefaultWorkHH2.Location = new System.Drawing.Point(654, 198);
            this.cmbDefaultWorkHH2.Name = "cmbDefaultWorkHH2";
            this.cmbDefaultWorkHH2.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkHH2.TabIndex = 121;
            this.cmbDefaultWorkHH2.TabStop = false;
            // 
            // cmbDefaultWorkHH3
            // 
            this.cmbDefaultWorkHH3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkHH3.Enabled = false;
            this.cmbDefaultWorkHH3.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkHH3.FormattingEnabled = true;
            this.cmbDefaultWorkHH3.Location = new System.Drawing.Point(654, 223);
            this.cmbDefaultWorkHH3.Name = "cmbDefaultWorkHH3";
            this.cmbDefaultWorkHH3.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkHH3.TabIndex = 127;
            this.cmbDefaultWorkHH3.TabStop = false;
            // 
            // cmbDefaultWorkHH1
            // 
            this.cmbDefaultWorkHH1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDefaultWorkHH1.Enabled = false;
            this.cmbDefaultWorkHH1.Font = new System.Drawing.Font("MS UI Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.cmbDefaultWorkHH1.FormattingEnabled = true;
            this.cmbDefaultWorkHH1.ItemHeight = 15;
            this.cmbDefaultWorkHH1.Location = new System.Drawing.Point(654, 173);
            this.cmbDefaultWorkHH1.Name = "cmbDefaultWorkHH1";
            this.cmbDefaultWorkHH1.Size = new System.Drawing.Size(41, 23);
            this.cmbDefaultWorkHH1.TabIndex = 117;
            this.cmbDefaultWorkHH1.TabStop = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(696, 226);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(16, 16);
            this.label9.TabIndex = 135;
            this.label9.Text = "：";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(696, 201);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 16);
            this.label8.TabIndex = 134;
            this.label8.Text = "：";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(696, 176);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 16);
            this.label1.TabIndex = 133;
            this.label1.Text = "：";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtDefaultWorkName3
            // 
            this.txtDefaultWorkName3.Enabled = false;
            this.txtDefaultWorkName3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultWorkName3.Location = new System.Drawing.Point(247, 223);
            this.txtDefaultWorkName3.MaxLength = 30;
            this.txtDefaultWorkName3.Name = "txtDefaultWorkName3";
            this.txtDefaultWorkName3.ReadOnly = true;
            this.txtDefaultWorkName3.Size = new System.Drawing.Size(403, 23);
            this.txtDefaultWorkName3.TabIndex = 125;
            this.txtDefaultWorkName3.TabStop = false;
            this.txtDefaultWorkName3.Text = "ああああああああああああああああああああああああああああああ";
            // 
            // txtDefaultChargeNo3
            // 
            this.txtDefaultChargeNo3.Enabled = false;
            this.txtDefaultChargeNo3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultChargeNo3.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDefaultChargeNo3.Location = new System.Drawing.Point(136, 223);
            this.txtDefaultChargeNo3.MaxLength = 9;
            this.txtDefaultChargeNo3.Name = "txtDefaultChargeNo3";
            this.txtDefaultChargeNo3.ReadOnly = true;
            this.txtDefaultChargeNo3.Size = new System.Drawing.Size(105, 23);
            this.txtDefaultChargeNo3.TabIndex = 124;
            this.txtDefaultChargeNo3.TabStop = false;
            this.txtDefaultChargeNo3.Text = "123456789";
            this.txtDefaultChargeNo3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(17, 226);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 16);
            this.label2.TabIndex = 132;
            this.label2.Text = "デフォルト作業３";
            // 
            // txtDefaultWorkName2
            // 
            this.txtDefaultWorkName2.Enabled = false;
            this.txtDefaultWorkName2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultWorkName2.Location = new System.Drawing.Point(247, 198);
            this.txtDefaultWorkName2.MaxLength = 30;
            this.txtDefaultWorkName2.Name = "txtDefaultWorkName2";
            this.txtDefaultWorkName2.ReadOnly = true;
            this.txtDefaultWorkName2.Size = new System.Drawing.Size(403, 23);
            this.txtDefaultWorkName2.TabIndex = 120;
            this.txtDefaultWorkName2.TabStop = false;
            this.txtDefaultWorkName2.Text = "ああああああああああああああああああああああああああああああ";
            // 
            // txtDefaultChargeNo2
            // 
            this.txtDefaultChargeNo2.Enabled = false;
            this.txtDefaultChargeNo2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultChargeNo2.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDefaultChargeNo2.Location = new System.Drawing.Point(136, 198);
            this.txtDefaultChargeNo2.MaxLength = 9;
            this.txtDefaultChargeNo2.Name = "txtDefaultChargeNo2";
            this.txtDefaultChargeNo2.ReadOnly = true;
            this.txtDefaultChargeNo2.Size = new System.Drawing.Size(105, 23);
            this.txtDefaultChargeNo2.TabIndex = 119;
            this.txtDefaultChargeNo2.TabStop = false;
            this.txtDefaultChargeNo2.Text = "123456789";
            this.txtDefaultChargeNo2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.Location = new System.Drawing.Point(17, 201);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(110, 16);
            this.label10.TabIndex = 131;
            this.label10.Text = "デフォルト作業２";
            // 
            // txtDefaultWorkName1
            // 
            this.txtDefaultWorkName1.Enabled = false;
            this.txtDefaultWorkName1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultWorkName1.Location = new System.Drawing.Point(247, 173);
            this.txtDefaultWorkName1.MaxLength = 30;
            this.txtDefaultWorkName1.Name = "txtDefaultWorkName1";
            this.txtDefaultWorkName1.ReadOnly = true;
            this.txtDefaultWorkName1.Size = new System.Drawing.Size(403, 23);
            this.txtDefaultWorkName1.TabIndex = 116;
            this.txtDefaultWorkName1.TabStop = false;
            this.txtDefaultWorkName1.Text = "ああああああああああああああああああああああああああああああ";
            // 
            // txtDefaultChargeNo1
            // 
            this.txtDefaultChargeNo1.Enabled = false;
            this.txtDefaultChargeNo1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDefaultChargeNo1.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtDefaultChargeNo1.Location = new System.Drawing.Point(136, 173);
            this.txtDefaultChargeNo1.MaxLength = 9;
            this.txtDefaultChargeNo1.Name = "txtDefaultChargeNo1";
            this.txtDefaultChargeNo1.ReadOnly = true;
            this.txtDefaultChargeNo1.Size = new System.Drawing.Size(105, 23);
            this.txtDefaultChargeNo1.TabIndex = 115;
            this.txtDefaultChargeNo1.TabStop = false;
            this.txtDefaultChargeNo1.Text = "123456789";
            this.txtDefaultChargeNo1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.Location = new System.Drawing.Point(17, 176);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(110, 16);
            this.label11.TabIndex = 130;
            this.label11.Text = "デフォルト作業１";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(17, 147);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 16);
            this.label7.TabIndex = 129;
            this.label7.Text = "設定フラグ";
            // 
            // chkNightShift
            // 
            this.chkNightShift.AutoSize = true;
            this.chkNightShift.Enabled = false;
            this.chkNightShift.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkNightShift.Location = new System.Drawing.Point(511, 146);
            this.chkNightShift.Name = "chkNightShift";
            this.chkNightShift.Size = new System.Drawing.Size(107, 20);
            this.chkNightShift.TabIndex = 113;
            this.chkNightShift.Text = "夜勤担当者";
            this.chkNightShift.UseVisualStyleBackColor = true;
            // 
            // chkForcingBshift
            // 
            this.chkForcingBshift.AutoSize = true;
            this.chkForcingBshift.Enabled = false;
            this.chkForcingBshift.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkForcingBshift.Location = new System.Drawing.Point(392, 146);
            this.chkForcingBshift.Name = "chkForcingBshift";
            this.chkForcingBshift.Size = new System.Drawing.Size(104, 20);
            this.chkForcingBshift.TabIndex = 112;
            this.chkForcingBshift.Text = "強制Bシフト";
            this.chkForcingBshift.UseVisualStyleBackColor = true;
            // 
            // chkMHSC
            // 
            this.chkMHSC.AutoSize = true;
            this.chkMHSC.Enabled = false;
            this.chkMHSC.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkMHSC.Location = new System.Drawing.Point(268, 146);
            this.chkMHSC.Name = "chkMHSC";
            this.chkMHSC.Size = new System.Drawing.Size(102, 20);
            this.chkMHSC.TabIndex = 111;
            this.chkMHSC.Text = "MHSC所属";
            this.chkMHSC.UseVisualStyleBackColor = true;
            // 
            // chkDefaultWork
            // 
            this.chkDefaultWork.AutoSize = true;
            this.chkDefaultWork.Enabled = false;
            this.chkDefaultWork.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkDefaultWork.Location = new System.Drawing.Point(136, 146);
            this.chkDefaultWork.Name = "chkDefaultWork";
            this.chkDefaultWork.Size = new System.Drawing.Size(126, 20);
            this.chkDefaultWork.TabIndex = 110;
            this.chkDefaultWork.Text = "デフォ作業使用";
            this.chkDefaultWork.UseVisualStyleBackColor = true;
            // 
            // chkDelFlg
            // 
            this.chkDelFlg.AutoSize = true;
            this.chkDelFlg.Enabled = false;
            this.chkDelFlg.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkDelFlg.Location = new System.Drawing.Point(634, 146);
            this.chkDelFlg.Name = "chkDelFlg";
            this.chkDelFlg.Size = new System.Drawing.Size(59, 20);
            this.chkDelFlg.TabIndex = 114;
            this.chkDelFlg.Text = "削除";
            this.chkDelFlg.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(17, 118);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 16);
            this.label4.TabIndex = 126;
            this.label4.Text = "ファイル名";
            // 
            // txtFileName
            // 
            this.txtFileName.Enabled = false;
            this.txtFileName.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtFileName.Location = new System.Drawing.Point(136, 115);
            this.txtFileName.MaxLength = 16;
            this.txtFileName.Name = "txtFileName";
            this.txtFileName.ReadOnly = true;
            this.txtFileName.Size = new System.Drawing.Size(615, 23);
            this.txtFileName.TabIndex = 109;
            this.txtFileName.TabStop = false;
            this.txtFileName.Text = "あああああ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(17, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 122;
            this.label3.Text = "出力先";
            // 
            // txtOutputFolder
            // 
            this.txtOutputFolder.Enabled = false;
            this.txtOutputFolder.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtOutputFolder.Location = new System.Drawing.Point(136, 35);
            this.txtOutputFolder.MaxLength = 16;
            this.txtOutputFolder.Name = "txtOutputFolder";
            this.txtOutputFolder.ReadOnly = true;
            this.txtOutputFolder.Size = new System.Drawing.Size(615, 23);
            this.txtOutputFolder.TabIndex = 106;
            this.txtOutputFolder.TabStop = false;
            this.txtOutputFolder.Text = "C:\\User\\Desktop";
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.panel8);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 298);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(830, 76);
            this.panel7.TabIndex = 150;
            // 
            // panel8
            // 
            this.panel8.Controls.Add(this.btnFolderOpen);
            this.panel8.Controls.Add(this.btnCreate);
            this.panel8.Controls.Add(this.btnReCreate);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel8.Location = new System.Drawing.Point(361, 0);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(469, 76);
            this.panel8.TabIndex = 200;
            // 
            // btnFolderOpen
            // 
            this.btnFolderOpen.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnFolderOpen.Location = new System.Drawing.Point(49, 15);
            this.btnFolderOpen.Name = "btnFolderOpen";
            this.btnFolderOpen.Size = new System.Drawing.Size(123, 40);
            this.btnFolderOpen.TabIndex = 200;
            this.btnFolderOpen.Text = "保存場所を\r\n開く（F）";
            this.btnFolderOpen.UseVisualStyleBackColor = true;
            // 
            // btnCreate
            // 
            this.btnCreate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnCreate.Location = new System.Drawing.Point(175, 15);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(123, 40);
            this.btnCreate.TabIndex = 201;
            this.btnCreate.Text = "作成（O）";
            this.btnCreate.UseVisualStyleBackColor = true;
            // 
            // btnReCreate
            // 
            this.btnReCreate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnReCreate.Location = new System.Drawing.Point(301, 15);
            this.btnReCreate.Name = "btnReCreate";
            this.btnReCreate.Size = new System.Drawing.Size(123, 40);
            this.btnReCreate.TabIndex = 203;
            this.btnReCreate.Text = "再作成（R）";
            this.btnReCreate.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 499);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(830, 58);
            this.panel2.TabIndex = 250;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(620, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(210, 58);
            this.panel4.TabIndex = 350;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(75, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(123, 40);
            this.btnClose.TabIndex = 351;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnClear);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(224, 58);
            this.panel3.TabIndex = 300;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(12, 9);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(123, 40);
            this.btnClear.TabIndex = 301;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.linkLblEmploeeNum);
            this.panel1.Controls.Add(this.btnExecute);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.cmbSumSatrtDate);
            this.panel1.Controls.Add(this.txtEmploeeNum);
            this.panel1.Controls.Add(this.lblEmploeeNum);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(830, 125);
            this.panel1.TabIndex = 78;
            // 
            // linkLblEmploeeNum
            // 
            this.linkLblEmploeeNum.AutoSize = true;
            this.linkLblEmploeeNum.Location = new System.Drawing.Point(32, 84);
            this.linkLblEmploeeNum.Name = "linkLblEmploeeNum";
            this.linkLblEmploeeNum.Size = new System.Drawing.Size(115, 12);
            this.linkLblEmploeeNum.TabIndex = 2;
            this.linkLblEmploeeNum.TabStop = true;
            this.linkLblEmploeeNum.Text = "※社員番号とは？（H）";
            // 
            // btnExecute
            // 
            this.btnExecute.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnExecute.Location = new System.Drawing.Point(662, 68);
            this.btnExecute.Name = "btnExecute";
            this.btnExecute.Size = new System.Drawing.Size(123, 40);
            this.btnExecute.TabIndex = 3;
            this.btnExecute.Text = "実行（J）";
            this.btnExecute.UseVisualStyleBackColor = true;
            // 
            // txtEmploeeNum
            // 
            this.txtEmploeeNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtEmploeeNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtEmploeeNum.Location = new System.Drawing.Point(150, 55);
            this.txtEmploeeNum.MaxLength = 6;
            this.txtEmploeeNum.Name = "txtEmploeeNum";
            this.txtEmploeeNum.Size = new System.Drawing.Size(60, 23);
            this.txtEmploeeNum.TabIndex = 1;
            this.txtEmploeeNum.Text = "012345";
            this.txtEmploeeNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblEmploeeNum
            // 
            this.lblEmploeeNum.AutoSize = true;
            this.lblEmploeeNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblEmploeeNum.Location = new System.Drawing.Point(31, 58);
            this.lblEmploeeNum.Name = "lblEmploeeNum";
            this.lblEmploeeNum.Size = new System.Drawing.Size(72, 16);
            this.lblEmploeeNum.TabIndex = 55;
            this.lblEmploeeNum.Text = "社員番号";
            // 
            // TEF01000_CreateRosterBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 557);
            this.Controls.Add(this.pnlEmpInfo);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(850, 600);
            this.MinimumSize = new System.Drawing.Size(850, 600);
            this.Name = "TEF01000_CreateRosterBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "作業・時間外報告書作成";
            this.pnlEmpInfo.ResumeLayout(false);
            this.grpEmploeeInfo.ResumeLayout(false);
            this.grpEmploeeInfo.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.ComboBox cmbSumSatrtDate;
        public System.Windows.Forms.Panel pnlEmpInfo;
        public System.Windows.Forms.Panel panel7;
        public System.Windows.Forms.Panel panel8;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.TextBox txtEmploeeNum;
        public System.Windows.Forms.Label lblEmploeeNum;
        public System.Windows.Forms.Button btnFolderOpen;
        public System.Windows.Forms.Button btnCreate;
        public System.Windows.Forms.Button btnReCreate;
        public System.Windows.Forms.Button btnExecute;
        public System.Windows.Forms.LinkLabel linkLblEmploeeNum;
        private System.Windows.Forms.GroupBox grpEmploeeInfo;
        public System.Windows.Forms.ComboBox cmbGroup;
        public System.Windows.Forms.ComboBox cmbBuName;
        public System.Windows.Forms.Label lblCmbGroup;
        public System.Windows.Forms.Label lblCmbBu;
        public System.Windows.Forms.ComboBox cmbDefaultWorkMM2;
        public System.Windows.Forms.ComboBox cmbDefaultWorkMM3;
        public System.Windows.Forms.ComboBox cmbDefaultWorkMM1;
        public System.Windows.Forms.ComboBox cmbDefaultWorkHH2;
        public System.Windows.Forms.ComboBox cmbDefaultWorkHH3;
        public System.Windows.Forms.ComboBox cmbDefaultWorkHH1;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox txtDefaultWorkName3;
        public System.Windows.Forms.TextBox txtDefaultChargeNo3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txtDefaultWorkName2;
        public System.Windows.Forms.TextBox txtDefaultChargeNo2;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.TextBox txtDefaultWorkName1;
        public System.Windows.Forms.TextBox txtDefaultChargeNo1;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.CheckBox chkNightShift;
        public System.Windows.Forms.CheckBox chkForcingBshift;
        public System.Windows.Forms.CheckBox chkMHSC;
        public System.Windows.Forms.CheckBox chkDefaultWork;
        public System.Windows.Forms.CheckBox chkDelFlg;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtFileName;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox txtOutputFolder;
    }
}

