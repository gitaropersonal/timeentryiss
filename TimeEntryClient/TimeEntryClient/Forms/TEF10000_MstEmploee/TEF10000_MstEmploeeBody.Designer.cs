﻿namespace TimeEntryClient.Forms {
    partial class TEF10000_MstEmploeeBody
        {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TEF10000_MstEmploeeBody));
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvEmploee = new System.Windows.Forms.DataGridView();
            this.BuCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.BuName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GroupName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmploeeNum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmploeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DefaultWorkFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.MhscFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.ForcingShiftB = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.NightShiftFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.DeleteFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel5 = new System.Windows.Forms.Panel();
            this.cmbGroup = new System.Windows.Forms.ComboBox();
            this.cmbBuName = new System.Windows.Forms.ComboBox();
            this.lblCmbGroup = new System.Windows.Forms.Label();
            this.lblCmbBu = new System.Windows.Forms.Label();
            this.chkDelFlg = new System.Windows.Forms.CheckBox();
            this.chkNightShift = new System.Windows.Forms.CheckBox();
            this.chkForcingBshift = new System.Windows.Forms.CheckBox();
            this.chkMHSC = new System.Windows.Forms.CheckBox();
            this.chkDefaultWork = new System.Windows.Forms.CheckBox();
            this.txtEmploeeNum = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnRowEdit = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmploee)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvEmploee);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1030, 707);
            this.panel1.TabIndex = 30;
            // 
            // dgvEmploee
            // 
            this.dgvEmploee.AllowUserToDeleteRows = false;
            this.dgvEmploee.AllowUserToOrderColumns = true;
            this.dgvEmploee.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvEmploee.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvEmploee.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmploee.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.BuCode,
            this.BuName,
            this.GroupCode,
            this.GroupName,
            this.EmploeeNum,
            this.EmploeeName,
            this.DefaultWorkFlg,
            this.MhscFlg,
            this.ForcingShiftB,
            this.NightShiftFlg,
            this.DeleteFlg});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvEmploee.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvEmploee.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvEmploee.Location = new System.Drawing.Point(0, 132);
            this.dgvEmploee.Name = "dgvEmploee";
            this.dgvEmploee.ReadOnly = true;
            this.dgvEmploee.RowTemplate.Height = 21;
            this.dgvEmploee.Size = new System.Drawing.Size(1030, 515);
            this.dgvEmploee.TabIndex = 30;
            // 
            // BuCode
            // 
            this.BuCode.DataPropertyName = "BuCode";
            this.BuCode.HeaderText = "部署コード";
            this.BuCode.Name = "BuCode";
            this.BuCode.ReadOnly = true;
            this.BuCode.Visible = false;
            // 
            // BuName
            // 
            this.BuName.DataPropertyName = "BuName";
            this.BuName.HeaderText = "部署名";
            this.BuName.Name = "BuName";
            this.BuName.ReadOnly = true;
            this.BuName.Width = 115;
            // 
            // GroupCode
            // 
            this.GroupCode.DataPropertyName = "GroupCode";
            this.GroupCode.HeaderText = "グループコード";
            this.GroupCode.Name = "GroupCode";
            this.GroupCode.ReadOnly = true;
            this.GroupCode.Visible = false;
            // 
            // GroupName
            // 
            this.GroupName.DataPropertyName = "GroupName";
            this.GroupName.HeaderText = "グループ名";
            this.GroupName.Name = "GroupName";
            this.GroupName.ReadOnly = true;
            this.GroupName.Width = 180;
            // 
            // EmploeeNum
            // 
            this.EmploeeNum.DataPropertyName = "EmploeeNum";
            this.EmploeeNum.HeaderText = "社員番号";
            this.EmploeeNum.Name = "EmploeeNum";
            this.EmploeeNum.ReadOnly = true;
            this.EmploeeNum.Width = 95;
            // 
            // EmploeeName
            // 
            this.EmploeeName.DataPropertyName = "EmploeeName";
            this.EmploeeName.HeaderText = "社員名";
            this.EmploeeName.Name = "EmploeeName";
            this.EmploeeName.ReadOnly = true;
            this.EmploeeName.Width = 90;
            // 
            // DefaultWorkFlg
            // 
            this.DefaultWorkFlg.DataPropertyName = "DefaultWorkFlg";
            this.DefaultWorkFlg.HeaderText = "デフォ作業使用";
            this.DefaultWorkFlg.Name = "DefaultWorkFlg";
            this.DefaultWorkFlg.ReadOnly = true;
            this.DefaultWorkFlg.Width = 120;
            // 
            // MhscFlg
            // 
            this.MhscFlg.DataPropertyName = "MhscFlg";
            this.MhscFlg.HeaderText = "MHSC所属";
            this.MhscFlg.Name = "MhscFlg";
            this.MhscFlg.ReadOnly = true;
            this.MhscFlg.Width = 90;
            // 
            // ForcingShiftB
            // 
            this.ForcingShiftB.DataPropertyName = "ForcingShiftB";
            this.ForcingShiftB.HeaderText = "強制Bシフト使用";
            this.ForcingShiftB.Name = "ForcingShiftB";
            this.ForcingShiftB.ReadOnly = true;
            this.ForcingShiftB.Width = 130;
            // 
            // NightShiftFlg
            // 
            this.NightShiftFlg.DataPropertyName = "NightShiftFlg";
            this.NightShiftFlg.FillWeight = 90F;
            this.NightShiftFlg.HeaderText = "夜勤担当者";
            this.NightShiftFlg.Name = "NightShiftFlg";
            this.NightShiftFlg.ReadOnly = true;
            // 
            // DeleteFlg
            // 
            this.DeleteFlg.DataPropertyName = "DeleteFlg";
            this.DeleteFlg.HeaderText = "削除";
            this.DeleteFlg.Name = "DeleteFlg";
            this.DeleteFlg.ReadOnly = true;
            this.DeleteFlg.Width = 50;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.cmbGroup);
            this.panel5.Controls.Add(this.cmbBuName);
            this.panel5.Controls.Add(this.lblCmbGroup);
            this.panel5.Controls.Add(this.lblCmbBu);
            this.panel5.Controls.Add(this.chkDelFlg);
            this.panel5.Controls.Add(this.chkNightShift);
            this.panel5.Controls.Add(this.chkForcingBshift);
            this.panel5.Controls.Add(this.chkMHSC);
            this.panel5.Controls.Add(this.chkDefaultWork);
            this.panel5.Controls.Add(this.txtEmploeeNum);
            this.panel5.Controls.Add(this.label4);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1030, 132);
            this.panel5.TabIndex = 0;
            // 
            // cmbGroup
            // 
            this.cmbGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbGroup.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.cmbGroup.FormattingEnabled = true;
            this.cmbGroup.Location = new System.Drawing.Point(111, 39);
            this.cmbGroup.Name = "cmbGroup";
            this.cmbGroup.Size = new System.Drawing.Size(466, 24);
            this.cmbGroup.TabIndex = 1;
            // 
            // cmbBuName
            // 
            this.cmbBuName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBuName.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.cmbBuName.FormattingEnabled = true;
            this.cmbBuName.Location = new System.Drawing.Point(111, 12);
            this.cmbBuName.Name = "cmbBuName";
            this.cmbBuName.Size = new System.Drawing.Size(466, 24);
            this.cmbBuName.TabIndex = 0;
            // 
            // lblCmbGroup
            // 
            this.lblCmbGroup.AutoSize = true;
            this.lblCmbGroup.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCmbGroup.Location = new System.Drawing.Point(30, 42);
            this.lblCmbGroup.Name = "lblCmbGroup";
            this.lblCmbGroup.Size = new System.Drawing.Size(58, 16);
            this.lblCmbGroup.TabIndex = 77;
            this.lblCmbGroup.Text = "グループ";
            // 
            // lblCmbBu
            // 
            this.lblCmbBu.AutoSize = true;
            this.lblCmbBu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblCmbBu.Location = new System.Drawing.Point(31, 15);
            this.lblCmbBu.Name = "lblCmbBu";
            this.lblCmbBu.Size = new System.Drawing.Size(40, 16);
            this.lblCmbBu.TabIndex = 76;
            this.lblCmbBu.Text = "部署";
            // 
            // chkDelFlg
            // 
            this.chkDelFlg.AutoSize = true;
            this.chkDelFlg.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkDelFlg.Location = new System.Drawing.Point(489, 99);
            this.chkDelFlg.Name = "chkDelFlg";
            this.chkDelFlg.Size = new System.Drawing.Size(88, 20);
            this.chkDelFlg.TabIndex = 7;
            this.chkDelFlg.Text = "削除含む";
            this.chkDelFlg.UseVisualStyleBackColor = true;
            // 
            // chkNightShift
            // 
            this.chkNightShift.AutoSize = true;
            this.chkNightShift.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkNightShift.Location = new System.Drawing.Point(376, 99);
            this.chkNightShift.Name = "chkNightShift";
            this.chkNightShift.Size = new System.Drawing.Size(107, 20);
            this.chkNightShift.TabIndex = 6;
            this.chkNightShift.Text = "夜勤担当者";
            this.chkNightShift.UseVisualStyleBackColor = true;
            // 
            // chkForcingBshift
            // 
            this.chkForcingBshift.AutoSize = true;
            this.chkForcingBshift.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkForcingBshift.Location = new System.Drawing.Point(267, 99);
            this.chkForcingBshift.Name = "chkForcingBshift";
            this.chkForcingBshift.Size = new System.Drawing.Size(104, 20);
            this.chkForcingBshift.TabIndex = 5;
            this.chkForcingBshift.Text = "強制Bシフト";
            this.chkForcingBshift.UseVisualStyleBackColor = true;
            // 
            // chkMHSC
            // 
            this.chkMHSC.AutoSize = true;
            this.chkMHSC.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkMHSC.Location = new System.Drawing.Point(157, 99);
            this.chkMHSC.Name = "chkMHSC";
            this.chkMHSC.Size = new System.Drawing.Size(102, 20);
            this.chkMHSC.TabIndex = 4;
            this.chkMHSC.Text = "MHSC所属";
            this.chkMHSC.UseVisualStyleBackColor = true;
            // 
            // chkDefaultWork
            // 
            this.chkDefaultWork.AutoSize = true;
            this.chkDefaultWork.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.chkDefaultWork.Location = new System.Drawing.Point(34, 99);
            this.chkDefaultWork.Name = "chkDefaultWork";
            this.chkDefaultWork.Size = new System.Drawing.Size(126, 20);
            this.chkDefaultWork.TabIndex = 3;
            this.chkDefaultWork.Text = "デフォ作業使用";
            this.chkDefaultWork.UseVisualStyleBackColor = true;
            // 
            // txtEmploeeNum
            // 
            this.txtEmploeeNum.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtEmploeeNum.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtEmploeeNum.Location = new System.Drawing.Point(111, 68);
            this.txtEmploeeNum.MaxLength = 6;
            this.txtEmploeeNum.Name = "txtEmploeeNum";
            this.txtEmploeeNum.Size = new System.Drawing.Size(60, 23);
            this.txtEmploeeNum.TabIndex = 2;
            this.txtEmploeeNum.Text = "012345";
            this.txtEmploeeNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(31, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 25;
            this.label4.Text = "社員番号";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnSearch);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(883, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(147, 132);
            this.panel6.TabIndex = 20;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(12, 79);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(123, 40);
            this.btnSearch.TabIndex = 20;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 649);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1030, 58);
            this.panel2.TabIndex = 80;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(747, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(283, 58);
            this.panel4.TabIndex = 90;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(148, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(123, 40);
            this.btnClose.TabIndex = 90;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnRowEdit);
            this.panel3.Controls.Add(this.btnInsert);
            this.panel3.Controls.Add(this.btnClear);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(419, 58);
            this.panel3.TabIndex = 70;
            // 
            // btnRowEdit
            // 
            this.btnRowEdit.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnRowEdit.Location = new System.Drawing.Point(270, 9);
            this.btnRowEdit.Name = "btnRowEdit";
            this.btnRowEdit.Size = new System.Drawing.Size(123, 40);
            this.btnRowEdit.TabIndex = 72;
            this.btnRowEdit.Text = "行編集（E）";
            this.btnRowEdit.UseVisualStyleBackColor = true;
            // 
            // btnInsert
            // 
            this.btnInsert.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnInsert.Location = new System.Drawing.Point(141, 9);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(123, 40);
            this.btnInsert.TabIndex = 71;
            this.btnInsert.Text = "行追加（I）";
            this.btnInsert.UseVisualStyleBackColor = true;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(12, 9);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(123, 40);
            this.btnClear.TabIndex = 70;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // TEF10000_MstEmploeeBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 707);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1050, 750);
            this.MinimumSize = new System.Drawing.Size(1050, 750);
            this.Name = "TEF10000_MstEmploeeBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "社員マスタ";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmploee)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.Panel panel5;
        public System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox txtEmploeeNum;
        public System.Windows.Forms.CheckBox chkForcingBshift;
        public System.Windows.Forms.CheckBox chkMHSC;
        public System.Windows.Forms.CheckBox chkDefaultWork;
        public System.Windows.Forms.Button btnInsert;
        public System.Windows.Forms.Button btnRowEdit;
        public System.Windows.Forms.CheckBox chkNightShift;
        public System.Windows.Forms.CheckBox chkDelFlg;
        public System.Windows.Forms.DataGridView dgvEmploee;
        public System.Windows.Forms.ComboBox cmbGroup;
        public System.Windows.Forms.ComboBox cmbBuName;
        public System.Windows.Forms.Label lblCmbGroup;
        public System.Windows.Forms.Label lblCmbBu;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn BuName;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn GroupName;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmploeeNum;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmploeeName;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DefaultWorkFlg;
        private System.Windows.Forms.DataGridViewCheckBoxColumn MhscFlg;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ForcingShiftB;
        private System.Windows.Forms.DataGridViewCheckBoxColumn NightShiftFlg;
        private System.Windows.Forms.DataGridViewCheckBoxColumn DeleteFlg;
    }
}

