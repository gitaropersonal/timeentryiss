﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TimeEntryClient.Const;
using TimeEntryClient.Dto;
using TimeEntryShared.Const;
using TimeEntryShared.Entity;
using TimeEntryShared.Util;
using TimeEntryClient.Util;

namespace TimeEntryClient.Forms {
    public class TEF10000_MstEmploeeFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private TEF10000_MstEmploeeBody _Body;
        /// <summary>
        /// Config情報
        /// </summary>
        private TimeEntryConfigEntity _ConfigInfo = new TimeEntryConfigEntity();
        /// <summary>
        /// 従業員情報
        /// </summary>
        private List<MstEmploeeEntity> _EmploeeInfos = new List<MstEmploeeEntity>();
        /// <summary>
        /// グループマスタ
        /// </summary>
        private List<MstGroupEntity> _GroupList = new List<MstGroupEntity>();
        /// <summary>
        /// 部署マスタ
        /// </summary>
        private List<MstBuEntity> _BuList = new List<MstBuEntity>();
        /// <summary>
        /// グループコンボボックスのデータソース用リスト
        /// </summary>
        private Dictionary<string, string> _CmbBuSourceList = new Dictionary<string, string>();
        /// <summary>
        /// グループコンボボックスのデータソース用リスト
        /// </summary>
        private Dictionary<string, string> _CmbGroupSourceList = new Dictionary<string, string>();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        public TEF10000_MstEmploeeFormLogic(TEF10000_MstEmploeeBody body) {
            // 初期化
            _Body = body;
            Init();
            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.O:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.I:
                        if (_Body.dgvEmploee.Focused) {
                            _Body.btnInsert.PerformClick();
                            return;
                        }
                        _Body.btnInsert.Focus();
                        _Body.btnInsert.PerformClick();
                        break;
                    case Keys.E:
                        if (_Body.dgvEmploee.Focused) {
                            _Body.btnRowEdit.PerformClick();
                            return;
                        }
                        _Body.btnRowEdit.Focus();
                        _Body.btnRowEdit.PerformClick();
                        break;
                }
            };
            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                ShowGrid();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var result = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLEAR);
                if (result == DialogResult.OK) {
                    Init();
                }
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // 行追加ボタン押下
            _Body.btnInsert.Click += (s, e) => {
                ClickRowEdit(ClientConst.ScreenMode.INSERT);
            };
            // 行編集ボタン押下
            _Body.btnRowEdit.Click += (s, e) => {
                ClickRowEdit();
            };
            // グリッドセルダブルクリック
            _Body.dgvEmploee.CellDoubleClick += (s, e) => {
                if (e.RowIndex < 0) {
                    return;
                }
                if (e.RowIndex == _Body.dgvEmploee.Rows.Count - 1) {
                    ClickRowEdit(ClientConst.ScreenMode.INSERT);
                    return;
                }
                ClickRowEdit();
            };
            // グリッド行番号描画
            _Body.dgvEmploee.RowPostPaint += (s, e) => {
                GridRowUtil<MstEmploeeDto>.SetRowNum(s, e);
            };
            // 所属コンボボックスSelectionChangeCommitted
            _Body.cmbBuName.SelectionChangeCommitted += (s, e) => {
                ControlUtil.InitGroupCombo(_GroupList, ref _CmbGroupSourceList, _Body.cmbGroup, _Body.cmbBuName.Text, false);
            };
        }
        /// <summary>
        /// 行編集ボタン押下処理
        /// </summary>
        /// <param name="ScreenMode"></param>
        private void ClickRowEdit(ClientConst.ScreenMode ScreenMode = ClientConst.ScreenMode.UPDATE) {
            var tgtEmploeeInfo = new MstEmploeeEntity();
            int currRowIdx = -1;
            int currColIdx = -1;
            if (_Body.dgvEmploee.CurrentCell != null) {
                currRowIdx = _Body.dgvEmploee.CurrentCell.RowIndex;
                currColIdx = _Body.dgvEmploee.CurrentCell.ColumnIndex;
            }
            var currRow = new DataGridViewRow();
            switch (ScreenMode) {
                case ClientConst.ScreenMode.INSERT:
                    tgtEmploeeInfo.DefaultWorkFlg    = FlgValConst._STR_FLG_OFF;
                    tgtEmploeeInfo.DeleteFlg         = FlgValConst._STR_FLG_OFF;
                    tgtEmploeeInfo.MhscFlg           = FlgValConst._STR_FLG_OFF;
                    tgtEmploeeInfo.ForcingShiftBFlg  = FlgValConst._STR_FLG_OFF;
                    tgtEmploeeInfo.NightShiftFlg     = FlgValConst._STR_FLG_OFF;
                    tgtEmploeeInfo.LoginPass         = BatConst._LOGIN_PASS;
                    tgtEmploeeInfo.BuCode            = string.IsNullOrEmpty(_Body.cmbBuName.Text) ? string.Empty : _Body.cmbBuName.Text.Substring(0,3);
                    tgtEmploeeInfo.GroupCode         = string.IsNullOrEmpty(_Body.cmbGroup.Text)  ? string.Empty : _Body.cmbGroup.Text.Substring(0,6);
                    break;
                case ClientConst.ScreenMode.UPDATE:
                    currRow = _Body.dgvEmploee.CurrentRow;
                    if (currRow == null || currRowIdx < 0) {
                        MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_SELECTED_ROW_NOTHING);
                        return;
                    }
                    var tgtRowDto = GridRowUtil<MstEmploeeDto>.GetRowModel(currRow);
                    if (string.IsNullOrEmpty(tgtRowDto.EmploeeNum)) {
                        MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_SELECTED_ROW_NOTHING);
                        return;
                    }
                    tgtEmploeeInfo = _EmploeeInfos.Where(n => n.EmploeeNum == tgtRowDto.EmploeeNum).FirstOrDefault();
                    break;
            }
            DialogResult dr = DialogResult.None;
            using (var prgBar = new TEF01010_SettingDialog(tgtEmploeeInfo, ScreenMode)) {
                dr = prgBar.ShowDialog();
            }
            if (dr != DialogResult.Cancel) {
                // グリッド再描画
                ShowGrid();
            }
            if (0 <= currRowIdx && currRowIdx < _Body.dgvEmploee.Rows.Count) {
                _Body.dgvEmploee.CurrentCell = _Body.dgvEmploee.Rows[currRowIdx].Cells[currColIdx];
            }
        }
        /// <summary>
        /// グリッド描画
        /// </summary>
        private void ShowGrid() {
            // 最新の従業員情報取得
            _EmploeeInfos = ModelUtil.LoadEmploeeMst(_ConfigInfo);

            // 初期化
            _Body.dgvEmploee.Rows.Clear();

            // エラーメッセージ
            var tgtEmploeeInfos = GetTgtEmploeeInfos();
            if (tgtEmploeeInfos.Count == 0) {
                MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_EXIST_DATA);
                return;
            }
            // ソート
            tgtEmploeeInfos = tgtEmploeeInfos.OrderBy(n => n.BuCode)
                                              .ThenBy(n => n.GroupCode)
                                              .ThenBy(n => n.EmploeeNum)
                                              .ThenBy(n => n.Name)
                                              .ToList();

            // グリッドオブジェクトを作成
            _Body.dgvEmploee.DataSource = GetGridBindingSource(tgtEmploeeInfos);

            // 削除行の背景色
            SetColor_DeletedGridCell();

            _Body.dgvEmploee.Show();
            _Body.dgvEmploee.Focus();
        }
        /// <summary>
        /// グリッドViewModel取得
        /// </summary>
        /// <param name="tgtEmploeeInfos"></param>
        /// <returns></returns>
        private BindingList<MstEmploeeDto> GetGridBindingSource(List<MstEmploeeEntity> tgtEmploeeInfos) {
            var ret = new BindingList<MstEmploeeDto>();
            foreach (var tgtEmp in tgtEmploeeInfos) {
                if (_BuList.Where(n => n.BuCode == tgtEmp.BuCode).FirstOrDefault() == null) {
                    continue;
                }
                string tgtBuName = _BuList.Where(n => n.BuCode == tgtEmp.BuCode).FirstOrDefault().BuName;
                if (!string.IsNullOrEmpty(tgtEmp.GroupCode) && _GroupList.Where(n => n.GroupCode == tgtEmp.GroupCode).FirstOrDefault() == null) {
                    continue;
                }
                var tgtGroupCode = _GroupList.Where(n => n.GroupCode == tgtEmp.GroupCode).FirstOrDefault().GroupCode;
                var tgtGroupName = _GroupList.Where(n => n.GroupCode == tgtEmp.GroupCode).FirstOrDefault().GroupName;
                var rowDto = new MstEmploeeDto();
                rowDto.EmploeeNum     = tgtEmp.EmploeeNum;
                rowDto.EmploeeName    = tgtEmp.Name;
                rowDto.BuCode         = tgtEmp.BuCode;
                rowDto.BuName         = tgtBuName;
                rowDto.GroupCode      = StrUtil.TostringNullForbid(tgtGroupCode);
                rowDto.GroupName      = StrUtil.TostringNullForbid(tgtGroupName);
                rowDto.DefaultWorkFlg = int.Parse(tgtEmp.DefaultWorkFlg);
                rowDto.NightShiftFlg  = int.Parse(tgtEmp.NightShiftFlg);
                rowDto.MhscFlg        = int.Parse(tgtEmp.MhscFlg);
                rowDto.ForcingShiftB  = int.Parse(tgtEmp.ForcingShiftBFlg);
                rowDto.DeleteFlg      = int.Parse(tgtEmp.DeleteFlg);
                ret.Add(rowDto);
            }
            return ret;
        }
        /// <summary>
        /// 削除行の背景色をセット
        /// </summary>
        private void SetColor_DeletedGridCell() {
            foreach (DataGridViewRow r in _Body.dgvEmploee.Rows) {
                var rowDto = GridRowUtil<MstEmploeeDto>.GetRowModel(r);
                if (rowDto.DeleteFlg == FlgValConst._INT_FLG_ON) {
                    foreach (DataGridViewCell c in r.Cells) {
                        c.Style.BackColor = Color.LightGray;
                    }
                }
            }
        }
        /// <summary>
        /// 検索条件に合致する従業員情報を取得
        /// </summary>
        /// <returns></returns>
        private List<MstEmploeeEntity> GetTgtEmploeeInfos() {
            var ret = new List<MstEmploeeEntity>();
            _EmploeeInfos = ModelUtil.LoadEmploeeMst(_ConfigInfo);
            _EmploeeInfos.ForEach(n => ret.Add(n));

            // 部署
            if (!string.IsNullOrEmpty(_Body.cmbBuName.Text)) {
                ret = ret.Where(n => n.BuCode.Contains(_Body.cmbBuName.Text.Substring(0, 3))).ToList();
            }
            // グループ
            if (!string.IsNullOrEmpty(_Body.cmbGroup.Text)) {
                ret = ret.Where(n => n.GroupCode.Contains(_Body.cmbGroup.Text.Substring(0, 6))).ToList();
            }
            // 社員番号
            if (!string.IsNullOrEmpty(_Body.txtEmploeeNum.Text)) {
                ret = ret.Where(n => n.EmploeeNum == _Body.txtEmploeeNum.Text).ToList();
            }
            // 削除フラグ
            if (!_Body.chkDelFlg.Checked) {
                ret = ret.Where(n => n.DeleteFlg == FlgValConst._STR_FLG_OFF).ToList();
            }
            // デフォ作業使用フラグ
            if (_Body.chkDefaultWork.Checked) {
                ret = ret.Where(n => n.DefaultWorkFlg == FlgValConst._STR_FLG_ON).ToList();
            }
            // MHSCフラグ
            if (_Body.chkMHSC.Checked) {
                ret = ret.Where(n => n.MhscFlg == FlgValConst._STR_FLG_ON).ToList();
            }
            // 強制Bシフトフラグ
            if (_Body.chkForcingBshift.Checked) {
                ret = ret.Where(n => n.ForcingShiftBFlg == FlgValConst._STR_FLG_ON).ToList();
            }
            // 夜勤担当者フラグ
            if (_Body.chkNightShift.Checked) {
                ret = ret.Where(n => n.NightShiftFlg == FlgValConst._STR_FLG_ON).ToList();
            }
            return ret;
        }
        /// <summary>
        /// 初期化処理
        /// </summary>
        private void Init() {
            _ConfigInfo = ModelUtil.LoadConfig();
            _EmploeeInfos = ModelUtil.LoadEmploeeMst(_ConfigInfo);
            _GroupList = ModelUtil.LoadGroupMst(_ConfigInfo);
            _BuList = ModelUtil.LoadBuMst(_ConfigInfo);
            ControlUtil.InitBuCombo(_BuList, ref _CmbBuSourceList, _Body.cmbBuName, true);
            ControlUtil.InitGroupCombo(_GroupList, ref _CmbGroupSourceList, _Body.cmbGroup, _Body.cmbBuName.Text, false);
            _Body.chkDefaultWork.Checked = false;
            _Body.chkDelFlg.Checked = false;
            _Body.chkForcingBshift.Checked = false;
            _Body.chkMHSC.Checked = false;
            _Body.chkNightShift.Checked = false;
            _Body.txtEmploeeNum.Text = string.Empty;
            _Body.dgvEmploee.Rows.Clear();
            _Body.cmbBuName.Focus();
        }
    }
}
