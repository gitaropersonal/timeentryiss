﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TimeEntryClient.Forms.Dialog {
    public partial class EmploeeCardDialog : Form {
        public EmploeeCardDialog() {
            InitializeComponent();
            btnOK.Focus();
        }
        /// <summary>
        /// OKボタン押下
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOK_Click(object sender, EventArgs e) {
            Close();
        }
    }
}
