﻿using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using TimeEntryClient.Const;
using TimeEntryShared.Const;
using TimeEntryShared.Entity;
using TimeEntryShared.Util;
using TimeEntryClient.Util;

namespace TimeEntryClient.Forms {
    public class TEF10200_MstBuFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private TEF10200_MstBuBody _Body;
        /// <summary>
        /// Config情報
        /// </summary>
        private TimeEntryConfigEntity _ConfigInfo = new TimeEntryConfigEntity();
        private List<MstBuEntity> _BuList = new List<MstBuEntity>();
        /// <summary>
        /// グリッドセル名
        /// </summary>
        private const string _GRID_CELL_BUCODE  = "BuCode";
        private const string _GRID_CELL_BUNAME  = "BuName";
        private const string _GRID_CELL_DEL_FLG = "DelFlg";
        private const string _GRID_CELL_EDITED  = "EditedFlg";
        /// <summary>
        /// 検索時行数
        /// </summary>
        private int _SEARCHED_ROW_COUNT = 0;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        public TEF10200_MstBuFormLogic(TEF10200_MstBuBody body) {
            // 初期化
            _Body = body;
            Init();
            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                }
            };
            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                ShowGrid();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var result = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLEAR);
                if (result == DialogResult.OK) {
                    Init();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                Update();
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // グリッドCellEnter
            _Body.dgvMstBu.CellEnter += (s, e) => {
                GridCelEnter(e.RowIndex, e.ColumnIndex);
            };
            // グリッド行番号描画
            _Body.dgvMstBu.RowPostPaint += (s, e) => {
                GridRowUtil<MstBuDto>.SetRowNum(s, e);
            };
            // グリッドCellValueChanged
            _Body.dgvMstBu.CellValueChanged += GridCellValueChanged;
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="RowIdx"></param>
        /// <param name="ColIdx"></param>
        private void GridCelEnter(int RowIdx, int ColIdx) {
            if (RowIdx < 0 || ColIdx < 0) {
                return;
            }
            var tgtCol = _Body.dgvMstBu.Columns[ColIdx];
            if (tgtCol != null) {
                // IMEモードを設定
                string tgtColName = tgtCol.Name;
                switch (tgtColName) {
                    case _GRID_CELL_BUCODE:
                        _Body.dgvMstBu.ImeMode = ImeMode.Disable;
                        break;
                    case _GRID_CELL_BUNAME:
                        _Body.dgvMstBu.ImeMode = ImeMode.Hiragana;
                        break;
                    default:
                        _Body.dgvMstBu.ImeMode = ImeMode.NoControl;
                        break;
                }
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(object s, DataGridViewCellEventArgs e) {

            // イベント退避
            _Body.dgvMstBu.CellValueChanged -= GridCellValueChanged;

            string tgtCellName = _Body.dgvMstBu.Columns[e.ColumnIndex].Name;
            switch (tgtCellName) {
                case _GRID_CELL_BUCODE: // 部署コード
                    GridCellValueChangedBuCode(e.RowIndex);
                    break;
                case _GRID_CELL_BUNAME: // 部署名
                    GridCellValueChangedBuName(e.RowIndex);
                    break;
            }
            if (0 <= e.ColumnIndex && 0 <= e.RowIndex && (tgtCellName == _GRID_CELL_BUCODE || tgtCellName == _GRID_CELL_BUNAME || tgtCellName == _GRID_CELL_DEL_FLG)) {
                // セル背景色
                _Body.dgvMstBu.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;

                // 編集ステータス
                if (e.RowIndex < _SEARCHED_ROW_COUNT - 1) {
                    _Body.dgvMstBu.Rows[e.RowIndex].Cells[_GRID_CELL_EDITED].Value = ClientConst.EditStatus.UPDATE;
                } else {
                    _Body.dgvMstBu.Rows[e.RowIndex].Cells[_GRID_CELL_EDITED].Value = ClientConst.EditStatus.INSERT;
                }
            }
            // イベント回復
            _Body.dgvMstBu.CellValueChanged += GridCellValueChanged;
        }
        /// <summary>
        /// グリッドCellValueChanged（部コード）
        /// </summary>
        /// <param name="RowIdx"></param>
        private void GridCellValueChangedBuCode(int RowIdx) {
            var BuCode = _Body.dgvMstBu.Rows[RowIdx].Cells[_GRID_CELL_BUCODE].Value;
            if (BuCode == null || string.IsNullOrEmpty(BuCode.ToString())) {
                _Body.dgvMstBu.Rows[RowIdx].Cells[_GRID_CELL_BUCODE].Value = string.Empty;
                return;
            }
            int intBuCode;
            if (!int.TryParse(BuCode.ToString(), out intBuCode)) {
                _Body.dgvMstBu.Rows[RowIdx].Cells[_GRID_CELL_BUCODE].Value = string.Empty;
                return;
            }
            _Body.dgvMstBu.Rows[RowIdx].Cells[_GRID_CELL_BUCODE].Value = StrUtil.SelNumericFromStr(BuCode.ToString()).PadLeft(3, CommonLiteral._CHAR_PAD_ZERO);
        }
        /// <summary>
        /// グリッドCellValueChanged（部署名）
        /// </summary>
        /// <param name="RowIdx"></param>
        private void GridCellValueChangedBuName(int RowIdx) {
            var BuName = _Body.dgvMstBu.Rows[RowIdx].Cells[_GRID_CELL_BUNAME].Value;
            if (BuName == null || string.IsNullOrEmpty(BuName.ToString())) {
                _Body.dgvMstBu.Rows[RowIdx].Cells[_GRID_CELL_BUNAME].Value = string.Empty;
                return;
            }
            _Body.dgvMstBu.Rows[RowIdx].Cells[_GRID_CELL_BUNAME].Value = StrUtil.CutHalfMarkChars(BuName.ToString());
        }
        /// <summary>
        /// 更新処理
        /// </summary>
        private void Update() {
            // チェック処理
            if (!UpdValidate()) {
                return;
            }
            // 確認ダイアログ
            if (MessageDialogUtil.ShowInfoExc(ClientMessages._MSG_ASK_POSITION_MST_CHANGE) != DialogResult.OK) {
                return;
            }
            // 更新データ作成
            var addGroupInfos = new List<MstBuEntity>();
            var updGroupInfos = new List<MstBuEntity>();
            foreach (DataGridViewRow r in _Body.dgvMstBu.Rows) {
                var rowDto = GridRowUtil<MstBuDto>.GetRowModel(r);
                switch (rowDto.EditedFlg) {
                    case ClientConst.EditStatus.INSERT:
                        // 新規登録
                        var newEntity = new MstBuEntity() {
                            BuCode = rowDto.BuCode,
                            BuName = rowDto.BuName,
                            DelFlg = rowDto.DelFlg ? FlgValConst._STR_FLG_ON : FlgValConst._STR_FLG_OFF,
                        };
                        addGroupInfos.Add(newEntity);
                        break;
                    case ClientConst.EditStatus.UPDATE:
                        // データ更新
                        var tgtEntity = _BuList.Where(n => n.BuCode == rowDto.BuCode).FirstOrDefault();
                        var updEntity = new MstBuEntity() {
                            BuCode = tgtEntity.BuCode,
                            BuName = rowDto.BuName,
                            DelFlg = rowDto.DelFlg ? FlgValConst._STR_FLG_ON : FlgValConst._STR_FLG_OFF,
                        };
                        updGroupInfos.Add(updEntity);
                        break;
                }
            }
            // 登録
            ModelUtil.InsBuMst(_ConfigInfo.ConnStrTimeEntryDB, addGroupInfos);

            // 更新
            ModelUtil.UpdBuMst(_ConfigInfo.ConnStrTimeEntryDB, updGroupInfos);

            // グリッド描画
            ShowGrid();

            // メッセージ表示
            MessageDialogUtil.ShowInfolMsgOK(ClientMessages._MSG_FINISH_UPDATE);
        }
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <returns></returns>
        private bool UpdValidate() {
            int updRowCount = 0;
            var keyInfos = new List<MstBuDto>();
            foreach (DataGridViewRow r in _Body.dgvMstBu.Rows) {
                var rowDto = GridRowUtil<MstBuDto>.GetRowModel(r);

                // キー情報取得
                var key = new MstBuDto() {
                    BuCode = rowDto.BuCode,
                    EditedFlg = rowDto.EditedFlg,
                };
                keyInfos.Add(key);

                // 編集行以外はスルー
                if (rowDto.EditedFlg == ClientConst.EditStatus.NONE) {
                    continue;
                }
                updRowCount++;

                if (string.IsNullOrEmpty(rowDto.BuCode)) {
                    // 部署コード未入力
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _Body.dgvMstBu.Columns[nameof(rowDto.BuCode)].HeaderText));
                    r.Cells[nameof(rowDto.BuCode)].Style.BackColor = Color.Red;
                    return false;
                }
                if (!string.IsNullOrEmpty(rowDto.BuCode) && string.IsNullOrEmpty(rowDto.BuName)) {
                    // 部署名未入力
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _Body.dgvMstBu.Columns[nameof(rowDto.BuName)].HeaderText));
                    r.Cells[nameof(rowDto.BuName)].Style.BackColor = Color.Red;
                    return false;
                }
            }
            if (updRowCount == 0) {
                // 更新対象データなし
                MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_EXIST_DATA);
                return false;
            }
            // 重複チェック
            foreach (var keyInfo in keyInfos) {
                if (string.IsNullOrEmpty(keyInfo.BuCode)) {
                    continue;
                }
                int doupleCountMst = _BuList.Where(n => n.BuCode == keyInfo.BuCode).ToList().Count;
                if ((keyInfo.EditedFlg == ClientConst.EditStatus.INSERT && 0 < doupleCountMst) || 1 < doupleCountMst) {
                    // 重複エラー時イベント
                    DopleErrEvent(keyInfo.BuCode, keyInfos.IndexOf(keyInfo));
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 重複エラー時イベント
        /// </summary>
        /// <param name="tgtBuCode"></param>
        /// <param name="rowIdx"></param>
        private void DopleErrEvent(string tgtBuCode, int rowIdx) {
            // キー重複
            var dto = new MstGroupDto();
            string errCol = _Body.dgvMstBu.Columns[nameof(dto.BuCode)].HeaderText;

            // キー重複している行の背景色を着色
            _Body.dgvMstBu.Rows[rowIdx].Cells[_GRID_CELL_BUCODE].Style.BackColor = Color.Red;

            // ダイアログ表示
            MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_DOUPLE, errCol));
        }
        /// <summary>
        /// グリッド描画
        /// </summary>
        private void ShowGrid() {
            // 初期化
            _SEARCHED_ROW_COUNT = 0;
            _Body.dgvMstBu.Rows.Clear();
            _BuList = ModelUtil.LoadBuMst(_ConfigInfo);

            // ソート
            _BuList = _BuList.OrderBy(n => n.BuCode).ToList();

            // グリッドオブジェクトを作成
            var gridRowDtoList  = new BindingList<MstBuDto>();
            foreach (var rec in _BuList) {
                if (!_Body.chkDelFlg.Checked && int.Parse(rec.DelFlg) == FlgValConst._INT_FLG_ON) {
                    // 削除含むのチェックがついていない場合、削除データは対象外
                    continue;
                }
                var rowDto = new MstBuDto();
                rowDto.BuCode = rec.BuCode;
                rowDto.BuName = rec.BuName;
                rowDto.DelFlg = (int.Parse(rec.DelFlg) == FlgValConst._INT_FLG_ON);
                rowDto.EditedFlg = ClientConst.EditStatus.NONE;
                gridRowDtoList.Add(rowDto);
            }
            // エラーメッセージ
            if (gridRowDtoList == null || gridRowDtoList.Count == 0) {
                MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_EXIST_DATA);
                return;
            }
            _Body.dgvMstBu.DataSource = gridRowDtoList;
            
            foreach (DataGridViewRow r in _Body.dgvMstBu.Rows) {
                var rowDto = GridRowUtil<MstBuDto>.GetRowModel(r);
                if (!string.IsNullOrEmpty(rowDto.BuCode)) {
                    // 主キーの背景色
                    r.Cells[nameof(rowDto.BuCode)].ReadOnly = true;
                    r.Cells[nameof(rowDto.BuCode)].Style.BackColor = Color.LightGray;
                }
            }
            // 行数
            _SEARCHED_ROW_COUNT = _Body.dgvMstBu.Rows.Count;

            _Body.dgvMstBu.Show();
            _Body.dgvMstBu.Focus();
        }
        /// <summary>
        /// 初期化処理
        /// </summary>
        private void Init() {
            _ConfigInfo = ModelUtil.LoadConfig();
            _BuList = ModelUtil.LoadBuMst(_ConfigInfo);
            _Body.chkDelFlg.Checked = false;
            _Body.dgvMstBu.Rows.Clear();
            _Body.btnSearch.Focus();
        }
    }
}
