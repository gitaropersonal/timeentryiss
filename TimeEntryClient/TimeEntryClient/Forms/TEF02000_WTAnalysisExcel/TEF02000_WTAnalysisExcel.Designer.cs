﻿namespace TimeEntryClient.Forms {
    partial class TEF02000_WTAnalysisExcelBody {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TEF02000_WTAnalysisExcelBody));
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnClear = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dgvWorkTime = new System.Windows.Forms.DataGridView();
            this.UserId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProjectId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EmploeeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ChargeNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.WorkName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActualTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActualManHour = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActualWorkPct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActualManMoon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedTime1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManHour1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedWorkPct1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManMoon1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedTime2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManHour2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedWorkPct2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManMoon2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedTime3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManHour3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedWorkPct3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManMoon3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedTime4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManHour4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedWorkPct4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManMoon4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedTime5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManHour5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedWorkPct5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManMoon5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedTime6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManHour6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedWorkPct6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PlannedManMoon6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TgtMonth1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TgtMonth2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TgtMonth3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TgtMonth4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TgtMonth5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TgtMonth6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EditedFlg = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.panel100 = new System.Windows.Forms.Panel();
            this.txtTougetsu = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDayBunshi = new System.Windows.Forms.TextBox();
            this.txtHyojunTimeBunbo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtHyojunTimeBunshi = new System.Windows.Forms.TextBox();
            this.txtDayBunbo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbTTGroup = new System.Windows.Forms.ComboBox();
            this.rdo6Month = new System.Windows.Forms.RadioButton();
            this.rdo3Month = new System.Windows.Forms.RadioButton();
            this.rdo1Month = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnExcelOutput = new System.Windows.Forms.Button();
            this.cmbSumEndDate = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbSumSatrtDate = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkTime)).BeginInit();
            this.panel100.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 699);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1330, 58);
            this.panel2.TabIndex = 80;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnUpdate);
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(1054, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(276, 58);
            this.panel4.TabIndex = 90;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnUpdate.Location = new System.Drawing.Point(12, 9);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(123, 40);
            this.btnUpdate.TabIndex = 91;
            this.btnUpdate.Text = "更新（O）";
            this.btnUpdate.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(141, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(123, 40);
            this.btnClose.TabIndex = 90;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnClear);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(224, 58);
            this.panel3.TabIndex = 70;
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClear.Location = new System.Drawing.Point(12, 9);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(123, 40);
            this.btnClear.TabIndex = 70;
            this.btnClear.Text = "クリア（A）";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.dgvWorkTime);
            this.panel1.Controls.Add(this.panel100);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1334, 697);
            this.panel1.TabIndex = 30;
            // 
            // dgvWorkTime
            // 
            this.dgvWorkTime.AllowUserToAddRows = false;
            this.dgvWorkTime.AllowUserToDeleteRows = false;
            this.dgvWorkTime.AllowUserToOrderColumns = true;
            this.dgvWorkTime.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvWorkTime.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvWorkTime.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvWorkTime.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.UserId,
            this.ProjectId,
            this.EmploeeName,
            this.ChargeNo,
            this.WorkName,
            this.ActualTime,
            this.ActualManHour,
            this.ActualWorkPct,
            this.ActualManMoon,
            this.PlannedTime1,
            this.PlannedManHour1,
            this.PlannedWorkPct1,
            this.PlannedManMoon1,
            this.PlannedTime2,
            this.PlannedManHour2,
            this.PlannedWorkPct2,
            this.PlannedManMoon2,
            this.PlannedTime3,
            this.PlannedManHour3,
            this.PlannedWorkPct3,
            this.PlannedManMoon3,
            this.PlannedTime4,
            this.PlannedManHour4,
            this.PlannedWorkPct4,
            this.PlannedManMoon4,
            this.PlannedTime5,
            this.PlannedManHour5,
            this.PlannedWorkPct5,
            this.PlannedManMoon5,
            this.PlannedTime6,
            this.PlannedManHour6,
            this.PlannedWorkPct6,
            this.PlannedManMoon6,
            this.TgtMonth1,
            this.TgtMonth2,
            this.TgtMonth3,
            this.TgtMonth4,
            this.TgtMonth5,
            this.TgtMonth6,
            this.EditedFlg});
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvWorkTime.DefaultCellStyle = dataGridViewCellStyle30;
            this.dgvWorkTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvWorkTime.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.dgvWorkTime.Location = new System.Drawing.Point(0, 110);
            this.dgvWorkTime.Name = "dgvWorkTime";
            this.dgvWorkTime.RowTemplate.Height = 21;
            this.dgvWorkTime.Size = new System.Drawing.Size(1334, 587);
            this.dgvWorkTime.TabIndex = 30;
            // 
            // UserId
            // 
            this.UserId.DataPropertyName = "UserId";
            this.UserId.Frozen = true;
            this.UserId.HeaderText = "ユーザID";
            this.UserId.Name = "UserId";
            this.UserId.ReadOnly = true;
            this.UserId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.UserId.Visible = false;
            // 
            // ProjectId
            // 
            this.ProjectId.DataPropertyName = "ProjectId";
            this.ProjectId.Frozen = true;
            this.ProjectId.HeaderText = "プロジェクトID";
            this.ProjectId.Name = "ProjectId";
            this.ProjectId.ReadOnly = true;
            this.ProjectId.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ProjectId.Visible = false;
            // 
            // EmploeeName
            // 
            this.EmploeeName.DataPropertyName = "EmploeeName";
            this.EmploeeName.Frozen = true;
            this.EmploeeName.HeaderText = "社員名";
            this.EmploeeName.Name = "EmploeeName";
            this.EmploeeName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EmploeeName.Width = 110;
            // 
            // ChargeNo
            // 
            this.ChargeNo.DataPropertyName = "ChargeNo";
            this.ChargeNo.Frozen = true;
            this.ChargeNo.HeaderText = "チャージ№";
            this.ChargeNo.Name = "ChargeNo";
            this.ChargeNo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ChargeNo.Width = 130;
            // 
            // WorkName
            // 
            this.WorkName.DataPropertyName = "WorkName";
            this.WorkName.Frozen = true;
            this.WorkName.HeaderText = "作業名";
            this.WorkName.Name = "WorkName";
            this.WorkName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.WorkName.Width = 400;
            // 
            // ActualTime
            // 
            this.ActualTime.DataPropertyName = "ActualTime";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ActualTime.DefaultCellStyle = dataGridViewCellStyle2;
            this.ActualTime.HeaderText = "当月実績（168h）";
            this.ActualTime.Name = "ActualTime";
            this.ActualTime.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ActualTime.Width = 80;
            // 
            // ActualManHour
            // 
            this.ActualManHour.DataPropertyName = "ActualManHour";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ActualManHour.DefaultCellStyle = dataGridViewCellStyle3;
            this.ActualManHour.HeaderText = "工数";
            this.ActualManHour.Name = "ActualManHour";
            this.ActualManHour.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ActualManHour.Width = 80;
            // 
            // ActualWorkPct
            // 
            this.ActualWorkPct.DataPropertyName = "ActualWorkPct";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ActualWorkPct.DefaultCellStyle = dataGridViewCellStyle4;
            this.ActualWorkPct.HeaderText = "割合";
            this.ActualWorkPct.Name = "ActualWorkPct";
            this.ActualWorkPct.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ActualWorkPct.Width = 65;
            // 
            // ActualManMoon
            // 
            this.ActualManMoon.DataPropertyName = "ActualManMoon";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.ActualManMoon.DefaultCellStyle = dataGridViewCellStyle5;
            this.ActualManMoon.HeaderText = "人月";
            this.ActualManMoon.Name = "ActualManMoon";
            this.ActualManMoon.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ActualManMoon.Visible = false;
            this.ActualManMoon.Width = 65;
            // 
            // PlannedTime1
            // 
            this.PlannedTime1.DataPropertyName = "PlannedTime1";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedTime1.DefaultCellStyle = dataGridViewCellStyle6;
            this.PlannedTime1.HeaderText = "①月予定（168h）";
            this.PlannedTime1.Name = "PlannedTime1";
            this.PlannedTime1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedTime1.Width = 80;
            // 
            // PlannedManHour1
            // 
            this.PlannedManHour1.DataPropertyName = "PlannedManHour1";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManHour1.DefaultCellStyle = dataGridViewCellStyle7;
            this.PlannedManHour1.HeaderText = "工数";
            this.PlannedManHour1.Name = "PlannedManHour1";
            this.PlannedManHour1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManHour1.Width = 80;
            // 
            // PlannedWorkPct1
            // 
            this.PlannedWorkPct1.DataPropertyName = "PlannedWorkPct1";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedWorkPct1.DefaultCellStyle = dataGridViewCellStyle8;
            this.PlannedWorkPct1.HeaderText = "割合";
            this.PlannedWorkPct1.Name = "PlannedWorkPct1";
            this.PlannedWorkPct1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedWorkPct1.Width = 65;
            // 
            // PlannedManMoon1
            // 
            this.PlannedManMoon1.DataPropertyName = "PlannedManMoon1";
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManMoon1.DefaultCellStyle = dataGridViewCellStyle9;
            this.PlannedManMoon1.HeaderText = "人月";
            this.PlannedManMoon1.Name = "PlannedManMoon1";
            this.PlannedManMoon1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManMoon1.Visible = false;
            this.PlannedManMoon1.Width = 65;
            // 
            // PlannedTime2
            // 
            this.PlannedTime2.DataPropertyName = "PlannedTime2";
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedTime2.DefaultCellStyle = dataGridViewCellStyle10;
            this.PlannedTime2.HeaderText = "②月予定（168h）";
            this.PlannedTime2.Name = "PlannedTime2";
            this.PlannedTime2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedTime2.Width = 80;
            // 
            // PlannedManHour2
            // 
            this.PlannedManHour2.DataPropertyName = "PlannedManHour2";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManHour2.DefaultCellStyle = dataGridViewCellStyle11;
            this.PlannedManHour2.HeaderText = "工数";
            this.PlannedManHour2.Name = "PlannedManHour2";
            this.PlannedManHour2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManHour2.Width = 80;
            // 
            // PlannedWorkPct2
            // 
            this.PlannedWorkPct2.DataPropertyName = "PlannedWorkPct2";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedWorkPct2.DefaultCellStyle = dataGridViewCellStyle12;
            this.PlannedWorkPct2.HeaderText = "割合";
            this.PlannedWorkPct2.Name = "PlannedWorkPct2";
            this.PlannedWorkPct2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedWorkPct2.Width = 65;
            // 
            // PlannedManMoon2
            // 
            this.PlannedManMoon2.DataPropertyName = "PlannedManMoon2";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManMoon2.DefaultCellStyle = dataGridViewCellStyle13;
            this.PlannedManMoon2.HeaderText = "人月";
            this.PlannedManMoon2.Name = "PlannedManMoon2";
            this.PlannedManMoon2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManMoon2.Visible = false;
            this.PlannedManMoon2.Width = 65;
            // 
            // PlannedTime3
            // 
            this.PlannedTime3.DataPropertyName = "PlannedTime3";
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedTime3.DefaultCellStyle = dataGridViewCellStyle14;
            this.PlannedTime3.HeaderText = "③月予定（168h）";
            this.PlannedTime3.Name = "PlannedTime3";
            this.PlannedTime3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedTime3.Width = 80;
            // 
            // PlannedManHour3
            // 
            this.PlannedManHour3.DataPropertyName = "PlannedManHour3";
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManHour3.DefaultCellStyle = dataGridViewCellStyle15;
            this.PlannedManHour3.HeaderText = "工数";
            this.PlannedManHour3.Name = "PlannedManHour3";
            this.PlannedManHour3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManHour3.Width = 80;
            // 
            // PlannedWorkPct3
            // 
            this.PlannedWorkPct3.DataPropertyName = "PlannedWorkPct3";
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedWorkPct3.DefaultCellStyle = dataGridViewCellStyle16;
            this.PlannedWorkPct3.HeaderText = "割合";
            this.PlannedWorkPct3.Name = "PlannedWorkPct3";
            this.PlannedWorkPct3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedWorkPct3.Width = 65;
            // 
            // PlannedManMoon3
            // 
            this.PlannedManMoon3.DataPropertyName = "PlannedManMoon3";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManMoon3.DefaultCellStyle = dataGridViewCellStyle17;
            this.PlannedManMoon3.HeaderText = "人月";
            this.PlannedManMoon3.Name = "PlannedManMoon3";
            this.PlannedManMoon3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManMoon3.Visible = false;
            this.PlannedManMoon3.Width = 65;
            // 
            // PlannedTime4
            // 
            this.PlannedTime4.DataPropertyName = "PlannedTime4";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedTime4.DefaultCellStyle = dataGridViewCellStyle18;
            this.PlannedTime4.HeaderText = "④月予定（168h）";
            this.PlannedTime4.Name = "PlannedTime4";
            this.PlannedTime4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedTime4.Width = 80;
            // 
            // PlannedManHour4
            // 
            this.PlannedManHour4.DataPropertyName = "PlannedManHour4";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManHour4.DefaultCellStyle = dataGridViewCellStyle19;
            this.PlannedManHour4.HeaderText = "工数";
            this.PlannedManHour4.Name = "PlannedManHour4";
            this.PlannedManHour4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManHour4.Width = 80;
            // 
            // PlannedWorkPct4
            // 
            this.PlannedWorkPct4.DataPropertyName = "PlannedWorkPct4";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedWorkPct4.DefaultCellStyle = dataGridViewCellStyle20;
            this.PlannedWorkPct4.HeaderText = "割合";
            this.PlannedWorkPct4.Name = "PlannedWorkPct4";
            this.PlannedWorkPct4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedWorkPct4.Width = 65;
            // 
            // PlannedManMoon4
            // 
            this.PlannedManMoon4.DataPropertyName = "PlannedManMoon4";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManMoon4.DefaultCellStyle = dataGridViewCellStyle21;
            this.PlannedManMoon4.HeaderText = "人月";
            this.PlannedManMoon4.Name = "PlannedManMoon4";
            this.PlannedManMoon4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManMoon4.Visible = false;
            this.PlannedManMoon4.Width = 65;
            // 
            // PlannedTime5
            // 
            this.PlannedTime5.DataPropertyName = "PlannedTime5";
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedTime5.DefaultCellStyle = dataGridViewCellStyle22;
            this.PlannedTime5.HeaderText = "⑤月予定（168h）";
            this.PlannedTime5.Name = "PlannedTime5";
            this.PlannedTime5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedTime5.Width = 80;
            // 
            // PlannedManHour5
            // 
            this.PlannedManHour5.DataPropertyName = "PlannedManHour5";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManHour5.DefaultCellStyle = dataGridViewCellStyle23;
            this.PlannedManHour5.HeaderText = "工数";
            this.PlannedManHour5.Name = "PlannedManHour5";
            this.PlannedManHour5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManHour5.Width = 80;
            // 
            // PlannedWorkPct5
            // 
            this.PlannedWorkPct5.DataPropertyName = "PlannedWorkPct5";
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedWorkPct5.DefaultCellStyle = dataGridViewCellStyle24;
            this.PlannedWorkPct5.HeaderText = "割合";
            this.PlannedWorkPct5.Name = "PlannedWorkPct5";
            this.PlannedWorkPct5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedWorkPct5.Width = 65;
            // 
            // PlannedManMoon5
            // 
            this.PlannedManMoon5.DataPropertyName = "PlannedManMoon5";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManMoon5.DefaultCellStyle = dataGridViewCellStyle25;
            this.PlannedManMoon5.HeaderText = "人月";
            this.PlannedManMoon5.Name = "PlannedManMoon5";
            this.PlannedManMoon5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManMoon5.Visible = false;
            this.PlannedManMoon5.Width = 65;
            // 
            // PlannedTime6
            // 
            this.PlannedTime6.DataPropertyName = "PlannedTime6";
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedTime6.DefaultCellStyle = dataGridViewCellStyle26;
            this.PlannedTime6.HeaderText = "⑥月予定（168h）";
            this.PlannedTime6.Name = "PlannedTime6";
            this.PlannedTime6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedTime6.Width = 80;
            // 
            // PlannedManHour6
            // 
            this.PlannedManHour6.DataPropertyName = "PlannedManHour6";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManHour6.DefaultCellStyle = dataGridViewCellStyle27;
            this.PlannedManHour6.HeaderText = "工数";
            this.PlannedManHour6.Name = "PlannedManHour6";
            this.PlannedManHour6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManHour6.Width = 80;
            // 
            // PlannedWorkPct6
            // 
            this.PlannedWorkPct6.DataPropertyName = "PlannedWorkPct6";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedWorkPct6.DefaultCellStyle = dataGridViewCellStyle28;
            this.PlannedWorkPct6.HeaderText = "割合";
            this.PlannedWorkPct6.Name = "PlannedWorkPct6";
            this.PlannedWorkPct6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedWorkPct6.Width = 65;
            // 
            // PlannedManMoon6
            // 
            this.PlannedManMoon6.DataPropertyName = "PlannedManMoon6";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PlannedManMoon6.DefaultCellStyle = dataGridViewCellStyle29;
            this.PlannedManMoon6.HeaderText = "人月";
            this.PlannedManMoon6.Name = "PlannedManMoon6";
            this.PlannedManMoon6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PlannedManMoon6.Visible = false;
            this.PlannedManMoon6.Width = 65;
            // 
            // TgtMonth1
            // 
            this.TgtMonth1.DataPropertyName = "TgtMonth1";
            this.TgtMonth1.HeaderText = "TgtMonth1";
            this.TgtMonth1.Name = "TgtMonth1";
            this.TgtMonth1.ReadOnly = true;
            this.TgtMonth1.Visible = false;
            // 
            // TgtMonth2
            // 
            this.TgtMonth2.DataPropertyName = "TgtMonth2";
            this.TgtMonth2.HeaderText = "TgtMonth2";
            this.TgtMonth2.Name = "TgtMonth2";
            this.TgtMonth2.ReadOnly = true;
            this.TgtMonth2.Visible = false;
            // 
            // TgtMonth3
            // 
            this.TgtMonth3.DataPropertyName = "TgtMonth3";
            this.TgtMonth3.HeaderText = "TgtMonth3";
            this.TgtMonth3.Name = "TgtMonth3";
            this.TgtMonth3.ReadOnly = true;
            this.TgtMonth3.Visible = false;
            // 
            // TgtMonth4
            // 
            this.TgtMonth4.DataPropertyName = "TgtMonth4";
            this.TgtMonth4.HeaderText = "TgtMonth4";
            this.TgtMonth4.Name = "TgtMonth4";
            this.TgtMonth4.ReadOnly = true;
            this.TgtMonth4.Visible = false;
            // 
            // TgtMonth5
            // 
            this.TgtMonth5.DataPropertyName = "TgtMonth5";
            this.TgtMonth5.HeaderText = "TgtMonth5";
            this.TgtMonth5.Name = "TgtMonth5";
            this.TgtMonth5.ReadOnly = true;
            this.TgtMonth5.Visible = false;
            // 
            // TgtMonth6
            // 
            this.TgtMonth6.DataPropertyName = "TgtMonth6";
            this.TgtMonth6.HeaderText = "TgtMonth6";
            this.TgtMonth6.Name = "TgtMonth6";
            this.TgtMonth6.ReadOnly = true;
            this.TgtMonth6.Visible = false;
            // 
            // EditedFlg
            // 
            this.EditedFlg.DataPropertyName = "EditedFlg";
            this.EditedFlg.HeaderText = "更新フラグ";
            this.EditedFlg.Name = "EditedFlg";
            this.EditedFlg.ReadOnly = true;
            this.EditedFlg.Visible = false;
            // 
            // panel100
            // 
            this.panel100.Controls.Add(this.txtTougetsu);
            this.panel100.Controls.Add(this.label9);
            this.panel100.Controls.Add(this.txtDayBunshi);
            this.panel100.Controls.Add(this.txtHyojunTimeBunbo);
            this.panel100.Controls.Add(this.label8);
            this.panel100.Controls.Add(this.txtHyojunTimeBunshi);
            this.panel100.Controls.Add(this.txtDayBunbo);
            this.panel100.Controls.Add(this.label7);
            this.panel100.Controls.Add(this.label6);
            this.panel100.Controls.Add(this.label5);
            this.panel100.Controls.Add(this.cmbTTGroup);
            this.panel100.Controls.Add(this.rdo6Month);
            this.panel100.Controls.Add(this.rdo3Month);
            this.panel100.Controls.Add(this.rdo1Month);
            this.panel100.Controls.Add(this.label4);
            this.panel100.Controls.Add(this.panel6);
            this.panel100.Controls.Add(this.cmbSumEndDate);
            this.panel100.Controls.Add(this.label3);
            this.panel100.Controls.Add(this.cmbSumSatrtDate);
            this.panel100.Controls.Add(this.label1);
            this.panel100.Controls.Add(this.label2);
            this.panel100.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel100.Location = new System.Drawing.Point(0, 0);
            this.panel100.Name = "panel100";
            this.panel100.Size = new System.Drawing.Size(1334, 110);
            this.panel100.TabIndex = 0;
            // 
            // txtTougetsu
            // 
            this.txtTougetsu.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtTougetsu.Location = new System.Drawing.Point(674, 17);
            this.txtTougetsu.Name = "txtTougetsu";
            this.txtTougetsu.ReadOnly = true;
            this.txtTougetsu.Size = new System.Drawing.Size(141, 23);
            this.txtTougetsu.TabIndex = 7;
            this.txtTougetsu.TabStop = false;
            this.txtTougetsu.Text = "2020年07月";
            this.txtTougetsu.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label9.Location = new System.Drawing.Point(557, 20);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(72, 16);
            this.label9.TabIndex = 33;
            this.label9.Text = "集計初月";
            // 
            // txtDayBunshi
            // 
            this.txtDayBunshi.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayBunshi.Location = new System.Drawing.Point(674, 48);
            this.txtDayBunshi.Name = "txtDayBunshi";
            this.txtDayBunshi.ReadOnly = true;
            this.txtDayBunshi.Size = new System.Drawing.Size(53, 23);
            this.txtDayBunshi.TabIndex = 7;
            this.txtDayBunshi.TabStop = false;
            this.txtDayBunshi.Text = "14";
            this.txtDayBunshi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtHyojunTimeBunbo
            // 
            this.txtHyojunTimeBunbo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHyojunTimeBunbo.Location = new System.Drawing.Point(762, 77);
            this.txtHyojunTimeBunbo.Name = "txtHyojunTimeBunbo";
            this.txtHyojunTimeBunbo.ReadOnly = true;
            this.txtHyojunTimeBunbo.Size = new System.Drawing.Size(53, 23);
            this.txtHyojunTimeBunbo.TabIndex = 10;
            this.txtHyojunTimeBunbo.TabStop = false;
            this.txtHyojunTimeBunbo.Text = "168h";
            this.txtHyojunTimeBunbo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label8.Location = new System.Drawing.Point(733, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 16);
            this.label8.TabIndex = 31;
            this.label8.Text = "／";
            // 
            // txtHyojunTimeBunshi
            // 
            this.txtHyojunTimeBunshi.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtHyojunTimeBunshi.Location = new System.Drawing.Point(674, 77);
            this.txtHyojunTimeBunshi.Name = "txtHyojunTimeBunshi";
            this.txtHyojunTimeBunshi.ReadOnly = true;
            this.txtHyojunTimeBunshi.Size = new System.Drawing.Size(53, 23);
            this.txtHyojunTimeBunshi.TabIndex = 9;
            this.txtHyojunTimeBunshi.TabStop = false;
            this.txtHyojunTimeBunshi.Text = "112h";
            this.txtHyojunTimeBunshi.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtDayBunbo
            // 
            this.txtDayBunbo.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.txtDayBunbo.Location = new System.Drawing.Point(762, 48);
            this.txtDayBunbo.Name = "txtDayBunbo";
            this.txtDayBunbo.ReadOnly = true;
            this.txtDayBunbo.Size = new System.Drawing.Size(53, 23);
            this.txtDayBunbo.TabIndex = 8;
            this.txtDayBunbo.TabStop = false;
            this.txtDayBunbo.Text = "21";
            this.txtDayBunbo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label7.Location = new System.Drawing.Point(733, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 16);
            this.label7.TabIndex = 28;
            this.label7.Text = "／";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(557, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 16);
            this.label6.TabIndex = 26;
            this.label6.Text = "標準稼働時間";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(557, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(72, 16);
            this.label5.TabIndex = 25;
            this.label5.Text = "稼働日数";
            // 
            // cmbTTGroup
            // 
            this.cmbTTGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTTGroup.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.cmbTTGroup.FormattingEnabled = true;
            this.cmbTTGroup.Location = new System.Drawing.Point(132, 17);
            this.cmbTTGroup.Name = "cmbTTGroup";
            this.cmbTTGroup.Size = new System.Drawing.Size(400, 24);
            this.cmbTTGroup.TabIndex = 0;
            // 
            // rdo6Month
            // 
            this.rdo6Month.AutoSize = true;
            this.rdo6Month.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdo6Month.Location = new System.Drawing.Point(271, 49);
            this.rdo6Month.Name = "rdo6Month";
            this.rdo6Month.Size = new System.Drawing.Size(61, 20);
            this.rdo6Month.TabIndex = 4;
            this.rdo6Month.Text = "6ヶ月";
            this.rdo6Month.UseVisualStyleBackColor = true;
            // 
            // rdo3Month
            // 
            this.rdo3Month.AutoSize = true;
            this.rdo3Month.Checked = true;
            this.rdo3Month.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdo3Month.Location = new System.Drawing.Point(204, 49);
            this.rdo3Month.Name = "rdo3Month";
            this.rdo3Month.Size = new System.Drawing.Size(61, 20);
            this.rdo3Month.TabIndex = 3;
            this.rdo3Month.TabStop = true;
            this.rdo3Month.Text = "3ヶ月";
            this.rdo3Month.UseVisualStyleBackColor = true;
            // 
            // rdo1Month
            // 
            this.rdo1Month.AutoSize = true;
            this.rdo1Month.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.rdo1Month.Location = new System.Drawing.Point(137, 49);
            this.rdo1Month.Name = "rdo1Month";
            this.rdo1Month.Size = new System.Drawing.Size(61, 20);
            this.rdo1Month.TabIndex = 2;
            this.rdo1Month.Text = "1ヶ月";
            this.rdo1Month.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label4.Location = new System.Drawing.Point(27, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 24;
            this.label4.Text = "集計単位";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnSearch);
            this.panel6.Controls.Add(this.btnExcelOutput);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel6.Location = new System.Drawing.Point(1058, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(276, 110);
            this.panel6.TabIndex = 20;
            // 
            // btnSearch
            // 
            this.btnSearch.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnSearch.Location = new System.Drawing.Point(141, 62);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(123, 40);
            this.btnSearch.TabIndex = 21;
            this.btnSearch.Text = "検索（S）";
            this.btnSearch.UseVisualStyleBackColor = true;
            // 
            // btnExcelOutput
            // 
            this.btnExcelOutput.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnExcelOutput.Location = new System.Drawing.Point(12, 62);
            this.btnExcelOutput.Name = "btnExcelOutput";
            this.btnExcelOutput.Size = new System.Drawing.Size(123, 40);
            this.btnExcelOutput.TabIndex = 20;
            this.btnExcelOutput.Text = "Excel出力（E）";
            this.btnExcelOutput.UseVisualStyleBackColor = true;
            // 
            // cmbSumEndDate
            // 
            this.cmbSumEndDate.Enabled = false;
            this.cmbSumEndDate.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.cmbSumEndDate.FormattingEnabled = true;
            this.cmbSumEndDate.Location = new System.Drawing.Point(271, 78);
            this.cmbSumEndDate.Name = "cmbSumEndDate";
            this.cmbSumEndDate.Size = new System.Drawing.Size(103, 24);
            this.cmbSumEndDate.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label3.Location = new System.Drawing.Point(241, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 16);
            this.label3.TabIndex = 21;
            this.label3.Text = "～";
            // 
            // cmbSumSatrtDate
            // 
            this.cmbSumSatrtDate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSumSatrtDate.Font = new System.Drawing.Font("MS UI Gothic", 12F);
            this.cmbSumSatrtDate.FormattingEnabled = true;
            this.cmbSumSatrtDate.Location = new System.Drawing.Point(132, 78);
            this.cmbSumSatrtDate.Name = "cmbSumSatrtDate";
            this.cmbSumSatrtDate.Size = new System.Drawing.Size(103, 24);
            this.cmbSumSatrtDate.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(27, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 16);
            this.label1.TabIndex = 17;
            this.label1.Text = "所属グループ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(27, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 16);
            this.label2.TabIndex = 18;
            this.label2.Text = "集計月";
            // 
            // TEF02000_WTAnalysisExcelBody
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1330, 757);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1350, 800);
            this.MinimumSize = new System.Drawing.Size(1350, 800);
            this.Name = "TEF02000_WTAnalysisExcelBody";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "計画工数編集";
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvWorkTime)).EndInit();
            this.panel100.ResumeLayout(false);
            this.panel100.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Button btnClear;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel100;
        public System.Windows.Forms.Panel panel6;
        public System.Windows.Forms.Button btnExcelOutput;
        public System.Windows.Forms.ComboBox cmbSumEndDate;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox cmbSumSatrtDate;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.RadioButton rdo6Month;
        public System.Windows.Forms.RadioButton rdo3Month;
        public System.Windows.Forms.RadioButton rdo1Month;
        public System.Windows.Forms.Button btnSearch;
        public System.Windows.Forms.ComboBox cmbTTGroup;
        public System.Windows.Forms.Button btnUpdate;
        public System.Windows.Forms.TextBox txtHyojunTimeBunbo;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox txtHyojunTimeBunshi;
        public System.Windows.Forms.TextBox txtDayBunbo;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox txtDayBunshi;
        public System.Windows.Forms.TextBox txtTougetsu;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.DataGridView dgvWorkTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn UserId;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProjectId;
        private System.Windows.Forms.DataGridViewTextBoxColumn EmploeeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ChargeNo;
        private System.Windows.Forms.DataGridViewTextBoxColumn WorkName;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualManHour;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualWorkPct;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActualManMoon;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedTime1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManHour1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedWorkPct1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManMoon1;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedTime2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManHour2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedWorkPct2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManMoon2;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedTime3;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManHour3;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedWorkPct3;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManMoon3;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedTime4;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManHour4;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedWorkPct4;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManMoon4;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedTime5;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManHour5;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedWorkPct5;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManMoon5;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedTime6;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManHour6;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedWorkPct6;
        private System.Windows.Forms.DataGridViewTextBoxColumn PlannedManMoon6;
        private System.Windows.Forms.DataGridViewTextBoxColumn TgtMonth1;
        private System.Windows.Forms.DataGridViewTextBoxColumn TgtMonth2;
        private System.Windows.Forms.DataGridViewTextBoxColumn TgtMonth3;
        private System.Windows.Forms.DataGridViewTextBoxColumn TgtMonth4;
        private System.Windows.Forms.DataGridViewTextBoxColumn TgtMonth5;
        private System.Windows.Forms.DataGridViewTextBoxColumn TgtMonth6;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EditedFlg;
    }
}

