﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using TimeEntryClient.Const;
using TimeEntryClient.Dto;
using TimeEntryShared.Const;
using TimeEntryShared.Dto;
using TimeEntryShared.Entity;
using TimeEntryShared.Service;
using TimeEntryShared.Util;
using TimeEntryClient.Forms.Dialog;
using TimeEntryClient.Util;

namespace TimeEntryClient.Forms {
    public class TEF02000_WTAnalysisExcelFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private TEF02000_WTAnalysisExcelBody _Body;
        /// <summary>
        /// Config情報
        /// </summary>
        private TimeEntryConfigEntity _ConfigInfo = new TimeEntryConfigEntity();
        /// <summary>
        /// TimeTracker出力情報
        /// </summary>
        private List<UserInfoDto> _TimeTrackerUserInfos = new List<UserInfoDto>();
        /// <summary>
        /// 集計月コンボDataList
        /// </summary>
        List<string> _CmbDataList = new List<string>();
        /// <summary>
        /// GridViewModel
        /// </summary>
        private BindingList<WTAnalysisGridDto> _GridVm = new BindingList<WTAnalysisGridDto>();

        private const int _COL_IDX_WORK_TIME_START = 5;
        private const int _COL_BUF_WORK_TIME = 4;

        private string _SAVE_CELL_VALUE = string.Empty;
        private bool _GRID_SHOWED = false;
        private const string _NEN_PREFIX = "20";
        private const string _MONTH_TXT = "月";
        private const int _HALF_DAY_OF_MONTH = 15;

        /// <summary>
        /// ラジオボタンステータス（集計単位）
        /// </summary>
        private enum RdoSumUnit {
            ONE_MONTH = 1,
            THREE_MONTH = 3,
            SIX_MONTH = 6,
        }

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        public TEF02000_WTAnalysisExcelFormLogic(TEF02000_WTAnalysisExcelBody body) {
            // 初期化
            _Body = body;
            Init(true);
            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.E:
                        _Body.btnExcelOutput.Focus();
                        _Body.btnExcelOutput.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };
            // Excel作成ボタン押下
            _Body.btnExcelOutput.Click += (s, e) => {
                ClickBtnExcelCreate();
            };
            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                ClickBtnSearch();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var result = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLEAR);
                if (result == DialogResult.OK) {
                    Init();
                }
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                UpdatePlannedTime();
            };
            // 集計単位Click
            _Body.rdo1Month.Click += (s, e) => {
                SelectedSumStartDateCombo(_Body.cmbSumSatrtDate.Text);
                InitGridColH();
            };
            _Body.rdo3Month.Click += (s, e) => {
                SelectedSumStartDateCombo(_Body.cmbSumSatrtDate.Text);
                InitGridColH();
            };
            _Body.rdo6Month.Click += (s, e) => {
                SelectedSumStartDateCombo(_Body.cmbSumSatrtDate.Text);
                InitGridColH();
            };
            // 集計月（開始）SelectionChangeCommitted
            _Body.cmbSumSatrtDate.SelectionChangeCommitted += (s, e) => {
                var tgtSumStartDate = _Body.cmbSumSatrtDate.SelectedItem.ToString();
                var dtSumStartDate = DateTime.Parse(tgtSumStartDate);
                dtSumStartDate = new DateTime(dtSumStartDate.Year, dtSumStartDate.Month, 1);
                int sumUnit = GetSumUnit();
                var dtSumEndDate = dtSumStartDate.AddMonths(sumUnit).AddDays(-1);
                _Body.cmbSumEndDate.Text = dtSumEndDate.ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI);
                InitGridColH();
                InitHyojun();
            };
            // グリッド行番号描画
            _Body.dgvWorkTime.RowPostPaint += (s, e) => {
                GridRowUtil<MstEmploeeDto>.SetRowNum(s, e);
            };
            // グリッドCellEnter
            _Body.dgvWorkTime.CellEnter += (s, e) => {
                int colIdx = e.ColumnIndex;
                int rowIdx = e.RowIndex;
                if (colIdx < 0 || rowIdx < 0) {
                    return;
                }
                if (!_Body.dgvWorkTime.Columns[colIdx].HeaderText.Contains(WtAnalysisConst._HEADER_COL_PLANNED)) {
                    return;
                }
                _SAVE_CELL_VALUE = _Body.dgvWorkTime.Rows[rowIdx].Cells[colIdx].Value.ToString();
            };
            // グリッドCellValueChanged
            _Body.dgvWorkTime.CellValueChanged += GridCellValueChange;
        }
        /// <summary>
        /// 初期化処理
        /// </summary>
        /// <param name="isFirstInit"></param>
        private void Init(bool isFirstInit = false) {
            // TimeTrackerユーザ情報ロード
            _ConfigInfo = ModelUtil.LoadConfig();
            try {
                // グリッド初期化
                _Body.dgvWorkTime.Rows.Clear();

                // 従業員情報ロード
                GetMstGridDatasProxy(isFirstInit);

                // コントロール初期化
                _Body.cmbTTGroup.DataSource = _CmbDataList;
                InitSumStartDateCombo();
                if (!isFirstInit) {
                    _Body.rdo3Month.Select();
                }
                // グリッド列初期化
                InitGridColH();
                _Body.dgvWorkTime.AutoGenerateColumns = false;
                InitHyojun();

                // フォーカス
                _Body.cmbTTGroup.Focus();

            } catch (Exception ex) {
                MessageDialogUtil.ShowErroMsg(ex);
            }
            _GRID_SHOWED = false;
            _SAVE_CELL_VALUE = string.Empty;
        }
        /// <summary>
        /// マスタ情報取得（Proxy）
        /// </summary>
        /// <param name="isFirstInit"></param>
        private void GetMstGridDatasProxy(bool isFirstInit) {
            if (isFirstInit) {
                GetMstGridDatas();
                return;
            }
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => GetMstGridDatas()));
                prgBar.ShowDialog();
            }
        }
        /// <summary>
        /// マスタ情報取得
        /// </summary>
        private void GetMstGridDatas() {
            // TimeTrackerユーザ情報
            _TimeTrackerUserInfos = TimeTrackerUtil.LoadTimeTrackerUserInfo();

            // 集計月コンボデータソース
            _CmbDataList = new List<string>();
            _CmbDataList.Add(string.Empty);
            foreach (var ttinfo in _TimeTrackerUserInfos) {
                if (!_CmbDataList.Contains(ttinfo.OrganizationName)) {
                    _CmbDataList.Add(ttinfo.OrganizationName);
                }
            }
            _CmbDataList.Sort();
        }
        /// <summary>
        /// 集計月コンボボックス初期化
        /// </summary>
        private void InitSumStartDateCombo() {
            _Body.cmbSumSatrtDate.Items.Clear();
            _Body.cmbSumSatrtDate.Text = string.Empty;
            _Body.cmbSumEndDate.Text = string.Empty;
            DateTime dtSumStart = DateTime.Parse(_ConfigInfo.SumStartDate).AddMonths(-6);
            DateTime dtSumEnd = DateTime.Parse(_ConfigInfo.SumStartDate).AddMonths(7).AddDays(-1);
            for (DateTime dt = dtSumStart; dt < dtSumEnd; dt = dt.AddMonths(1)) {
                _Body.cmbSumSatrtDate.Items.Add(dt.ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI));
            }
            SelectedSumStartDateCombo(_ConfigInfo.SumStartDate);
        }
        /// <summary>
        /// 集計月コンボボックス選択イベント
        /// </summary>
        /// <param name="sumStartDatem"></param>
        private void SelectedSumStartDateCombo(string sumStartDatem) {
            _Body.cmbSumSatrtDate.Text = DateTime.Parse(sumStartDatem).ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI);
            int sumUnit = GetSumUnit();
            _Body.cmbSumEndDate.Text = DateTime.Parse(sumStartDatem).AddMonths(sumUnit).AddDays(-1).ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI);
        }
        /// <summary>
        /// 集計単位取得
        /// </summary>
        /// <returns></returns>
        private int GetSumUnit() {
            if (_Body.rdo1Month.Checked) {
                return (int)RdoSumUnit.ONE_MONTH;
            }
            if(_Body.rdo3Month.Checked){
                return (int)RdoSumUnit.THREE_MONTH;
            }
            return (int)RdoSumUnit.SIX_MONTH;
        }
        /// <summary>
        /// Excel作成ボタン押下処理
        /// </summary>
        private void ClickBtnExcelCreate() {
            // 保存ダイアログ表示
            var fbd = new FolderBrowserDialog();
            fbd.Description = SharedMessages._MSG_SELECT_FOLDER;
            fbd.RootFolder = Environment.SpecialFolder.Desktop;
            fbd.SelectedPath = WtAnalysisConst._PATH_DEFAULT_FOLDER_PATH;
            string outputPath = Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
            fbd.ShowNewFolderButton = true;
            if (fbd.ShowDialog() != DialogResult.OK) {
                return;
            }
            if (fbd.SelectedPath != WtAnalysisConst._PATH_DEFAULT_FOLDER_PATH) {
                outputPath = fbd.SelectedPath;
            }
            // 工数分析Excel作成
            var preCursor = Cursor.Current;
            Cursor.Current = Cursors.WaitCursor;
            string sumStartDate = DateTime.Parse(_Body.cmbSumSatrtDate.Text).ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH);
            string sumEndDate = DateTime.Parse(_Body.cmbSumEndDate.Text).AddMonths(1).AddDays(-1).ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH);
            string tgtTTGroupName = _Body.cmbTTGroup.Text;
            int recCount = 0;
            try {
                // Excel作成
                using (var prgBar = new ProgressDialog()) {
                    prgBar.Method = (new Action(() => CreateWTAnalysisExcel(sumStartDate, sumEndDate, outputPath, tgtTTGroupName, ref recCount)));
                    prgBar.ShowDialog();
                }
                if (recCount == 0) {
                    MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_EXIST_DATA);
                    return;
                }
                if (recCount == -1) {
                    MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_CREATE_EXCEL_FAILED);
                    return;
                }
                // 終了メッセージ表示
                var drFinish = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_FINISH_CREATE_ANALYSIS);
                if (drFinish != DialogResult.OK) {
                    Cursor.Current = preCursor;
                    return;
                }
                Cursor.Current = Cursors.WaitCursor;

                // 作成した工数分析Excelを開く
                string excelPath = WorkTimeAnalysisUtil.GetWTAnalysisExcel(DateTime.Parse(sumStartDate), DateTime.Parse(sumEndDate), outputPath);
                Process.Start(excelPath);
                Cursor.Current = preCursor;

            } catch (Exception e) {
                MessageDialogUtil.ShowErroMsg(e);
                return;
            }
        }
        /// <summary>
        /// 検索ボタン押下
        /// </summary>
        private void ClickBtnSearch() {
            _GRID_SHOWED = false;
            _SAVE_CELL_VALUE = string.Empty;

            // 行と列の初期化
            _Body.dgvWorkTime.Rows.Clear();
            InitGridColH();

            // 検索条件セット
            string sumStartDate = DateTime.Parse(_Body.cmbSumSatrtDate.Text).ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH);
            string sumEndDate = DateTime.Parse(_Body.cmbSumEndDate.Text).AddMonths(1).AddDays(-1).ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH);
            string tgtTTGroupName = _Body.cmbTTGroup.Text;
            try {
                // 検索
                using (var prgBar = new ProgressDialog()) {
                    prgBar.Method = (new Action(() => CreateGridDtoList(sumStartDate, sumEndDate, tgtTTGroupName)));
                    prgBar.ShowDialog();
                }
                if (_GridVm.Count == 0) {
                    MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_EXIST_DATA);
                    return;
                }
                // 検索結果をグリッドに紐づけ
                _Body.dgvWorkTime.DataSource = _GridVm;

                // グリッドセル編集
                EditGridCell_AfterSearch();

                // グリッド表示
                _Body.dgvWorkTime.Show();
                _Body.dgvWorkTime.Focus();

            } catch (Exception err) {
                MessageDialogUtil.ShowErroMsg(err);
                return;
            }
            _GRID_SHOWED = true;
        }
        /// <summary>
        /// グリッドセル編集（検索処理後）
        /// </summary>
        private void EditGridCell_AfterSearch() {
            // セル背景色・使用可否修正
            foreach (DataGridViewRow r in _Body.dgvWorkTime.Rows) {
                var dto = new WTAnalysisGridDto();
                if (r.Cells[nameof(dto.PlannedTime1)].Value == null) {
                    // 計画がない実績はハイフン
                    r.Cells[nameof(dto.ActualTime)].Value = CommonLiteral._LITERAL_HYPHEN;
                    r.Cells[nameof(dto.ActualManHour)].Value = CommonLiteral._LITERAL_HYPHEN;
                    r.Cells[nameof(dto.ActualWorkPct)].Value = CommonLiteral._LITERAL_HYPHEN;
                    r.Cells[nameof(dto.ActualManMoon)].Value = CommonLiteral._LITERAL_HYPHEN;
                }
                foreach (DataGridViewCell c in r.Cells) {
                    if (r.Cells[nameof(dto.WorkName)].Value.ToString() == WtAnalysisConst._SUM_VALUE) {

                        // 小計行の編集
                        r.Cells[nameof(dto.WorkName)].Style.Alignment = DataGridViewContentAlignment.MiddleRight;
                        c.Style.BackColor = Color.LightSlateGray;
                        c.Style.ForeColor = Color.White;
                        c.ReadOnly = true;
                        continue;
                    }
                    if (_COL_IDX_WORK_TIME_START < c.ColumnIndex
                     && (c.Value == null || string.IsNullOrEmpty(c.Value.ToString()))) {
                        // 計画がないセルは編集不可
                        c.Value = CommonLiteral._LITERAL_HYPHEN;
                        c.Style.BackColor = Color.LightGray;
                        c.ReadOnly = true;
                    }
                    if (!JudgeIsWorkTimeCol(c.ColumnIndex)) {
                        // 作業時間の列でなければ使用不可
                        c.Style.BackColor = Color.LightGray;
                        c.ReadOnly = true;
                    }
                }
            }
        }
        /// <summary>
        /// 作業時間の列か判定
        /// </summary>
        /// <param name="ColumnIndex"></param>
        /// <returns></returns>
        private bool JudgeIsWorkTimeCol(int ColumnIndex) {
            if (_COL_IDX_WORK_TIME_START + (_COL_BUF_WORK_TIME - 1) <= ColumnIndex
            && ColumnIndex % _COL_BUF_WORK_TIME == 1) {
                return true;
            }
            return false;
        }
        /// <summary>
        /// グリッドセル値変更時
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChange(object s, DataGridViewCellEventArgs e) {
            if (!_GRID_SHOWED) {
                return;
            }
            int colIdx = e.ColumnIndex;
            int rowIdx = e.RowIndex;
            if (colIdx < 0 || rowIdx < 0) {
                return;
            }
            if (!_Body.dgvWorkTime.Columns[colIdx].HeaderText.Contains(WtAnalysisConst._HEADER_COL_PLANNED)) {
                return;
            }
            // イベント一旦削除
            _Body.dgvWorkTime.CellValueChanged -= GridCellValueChange;

            // 入力値変換
            var currCell = _Body.dgvWorkTime.Rows[rowIdx].Cells[colIdx];
            string newVal = CommonUtil.ConvertCellValToHHMM(currCell.Value, _SAVE_CELL_VALUE);
            if (newVal != _SAVE_CELL_VALUE) {
                var dto = new WTAnalysisGridDto();
                currCell.Style.BackColor = Color.Yellow;

                // 更新フラグON
                _Body.dgvWorkTime.Rows[rowIdx].Cells[nameof(dto.EditedFlg)].Value = true;
            }
            // 編集セルの値更新
            currCell.Value = newVal;
            
            // 工数・割合・人月・合計行の更新
            UpdEditWorkTimes(rowIdx, colIdx, newVal);

            // イベント回復
            _Body.dgvWorkTime.CellValueChanged += GridCellValueChange;
        }
        /// <summary>
        /// 工数・割合・人月・合計行の更新
        /// </summary>
        /// <param name="rowIdx"></param>
        /// <param name="colIdx"></param>
        /// <param name="newVal"></param>
        private void UpdEditWorkTimes(int rowIdx, int colIdx, string newVal) {
            // 行インデックス取得
            DataGridViewRow r = _Body.dgvWorkTime.Rows[rowIdx];
            DataGridViewCell c = r.Cells[colIdx];
            string tgtUserId = _Body.dgvWorkTime.Rows[rowIdx].Cells[0].Value.ToString();
            int intMinIdx = 0;
            for (intMinIdx = rowIdx; 0 < intMinIdx; intMinIdx--) {
                if (tgtUserId != _Body.dgvWorkTime.Rows[intMinIdx].Cells[0].Value.ToString()) {
                    intMinIdx++;
                    break;
                }
            }
            int intMaxIdx = 0;
            for (intMaxIdx = rowIdx; intMaxIdx < _Body.dgvWorkTime.Rows.Count; intMaxIdx++) {
                if (tgtUserId != _Body.dgvWorkTime.Rows[intMaxIdx].Cells[0].Value.ToString()) {
                    intMaxIdx--;
                    break;
                }
            }
            if (_Body.dgvWorkTime.Rows.Count <= intMaxIdx) {
                intMaxIdx = _Body.dgvWorkTime.Rows.Count - 1;
            }
            // 編集行の作業時間取得
            decimal decWorkTime = CommonUtil.ConvertStrToWorkTimeMinutes(newVal);

            // 編集行の工数更新
            int hIdx = c.OwningColumn.HeaderText.IndexOf(WtAnalysisConst._HEADER_COL_WORK_TIME_H.ToCharArray()[0]);
            string strMonthWorkTime = c.OwningColumn.HeaderText.Substring(hIdx - 3, 3);
            int intMonthWorkTime = int.Parse(strMonthWorkTime);
            decimal decManHour = WorkTimeAnalysisUtil.ConvertWtToManHour(decWorkTime, intMonthWorkTime);
            r.Cells[colIdx + 1].Value = decManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);

            // 計画工数の合計時間取得
            decimal totalWorkTime = 0;
            for (int i = intMinIdx; i < intMaxIdx; i++) {
                string strWorkTime = _Body.dgvWorkTime.Rows[i].Cells[colIdx].Value.ToString();
                if (strWorkTime == CommonLiteral._LITERAL_HYPHEN) {
                    continue;
                }
                totalWorkTime += CommonUtil.ConvertStrToWorkTimeMinutes(strWorkTime);
            }
            // 割合・人月更新
            decimal decTotalManhour = WorkTimeAnalysisUtil.ConvertWtToManHour(totalWorkTime, intMonthWorkTime);
            for (int i = intMinIdx; i < intMaxIdx; i++) {
                string strWorkTime = _Body.dgvWorkTime.Rows[i].Cells[colIdx].Value.ToString();
                if (strWorkTime == CommonLiteral._LITERAL_HYPHEN) {
                    continue;
                }
                decimal rowWorkTime = CommonUtil.ConvertStrToWorkTimeMinutes(strWorkTime);
                decimal rowManHour = WorkTimeAnalysisUtil.ConvertWtToManHour(rowWorkTime, intMonthWorkTime);
                _Body.dgvWorkTime.Rows[i].Cells[colIdx + 2].Value = WorkTimeAnalysisUtil.CalcPct(rowManHour, decTotalManhour).ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                _Body.dgvWorkTime.Rows[i].Cells[colIdx + 3].Value = WorkTimeAnalysisUtil.CalcHumanMoon(rowManHour, decTotalManhour).ToString(FormatConst._FORMAT_FLOAT_2);
            }
            // 合計行更新
            decimal decTotalPct = 0;
            decimal decTotalManMoon = 0;
            for (int i = intMinIdx; i < intMaxIdx; i++) {
                string strWorkTime = _Body.dgvWorkTime.Rows[i].Cells[colIdx].Value.ToString();
                if (strWorkTime == CommonLiteral._LITERAL_HYPHEN) {
                    continue;
                }
                string strPct = _Body.dgvWorkTime.Rows[i].Cells[colIdx + 2].Value.ToString();
                decTotalPct += decimal.Parse(strPct.Substring(0, strPct.Length - 1));
                decTotalManMoon += decimal.Parse(_Body.dgvWorkTime.Rows[i].Cells[colIdx + 3].Value.ToString());
            }
            _Body.dgvWorkTime.Rows[intMaxIdx].Cells[colIdx].Value = CommonUtil.ConvertWorkTimeToHHMMColon(totalWorkTime);
            _Body.dgvWorkTime.Rows[intMaxIdx].Cells[colIdx + 1].Value = decTotalManhour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
            _Body.dgvWorkTime.Rows[intMaxIdx].Cells[colIdx + 2].Value = decTotalManMoon.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
            _Body.dgvWorkTime.Rows[intMaxIdx].Cells[colIdx + 3].Value = decTotalManMoon.ToString(FormatConst._FORMAT_FLOAT_2);
        }
        /// <summary>
        /// グリッドヘッダーテキスト初期化
        /// </summary>
        private void InitGridColH() {
            if (_Body.dgvWorkTime.Rows.Count != 0) {
                return;
            }
            var dtSumStart = DateTime.Parse(_Body.cmbSumSatrtDate.Text);
            var dtSumEnd = dtSumStart.AddMonths(5);
            var dicMonth = WorkTimeAnalysisUtil.GetWorkMonthTimeList(_ConfigInfo.DefaultKinmuhyoPath, dtSumStart, dtSumEnd);
            int dicIdx = 0;
            foreach (var dic in dicMonth) {
                int colIdxBuf = (dicIdx + 1) * _COL_BUF_WORK_TIME;
                if (dicIdx == 0) {
                    // 当月実績
                    _Body.dgvWorkTime.Columns[_COL_IDX_WORK_TIME_START].HeaderText = GetColHText(dic.Key, dic.Value, WtAnalysisConst._HEADER_COL_ACTUAL);
                }
                // 各月予定
                _Body.dgvWorkTime.Columns[_COL_IDX_WORK_TIME_START + colIdxBuf].HeaderText = GetColHText(dic.Key, dic.Value, WtAnalysisConst._HEADER_COL_PLANNED);
                dicIdx++;
            }
            //  工数欄の開始列インデックス取得
            int stratColIdx = GetStartColIdx();

            // 集計月に応じて表示列を調整
            for (int i = 2; i < _Body.dgvWorkTime.Columns.Count; i++) {
                _Body.dgvWorkTime.Columns[i].Visible = (i < stratColIdx);
            }
        }
        /// <summary>
        ///  工数欄の開始列インデックス取得
        /// </summary>
        /// <returns></returns>
        private int GetStartColIdx() {
            if (_Body.rdo1Month.Checked) {
                return _COL_IDX_WORK_TIME_START + (_COL_BUF_WORK_TIME * 2);
            }
            if (_Body.rdo3Month.Checked) {
                return _COL_IDX_WORK_TIME_START + (_COL_BUF_WORK_TIME * 4);
            }
            if (_Body.rdo6Month.Checked) {
                return _COL_IDX_WORK_TIME_START + (_COL_BUF_WORK_TIME * 7);
            }
            return -1;
        }
        /// <summary>
        /// 月間標準時刻・稼働日数初期化
        /// </summary>
        private void InitHyojun() {
            var dtSumStart = DateTime.Parse(_Body.cmbSumSatrtDate.Text);
            var dtSumEnd = dtSumStart.AddMonths(6);
            int tgtDay = TimeConst._MAX_DAY_COUNT_MONTH;
            if (DateTime.Today.Year == dtSumStart.Year && DateTime.Today.Month == dtSumStart.Month) {
                tgtDay = DateTime.Today.Day;
            }
            var dicMonth = WorkTimeAnalysisUtil.GetWorkMonthTimeList(_ConfigInfo.DefaultKinmuhyoPath, dtSumStart, dtSumEnd, TimeConst._MAX_DAY_COUNT_MONTH, false);
            var dicMonthToday = WorkTimeAnalysisUtil.GetWorkMonthTimeList(_ConfigInfo.DefaultKinmuhyoPath, dtSumStart, dtSumEnd, tgtDay, false);
            var tgtDic = dicMonth[dtSumStart];
            int intDayBunshi = 0;
            if (dtSumStart <= new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)) {
                intDayBunshi = dicMonthToday[dtSumStart];
            }
            _Body.txtTougetsu.Text = dtSumStart.ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI);
            _Body.txtDayBunshi.Text = ((int)(intDayBunshi / TimeConst._DEFAULT_WORK_TIME_PER_DAY)).ToString();
            _Body.txtDayBunbo.Text = ((int)(tgtDic / TimeConst._DEFAULT_WORK_TIME_PER_DAY)).ToString();
            _Body.txtHyojunTimeBunshi.Text = string.Concat(intDayBunshi.ToString(), WtAnalysisConst._HEADER_COL_WORK_TIME_H);
            _Body.txtHyojunTimeBunbo.Text = string.Concat(tgtDic.ToString(), WtAnalysisConst._HEADER_COL_WORK_TIME_H);
        }
        /// <summary>
        /// ヘッダーテキスト取得
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="workTimeMonth"></param>
        /// <param name="strType"></param>
        /// <returns></returns>
        private string GetColHText(DateTime dt, int workTimeMonth, string strType) {
            return dt.ToString(FormatConst._FORMAT_DATE_YYMM_NENTSUKI) + Environment.NewLine + strType + string.Format(WtAnalysisConst._HEADER_FORMAT_MONTH_WORK_TIME, workTimeMonth);
        }
        /// <summary>
        /// グリッドDto作成
        /// </summary>
        /// <param name="sumStartDate"></param>
        /// <param name="sumEndDate"></param>
        /// <param name="tgtTTGroupName"></param>
        private void CreateGridDtoList(string sumStartDate, string sumEndDate, string tgtTTGroupName) {
            var service = new WorkTimeAnalysisService();
            _GridVm = new BindingList<WTAnalysisGridDto>();
            _GridVm = service.GetWTAnalysisGridDto(_ConfigInfo, sumStartDate, sumEndDate, tgtTTGroupName);
        }
        /// <summary>
        /// 工数分析Excel作成
        /// </summary>
        /// <param name="sumStartDate"></param>
        /// <param name="sumEndDate"></param>
        /// <param name="outputPath"></param>
        /// <param name="tgtTTGroupName"></param>
        /// <param name="recCount"></param>
        /// <param name="isErr"></param>
        private void CreateWTAnalysisExcel(string sumStartDate, string sumEndDate, string outputPath, string tgtTTGroupName, ref int recCount) {
            var service = new WorkTimeAnalysisService();
            recCount = service.Main(_ConfigInfo, sumStartDate, sumEndDate, outputPath, tgtTTGroupName);
        }
        /// <summary>
        /// 計画工数更新
        /// </summary>
        private void UpdatePlannedTime() {
            var updDtoList = new List<UpdPlannedTimeDto>();
            var nameofDto = new WTAnalysisGridDto();

            // グリッド行の数だけループ
            foreach (DataGridViewRow r in _Body.dgvWorkTime.Rows) {

                // 編集フラグONの行だけ対象
                if (r.Cells[nameof(nameofDto.EditedFlg)].Value.ToString() != true.ToString()) {
                    continue;
                }
                // 対象行のユーザ情報取得
                string userNum = r.Cells[nameof(nameofDto.UserId)].Value.ToString();
                var tgtUserInfo = _TimeTrackerUserInfos.Where(n => n.UserId == userNum).FirstOrDefault();
                if (tgtUserInfo == null) {
                    continue;
                }
                // 更新されたセル（背景色が黄色）の分だけdto作成
                foreach (DataGridViewCell c in r.Cells) {
                    if (c.Style.BackColor != Color.Yellow) {
                        continue;
                    }
                    var dto = new UpdPlannedTimeDto();
                    dto.UserID = tgtUserInfo.LoginName;
                    dto.ProjectID = r.Cells[nameof(nameofDto.ProjectId)].Value.ToString();
                    dto.TgtYearMonth = DateTime.Parse(_NEN_PREFIX + r.Cells[c.ColumnIndex].OwningColumn.HeaderText.Substring(0, 6));
                    dto.UpdPlannedTime = CommonUtil.ConvertStrToWorkTimeMinutes(r.Cells[c.ColumnIndex].Value.ToString());
                    updDtoList.Add(dto);
                }
            }
            if (updDtoList.Count == 0) {
                // 対象データなし
                MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_EXIST_DATA);
                return;
            }
            // 確認ダイアログ
            var drConfirm = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_UPDATE);
            if (drConfirm != DialogResult.OK) {
                return;
            }
            // ダイアログ下で処理実行
            bool IsErr = false;
            using (var prgBar = new ProgressDialog()) {
                prgBar.Method = (new Action(() => UpdFunc(updDtoList, ref IsErr)));
                prgBar.ShowDialog();
            }
            // 行と列の初期化
            _Body.dgvWorkTime.Rows.Clear();
            InitGridColH();

            // エラーが発生した場合は処理終了
            if (IsErr) {
                return;
            }
            // メッセージ表示
            MessageDialogUtil.ShowInfolMsgOK(ClientMessages._MSG_FINISH_UPDATE);

            // 再検索
            ClickBtnSearch();
        }
        /// <summary>
        /// 更新処理
        /// </summary>
        /// <param name="updDtoList"></param>
        /// <param name="IsErr"></param>
        private void UpdFunc(List<UpdPlannedTimeDto> updDtoList, ref bool IsErr) {
            IsErr = false;
            try {
                // 更新用Dtoリスト編集
                updDtoList = EditUpdDto(updDtoList);

                if (updDtoList.Exists(n => string.IsNullOrEmpty(n.WorkItemID))) {

                    // ワークアイテムIDが取得できないオブジェクトが混入していた場合、エラーとする
                    var errDto = updDtoList.Where(n => string.IsNullOrEmpty(n.WorkItemID)).First();

                    // プロジェクトID
                    string errProjectId = StrUtil.TostringNullForbid(errDto.ProjectID);

                    // ユーザID
                    string errUser = StrUtil.TostringNullForbid(errDto.UserID);
                    var tgtUserInfo = _TimeTrackerUserInfos.Where(n => n.UserId == errDto.UserID).FirstOrDefault();
                    if (tgtUserInfo != null) {
                        errUser = tgtUserInfo.UserName;
                    }
                    // 対象月
                    string errYearMonth = errDto.TgtYearMonth.ToString(FormatConst._FORMAT_DATE_YYMM_NENTSUKI);

                    // メッサ―ジ
                    string msg = string.Format(ClientMessages._MSG_ERROR_WORK_ITEM_ID_FAILED, errProjectId, errUser, errYearMonth);
                    throw new Exception(msg);
                }
                // 計画工数更新
                foreach (var dto in updDtoList) {
                    BatUtil.ExecBat_UpdatePlannedTime(dto.WorkItemID, dto.UserID, dto.UpdPlannedTime);
                    var updRecDto = new UpdPlannedTimeRecEntity() {
                        ProjectId = dto.ProjectID,
                        UserId = _TimeTrackerUserInfos.Where(n => n.LoginName == dto.UserID).FirstOrDefault().UserId,
                        YearMonth = dto.TgtYearMonth,
                        InitHostName = Environment.MachineName,
                        InitDate = DateTime.Now,
                        PlannedTime = dto.UpdPlannedTime,
                    };
                    // 計画工数更新レコード更新or登録
                    if (ModelUtil.IsExistUpdPlanndeTimeRec(_ConfigInfo.ConnStrTimeEntryDB, updRecDto)) {
                        ModelUtil.UpdateUpdPlannedTimeRec(_ConfigInfo.ConnStrTimeEntryDB, updRecDto);
                        continue;
                    }
                    ModelUtil.InsertUpdPlannedTimeRec(_ConfigInfo.ConnStrTimeEntryDB, updRecDto);
                }
            } catch (Exception e) {
                MessageDialogUtil.ShowErroMsg(e);
                IsErr = true;
            }
        }
        /// <summary>
        /// 更新用Dtoリスト編集
        /// </summary>
        /// <param name="updDtoList"></param>
        /// <returns></returns>
        private List<UpdPlannedTimeDto> EditUpdDto(List<UpdPlannedTimeDto> updDtoList) {
            foreach (var dto in updDtoList) {
                // プロジェクト一覧取得バッチ実行
                BatUtil.ExecBat_OutputProjectItems(dto.ProjectID);

                // プロジェクトのフィールド取得
                var jsonProjectItems = new JsonUtil<ProjectItemsEntity>();
                string jsProjectItemsPath = Path.Combine(Environment.CurrentDirectory, string.Format(BatConst._TXT_NAME_JS_PROJECT_ITEMS, Environment.MachineName));
                var projectDatas = jsonProjectItems.JsonConverter_Project(jsProjectItemsPath);

                // 出力ファイル削除
                CommonUtil.DeleteFile(jsProjectItemsPath);

                if (projectDatas.Count == 0) {
                    // 対象レコードなし
                    continue;
                }
                // ワークアイテム取得バッチ実行
                BatUtil.ExecBat_OutputWorkItems(projectDatas[0].workItemRootFolderId);

                // ワークアイテム取得
                var jsonWorkItems = new JsonUtil<OutputWorkItemEntity>();
                string jsWorkTimesPath = Path.Combine(Environment.CurrentDirectory, string.Format(BatConst._TXT_NAME_JS_WORK_ITEMS, Environment.MachineName));
                var jsDatas = jsonWorkItems.JsonConverter_WorkItem(jsWorkTimesPath);

                // 出力ファイル削除
                CommonUtil.DeleteFile(jsWorkTimesPath);

                if (jsDatas.Count == 0) {
                    // 対象レコードなし
                    continue;
                }
                // アイテム名に「月」が含まれる＆ターゲットの月に該当するか判定
                var newJsDatas = jsDatas.Where(n => n != null && n.Name.Contains(_MONTH_TXT)).ToList();
                foreach (var jsData in newJsDatas) {
                    // アイテム名の「月」からさかのぼり、○○月の○○に相当する数値を取得する
                    string month = string.Empty;
                    for (int i = jsData.Name.IndexOf(_MONTH_TXT); 0 <= i; i--) {
                        char c = jsData.Name[i];
                        if (char.IsNumber(c)) {
                            int integer = (int)char.GetNumericValue(c);
                            month = integer.ToString() + month;
                        }
                    }
                    // 取得した数値がターゲットの月に該当していれば確定
                    int intMonth;
                    if (int.TryParse(month, out intMonth) && intMonth == dto.TgtYearMonth.Month) {
                        dto.WorkItemID = jsData.Id;
                        break;
                    }
                }
            }
            return updDtoList;
        }
    }
}
