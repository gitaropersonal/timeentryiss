﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using TimeEntryClient.Const;
using TimeEntryClient.Dto;
using TimeEntryClient.Util;
using TimeEntryShared.Const;
using TimeEntryShared.Entity;
using TimeEntryShared.Util;

namespace TimeEntryClient.Forms {
    public class TEF10100_MstGroupFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private TEF10100_MstGroupBody _Body;
        /// <summary>
        /// Config情報
        /// </summary>
        private TimeEntryConfigEntity _ConfigInfo = new TimeEntryConfigEntity();
        /// <summary>
        /// グループコンボボックスのデータソース用リスト
        /// </summary>
        private Dictionary<string, string> _CmbBuSourceList = new Dictionary<string, string>();
        /// <summary>
        /// グループコンボボックスのデータソース用リスト
        /// </summary>
        private Dictionary<string, string> _CmbGroupSourceList = new Dictionary<string, string>();
        private List<MstGroupEntity> _GroupList = new List<MstGroupEntity>();
        private List<MstBuEntity> _BuList = new List<MstBuEntity>();
        /// <summary>
        /// グリッドセル名
        /// </summary>
        private const string _GRID_CELL_BUCODE  = "BuCode";
        private const string _GRID_CELL_BUNAME  = "BuName";
        private const string _GRID_CELL_GRCODE  = "GroupCode";
        private const string _GRID_CELL_GRNAME  = "GroupName";
        private const string _GRID_CELL_DEL_FLG = "DelFlg";
        private const string _GRID_CELL_EDITED  = "EditedFlg";
        private string[] _EditableGridCells = new string[] { _GRID_CELL_BUCODE, _GRID_CELL_GRCODE, _GRID_CELL_GRNAME , _GRID_CELL_DEL_FLG };
        /// <summary>
        /// 検索時行数
        /// </summary>
        private int _SEARCHED_ROW_COUNT = 0;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        public TEF10100_MstGroupFormLogic(TEF10100_MstGroupBody body) {
            // 初期化
            _Body = body;
            Init();
            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.O:
                        _Body.btnUpdate.Focus();
                        _Body.btnUpdate.PerformClick();
                        break;
                    case Keys.A:
                        _Body.btnClear.Focus();
                        _Body.btnClear.PerformClick();
                        break;
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                    case Keys.S:
                        _Body.btnSearch.Focus();
                        _Body.btnSearch.PerformClick();
                        break;
                }
            };
            // 検索ボタン押下
            _Body.btnSearch.Click += (s, e) => {
                ShowGrid();
            };
            // クリアボタン押下
            _Body.btnClear.Click += (s, e) => {
                var result = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLEAR);
                if (result == DialogResult.OK) {
                    Init();
                }
            };
            // 更新ボタン押下
            _Body.btnUpdate.Click += (s, e) => {
                Update();
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
            // グリッド行番号描画
            _Body.dgvMstGroup.RowPostPaint += (s, e) => {
                GridRowUtil<MstEmploeeDto>.SetRowNum(s, e);
            };
            // グリッドCellValueChanged
            _Body.dgvMstGroup.CellValueChanged += GridCellValueChanged;

            // グリッドCellEnter
            _Body.dgvMstGroup.CellEnter += (s, e) => {
                GridCelEnter(e.RowIndex, e.ColumnIndex);
            };
            // 所属コンボボックスSelectionChangeCommitted
            _Body.cmbBuName.SelectionChangeCommitted += (s, e) => {
                ControlUtil.InitGroupCombo(_GroupList, ref _CmbGroupSourceList, _Body.cmbGroup, _Body.cmbBuName.Text);
            };
        }
        /// <summary>
        /// グリッドセルEnter
        /// </summary>
        /// <param name="RowIdx"></param>
        /// <param name="ColIdx"></param>
        private void GridCelEnter(int RowIdx, int ColIdx) {
            if (RowIdx < 0 || ColIdx < 0) {
                return;
            }
            var tgtCol = _Body.dgvMstGroup.Columns[ColIdx];
            if (tgtCol == null) {
                return;
            }
            // IMEモードを設定
            string tgtColName = tgtCol.Name;
            switch (tgtColName) {
                case _GRID_CELL_BUCODE:
                case _GRID_CELL_GRCODE:
                    _Body.dgvMstGroup.ImeMode = ImeMode.Disable;
                    break;
                case _GRID_CELL_GRNAME:
                    _Body.dgvMstGroup.ImeMode = ImeMode.Hiragana;
                    break;
                default:
                    _Body.dgvMstGroup.ImeMode = ImeMode.NoControl;
                    break;
            }
        }
        /// <summary>
        /// グリッドCellValueChanged
        /// </summary>
        /// <param name="s"></param>
        /// <param name="e"></param>
        private void GridCellValueChanged(object s, DataGridViewCellEventArgs e) {

            // イベント退避
            _Body.dgvMstGroup.CellValueChanged -= GridCellValueChanged;

            string tgtCellName = _Body.dgvMstGroup.Columns[e.ColumnIndex].Name;
            switch (tgtCellName) {
                case _GRID_CELL_BUCODE:
                    // 部署コード
                     CellValueChangeBuCode(e.RowIndex);
                    break;
                case _GRID_CELL_GRCODE:
                    // グループコード
                    CellValueChangGroupCode(e.RowIndex);
                    break;
                case _GRID_CELL_GRNAME:
                    // グループ名
                    CellValueChangGroupName(e.RowIndex);
                    break;
            }
            if (0 <= e.ColumnIndex && 0 <= e.RowIndex && _EditableGridCells.Contains(tgtCellName)) {
                // セル背景色
                _Body.dgvMstGroup.Rows[e.RowIndex].Cells[e.ColumnIndex].Style.BackColor = Color.Yellow;

                // 編集ステータス
                if (e.RowIndex < _SEARCHED_ROW_COUNT - 1) {
                    _Body.dgvMstGroup.Rows[e.RowIndex].Cells[_GRID_CELL_EDITED].Value = ClientConst.EditStatus.UPDATE;
                } else {
                    _Body.dgvMstGroup.Rows[e.RowIndex].Cells[_GRID_CELL_EDITED].Value = ClientConst.EditStatus.INSERT;
                }
            }
            // イベント回復
            _Body.dgvMstGroup.CellValueChanged += GridCellValueChanged;
        }
        /// <summary>
        /// グリッドCellValueChanged（部コード）
        /// </summary>
        /// <param name="RowIdx"></param>
        private void CellValueChangeBuCode(int RowIdx) {
            var BuCode = _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_BUCODE].Value;
            if (BuCode == null || string.IsNullOrEmpty(BuCode.ToString())) {
                _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_BUCODE].Value = string.Empty;
                _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_BUNAME].Value = string.Empty;
                return;
            }
            int intBuCode;
            if (!int.TryParse(BuCode.ToString(), out intBuCode)) {
                _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_BUCODE].Value = string.Empty;
                _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_BUNAME].Value = string.Empty;
                return;
            }
            _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_BUCODE].Value = StrUtil.SelNumericFromStr(BuCode.ToString()).PadLeft(3, CommonLiteral._CHAR_PAD_ZERO);
            _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_BUNAME].Value = _CmbBuSourceList.Where(n => n.Key == BuCode.ToString()).FirstOrDefault().Value;
        }
        /// <summary>
        /// グリッドCellValueChanged（グループコード）
        /// </summary>
        /// <param name="RowIdx"></param>
        private void CellValueChangGroupCode(int RowIdx) {
            var GroupCode = _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_GRCODE].Value;
            if (GroupCode == null || string.IsNullOrEmpty(GroupCode.ToString())) {
                _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_GRCODE].Value = string.Empty;
                return;
            }
            int intGroupCode;
            if (!int.TryParse(GroupCode.ToString(), out intGroupCode)) {
                _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_GRCODE].Value = string.Empty;
                return;
            }
            _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_GRCODE].Value = StrUtil.SelNumericFromStr(GroupCode.ToString()).PadLeft(6, CommonLiteral._CHAR_PAD_ZERO);
        }
        /// <summary>
        /// グリッドCellValueChanged（グループ名）
        /// </summary>
        /// <param name="RowIdx"></param>
        private void CellValueChangGroupName(int RowIdx) {
            var GroupName = _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_GRNAME].Value;
            if (GroupName == null || string.IsNullOrEmpty(GroupName.ToString())) {
                _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_BUNAME].Value = string.Empty;
                return;
            }
            _Body.dgvMstGroup.Rows[RowIdx].Cells[_GRID_CELL_GRNAME].Value = StrUtil.CutHalfMarkChars(GroupName.ToString());
        }
        /// <summary>
        /// 更新処理
        /// </summary>
        private void Update() {
            // チェック処理
            if (!UpdValidate()) {
                return;
            }
            // 確認ダイアログ
            if (MessageDialogUtil.ShowInfoExc(ClientMessages._MSG_ASK_POSITION_MST_CHANGE) != DialogResult.OK) {
                return;
            }
            // 更新データ作成
            var addGroupInfos = new List<MstGroupEntity>();
            var updGroupInfos = new List<MstGroupEntity>();
            foreach (DataGridViewRow r in _Body.dgvMstGroup.Rows) {
                var rowDto = GridRowUtil<MstGroupDto>.GetRowModel(r);
                switch (rowDto.EditedFlg) {
                    case ClientConst.EditStatus.INSERT:
                        // 新規登録
                        var newEntity = new MstGroupEntity() {
                            BuCode = rowDto.BuCode,
                            GroupCode = rowDto.GroupCode,
                            GroupName = rowDto.GroupName,
                            DelFlg = rowDto.DelFlg ? FlgValConst._STR_FLG_ON : FlgValConst._STR_FLG_OFF,
                        };
                        addGroupInfos.Add(newEntity);
                        break;
                    case ClientConst.EditStatus.UPDATE:
                        // 更新データ
                        var tgtEntity = _GroupList.Where(n => n.BuCode == rowDto.BuCode && n.GroupCode == rowDto.GroupCode).FirstOrDefault();
                        var updEntity = new MstGroupEntity() {
                            BuCode = tgtEntity.BuCode,
                            GroupCode = tgtEntity.GroupCode,
                            GroupName = rowDto.GroupName,
                            DelFlg = rowDto.DelFlg ? FlgValConst._STR_FLG_ON : FlgValConst._STR_FLG_OFF,
                        };
                        updGroupInfos.Add(updEntity);
                        break;
                }
            }
            // 登録
            ModelUtil.InsGroupMst(_ConfigInfo.ConnStrTimeEntryDB, addGroupInfos);

            // 更新
            ModelUtil.UpdGroupMst(_ConfigInfo.ConnStrTimeEntryDB, updGroupInfos);

            // グリッド描画
            ShowGrid();

            // メッセージ表示
            MessageDialogUtil.ShowInfolMsgOK(ClientMessages._MSG_FINISH_UPDATE);
        }
        /// <summary>
        /// 更新時Validate
        /// </summary>
        /// <returns></returns>
        private bool UpdValidate() {
            int updRowCount = 0;
            var keyInfos = new List<MstGroupDto>();
            foreach (DataGridViewRow r in _Body.dgvMstGroup.Rows) {
                var rowDto = GridRowUtil<MstGroupDto>.GetRowModel(r);

                // キー情報取得
                var key = new MstGroupDto() {
                    BuCode = rowDto.BuCode,
                    GroupCode = StrUtil.TostringNullForbid(rowDto.GroupCode),
                    EditedFlg = rowDto.EditedFlg,
                };
                keyInfos.Add(key);

                // 編集行以外はスルー
                if (rowDto.EditedFlg == ClientConst.EditStatus.NONE) {
                    continue;
                }
                updRowCount++;

                if (string.IsNullOrEmpty(rowDto.BuCode)) {
                    // 部署コード未入力
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _Body.dgvMstGroup.Columns[nameof(rowDto.BuCode)].HeaderText));
                    r.Cells[nameof(rowDto.BuCode)].Style.BackColor = Color.Red;
                    return false;
                }
                if (!string.IsNullOrEmpty(rowDto.GroupCode) && rowDto.BuCode != rowDto.GroupCode.Substring(0, 3)) {
                    // グループコードが部署コードと不整合
                    MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_MUTCH_BU_GROUP_CODE);
                    r.Cells[nameof(rowDto.GroupCode)].Style.BackColor = Color.Red;
                    return false;
                }
                if (!string.IsNullOrEmpty(rowDto.GroupCode) && string.IsNullOrEmpty(rowDto.GroupName)) {
                    // グループ名未入力
                    MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_EMPTY, _Body.dgvMstGroup.Columns[nameof(rowDto.GroupName)].HeaderText));
                    r.Cells[nameof(rowDto.GroupName)].Style.BackColor = Color.Red;
                    return false;
                } 
            }
            if (updRowCount == 0) {
                // 更新対象データなし
                MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_EXIST_DATA);
                return false;
            }
            // 重複チェック
            if (!ValidateDouple(keyInfos)) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// 重複チェック
        /// </summary>
        /// <param name="keyInfos"></param>
        /// <returns></returns>
        private bool ValidateDouple(List<MstGroupDto> keyInfos) {
            foreach (var keyInfo in keyInfos) {
                if (string.IsNullOrEmpty(keyInfo.BuCode)) {
                    continue;
                }
                int doupleCountMst = _GroupList.Where(n => n.BuCode == keyInfo.BuCode && n.GroupCode == keyInfo.GroupCode).ToList().Count;
                if ((keyInfo.EditedFlg == ClientConst.EditStatus.INSERT && 0 < doupleCountMst) || 1 < doupleCountMst) {
                    // 重複エラー時イベント
                    DopleErrEvent(keyInfo.BuCode, keyInfo.GroupCode, keyInfos.IndexOf(keyInfo));
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 重複エラー時イベント
        /// </summary>
        /// <param name="tgtBuCode"></param>
        /// <param name="tgtGroupCode"></param>
        /// <param name="rowIdx"></param>
        private void DopleErrEvent(string tgtBuCode, string tgtGroupCode, int rowIdx) {
            // キー重複
            var dto = new MstGroupDto();
            string errCols = _Body.dgvMstGroup.Columns[nameof(dto.BuCode)].HeaderText;
            errCols += CommonLiteral._LITERAL_PUNCTUATION;
            errCols += _Body.dgvMstGroup.Columns[nameof(dto.GroupCode)].HeaderText;

            // キー重複している行の背景色を着色
            _Body.dgvMstGroup.Rows[rowIdx].Cells[_GRID_CELL_BUCODE].Style.BackColor = Color.Red;
            _Body.dgvMstGroup.Rows[rowIdx].Cells[_GRID_CELL_GRCODE].Style.BackColor = Color.Red;

            // ダイアログ表示
            MessageDialogUtil.ShowErroMsg(string.Format(ClientMessages._MSG_ERROR_DOUPLE, errCols));
        }
        /// <summary>
        /// グリッド描画
        /// </summary>
        private void ShowGrid() {
            // 初期化
            _SEARCHED_ROW_COUNT = 0;
            _Body.dgvMstGroup.Rows.Clear();
            _GroupList = ModelUtil.LoadGroupMst(_ConfigInfo);
            _BuList = ModelUtil.LoadBuMst(_ConfigInfo);

            // ソート
            _GroupList = _GroupList.OrderBy(n => n.BuCode)
                                    .ThenBy(n => n.GroupCode)
                                    .ToList();

            // グリッドオブジェクトを作成
            string tgtBuCode = StrUtil.SubstringCheckNull(_Body.cmbBuName.Text, 0, 3);
            string tgtGroupCode = StrUtil.SubstringCheckNull(_Body.cmbGroup.Text, 0, 6);
            var gridRowDtoList = CreateGridDtoList(tgtBuCode, tgtGroupCode, _Body.chkDelFlg.Checked);

            // エラーメッセージ
            if (gridRowDtoList.Count == 0) {
                MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_NOT_EXIST_DATA);
                return;
            }
            // データソース割り当て
            _Body.dgvMstGroup.DataSource = gridRowDtoList;

            // グリッドセル背景色編集
            EditGridCellColor();

            _Body.dgvMstGroup.Show();
            _Body.dgvMstGroup.Focus();

            // 検索前のテキストをセット
            SetTextCmbText(_Body.cmbGroup.Text);

            // 行数
            _SEARCHED_ROW_COUNT = _Body.dgvMstGroup.Rows.Count;
        }
        /// <summary>
        /// グリッドオブジェクトを作成
        /// </summary>
        /// <param name="tgtBuCode"></param>
        /// <param name="tgtGroupCode"></param>
        /// <param name="chkDelChecked"></param>
        /// <returns></returns>
        private BindingList<MstGroupDto> CreateGridDtoList(string tgtBuCode, string tgtGroupCode, bool chkDelChecked) {

            var ret = new BindingList<MstGroupDto>();
            foreach (var rec in _GroupList) {
                if (!chkDelChecked && int.Parse(rec.DelFlg) == FlgValConst._INT_FLG_ON) {
                    // 削除含むのチェックがついていない場合、削除データは対象外
                    continue;
                }
                if (string.IsNullOrEmpty(rec.GroupCode)) {
                    // 所属コードが空白の場合は対象外
                    continue;
                }
                var rowDto = new MstGroupDto();
                rowDto.BuCode = rec.BuCode;
                rowDto.BuName = _BuList.Where(n => n.BuCode == rec.BuCode).FirstOrDefault().BuName;
                rowDto.GroupCode = rec.GroupCode;
                rowDto.GroupName = rec.GroupName;
                rowDto.DelFlg = (int.Parse(rec.DelFlg) == FlgValConst._INT_FLG_ON);
                rowDto.EditedFlg = ClientConst.EditStatus.NONE;
                if (string.IsNullOrEmpty(tgtBuCode)) {
                    ret.Add(rowDto);
                    continue;
                }
                if (rowDto.BuCode == tgtBuCode && string.IsNullOrEmpty(tgtGroupCode)) {
                    ret.Add(rowDto);
                    continue;
                }
                if (rowDto.BuCode == tgtBuCode && rowDto.GroupCode == tgtGroupCode) {
                    ret.Add(rowDto);
                }
            }
            return ret;
        }
        /// <summary>
        /// グリッドセル背景色編集
        /// </summary>
        private void EditGridCellColor() {
            foreach (DataGridViewRow r in _Body.dgvMstGroup.Rows) {
                var rowDto = GridRowUtil<MstGroupDto>.GetRowModel(r);
                if (!string.IsNullOrEmpty(rowDto.BuCode)) {
                    // 主キーの背景色
                    r.Cells[nameof(rowDto.BuCode)].ReadOnly = true;
                    r.Cells[nameof(rowDto.GroupCode)].ReadOnly = true;
                    r.Cells[nameof(rowDto.BuCode)].Style.BackColor = Color.LightGray;
                    r.Cells[nameof(rowDto.GroupCode)].Style.BackColor = Color.LightGray;
                }
            }
        }
        /// <summary>
        /// 検索前のテキストをセット
        /// </summary>
        /// <param name="oldText"></param>
        private void SetTextCmbText(string oldText) {
            string oldGrouopCode = oldText;
            ControlUtil.InitGroupCombo(_GroupList, ref _CmbGroupSourceList, _Body.cmbGroup, _Body.cmbBuName.Text);
            foreach (var pair in _CmbGroupSourceList) {
                string source = string.Format(FormatConst._FORMAT_CMB_GROUP_FORMAT, pair.Key, pair.Value);
                if (source == oldGrouopCode) {
                    _Body.cmbGroup.Text = oldGrouopCode;
                    break;
                }
            }
        }
        /// <summary>
        /// 初期化処理
        /// </summary>
        private void Init() {
            _ConfigInfo = ModelUtil.LoadConfig();
            _BuList = ModelUtil.LoadBuMst(_ConfigInfo);
            _GroupList = ModelUtil.LoadGroupMst(_ConfigInfo);
            ControlUtil.InitBuCombo(_BuList, ref _CmbBuSourceList, _Body.cmbBuName, true);
            ControlUtil.InitGroupCombo(_GroupList, ref _CmbGroupSourceList, _Body.cmbGroup, _Body.cmbBuName.Text);
            _Body.chkDelFlg.Checked = false;
            _Body.dgvMstGroup.Rows.Clear();
            _Body.cmbBuName.Focus();
            _SEARCHED_ROW_COUNT = 0;
        }
    }
}
