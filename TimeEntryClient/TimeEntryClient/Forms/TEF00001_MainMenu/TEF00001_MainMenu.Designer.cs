﻿namespace TimeEntryClient.Forms {
    partial class TEF00001_MainMenu {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TEF00001_MainMenu));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblRevision = new System.Windows.Forms.Label();
            this.btnTEF10200 = new System.Windows.Forms.Button();
            this.btnTEF10100 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnTEF10300 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnTEF10000 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.btnTEF01000 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.btnTEF02000 = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.btnTEF10200);
            this.panel1.Controls.Add(this.btnTEF10100);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btnTEF10300);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.btnTEF10000);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.btnTEF01000);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.btnTEF02000);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(830, 493);
            this.panel1.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(830, 38);
            this.panel5.TabIndex = 234;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblRevision);
            this.panel6.Location = new System.Drawing.Point(622, 3);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(196, 35);
            this.panel6.TabIndex = 0;
            // 
            // lblRevision
            // 
            this.lblRevision.AutoSize = true;
            this.lblRevision.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblRevision.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblRevision.Location = new System.Drawing.Point(158, 0);
            this.lblRevision.Name = "lblRevision";
            this.lblRevision.Size = new System.Drawing.Size(38, 16);
            this.lblRevision.TabIndex = 233;
            this.lblRevision.Text = "rev：";
            this.lblRevision.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnTEF10200
            // 
            this.btnTEF10200.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnTEF10200.Location = new System.Drawing.Point(550, 127);
            this.btnTEF10200.Name = "btnTEF10200";
            this.btnTEF10200.Size = new System.Drawing.Size(225, 50);
            this.btnTEF10200.TabIndex = 201;
            this.btnTEF10200.Text = "部署マスタ";
            this.btnTEF10200.UseVisualStyleBackColor = true;
            // 
            // btnTEF10100
            // 
            this.btnTEF10100.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnTEF10100.Location = new System.Drawing.Point(550, 183);
            this.btnTEF10100.Name = "btnTEF10100";
            this.btnTEF10100.Size = new System.Drawing.Size(225, 50);
            this.btnTEF10100.TabIndex = 202;
            this.btnTEF10100.Text = "グループマスタ";
            this.btnTEF10100.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label1.Location = new System.Drawing.Point(42, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 232;
            this.label1.Text = "通常業務";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label2.Location = new System.Drawing.Point(316, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(115, 16);
            this.label2.TabIndex = 231;
            this.label2.Text = "マスタメンテナンス";
            // 
            // btnTEF10300
            // 
            this.btnTEF10300.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnTEF10300.Location = new System.Drawing.Point(550, 239);
            this.btnTEF10300.Name = "btnTEF10300";
            this.btnTEF10300.Size = new System.Drawing.Size(225, 50);
            this.btnTEF10300.TabIndex = 203;
            this.btnTEF10300.Text = "MHSC作業マスタ";
            this.btnTEF10300.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Enabled = false;
            this.button3.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button3.Location = new System.Drawing.Point(319, 71);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(225, 50);
            this.button3.TabIndex = 100;
            this.button3.Text = "TimeTracker作業マスタ";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Enabled = false;
            this.button2.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button2.Location = new System.Drawing.Point(319, 183);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(225, 50);
            this.button2.TabIndex = 102;
            this.button2.Text = "TimeTrackerユーザマスタ";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // btnTEF10000
            // 
            this.btnTEF10000.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnTEF10000.Location = new System.Drawing.Point(550, 71);
            this.btnTEF10000.Name = "btnTEF10000";
            this.btnTEF10000.Size = new System.Drawing.Size(225, 50);
            this.btnTEF10000.TabIndex = 200;
            this.btnTEF10000.Text = "社員マスタ";
            this.btnTEF10000.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button4.Location = new System.Drawing.Point(319, 127);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(225, 50);
            this.button4.TabIndex = 101;
            this.button4.Text = "TimeTracker所属マスタ";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // btnTEF01000
            // 
            this.btnTEF01000.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnTEF01000.Location = new System.Drawing.Point(45, 71);
            this.btnTEF01000.Name = "btnTEF01000";
            this.btnTEF01000.Size = new System.Drawing.Size(225, 50);
            this.btnTEF01000.TabIndex = 0;
            this.btnTEF01000.Text = "作業・時間外報告書作成";
            this.btnTEF01000.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button6.Location = new System.Drawing.Point(45, 183);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(225, 50);
            this.button6.TabIndex = 2;
            this.button6.Text = "予測基礎データ作成";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // btnTEF02000
            // 
            this.btnTEF02000.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnTEF02000.Location = new System.Drawing.Point(45, 127);
            this.btnTEF02000.Name = "btnTEF02000";
            this.btnTEF02000.Size = new System.Drawing.Size(225, 50);
            this.btnTEF02000.TabIndex = 1;
            this.btnTEF02000.Text = "計画工数編集";
            this.btnTEF02000.UseVisualStyleBackColor = true;
            // 
            // btnClose
            // 
            this.btnClose.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnClose.Location = new System.Drawing.Point(141, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(123, 40);
            this.btnClose.TabIndex = 90;
            this.btnClose.Text = "終了（X）";
            this.btnClose.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 499);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(830, 58);
            this.panel2.TabIndex = 81;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel4.Location = new System.Drawing.Point(554, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(276, 58);
            this.panel4.TabIndex = 90;
            // 
            // panel3
            // 
            this.panel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(224, 58);
            this.panel3.TabIndex = 70;
            // 
            // TEF00001_MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(830, 557);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(850, 600);
            this.MinimumSize = new System.Drawing.Size(850, 600);
            this.Name = "TEF00001_MainMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "メインメニュー";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button btnTEF10300;
        public System.Windows.Forms.Button button3;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Button btnTEF10000;
        public System.Windows.Forms.Button button4;
        public System.Windows.Forms.Button btnTEF01000;
        public System.Windows.Forms.Button button6;
        public System.Windows.Forms.Button btnTEF02000;
        public System.Windows.Forms.Button btnClose;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Button btnTEF10100;
        public System.Windows.Forms.Button btnTEF10200;
        public System.Windows.Forms.Label lblRevision;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
    }
}

