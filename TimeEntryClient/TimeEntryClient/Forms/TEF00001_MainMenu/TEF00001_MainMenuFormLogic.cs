﻿using System.Deployment.Application;
using System.Windows.Forms;
using TimeEntryClient.Const;
using TimeEntryClient.Util;
using TimeEntryShared.Const;

namespace TimeEntryClient.Forms {
    public class TEF00001_MainMenuFormLogic {
        /// <summary>
        /// ボディパネル
        /// </summary>
        private TEF00001_MainMenu _Body;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="body"></param>
        public TEF00001_MainMenuFormLogic(TEF00001_MainMenu body) {
            // 初期化
            _Body = body;

            // バージョン
            _Body.lblRevision.Text += GetVersion();

            // ショートカットキー
            _Body.KeyPreview = true;
            _Body.KeyDown += (s, e) => {
                if (!e.Alt) {
                    return;
                }
                switch (e.KeyCode) {
                    case Keys.X:
                        _Body.btnClose.Focus();
                        _Body.btnClose.PerformClick();
                        break;
                }
            };

            // 作業・時間外報告書作成ボタン押下
            _Body.btnTEF01000.Click += (s, e) => {
                using (var prgBar = new TEF01000_CreateRosterBody()) {
                    ShowChildForm(prgBar);
                }
            };
            // 工数分析Excel出力ボタン押下
            _Body.btnTEF02000.Click += (s, e) => {
                using (var prgBar = new TEF02000_WTAnalysisExcelBody()) {
                    ShowChildForm(prgBar);
                }
            };
            // 社員マスタボタン押下
            _Body.btnTEF10000.Click += (s, e) => {
                using (var prgBar = new TEF10000_MstEmploeeBody()) {
                    ShowChildForm(prgBar);
                }
            };
            // 部署マスタボタン押下
            _Body.btnTEF10200.Click += (s, e) => {
                using (var prgBar = new TEF10200_MstBuBody()) {
                    ShowChildForm(prgBar);
                }
            };
            // グループマスタボタン押下
            _Body.btnTEF10100.Click += (s, e) => {
                using (var prgBar = new TEF10100_MstGroupBody()) {
                    ShowChildForm(prgBar);
                }
            };
            // MHSC作業マスタボタン押下
            _Body.btnTEF10300.Click += (s, e) => {
                using (var prgBar = new TEF10300_MstMhscWorkBody()) {
                    ShowChildForm(prgBar);
                }
            };
            // 終了ボタン押下
            _Body.btnClose.Click += (s, e) => {
                var drConfirm = MessageDialogUtil.ShowInfoMsgOKCancel(ClientMessages._MSG_ASK_CLOSE);
                if (drConfirm == DialogResult.OK) {
                    _Body.Close();
                }
            };
        }
        /// <summary>
        /// 子画面表示
        /// </summary>
        /// <param name="ChildForm"></param>
        private void ShowChildForm(Form ChildForm) {
            _Body.Visible = false;
            ChildForm.ShowDialog();
            _Body.Visible = true;
        }
        /// <summary>
        /// バージョン取得
        /// </summary>
        /// <returns></returns>
        private string GetVersion() {
            if (!ApplicationDeployment.IsNetworkDeployed) {
                return "1.0.0.XXX";
            }
            var version = ApplicationDeployment.CurrentDeployment.CurrentVersion;
            return (
            version.Major.ToString() + CommonLiteral._LITERAL_DOT +
            version.Minor.ToString() + CommonLiteral._LITERAL_DOT +
            version.Build.ToString() + CommonLiteral._LITERAL_DOT +
            version.Revision.ToString()
            );
        }
    }
}
