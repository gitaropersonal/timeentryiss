﻿using System;
using System.Windows.Forms;
using TimeEntryClient.Const;
using TimeEntryShared.Util;
using TimeEntryClient.Forms.Dialog;

namespace TimeEntryClient.Util {
    public static  class MessageDialogUtil {
        /// <summary>
        /// エラーメッセージ表示処理
        /// </summary>
        /// <param name="msgs"></param>
        public static void ShowErroMsg(string msg) {
            MessageBox.Show(msg
                          , ClientMessages._DIALOG_TITLE_ERROR
                          , MessageBoxButtons.OK
                          , MessageBoxIcon.Error);
        }
        /// <summary>
        /// エラーメッセージ表示処理
        /// </summary>
        /// <param name="msgs"></param>
        public static void ShowErroMsg(Exception e) {
            MessageBox.Show(e.Message
                          , ClientMessages._DIALOG_TITLE_ERROR
                          , MessageBoxButtons.OK
                          , MessageBoxIcon.Error);
            LogAndConsoleUtil.ShowLogAndConsoleErr(e.Message, e.StackTrace);
        }
        /// <summary>
        /// 警告メッセージ表示
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static DialogResult ShowInfoExc(string msg) {
            DialogResult result = MessageBox.Show(msg
                                                , ClientMessages._LITERAL_EXCRAMATION
                                                , MessageBoxButtons.OKCancel
                                                , MessageBoxIcon.Exclamation
                                                , MessageBoxDefaultButton.Button1);
            return result;
        }
        /// <summary>
        /// 確認メッセージ表示
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static DialogResult ShowInfoMsgOKCancel(string msg) {
            DialogResult result = MessageBox.Show(msg
                                                , ClientMessages._LITERAL_CNFIRM
                                                , MessageBoxButtons.OKCancel
                                                , MessageBoxIcon.Information
                                                , MessageBoxDefaultButton.Button1);
            return result;
        }
        /// <summary>
        /// 確認メッセージ表示
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static DialogResult ShowInfolMsgOK(string msg) {
            DialogResult result = MessageBox.Show(msg
                                                , ClientMessages._LITERAL_CNFIRM
                                                , MessageBoxButtons.OK
                                                , MessageBoxIcon.Information
                                                , MessageBoxDefaultButton.Button1);
            return result;
        }
        /// <summary>
        /// 説明ダイアログ表示（社員番号）
        /// </summary>
        public static void ShowExplainEmploeeNum() {
            using (var prgBar = new EmploeeCardDialog()) {
                var dr = prgBar.ShowDialog();
            }
        }
    }
}
