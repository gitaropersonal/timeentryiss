﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using TimeEntryShared.Const;
using TimeEntryShared.Entity;

namespace TimeEntryClient.Util {
    public static class ControlUtil {
        /// <summary>
        /// コンボボックスの設定（作業時間HH）
        /// </summary>
        /// <returns></returns>
        public static List<string> GetCmbHHList() {
            var ret = new List<string>();
            for (int i = 0; i < 9; i++) {
                ret.Add(i.ToString(FormatConst._FORMAT_PRE_ZERO_2));
            }
            return ret;
        }
        /// <summary>
        /// コンボボックスの設定（作業時間MM）
        /// </summary>
        /// <returns></returns>
        public static List<string> GetCmbMMList() {
            var ret = new List<string>();
            for (int i = 0; i < 4; i++) {
                ret.Add((i * 15).ToString(FormatConst._FORMAT_PRE_ZERO_2));
            }
            return ret;
        }
        /// <summary>
        /// コンボボックスのインデックスセット
        /// </summary>
        /// <param name="cmb"></param>
        /// <param name="str"></param>
        public static void InitCmbHHIdx(ComboBox cmb, string str) {
            foreach (string val in (List<string>)cmb.DataSource) {
                if (val == str) {
                    cmb.SelectedIndex = ((List<string>)cmb.DataSource).IndexOf(val);
                }
            }
        }
        /// <summary>
        /// 部署コンボボックス初期化
        /// </summary>
        /// <param name="BuList"></param>
        /// <param name="CmbBuSourceList"></param>
        /// <param name="ComboBox"></param>
        /// <param name="isAddEmpty"></param>
        public static void InitBuCombo(List<MstBuEntity> BuList, ref Dictionary<string, string> CmbBuSourceList, ComboBox ComboBox, bool isAddEmpty = true) {

            // 元データ作成
            CmbBuSourceList = BuList.Where(n => n.DelFlg == FlgValConst._STR_FLG_OFF)
                                    .Distinct()
                                    .ToDictionary(n => n.BuCode, n => n.BuName);

            // コンボのソース作成
            var lst = new List<string>();
            if (isAddEmpty) {
                lst.Add(string.Empty);
            }
            CmbBuSourceList.Where(n => !string.IsNullOrEmpty(n.Key))
                           .Distinct()
                           .ToList()
                           .ForEach(n => lst.Add(string.Format(FormatConst._FORMAT_CMB_BU_FORMAT, n.Key, n.Value)));

            // コンボボックスにセット
            ComboBox.DataSource = lst;
        }
        /// <summary>
        /// グループコンボボックス初期化
        /// </summary>
        /// <param name="GroupList"></param>
        /// <param name="CmbGroupSourceList"></param>
        /// <param name="ComboBox"></param>
        /// <param name="BuCode"></param>
        /// <param name="isAddDeleted"></param>
        public static void InitGroupCombo(List<MstGroupEntity> GroupList, ref Dictionary<string, string> CmbGroupSourceList, ComboBox ComboBox, string BuCode,
            bool isAddDeleted = true) {

            // 元データ作成
            if (!isAddDeleted) {
                GroupList = GroupList.Where(n => n.DelFlg == FlgValConst._STR_FLG_OFF).ToList();
            }
            CmbGroupSourceList = GroupList.Where(n => !string.IsNullOrEmpty(n.GroupCode) && n.DelFlg == FlgValConst._STR_FLG_OFF)
                                          .Distinct()
                                          .ToDictionary(n => n.GroupCode, n => n.GroupName);

            // コンボのソース作成
            var lst = new List<string>() {
                string.Empty
            };
            // 部署コードが未選択の場合、空のソースとする
            if (string.IsNullOrEmpty(BuCode)) {
                ComboBox.DataSource = lst;
                return;
            }
            CmbGroupSourceList.Where(n => n.Key.Substring(0, 3) == BuCode.Substring(0, 3))
                              .Distinct()
                              .ToList()
                              .ForEach(n => lst.Add(string.Format(FormatConst._FORMAT_CMB_GROUP_FORMAT, n.Key, n.Value)));

            // コンボボックスにセット
            ComboBox.DataSource = lst;
        }
    }
}
