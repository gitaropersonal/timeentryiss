﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;

namespace TimeEntryClient.Util {
    public static class GridRowUtil<T> where T : new() {
        /// <summary>
        /// 行モデル取得
        /// </summary>
        /// <param name="r"></param>
        /// <returns></returns>
        public static T GetRowModel(DataGridViewRow r) {
            // TypeDescriptorを使用してプロパティ一覧を取得する
            var attributes = new Attribute[] { };
            T ret = new T();
            var pdc = TypeDescriptor.GetProperties(ret, attributes);

            // プロパティ一覧をリフレクションから取得
            var type = ret.GetType();
            foreach (var propertyInfo in type.GetProperties()) {
                propertyInfo.SetValue(ret, r.Cells[propertyInfo.Name].Value);
            }
            return ret;
        }
        /// <summary>
        /// 行番号の描画
        /// </summary>
        /// <param name="e"></param>
        /// <param name="dgv"></param>
        public static void SetRowNum(object s, DataGridViewRowPostPaintEventArgs e) {
            var dgv = (DataGridView)s;
            if (dgv.RowHeadersVisible) {
                //行番号を描画する範囲を決定する
                var rect = new Rectangle(
                    e.RowBounds.Left, e.RowBounds.Top,
                    dgv.RowHeadersWidth, e.RowBounds.Height);
                rect.Inflate(-2, -2);
                //行番号を描画する
                TextRenderer.DrawText(e.Graphics,
                    (e.RowIndex + 1).ToString(),
                    e.InheritedRowStyle.Font,
                    rect,
                    e.InheritedRowStyle.ForeColor,
                    TextFormatFlags.Right | TextFormatFlags.VerticalCenter);
            }
        }
    }
}
