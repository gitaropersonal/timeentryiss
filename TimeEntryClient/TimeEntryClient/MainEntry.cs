﻿using System;
using System.Drawing;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using TimeEntryClient.Const;
using TimeEntryClient.Forms;
using TimeEntryClient.Util;

namespace Main {
    static class MainEntry {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            bool createdNew;
            using (var mutex = new Mutex(true, ClientConst._MUTEX_NAME, out createdNew)) {
                if (!createdNew) {
                    MessageDialogUtil.ShowErroMsg(ClientMessages._MSG_ERROR_DOUPLE_BOOT);
                    return;
                }
                try {
                    typeof(Form).GetField("defaultIcon", BindingFlags.NonPublic | BindingFlags.Static).SetValue(null, new Icon(@"ICO_TimeEntryClient.ico"));
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new TEF00001_MainMenu());
                } catch (Exception e) {
                    MessageDialogUtil.ShowErroMsg(e);
                } finally {
                    mutex.ReleaseMutex();
                }
            }
        }
    }
}
