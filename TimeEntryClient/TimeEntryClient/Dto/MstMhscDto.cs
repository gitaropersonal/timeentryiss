﻿using TimeEntryShared.Entity;
using TimeEntryClient.Const;

namespace TimeEntryClient.Dto {
    public class MstMhscDto {
        public string Nendo { get; set; }
        public string ChargeNo { get; set; }
        public string WorkName { get; set; }
        public bool RestFlg { get; set; }
        public bool DelFlg { get; set; }
        public ClientConst.EditStatus EditedFlg { get; set; }
    }
}
