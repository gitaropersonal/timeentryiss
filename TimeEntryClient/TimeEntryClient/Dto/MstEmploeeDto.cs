﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryClient.Dto {
    public class MstEmploeeDto {
        public string BuCode { get; set; }
        public string BuName { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string EmploeeNum { get; set; }
        public string EmploeeName { get; set; }
        public int DefaultWorkFlg { get; set; }
        public int MhscFlg { get; set; }
        public int ForcingShiftB { get; set; }
        public int NightShiftFlg { get; set; }
        public int DeleteFlg { get; set; }
    }
}
