﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeEntryClient.Const;

namespace TimeEntryShared.Entity{
    public class MstGroupDto {
        public string BuCode { get; set; }
        public string BuName { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public bool DelFlg { get; set; }
        public ClientConst.EditStatus EditedFlg { get; set; }
    }
}
