﻿using System;
using System.Collections.Generic;
using System.Text;
using TimeEntryClient.Const;

namespace TimeEntryShared.Entity{
    public class MstBuDto {
        public string BuCode { get; set; }
        public string BuName { get; set; }
        public bool DelFlg { get; set; }
        public ClientConst.EditStatus EditedFlg { get; set; }
    }
}
