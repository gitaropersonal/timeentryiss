﻿using System;
using TimeEntryShared.Util;
using TimeEntryShared.Service;

namespace TEB01099_CreateRoster {
    class Program {

        static void Main(string[] args) {
            try {
                var propaties = CommonUtil.GetPropaties();
                var service1041 = new EditTimeEntryService();
                var service1050 = new EditNightShiftService();
                service1041.Main(propaties, string.Empty, true);
                service1050.Main(propaties);

            } catch (Exception e) {

                LogAndConsoleUtil.ShowLogAndConsoleErr(e.Message, e.StackTrace);

            }
        }
    }
}
