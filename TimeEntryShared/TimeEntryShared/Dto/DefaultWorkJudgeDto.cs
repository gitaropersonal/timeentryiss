﻿using System;
using TimeEntryShared.Const;

namespace TimeEntryShared.Dto {
    public class DefaultWorkJudgeDto {
        public DateTime WorkDate { get; set; }
        public decimal WorkTime { get; set; }
        public decimal StartTime { get; set; }
        public decimal EndTime { get; set; }
        public TimeConst.RestType RestType{get; set;}
    }
}
