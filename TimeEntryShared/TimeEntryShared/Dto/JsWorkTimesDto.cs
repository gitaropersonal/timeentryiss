﻿using System.Collections.Generic;
using TimeEntryShared.Entity;

namespace TimeEntryShared.Dto {
    public class JsWorkTimesDto {
        public int TotalCount { get; set; }
        public List<WorkTimesEntity> Data { get; set; }
    }
}
