﻿namespace TimeEntryShared.Dto {
    public class EditTimeEntryDto {
        /// <summary>
        /// ユーザー名
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// プロジェクト名
        /// </summary>
        public string ProjectName { get; set; }
        /// <summary>
        /// プロジェクトコード
        /// </summary>
        public string ProjectCode { get; set; }
        /// <summary>
        /// 日付
        /// </summary>
        public string WorkDate { get; set; }
        /// <summary>
        /// 実績工数
        /// </summary>
        public decimal ManHour { get; set; }
        /// <summary>
        /// 作業時間（h）
        /// </summary>
        public int ManHourH { get; set; }
        /// <summary>
        /// 作業時間（m）
        /// </summary>
        public int ManHourM { get; set; }
    }
}
