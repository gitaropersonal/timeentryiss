﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeEntryShared.Dto {
    public class WTAnalysisGridDto {
        public void SetTgtMonth(List<DateTime> tgtMonthList) {
            for (int i = 0; i < tgtMonthList.Count; i++) {
                switch (i) {
                    case 0:
                        TgtMonth1 = tgtMonthList[i];
                        break;
                    case 1:
                        TgtMonth2 = tgtMonthList[i];
                        break;
                    case 2:
                        TgtMonth3 = tgtMonthList[i];
                        break;
                    case 3:
                        TgtMonth4 = tgtMonthList[i];
                        break;
                    case 4:
                        TgtMonth5 = tgtMonthList[i];
                        break;
                    case 5:
                        TgtMonth6 = tgtMonthList[i];
                        break;
                }
            }
        }
        public string UserId { get; set; }
        public string EmploeeName { get; set; }
        public string ProjectId { get; set; }
        public string ChargeNo { get; set; }
        public string WorkName { get; set; }

        public DateTime TgtMonth1 { get; set; }
        public string ActualTime { get; set; }
        public string ActualManHour { get; set; }
        public string ActualWorkPct { get; set; }
        public string ActualManMoon { get; set; }

        public string PlannedTime1 { get; set; }
        public string PlannedManHour1 { get; set; }
        public string PlannedWorkPct1 { get; set; }
        public string PlannedManMoon1 { get; set; }

        public DateTime TgtMonth2 { get; set; }
        public string PlannedTime2 { get; set; }
        public string PlannedManHour2 { get; set; }
        public string PlannedWorkPct2 { get; set; }
        public string PlannedManMoon2 { get; set; }

        public DateTime TgtMonth3 { get; set; }
        public string PlannedTime3 { get; set; }
        public string PlannedManHour3 { get; set; }
        public string PlannedWorkPct3 { get; set; }
        public string PlannedManMoon3 { get; set; }

        public DateTime TgtMonth4 { get; set; }
        public string PlannedTime4 { get; set; }
        public string PlannedManHour4 { get; set; }
        public string PlannedWorkPct4 { get; set; }
        public string PlannedManMoon4 { get; set; }

        public DateTime TgtMonth5 { get; set; }
        public string PlannedTime5 { get; set; }
        public string PlannedManHour5 { get; set; }
        public string PlannedWorkPct5 { get; set; }
        public string PlannedManMoon5 { get; set; }

        public DateTime TgtMonth6 { get; set; }
        public string PlannedTime6 { get; set; }
        public string PlannedManHour6 { get; set; }
        public string PlannedWorkPct6 { get; set; }
        public string PlannedManMoon6 { get; set; }

        public bool EditedFlg { get; set; }
    }
}
