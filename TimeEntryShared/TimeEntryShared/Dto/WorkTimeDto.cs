﻿using TimeEntryShared.Const;

namespace TimeEntryShared.Dto {
    public class WorkTimeDto {
        public WorkTimeDto(){
            ShiftType = TimeConst.WorkShiftType.UNKNOWN;
            RestType = TimeConst.RestType.NONE;
        }
        public int Day { get; set; }
        public string WeekDay { get; set; }
        public string StartHH { get; set; }
        public string StartMM { get; set; }
        public string EndHH { get; set; }
        public string EndMM { get; set; }
        public string RestMark { get; set; }
        public TimeConst.WorkShiftType ShiftType { get; set; }
        public TimeConst.RestType RestType { get; set; }
    }
}
