﻿using System;

namespace TimeEntryShared.Dto {
    public class UpdPlannedTimeDto {
        public string UserID { get; set; }
        public string ProjectID { get; set; }
        public string WorkItemID { get; set; }
        public decimal UpdPlannedTime { get; set; }
        public DateTime TgtYearMonth { get; set; }
    }
}
