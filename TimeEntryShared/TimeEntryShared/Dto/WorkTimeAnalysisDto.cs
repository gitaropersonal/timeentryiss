﻿using System.Collections.Generic;
using TimeEntryShared.Const;

namespace TimeEntryShared.Dto {
    public class WorkTimeAnalysisDto {
        public WorkTimeAnalysisDto() {
            rowType = WtAnalysisConst.ExcelRowType.BODY_ROW;
        }
        public string organization { get; set; }
        public string projectCode { get; set; }
        public string projectName { get; set; }
        public string userId { get; set; }
        public string userName { get; set; }
        public decimal totalPlannedTime { get; set; }
        public decimal totalActualTime { get; set; }
        public decimal totalPlannedManHour { get; set; }
        public decimal totalActualManHour { get; set; }
        public decimal totalPlannedPct { get; set; }
        public decimal totalActualPct { get; set; }
        public decimal totalPlannedHumanMoon { get; set; }
        public decimal totalActualHumanMoon { get; set; }
        public decimal totalPlannedCost { get; set; }
        public decimal totalActualCost { get; set; }
        public WtAnalysisConst.ExcelRowType rowType { get; set; }
        public List<WorkTimeColsDto> workTimeDatas {get; set;}
    }
    public class WorkTimeColsDto {
        public string projectCode { get; set; }
        public string projectName { get; set; }
        public string date { get; set; }
        public decimal decPlannedWorkTime { get; set; }
        public decimal decActualWorkTime { get; set; }
        public decimal decPlannedManHour { get; set; }
        public decimal decActualManHour { get; set; }
        public decimal decPlannedPct { get; set; }
        public decimal decActualPct { get; set; }
        public decimal decPlannedHumanMoon { get; set; }
        public decimal decActualHumanMoon { get; set; }
        public decimal decPlannedCost { get; set; }
        public decimal decActualCost { get; set; }
    }
}
