﻿namespace TimeEntryShared.Dto {
    public class WorkTimesCsvDto {
        public string userName { get; set; }
        public string workDate { get; set; }
        public string projectCode { get; set; }
        public string projectName { get; set; }
        public string startTime { get; set; }
        public string finishTime { get; set; }
    }
}
