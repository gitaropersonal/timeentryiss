﻿using System.Collections.Generic;
using TimeEntryShared.Entity;

namespace TimeEntryShared.Dto {
    public class ServicePropaties {
        /// <summary>
        /// Config情報
        /// </summary>
        public TimeEntryConfigEntity ConfigInfo { get; set; }
        /// <summary>
        /// TimeTracker出力情報
        /// </summary>
        public List<UserInfoDto> TimeTrackerUserInfos { get; set; }
        /// <summary>
        /// 従業員情報
        /// </summary>
        public List<MstEmploeeEntity> EmploeeInfos { get; set; }
        /// <summary>
        /// グループマスタ
        /// </summary>
        public List<MstGroupEntity> GroupList { get; set; }
        /// <summary>
        /// 部署マスタ
        /// </summary>
        public List<MstBuEntity> BuList { get; set; }
        /// <summary>
        /// MHSCマスタ
        /// </summary>
        public List<MhscMstEntity> MhscMstInfos { get; set; }
    }
}
