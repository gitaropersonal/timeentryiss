﻿namespace TimeEntryShared.Dto {
    public class UserInfoDto {
        public string LoginName { get; set; }
        public string Pass { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string OrganizationName { get; set; }
        public string UnitCost { get; set; }
    }
}
