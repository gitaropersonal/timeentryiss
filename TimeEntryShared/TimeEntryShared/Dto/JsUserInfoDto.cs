﻿using System.Collections.Generic;
using TimeEntryShared.Entity;

namespace TimeEntryShared.Dto {
    public class JsUserInfoRoot {
        public int TotalCount { get; set; }
        public List<UserInfoEntity> Data {get; set;}
    }
}
