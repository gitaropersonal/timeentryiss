﻿using System;
using System.Collections.Generic;
using System.IO;
using OfficeOpenXml;
using TimeEntryShared.Dto;
using TimeEntryShared.Const;
using TimeEntryShared.Entity;
using TimeEntryShared.Util;
using System.Linq;

namespace TimeEntryShared.Service {
    public class EditNightShiftService {
        private List<int> NightShiftRows = new List<int>();
        /// <summary>
        /// Config情報
        /// </summary>
        private TimeEntryConfigEntity _ConfigInfo = new TimeEntryConfigEntity();
        /// <summary>
        /// TimeTracker出力情報
        /// </summary>
        private List<UserInfoDto> _TimeTrackerUserInfos = new List<UserInfoDto>();
        /// <summary>
        /// 従業員情報
        /// </summary>
        private List<MstEmploeeEntity> _EmploeeInfos = new List<MstEmploeeEntity>();
        /// <summary>
        /// グループマスタ
        /// </summary>
        private List<MstGroupEntity> _GroupList = new List<MstGroupEntity>();
        /// <summary>
        /// 部署マスタ
        /// </summary>
        private List<MstBuEntity> _BuList = new List<MstBuEntity>();
        /// <summary>
        /// MHSCマスタ
        /// </summary>
        private List<MhscMstEntity> _MhscMstInfos = new List<MhscMstEntity>();

        /// <summary>
        /// メイン処理
        /// </summary>
        /// <param name="propaties"></param>
        /// <param name="tgtEmploeeNum"></param>
        /// <returns></returns>
        public string Main(ServicePropaties propaties, string tgtEmploeeNum = "") {
            LogAndConsoleUtil.ShowLogAndConsoleInfo(SharedMessages._MSG_OPERATION_START_N, LogAndConsoleUtil.StartEndLineType.Start);

            // TimeEntry.config
            _ConfigInfo = propaties.ConfigInfo;

            // TimeTrackerユーザ情報
            _TimeTrackerUserInfos = propaties.TimeTrackerUserInfos;

            // 従業員情報
            _EmploeeInfos = propaties.EmploeeInfos;

            // 部署マスタ
            _BuList = propaties.BuList;

            // グループマスタ
            _GroupList = propaties.GroupList;

            // 集計月
            DateTime dt = DateTime.Parse(_ConfigInfo.SumStartDate);
            dt = new DateTime(dt.Year, dt.Month, 1);
            string strDt = dt.ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI);

            // デフォルト勤務表の存在チェック
            string defaultKinmuhyoPath = Path.Combine(_ConfigInfo.DefaultKinmuhyoPath, _ConfigInfo.DefaultKinmuhyoName);
            if (!File.Exists(defaultKinmuhyoPath)) {
                var exNotExistDef = new Exception(SharedMessages._MSG_ERROR_NOT_EXIST_DEF);
                LogAndConsoleUtil.ShowLogAndConsoleErr(exNotExistDef.Message, exNotExistDef.StackTrace);
                return exNotExistDef.Message;
            }
            // 出力対象となる従業員情報を絞り込み
            var tgtEmploee = GetTgtEmploeeList(tgtEmploeeNum);

            foreach (var emp in tgtEmploee) {
                try {
                    // 夜勤対象者のみ限定処理
                    if (emp.NightShiftFlg == FlgValConst._STR_FLG_OFF) {
                        continue;
                    }
                    if (!KinmuhyoUtil.ValidateIsKinmuhyoOpened(_ConfigInfo, _BuList, _GroupList, emp, dt)) {
                        // 勤務表を開きっぱなしの場合はログ出力して処理終了
                        LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Format(SharedMessages._MSG_ERROR_KINMUHYO_OPEND, emp.Name));
                        continue;
                    }
                    // ユーザ種別取得
                    var userType = GetUserType(emp);

                    // 勤務表Excelパス取得
                    string commonPath = KinmuhyoUtil.GetKinmuhyoPath(_ConfigInfo, _BuList, _GroupList, emp, dt);
                    string KinmuhyoName = KinmuhyoUtil.GetKinmuhyoName(_ConfigInfo, emp.Name, dt);
                    string KinmuhyoPath = Path.Combine(commonPath, KinmuhyoName);
                    string NightShiftPath = Path.Combine(commonPath, CommonLiteral._MARK_STAR + KinmuhyoName);

                    // 勤務表Excel作成
                    if (File.Exists(KinmuhyoPath)) {
                        EditNightShiftKinmuhyo(emp, KinmuhyoPath, NightShiftPath, emp.Name, userType);
                    }
                } catch (Exception e) {
                    if (string.IsNullOrEmpty(tgtEmploeeNum)) {
                        // バッチからの起動時（社員番号未指定）は、エラーをスルー
                        LogAndConsoleUtil.ShowLogAndConsoleErr(SharedMessages._MSG_CONTINUE_ERR + Environment.NewLine + e.Message, e.StackTrace);
                        continue;
                    }
                    return e.Message;
                }
            }

            LogAndConsoleUtil.ShowLogAndConsoleInfo(SharedMessages._MSG_OPERATION_END_N, LogAndConsoleUtil.StartEndLineType.End);
            return string.Empty;
        }
        /// <summary>
        /// 出力対象となる従業員情報を絞り込み
        /// </summary>
        /// <param name="tgtEmploeeNum"></param>
        /// <returns></returns>
        private List<MstEmploeeEntity> GetTgtEmploeeList(string tgtEmploeeNum) {
            if (string.IsNullOrEmpty(tgtEmploeeNum)) {
                return _EmploeeInfos;
            }
            var tgtEmploee = _EmploeeInfos.Where(n => n.EmploeeNum == tgtEmploeeNum).FirstOrDefault();
            if (tgtEmploee == null) {
                return new List<MstEmploeeEntity>();
            }
            var newEmpList = new List<MstEmploeeEntity>();
            newEmpList.Add(tgtEmploee);
            return newEmpList;
        }
        /// <summary>
        /// ユーザ種別取得
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        private KinmuhyoConst.UserType GetUserType(MstEmploeeEntity emp) {
            if (emp.DefaultWorkFlg == FlgValConst._STR_FLG_ON) {
                return KinmuhyoConst.UserType.DEFAULT_WORK;
            }
            if (_TimeTrackerUserInfos.Where(n => CommonUtil.CutSpace(n.UserName) == CommonUtil.CutSpace(emp.Name)).FirstOrDefault() != null) {
                return KinmuhyoConst.UserType.TIME_TRACKER;
            }
            return KinmuhyoConst.UserType.NONE;
        }
        /// <summary>
        /// Excel作成（勤務実績表）
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="OriginXlsxPath"></param>
        /// <param name="NightShiftPath"></param>
        /// <param name="name"></param>
        /// <param name="userType"></param>
        public void EditNightShiftKinmuhyo(MstEmploeeEntity emp, string OriginXlsxPath, string NightShiftPath, string name, KinmuhyoConst.UserType userType) {
            LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Format(SharedMessages._MSG_NIGHT_SHIFT_TARGET, name));
            var xlsxFile = File.OpenRead(OriginXlsxPath);
            using (var package = new ExcelPackage(new FileInfo(OriginXlsxPath))) {

                // 作業内容シート作成
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var sheet = package.Workbook.Worksheets[KinmuhyoConst._SHEET_NAME_KINMUHYO];

                // 退勤時刻をチェックし、夜勤日をリストアップ
                NightShiftRows = new List<int>();
                for (int i = 1; i < TimeConst._MAX_DAY_COUNT_MONTH; i++) {
                    string tgtWorkEndCell = KinmuhyoConst._COL_TAIKIN_HH + (i + KinmuhyoConst._ROW_IDX_SHEET_HEADER);
                    var tgtWorkEndHH = sheet.Cells[tgtWorkEndCell].Value;
                    if (tgtWorkEndHH == null) {
                        continue;
                    }
                    string strWorkEndHH = tgtWorkEndHH.ToString();
                    int intWorkEndHH;
                    if (int.TryParse(strWorkEndHH, out intWorkEndHH)) {
                        if (TimeConst._HOURS_A_DAY < intWorkEndHH) {
                            NightShiftRows.Add(i + KinmuhyoConst._ROW_IDX_SHEET_HEADER);
                        }
                    }
                }
                // 夜勤編集
                NightShiftRows.ForEach(n => EditNightShiftCells(sheet, n, userType));

                // 保存
                package.SaveAs(new FileInfo(NightShiftPath));
            }
            xlsxFile.Close();
            CommonUtil.DeleteFile(OriginXlsxPath);
            File.Copy(NightShiftPath, OriginXlsxPath);
            CommonUtil.DeleteFile(NightShiftPath);
            LogAndConsoleUtil.ShowLogAndConsoleInfo(SharedMessages._MSG_NIGHT_SHIFT_END);
        }
        /// <summary>
        /// Excelセル編集（夜勤）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="RowNum"></param>
        /// <param name="userType"></param>
        private void EditNightShiftCells(ExcelWorksheet sheet, int RowNum, KinmuhyoConst.UserType userType) {
            // 退勤時刻
            string tgtCell = KinmuhyoConst._COL_TAIKIN_HH + RowNum;
            var tgtWeHour = sheet.Cells[tgtCell].Value;
            if (tgtWeHour != null) {
                string workEndHour = tgtWeHour.ToString();
                int intWeHour;
                if (int.TryParse(workEndHour, out intWeHour)) {
                    sheet.Cells[tgtCell].Value = intWeHour - TimeConst._HOURS_A_DAY;
                }
            }
            // 実働時間
            decimal jitsudouHH = KinmuhyoConst._NIGHT_SHIFT_WARIMASHI_TIME + KinmuhyoConst._NIGHT_SHIFT_SHINYA_TIME;
            EditCell(sheet, KinmuhyoConst._COL_JITSUDOU_HH, RowNum, jitsudouHH);
            EditCell(sheet, KinmuhyoConst._COL_JITSUDOU_MM, RowNum, decimal.Zero);

            // 休憩時間
            EditCell(sheet, KinmuhyoConst._COL_KYUKEI_HH, RowNum, KinmuhyoConst._NIGHT_SHIFT_KYUKEI_TIME);
            EditCell(sheet, KinmuhyoConst._COL_KYUKEI_MM, RowNum, decimal.Zero);

            // 時間外時間
            decimal zangyoTotal = GetZangyoTime(sheet, RowNum);
            decimal zangyoHH = GetManHourH(zangyoTotal);
            decimal zangyoMM = GetManHourM(zangyoTotal);
            if (zangyoTotal != decimal.Zero) {
                EditCell(sheet, KinmuhyoConst._COL_ZANGYO_HH, RowNum, zangyoHH);
                EditCell(sheet, KinmuhyoConst._COL_ZANGYO_MM, RowNum, zangyoMM);
            }
            // 夜勤時間
            EditCell(sheet, KinmuhyoConst._COL_WARIMASHI_HH, RowNum, KinmuhyoConst._NIGHT_SHIFT_WARIMASHI_TIME);
            EditCell(sheet, KinmuhyoConst._COL_WARIMASHI_MM, RowNum, decimal.Zero);
            EditCell(sheet, KinmuhyoConst._COL_SHINYA_HH, RowNum, KinmuhyoConst._NIGHT_SHIFT_SHINYA_TIME);
            EditCell(sheet, KinmuhyoConst._COL_SHINYA_MM, RowNum, decimal.Zero);

            // 作業時間
            EditWorkTime(sheet, RowNum, jitsudouHH, zangyoTotal, userType);
        }
        /// <summary>
        /// 作業時間編集（夜勤）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="RowNum"></param>
        /// <param name="jitsudouHH"></param>
        /// <param name="zangyoTotal"></param>
        /// <param name="userType"></param>
        private void EditWorkTime(ExcelWorksheet sheet, int RowNum, decimal jitsudouHH, decimal zangyoTotal, KinmuhyoConst.UserType userType) {
            // 対象日の作業・時間をすべて取得
            var nextWorkTimes = GetWorkTimes(sheet, RowNum + 1);

            switch (userType) {
                //==========================================================
                // デフォルト作業使用者
                case KinmuhyoConst.UserType.DEFAULT_WORK:
                    if (nextWorkTimes.Count == 0) {
                        var weekDay = sheet.Cells[KinmuhyoConst._COL_WEEK_DAY + (RowNum + 1)].Value.ToString();
                        var holiday = sheet.Cells[KinmuhyoConst._COL_HOLI_DAY + (RowNum + 1)].Value;
                        if (!KinmuhyoConst._WEEK_END_DAYS.Contains(weekDay) && holiday == null) {
                            // 「夜勤明け」を記載
                            string cellRange = ColConvertUtil.ToAlphabet(KinmuhyoConst._COL_IDX_FIRST_WORK_NAME) + (RowNum + 1);
                            sheet.Cells[cellRange].Value = KinmuhyoConst._MSG_END_OF_NIGHT_SHIFT;
                            AddNightShiftEndToMst(sheet);
                            return;
                        }
                    }
                    break;
                //==========================================================
                // TimeTracker作業使用者
                case KinmuhyoConst.UserType.TIME_TRACKER:
                    if (nextWorkTimes.Count != 0) {
                        // 翌日の作業から差分時間を吸収
                        var tgtWorkTimes = AbsorbNextDaysWorkTTUser(sheet, RowNum, jitsudouHH, zangyoTotal, nextWorkTimes);

                        // 夜勤明け判定
                        bool isEndOfNightShift = false;
                        var totalManHour = nextWorkTimes.Sum(n => n.ManHour);
                        if (totalManHour == 0) {
                            var weekDay = sheet.Cells[KinmuhyoConst._COL_WEEK_DAY + (RowNum + 1)].Value.ToString();
                            var holiday = sheet.Cells[KinmuhyoConst._COL_HOLI_DAY + (RowNum + 1)].Value;
                            if (!KinmuhyoConst._WEEK_END_DAYS.Contains(weekDay) && holiday == null) {
                                isEndOfNightShift = true;
                            }
                        }
                        // 対象日・翌日の作業・時間を再編集
                        ReEditWorkTime(sheet, RowNum, tgtWorkTimes, false);
                        ReEditWorkTime(sheet, RowNum + 1, nextWorkTimes, isEndOfNightShift);
                    }
                    break;
            }
        }
        /// <summary>
        /// 翌日の作業から差分時間を吸収（TimeTracker使用者）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="RowNum"></param>
        /// <param name="jitsudouHH"></param>
        /// <param name="zangyoTotal"></param>
        /// <param name="nextWorkTimes"></param>
        /// <returns></returns>
        private List<EditTimeEntryDto> AbsorbNextDaysWorkTTUser(ExcelWorksheet sheet, int RowNum, decimal jitsudouHH, decimal zangyoTotal, List<EditTimeEntryDto> nextWorkTimes) {
            var tgtWorkTimes = GetWorkTimes(sheet, RowNum);

            // 対象日の実働時間 - 作業時間の合計を取得
            decimal totalWorkHH = tgtWorkTimes.Sum(n => n.ManHourH);
            decimal totalWorkMM = tgtWorkTimes.Sum(n => n.ManHourM);
            decimal totalWorkTime = (totalWorkHH * TimeConst._MINUTE_AN_HOUR) + totalWorkMM;
            decimal KadouTime = (jitsudouHH * TimeConst._MINUTE_AN_HOUR) + zangyoTotal;
            decimal gap = KadouTime - totalWorkTime;

            // 翌日の作業から差分時間を吸収
            foreach (var nextWork in nextWorkTimes) {
                decimal nextWorkTime = (nextWork.ManHourH * TimeConst._MINUTE_AN_HOUR) + nextWork.ManHourM;
                var tgtWork = tgtWorkTimes.Where(n => n.ProjectName == nextWork.ProjectName).FirstOrDefault();
                if (tgtWork == null) {
                    // 当日と同じ作業がない
                    var dto = new EditTimeEntryDto() {
                        ProjectName = nextWork.ProjectName,
                    };
                    dto = AbsorbNextDaysWork_Calc(dto, nextWork, nextWorkTime, ref gap);
                    tgtWorkTimes.Add(dto);
                }
                // 当日と同じ作業がある
                tgtWork = AbsorbNextDaysWork_Calc(tgtWork, nextWork, nextWorkTime, ref gap);
            }
            return tgtWorkTimes;
        }
        /// <summary>
        /// 翌日の作業から差分時間を吸収（実計算処理）
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="nextWork"></param>
        /// <param name="nextWorkTime"></param>
        /// <param name="gap"></param>
        /// <returns></returns>
        private EditTimeEntryDto AbsorbNextDaysWork_Calc(EditTimeEntryDto dto, EditTimeEntryDto nextWork, decimal nextWorkTime, ref decimal gap) {
            if (nextWorkTime <= gap) {
                // 翌日の作業時間＜=稼働時間とのギャップの場合、
                // 翌日の作業時間をすべて当日に吸収する
                dto.ManHour += nextWorkTime;
                nextWork.ManHourH = 0;
                nextWork.ManHourM = 0;
                nextWork.ManHour = 0;
                dto.ManHourH = GetManHourH(dto.ManHour);
                dto.ManHourM = GetManHourM(dto.ManHour);

                // 稼働時間とのギャップから翌日の作業時間を減算
                DelNextWorkTimeFromGap(ref gap, nextWorkTime);

                return dto;
            }
            // 翌日の作業時間＜=稼働時間とのギャップの場合、
            // 翌日の作業時間から稼働時間とのギャップ時間分だけ当日に吸収する
            dto.ManHour += gap;
            nextWork.ManHour -= gap;
            nextWork.ManHourH = GetManHourH(nextWork.ManHour);
            nextWork.ManHourM = GetManHourM(nextWork.ManHour);
            dto.ManHourH = GetManHourH(dto.ManHour);
            dto.ManHourM = GetManHourM(dto.ManHour);

            // 稼働時間とのギャップから翌日の作業時間を減算
            DelNextWorkTimeFromGap(ref gap, nextWorkTime);

            return dto;
        }
        /// <summary>
        /// 稼働時間とのギャップから翌日の作業時間を減算
        /// </summary>
        /// <param name="del"></param>
        /// <param name="nextWorkTime"></param>
        private void DelNextWorkTimeFromGap(ref decimal del, decimal nextWorkTime) {
            del -= nextWorkTime;
            if (del < 0) {
                del = 0;
            }
        }
        /// <summary>
        /// 対象日の作業・時間を再編集
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="RowNum"></param>
        /// <param name="WorkTimes"></param>
        /// <param name="isEndOfNightShift"></param>
        private void ReEditWorkTime(ExcelWorksheet sheet, int RowNum, List<EditTimeEntryDto> WorkTimes, bool isEndOfNightShift) {
            int idx = KinmuhyoConst._COL_IDX_FIRST_WORK_NAME;
            WorkTimes = WorkTimes.OrderBy(n => n.ManHour).ToList();
            foreach (var tgtWork in WorkTimes) {
                if (isEndOfNightShift && idx == KinmuhyoConst._COL_IDX_FIRST_WORK_NAME) {
                    sheet.Cells[ColConvertUtil.ToAlphabet(idx) + RowNum].Value = KinmuhyoConst._MSG_END_OF_NIGHT_SHIFT;
                    sheet.Cells[ColConvertUtil.ToAlphabet(idx + 1) + RowNum].Value = string.Empty;
                    sheet.Cells[ColConvertUtil.ToAlphabet(idx + 2) + RowNum].Value = string.Empty;

                    // 作業マスタに「夜勤明け」を記載
                    AddNightShiftEndToMst(sheet);
                    idx += KinmuhyoConst._COL_INTERVAL_WORK_DETAILS_WORK;
                    continue;
                }
                if (isEndOfNightShift || tgtWork.ManHour == 0) {
                    sheet.Cells[ColConvertUtil.ToAlphabet(idx)     + RowNum].Value = string.Empty;
                    sheet.Cells[ColConvertUtil.ToAlphabet(idx + 1) + RowNum].Value = string.Empty;
                    sheet.Cells[ColConvertUtil.ToAlphabet(idx + 2) + RowNum].Value = string.Empty;
                    idx += KinmuhyoConst._COL_INTERVAL_WORK_DETAILS_WORK;
                    continue;
                }
                sheet.Cells[ColConvertUtil.ToAlphabet(idx) + RowNum].Value = tgtWork.ProjectName;
                sheet.Cells[ColConvertUtil.ToAlphabet(idx + 1) + RowNum].Value = tgtWork.ManHourH;
                sheet.Cells[ColConvertUtil.ToAlphabet(idx + 2) + RowNum].Value = tgtWork.ManHourM;
                idx += KinmuhyoConst._COL_INTERVAL_WORK_DETAILS_WORK;
            }
        }
        /// <summary>
        /// 作業マスタに「夜勤明け」を記載
        /// </summary>
        /// <param name="sheet"></param>
        private void AddNightShiftEndToMst(ExcelWorksheet sheet) {
            for (int i = KinmuhyoConst._ROW_IDX_MST_START; i < KinmuhyoConst._ROW_IDX_MST_END - 1; i++) {
                if (sheet.Cells[KinmuhyoConst._COL_MST_NAME + i].Value == null
                 && sheet.Cells[KinmuhyoConst._COL_MST_NAME + (i - 1)].Value != null
                 && sheet.Cells[KinmuhyoConst._COL_MST_NAME + (i - 1)].Value.ToString() != KinmuhyoConst._MSG_END_OF_NIGHT_SHIFT) {
                    sheet.Cells[KinmuhyoConst._COL_MST_NAME + i].Value = KinmuhyoConst._MSG_END_OF_NIGHT_SHIFT;
                    sheet.Cells[KinmuhyoConst._COL_MST_HIDDEN_WORK_TIME + (i - KinmuhyoConst._ROW_IDX_HIDDEN_WORK_BUF)].Value = 0;
                    return;
                }
            }
        }
        /// <summary>
        /// 対象日の作業・時間をすべて取得（汎用）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="RowNum"></param>
        /// <returns></returns>
        private List<EditTimeEntryDto> GetWorkTimes(ExcelWorksheet sheet, int RowNum) {
            var ret = new List<EditTimeEntryDto>();
            for (int FirstWorkColNum = KinmuhyoConst._COL_IDX_FIRST_WORK_NAME; FirstWorkColNum < KinmuhyoConst._MAX_COL_IDX_WORK_NAME;
                FirstWorkColNum += KinmuhyoConst._COL_INTERVAL_WORK_DETAILS_WORK) {
                var workNameCell = ColConvertUtil.ToAlphabet(FirstWorkColNum);
                var val = sheet.Cells[workNameCell + RowNum].Value;
                if (val != null) {
                    var dto = new EditTimeEntryDto() {
                        ProjectName = val.ToString(),
                        ManHourH = int.Parse(sheet.Cells[ColConvertUtil.ToAlphabet(FirstWorkColNum + 1) + RowNum].Value.ToString()),
                        ManHourM = int.Parse(sheet.Cells[ColConvertUtil.ToAlphabet(FirstWorkColNum + 2) + RowNum].Value.ToString()),
                    };
                    dto.ManHour = (dto.ManHourH * TimeConst._MINUTE_AN_HOUR) + dto.ManHourM;
                    ret.Add(dto);
                }
            }
            return ret;
        }
        /// <summary>
        /// 残業時間算出
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="RowNum"></param>
        /// <returns></returns>
        private decimal GetZangyoTime(ExcelWorksheet sheet, int RowNum) {
            // 前残業
            decimal ret = decimal.Zero;
            string outShukkinHHCell = KinmuhyoConst._COL_SHUKKIN_HH + RowNum;
            string outShukkinMMCell = KinmuhyoConst._COL_SHUKKIN_MM + RowNum;
            var shukkinHH = sheet.Cells[outShukkinHHCell].Value;
            var shukkinMM = sheet.Cells[outShukkinMMCell].Value;
            if (shukkinHH != null && shukkinMM != null) {
                int intShukkinHH;
                int intShukkinMM;
                if (int.TryParse(shukkinHH.ToString(), out intShukkinHH) && int.TryParse(shukkinMM.ToString(), out intShukkinMM)) {
                    int nightShiftDefaultShukkin = KinmuhyoConst._NIGHT_SHIFT_START_HH * TimeConst._MINUTE_AN_HOUR;
                    if ((intShukkinHH * TimeConst._MINUTE_AN_HOUR) + intShukkinMM < nightShiftDefaultShukkin) {
                        ret += nightShiftDefaultShukkin - ((intShukkinHH * TimeConst._MINUTE_AN_HOUR) + intShukkinMM);
                    }
                }
            }
            // 後残業
            string outTaikinHHCell = KinmuhyoConst._COL_TAIKIN_HH + RowNum;
            string outTaikinMMCell = KinmuhyoConst._COL_TAIKIN_MM + RowNum;
            var taikinHH = sheet.Cells[outTaikinHHCell].Value;
            var taikinMM = sheet.Cells[outTaikinMMCell].Value;
            if (taikinHH != null && taikinMM != null) {
                int intTaikinHH;
                int intTaikinMM;
                if (int.TryParse(taikinHH.ToString(), out intTaikinHH) && int.TryParse(taikinMM.ToString(), out intTaikinMM)) {
                    int nightShiftDefaultTaikin = KinmuhyoConst._NIGHT_SHIFT_END_HH * TimeConst._MINUTE_AN_HOUR;
                    if (nightShiftDefaultTaikin < (intTaikinHH * TimeConst._MINUTE_AN_HOUR) + intTaikinMM) {
                        ret += (intTaikinHH * TimeConst._MINUTE_AN_HOUR) + intTaikinMM - nightShiftDefaultTaikin;
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// Excelセル編集（汎用）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="Col"></param>
        /// <param name="RowNum"></param>
        /// <param name="val"></param>
        private void EditCell(ExcelWorksheet sheet, string Col, int RowNum, object val) {
            string tgtCell = Col + RowNum;
            sheet.Cells[tgtCell].Value = val;
        }
        /// <summary>
        /// 工数（時）取得
        /// </summary>
        /// <param name="manHour"></param>
        /// <returns></returns>
        public int GetManHourH(decimal manHour) {
            return (int)Math.Truncate(manHour / TimeConst._MINUTE_AN_HOUR);
        }
        /// <summary>
        /// 工数（分）取得
        /// </summary>
        /// <param name="manHour"></param>
        /// <returns></returns>
        public int GetManHourM(decimal manHour) {
            return (int)(manHour % TimeConst._MINUTE_AN_HOUR);
        }
    }
}
