﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using TimeEntryShared.Const;
using TimeEntryShared.Dto;
using TimeEntryShared.Entity;
using TimeEntryShared.Util;

namespace TimeEntryShared.Service {
    public class WorkTimeAnalysisService {
        /// <summary>
        /// TimeEntry.config
        /// </summary>
        private TimeEntryConfigEntity _ConfigInfo = new TimeEntryConfigEntity();
        /// <summary>
        /// TimeTrackerユーザ情報
        /// </summary>
        private List<UserInfoDto> _TimeTrackerUserInfos = new List<UserInfoDto>();
        /// <summary>
        /// 集計日付（開始）
        /// </summary>
        private string _SumStartDate = string.Empty;
        /// <summary>
        /// 集計日付（終了）
        /// </summary>
        private string _SumEndDate = string.Empty;
        /// <summary>
        /// 一時フォルダ名
        /// </summary>
        private const string _TEMP_FOLDER_NAME = "Temp";
        /// <summary>
        /// 各シート記載欄の最大列番号
        /// </summary>
        private int _MaxColNum = 0;

        /// <summary>
        /// メイン処理
        /// </summary>
        /// <param name="sumStartDate"></param>
        /// <param name="sumEndDate"></param>
        /// <param name="outputExcelPath"></param>
        /// <param name="tgtTTGroupName"></param>
        /// <returns></returns>
        public int Main(string sumStartDate, string sumEndDate, string outputExcelPath, string tgtTTGroupName = "") {
            var configInfo = ModelUtil.LoadConfig();
            return Main(configInfo, sumStartDate, sumEndDate, outputExcelPath, tgtTTGroupName);
        }
        /// <summary>
        /// メイン処理
        /// </summary>
        /// <param name="configInfo"></param>
        /// <param name="sumStartDate"></param>
        /// <param name="sumEndDate"></param>
        /// <param name="outputExcelPath"></param>
        /// <param name="tgtTTGroupName"></param>
        public int Main(TimeEntryConfigEntity configInfo, string sumStartDate, string sumEndDate, string outputExcelPath, string tgtTTGroupName = "") {
            try {
                // Configロード
                _ConfigInfo = configInfo;
                _SumStartDate = sumStartDate;
                _SumEndDate = sumEndDate;
                _TimeTrackerUserInfos = TimeTrackerUtil.LoadTimeTrackerUserInfo();

                // 工数分析結果取得
                var AnalysisRecords = GetEditedAnalysis(tgtTTGroupName);
                if (AnalysisRecords.Count == 0) {
                    return 0;
                }
                // 一時フォルダ作成
                string tempFolder = Path.Combine(Environment.CurrentDirectory, _TEMP_FOLDER_NAME);
                CommonUtil.CreateFolder(tempFolder);

                // Excelファイル原本をコピー
                string WorkFolderXlsxPath = Path.Combine(tempFolder, string.Concat(WtAnalysisConst._FILE_NAME_EXCEL, WtAnalysisConst._EX_XLSX));
                CommonUtil.DeleteFile(WorkFolderXlsxPath);
                string currentXlsxPath = Path.Combine(Environment.CurrentDirectory, CommonLiteral._FOLDER_NAME_EXCEL);
                currentXlsxPath = Path.Combine(currentXlsxPath, string.Concat(WtAnalysisConst._FILE_NAME_EXCEL, WtAnalysisConst._EX_XLSX));
                File.Copy(currentXlsxPath, WorkFolderXlsxPath);

                // 集計月の取得
                var dicSumMonth = WorkTimeAnalysisUtil.GetSumMonth(AnalysisRecords);

                // Excel出力
                DateTime dtStart = DateTime.Parse(_SumStartDate);
                DateTime dtEnd = DateTime.Parse(_SumEndDate);
                string savePath = WorkTimeAnalysisUtil.GetWTAnalysisExcel(dtStart, dtEnd, outputExcelPath);
                CreateAnalysisExcel(WorkFolderXlsxPath, savePath, AnalysisRecords, dicSumMonth);

                // 一時的に作成したオブジェクトを削除
                CommonUtil.DeleteFile(WorkFolderXlsxPath);
                CommonUtil.DeleteFolder(tempFolder);

                LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Format(WorkTimeAnalysisMessages._MSG_OUTPUT_EXCEL_PATH, savePath));
                return AnalysisRecords.Count;
            } catch (Exception ex) {
                LogAndConsoleUtil.ShowLogAndConsoleErr(ex.Message, ex.StackTrace);
                return -1;
            }
        }

        #region グリッド
        /// <summary>
        /// 工数分析グリッドDto作成
        /// </summary>
        /// <param name="configInfo"></param>
        /// <param name="sumStartDate"></param>
        /// <param name="sumEndDate"></param>
        /// <param name="tgtTTGroupName"></param>
        /// <returns></returns>
        public BindingList<WTAnalysisGridDto> GetWTAnalysisGridDto(TimeEntryConfigEntity configInfo, string sumStartDate, string sumEndDate, string tgtTTGroupName = "") {
            var ret = new BindingList<WTAnalysisGridDto>();
            try {
                // Configロード
                _ConfigInfo = configInfo;
                _SumStartDate = sumStartDate;
                _SumEndDate = sumEndDate;
                _TimeTrackerUserInfos = TimeTrackerUtil.LoadTimeTrackerUserInfo();

                // 工数分析結果取得
                var AnalysisRecords = GetEditedAnalysis(tgtTTGroupName);
                if (AnalysisRecords.Count == 0) {
                    return ret;
                }
                // ソート
                AnalysisRecords = AnalysisRecords.OrderBy(n => n.organization)
                                                  .ThenBy(n => n.userName)
                                                  .ThenBy(n => n.projectCode)
                                                  .ThenBy(n => n.date)
                                                  .ToList();

                // 対象月リスト作成
                var tgtMonthList = new List<DateTime>();
                var startMonth = DateTime.Parse(_SumStartDate);
                var endMonth = DateTime.Parse(_SumEndDate).AddDays(1).AddMonths(-1);
                for (var dt = startMonth; dt <= endMonth; dt = dt.AddMonths(1)) {
                    tgtMonthList.Add(new DateTime(dt.Year, dt.Month, 1));
                }
                // グリッドdto作成
                ret = CreateGridDtoList(AnalysisRecords, tgtMonthList);
                return ret;
            } catch (Exception ex) {
                LogAndConsoleUtil.ShowLogAndConsoleErr(ex.Message, ex.StackTrace);
            }
            return ret;
        }
        /// <summary>
        /// グリッドdto群作成
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <param name="tgtMonthList"></param>
        /// <returns></returns>
        private BindingList<WTAnalysisGridDto> CreateGridDtoList(List<WorkTimeAnalysisEntity> AnalysisRecords, List<DateTime> tgtMonthList) {
            var ret = new BindingList<WTAnalysisGridDto>();
            string oldUserId = string.Empty;
            foreach (var data in AnalysisRecords) {

                // 小計行追加
                if (!string.IsNullOrEmpty(oldUserId) && oldUserId != data.userId) {
                    var dto = GetSumRow(AnalysisRecords, data, tgtMonthList, oldUserId);
                    ret.Add(dto);
                }
                if (ret.Where(n => n.ProjectId == data.projectId && n.UserId == data.userId).ToList().Count == 0) {

                    // グリッドdto作成
                    var dto = CreateGridDto(AnalysisRecords, tgtMonthList, ret, data);
                    ret.Add(dto);
                }
                var dataMonth = new DateTime(DateTime.Parse(data.date).Year, DateTime.Parse(data.date).Month, 1);
                var tgtDto = ret.Where(n => n.ProjectId == data.projectId && n.UserId == data.userId).ToList().FirstOrDefault();
                if (tgtDto == null) {
                    oldUserId = data.userId;
                    continue;
                }
                // 計画工数取得
                tgtDto = GetPlannedData(data, tgtDto, dataMonth);

                // 小計行追加（最終行）
                if (AnalysisRecords.IndexOf(data) == AnalysisRecords.Count - 1) {
                    var dto = GetSumRow(AnalysisRecords, data, tgtMonthList, data.userId);
                    ret.Add(dto);
                }
                oldUserId = data.userId;
            }
            return ret;
        }
        /// <summary>
        /// グリッドdto作成
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <param name="tgtMonthList"></param>
        /// <param name="bindingList"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private WTAnalysisGridDto CreateGridDto(List<WorkTimeAnalysisEntity> AnalysisRecords, List<DateTime> tgtMonthList, BindingList<WTAnalysisGridDto> bindingList
            , WorkTimeAnalysisEntity data) {

            var ret = new WTAnalysisGridDto();
            ret.SetTgtMonth(tgtMonthList);

            // 明細キー情報取得
            ret.ProjectId = data.projectId;
            ret.UserId = data.userId;
            ret.ChargeNo = data.projectCode;
            ret.WorkName = data.projectName;
            if (bindingList.Where(n => n.EmploeeName == data.userName).ToList().Count == 0) {
                ret.EmploeeName = data.userName;
            }
            var tgtActual = AnalysisRecords.Where(n => DateTime.Parse(n.date) == ret.TgtMonth1).FirstOrDefault();
            if (tgtActual != null) {
                // 当月実績工数取得
                ret.ActualTime = CommonUtil.ConvertWorkTimeToHHMMColon(decimal.Parse(data.actualTime));
                ret.ActualManHour = data.CalcDatas.decActualManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.ActualWorkPct = data.CalcDatas.decActualPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.ActualManMoon = data.CalcDatas.decActualHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            return ret;
        }
        /// <summary>
        /// （グリッド）計画工数取得
        /// </summary>
        /// <param name="data"></param>
        /// <param name="ret"></param>
        /// <param name="dataMonth"></param>
        /// <returns></returns>
        private WTAnalysisGridDto GetPlannedData(WorkTimeAnalysisEntity data, WTAnalysisGridDto ret, DateTime dataMonth) {
            if (dataMonth == ret.TgtMonth1) {
                ret.PlannedTime1 = CommonUtil.ConvertWorkTimeToHHMMColon(decimal.Parse(data.plannedTime));
                ret.PlannedManHour1 = data.CalcDatas.decPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct1 = data.CalcDatas.decPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon1 = data.CalcDatas.decPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            if (dataMonth == ret.TgtMonth2) {
                ret.PlannedTime2 = CommonUtil.ConvertWorkTimeToHHMMColon(decimal.Parse(data.plannedTime));
                ret.PlannedManHour2 = data.CalcDatas.decPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct2 = data.CalcDatas.decPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon2 = data.CalcDatas.decPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            if (dataMonth == ret.TgtMonth3) {
                ret.PlannedTime3 = CommonUtil.ConvertWorkTimeToHHMMColon(decimal.Parse(data.plannedTime));
                ret.PlannedManHour3 = data.CalcDatas.decPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct3 = data.CalcDatas.decPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon3 = data.CalcDatas.decPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            if (dataMonth == ret.TgtMonth4) {
                ret.PlannedTime4 = CommonUtil.ConvertWorkTimeToHHMMColon(decimal.Parse(data.plannedTime));
                ret.PlannedManHour4 = data.CalcDatas.decPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct4 = data.CalcDatas.decPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon4 = data.CalcDatas.decPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            if (dataMonth == ret.TgtMonth5) {
                ret.PlannedTime5 = CommonUtil.ConvertWorkTimeToHHMMColon(decimal.Parse(data.plannedTime));
                ret.PlannedManHour5 = data.CalcDatas.decPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct5 = data.CalcDatas.decPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon5 = data.CalcDatas.decPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            if (dataMonth == ret.TgtMonth6) {
                ret.PlannedTime6 = CommonUtil.ConvertWorkTimeToHHMMColon(decimal.Parse(data.plannedTime));
                ret.PlannedManHour6 = data.CalcDatas.decPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct6 = data.CalcDatas.decPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon6 = data.CalcDatas.decPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            return ret;
        }
        /// <summary>
        /// （グリッド）小計行dto取得
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <param name="data"></param>
        /// <param name="tgtMonthList"></param>
        /// <param name="oldUserId"></param>
        /// <returns></returns>
        private WTAnalysisGridDto GetSumRow(List<WorkTimeAnalysisEntity> AnalysisRecords, WorkTimeAnalysisEntity data,  List<DateTime> tgtMonthList, string oldUserId) {
            var ret = new WTAnalysisGridDto();
            ret.SetTgtMonth(tgtMonthList);
            ret.ProjectId = string.Empty;
            ret.UserId = oldUserId;
            ret.ChargeNo = CommonLiteral._LITERAL_HYPHEN;
            ret.WorkName = WtAnalysisConst._SUM_VALUE;

            var tgtActual = GetTgtAnalysisRec(AnalysisRecords, oldUserId, ret.TgtMonth1);
            if (tgtActual != null) {
                ret.ActualTime = CommonUtil.ConvertWorkTimeToHHMMColon(tgtActual.CalcDatas.totalActualTime);
                ret.ActualManHour = tgtActual.CalcDatas.totalActualManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.ActualWorkPct = tgtActual.CalcDatas.totalActualPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.ActualManMoon = tgtActual.CalcDatas.totalActualHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            var tgtPlanned1 = GetTgtAnalysisRec(AnalysisRecords, oldUserId, ret.TgtMonth1);
            if (tgtPlanned1 != null) {
                ret.PlannedTime1 = CommonUtil.ConvertWorkTimeToHHMMColon(tgtPlanned1.CalcDatas.totalPlannedTime);
                ret.PlannedManHour1 = tgtPlanned1.CalcDatas.totalPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct1 = tgtPlanned1.CalcDatas.totalPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon1 = tgtPlanned1.CalcDatas.totalPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            var tgtPlanned2 = GetTgtAnalysisRec(AnalysisRecords, oldUserId, ret.TgtMonth2);
            if (tgtPlanned2 != null) {
                ret.PlannedTime2 = CommonUtil.ConvertWorkTimeToHHMMColon(tgtPlanned2.CalcDatas.totalPlannedTime);
                ret.PlannedManHour2 = tgtPlanned2.CalcDatas.totalPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct2 = tgtPlanned2.CalcDatas.totalPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon2 = tgtPlanned2.CalcDatas.totalPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            var tgtPlanned3 = GetTgtAnalysisRec(AnalysisRecords, oldUserId, ret.TgtMonth3);
            if (tgtPlanned3 != null) {
                ret.PlannedTime3 = CommonUtil.ConvertWorkTimeToHHMMColon(tgtPlanned3.CalcDatas.totalPlannedTime);
                ret.PlannedManHour3 = tgtPlanned3.CalcDatas.totalPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct3 = tgtPlanned3.CalcDatas.totalPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon3 = tgtPlanned3.CalcDatas.totalPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            var tgtPlanned4 = GetTgtAnalysisRec(AnalysisRecords, oldUserId, ret.TgtMonth4);
            if (tgtPlanned4 != null) {
                ret.PlannedTime4 = CommonUtil.ConvertWorkTimeToHHMMColon(tgtPlanned4.CalcDatas.totalPlannedTime);
                ret.PlannedManHour4 = tgtPlanned4.CalcDatas.totalPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct4 = tgtPlanned4.CalcDatas.totalPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon4 = tgtPlanned4.CalcDatas.totalPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            var tgtPlanned5 = GetTgtAnalysisRec(AnalysisRecords, oldUserId, ret.TgtMonth5);
            if (tgtPlanned5 != null) {
                ret.PlannedTime5 = CommonUtil.ConvertWorkTimeToHHMMColon(tgtPlanned5.CalcDatas.totalPlannedTime);
                ret.PlannedManHour5 = tgtPlanned5.CalcDatas.totalPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct5 = tgtPlanned5.CalcDatas.totalPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon5 = tgtPlanned5.CalcDatas.totalPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            var tgtPlanned6 = GetTgtAnalysisRec(AnalysisRecords, oldUserId, ret.TgtMonth6);
            if (tgtPlanned6 != null) {
                ret.PlannedTime6 = CommonUtil.ConvertWorkTimeToHHMMColon(tgtPlanned6.CalcDatas.totalPlannedTime);
                ret.PlannedManHour6 = tgtPlanned6.CalcDatas.totalPlannedManHour.ToString(FormatConst._FORMAT_FLOAT_2_HUMANMOON);
                ret.PlannedWorkPct6 = tgtPlanned6.CalcDatas.totalPlannedPct.ToString(FormatConst._FORMAT_FLOAT_1_PCT);
                ret.PlannedManMoon6 = tgtPlanned6.CalcDatas.totalPlannedHumanMoon.ToString(FormatConst._FORMAT_FLOAT_2);
            }
            return ret;
        }
        #endregion

        #region 共通処理
        /// <summary>
        /// ユーザIDと対象月からEntityを絞り込む
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <param name="oldUserId"></param>
        /// <param name="tgtMonth"></param>
        /// <returns></returns>
        private WorkTimeAnalysisEntity GetTgtAnalysisRec(List<WorkTimeAnalysisEntity> AnalysisRecords, string oldUserId, DateTime tgtMonth) {
            return AnalysisRecords.Where(n => n.userId == oldUserId && DateTime.Parse(n.date) == tgtMonth).FirstOrDefault();
        }
        /// <summary>
        /// 分析結果レコード取得（計算済み）
        /// </summary>
        /// <param name="tgtTTGroupName"></param>
        /// <param name="using160h"></param>
        /// <returns></returns>
        private List<WorkTimeAnalysisEntity> GetEditedAnalysis(string tgtTTGroupName = "", bool using160h = true) {
            // 工数分析バッチ起動
            BatUtil.ExecBat_WorkTimeAnalysis(BatConst._OUTPUT_TGT_ALL_TT_USERS, _SumStartDate, _SumEndDate);

            // 分析結果を作業フォルダに移動
            string WorkJsonPath = Path.Combine(Environment.CurrentDirectory, string.Format(BatConst._TXT_NAME_WORKTIME_ITEMS, Environment.MachineName));

            // JSONファイル読み込み
            var json = new JsonUtil<WorkTimeAnalysisEntity>();
            var ret = json.JsonConverter_WorkTimeAnalysis(WorkJsonPath);
            CommonUtil.DeleteFile(WorkJsonPath);

            // データ編集
            DateTime dtStart = DateTime.Parse(_SumStartDate);
            DateTime dtEnd = DateTime.Parse(_SumEndDate);
            ret = WorkTimeAnalysisUtil.EditAnalysisEntities(_ConfigInfo, ret, _TimeTrackerUserInfos, dtStart, dtEnd, tgtTTGroupName, using160h);
            return ret;
        }
        /// <summary>
        /// 分析結果Excel作成
        /// </summary>
        /// <param name="OriginXlsxPath"></param>
        /// <param name="savePath"></param>
        /// <param name="AnalysisRecords"></param>
        /// <param name="dicSumMonth"></param>
        public void CreateAnalysisExcel(string OriginXlsxPath, string savePath, List<WorkTimeAnalysisEntity> AnalysisRecords, Dictionary<int, string> dicSumMonth) {
            var xlsxFile = File.OpenRead(OriginXlsxPath);
            using (var package = new ExcelPackage(new FileInfo(OriginXlsxPath))) {
                ExcelPackage.LicenseContext = OfficeOpenXml.LicenseContext.NonCommercial;

                // シート作成（プロジェクト別ピボット）
                var sheetPibotProject = package.Workbook.Worksheets[WtAnalysisConst._SHEET_NAME_PIBOT_PROJECT];
                _MaxColNum = WorkTimeAnalysisUtil.GetMaxColNum(dicSumMonth, WtAnalysisConst.SheetType.PROJECT);
                CreateSheet_PibotProject(sheetPibotProject, AnalysisRecords, dicSumMonth);

                // シート作成（要員別ピボット）
                var sheetPibotUser = package.Workbook.Worksheets[WtAnalysisConst._SHEET_NAME_PIBOT_USER];
                _MaxColNum = WorkTimeAnalysisUtil.GetMaxColNum(dicSumMonth, WtAnalysisConst.SheetType.MEMBER);
                CreateSheet_PibotMember(sheetPibotUser, AnalysisRecords, dicSumMonth);

                // シート作成（コスト総括）
                var sheetSumCost = package.Workbook.Worksheets[WtAnalysisConst._SHEET_NAME_SUM_COST];
                _MaxColNum = WorkTimeAnalysisUtil.GetMaxColNum(dicSumMonth, WtAnalysisConst.SheetType.COST);
                CreateSheet_Cost(sheetSumCost, AnalysisRecords, dicSumMonth);

                // シート作成（一覧）
                var sheetLooks = package.Workbook.Worksheets[WtAnalysisConst._SHEET_NAME_LOOKS];
                _MaxColNum = WorkTimeAnalysisUtil.GetMaxColNum(dicSumMonth, WtAnalysisConst.SheetType.LOOKS);
                CreateSheet_Looks(sheetLooks, AnalysisRecords, dicSumMonth);

                // 保存
                package.SaveAs(new FileInfo(savePath));
            }
            xlsxFile.Close();
        }
        /// <summary>
        /// シート作成（共通）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="dto"></param>
        /// <param name="dicSumMonth"></param>
        /// <param name="sumType"></param>
        /// <param name="rowIdx"></param>
        private void PostingAnalysis_Common(ExcelWorksheet sheet, WorkTimeAnalysisDto dto, Dictionary<int, string> dicSumMonth, WtAnalysisConst.SumType sumType, int rowIdx) {
            foreach (var colsDto in dto.workTimeDatas) {
                var tgtDic = dicSumMonth.Where(n => n.Value == colsDto.date).FirstOrDefault();
                if (tgtDic.Equals(null)) {
                    continue;
                }
                int tgtColNum = ((tgtDic.Key) * WtAnalysisConst._BODY_COL_COUNT_PER_MONTH) + WtAnalysisConst._BODY_COL_START_PER_MONTH;

                // ヘッダ
                PostingAnalysis_CommonH(sheet, colsDto, tgtColNum);

                // 明細
                PostingAnalysis_CommonB(sheet, colsDto, dto, sumType, rowIdx, tgtColNum);
            }
        }
        /// <summary>
        /// シート作成（共通・ヘッダー）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="colsDto"></param>
        /// <param name="tgtColNum"></param>
        private void PostingAnalysis_CommonH(ExcelWorksheet sheet, WorkTimeColsDto colsDto, int tgtColNum) {

            // 集計月
            var valHeaderMonth = sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 0) + (WtAnalysisConst._HEADER_ROW_NUM - 2)].Value;
            if (valHeaderMonth == null || string.IsNullOrEmpty(valHeaderMonth.ToString())) {
                string range = ColConvertUtil.ToAlphabet(tgtColNum + 0) + (WtAnalysisConst._HEADER_ROW_NUM - 2);
                sheet.Cells[range].Value = WorkTimeAnalysisUtil.GetHeaderMonth(_ConfigInfo.ConnStrTimeEntryDB, DateTime.Parse(colsDto.date));
            }

            var valheaderPA = sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 0) + (WtAnalysisConst._HEADER_ROW_NUM - 1)].Value;
            if (valheaderPA == null || string.IsNullOrEmpty(valheaderPA.ToString())) {
                // 計画工数
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 0) + (WtAnalysisConst._HEADER_ROW_NUM - 1)].Value = WtAnalysisConst._HEADER_COL_PLANNED;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 0) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_TIME_H;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 1) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_TIME_M;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 2) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_TIME;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 3) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_TIME_PCT;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 4) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_MAN_MOON;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 5) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_COST;

                // 実績工数
                string rangeJ = ColConvertUtil.ToAlphabet(tgtColNum + (WtAnalysisConst._BODY_COL_COUNT_PER_MONTH / 2)) + (WtAnalysisConst._HEADER_ROW_NUM - 1);
                sheet.Cells[rangeJ].Value = WtAnalysisConst._HEADER_COL_ACTUAL;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 6) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_TIME_H;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 7) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_TIME_M;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 8) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_TIME;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 9) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_TIME_PCT;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 10) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_MAN_MOON;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 11) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_COST;
            }
        }
        /// <summary>
        /// シート作成（共通・明細）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="colsDto"></param>
        /// <param name="dto"></param>
        /// <param name="sumType"></param>
        /// <param name="rowIdx"></param>
        /// <param name="tgtColNum"></param>
        private void PostingAnalysis_CommonB(ExcelWorksheet sheet, WorkTimeColsDto colsDto, WorkTimeAnalysisDto dto, WtAnalysisConst.SumType sumType
            , int rowIdx, int tgtColNum) {

            int entityIdx = dto.workTimeDatas.IndexOf(colsDto);
            if (entityIdx == 0) {
                switch (dto.rowType) {
                    case WtAnalysisConst.ExcelRowType.BODY_ROW:
                        switch (sumType) {
                            case WtAnalysisConst.SumType.MEMBER:
                                sheet.Cells[ColConvertUtil.ToAlphabet(3) + rowIdx].Value = dto.projectCode;
                                sheet.Cells[ColConvertUtil.ToAlphabet(4) + rowIdx].Value = dto.projectName;
                                break;
                            case WtAnalysisConst.SumType.PROJECT:
                                sheet.Cells[ColConvertUtil.ToAlphabet(3) + rowIdx].Value = dto.organization;
                                sheet.Cells[ColConvertUtil.ToAlphabet(4) + rowIdx].Value = dto.userName;
                                break;
                        }
                        break;
                    case WtAnalysisConst.ExcelRowType.SUMMARY:
                        sheet.Cells[ColConvertUtil.ToAlphabet(3) + rowIdx].Value = CommonLiteral._LITERAL_HYPHEN;
                        sheet.Cells[ColConvertUtil.ToAlphabet(4) + rowIdx].Value = WtAnalysisConst._SUM_VALUE;
                        break;
                }
            }
            // 計画工数
            decimal decPlanned = colsDto.decPlannedWorkTime;
            int plannedH = (int)(decPlanned / TimeConst._MINUTE_AN_HOUR);
            int plannedM = (int)(decPlanned % TimeConst._MINUTE_AN_HOUR);
            sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 0) + rowIdx].Value = plannedH;
            sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 1) + rowIdx].Value = plannedM;
            sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 2) + rowIdx].Value = colsDto.decPlannedManHour;
            if (sumType == WtAnalysisConst.SumType.PROJECT && dto.rowType == WtAnalysisConst.ExcelRowType.SUMMARY) {
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 3) + rowIdx].Value = CommonLiteral._LITERAL_HYPHEN;
            } else {
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 3) + rowIdx].Value = colsDto.decPlannedPct;
            }
            sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 4) + rowIdx].Value = colsDto.decPlannedHumanMoon;
            sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 5) + rowIdx].Value = colsDto.decPlannedCost;

            // 実績工数
            decimal decActual = colsDto.decActualWorkTime;
            int actualH = (int)(decActual / TimeConst._MINUTE_AN_HOUR);
            int actualM = (int)(decActual % TimeConst._MINUTE_AN_HOUR);
            sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 6) + rowIdx].Value = actualH;
            sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 7) + rowIdx].Value = actualM;
            sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 8) + rowIdx].Value = colsDto.decActualManHour;
            if (sumType == WtAnalysisConst.SumType.PROJECT && dto.rowType == WtAnalysisConst.ExcelRowType.SUMMARY) {
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 9) + rowIdx].Value = CommonLiteral._LITERAL_HYPHEN;
            } else {
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 9) + rowIdx].Value = colsDto.decActualPct;
            }
            sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 10) + rowIdx].Value = colsDto.decActualHumanMoon;
            sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 11) + rowIdx].Value = colsDto.decActualCost;
        }
        /// <summary>
        /// ヘッダキー項目作成
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="sumType"></param>
        private void PostingHeaderKeys_Common(ExcelWorksheet sheet, WtAnalysisConst.SumType sumType) {
            switch (sumType) {
                case WtAnalysisConst.SumType.MEMBER:
                    sheet.Cells[ColConvertUtil.ToAlphabet(1) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_ORGANIZATION;
                    sheet.Cells[ColConvertUtil.ToAlphabet(2) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_USER_NAME;
                    sheet.Cells[ColConvertUtil.ToAlphabet(3) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_CHARGE_NO;
                    sheet.Cells[ColConvertUtil.ToAlphabet(4) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_NAME;
                    break;
                case WtAnalysisConst.SumType.PROJECT:
                    sheet.Cells[ColConvertUtil.ToAlphabet(1) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_CHARGE_NO;
                    sheet.Cells[ColConvertUtil.ToAlphabet(2) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_NAME;
                    sheet.Cells[ColConvertUtil.ToAlphabet(3) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_ORGANIZATION;
                    sheet.Cells[ColConvertUtil.ToAlphabet(4) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_USER_NAME;
                    break;
                case WtAnalysisConst.SumType.COST:
                    sheet.Cells[ColConvertUtil.ToAlphabet(1) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_CHARGE_NO;
                    sheet.Cells[ColConvertUtil.ToAlphabet(2) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_NAME;
                    break;
            }
        }
        /// <summary>
        /// Excelレイアウト調整
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="recordCount"></param>
        /// <param name="sheetName"></param>
        /// <param name="sheetType"></param>
        private void EditExcelLayout_Common(ExcelWorksheet sheet, int recordCount, string sheetName, WtAnalysisConst.SheetType sheetType) {
            int bodyColStartIdx = WtAnalysisConst._BODY_COL_START_PER_MONTH;
            int bodyColcount = WtAnalysisConst._BODY_COL_COUNT_PER_MONTH;
            if (sheetType == WtAnalysisConst.SheetType.COST) {
                bodyColStartIdx = WtAnalysisConst._BODY_COL_START_COST;
                bodyColcount = WtAnalysisConst._BODY_COL_COUNT_COST;
            }
            // 各セルごとの処理
            EditExcelLayout_EachCells(sheet, recordCount, sheetName, bodyColStartIdx, bodyColcount, sheetType);

            // フィルター
            string headerColsAlphabet = ColConvertUtil.GetCellRangeAlphabeth(1, _MaxColNum - 1, WtAnalysisConst._HEADER_ROW_NUM, WtAnalysisConst._HEADER_ROW_NUM);
            sheet.Cells[headerColsAlphabet].AutoFilter = true;

            // ウィンドウの固定
            sheet.View.FreezePanes(WtAnalysisConst._HEADER_ROW_NUM + 1, bodyColStartIdx);

            // ヘッダー列背景色
            string coloringHeaderCellsAlphabet = ColConvertUtil.GetCellRangeAlphabeth(1, _MaxColNum - 1, WtAnalysisConst._HEADER_ROW_NUM - 2, WtAnalysisConst._HEADER_ROW_NUM);
            WorkTimeAnalysisUtil.SetCellColor(sheet, coloringHeaderCellsAlphabet, Color.LightGray);

            // ヘッダー列マージ
            MergeHeader_Common(sheet, bodyColStartIdx, bodyColcount, sheetType);

            // フォントサイズ
            string bodyColsAlphabet = ColConvertUtil.GetColRangeAlphabeth(1, _MaxColNum - 1);
            sheet.Cells[bodyColsAlphabet].Style.Font.Size = WtAnalysisConst._BODY_FONT_SIZE;

            // 列幅
            EditColWidth_Common(sheet, bodyColStartIdx, sheetType);

            // 列グループ化
            GroupingCols_Common(sheet, bodyColStartIdx, sheetType);

            // ピボット専用の編集
            EditLayout_Pibot(sheet, recordCount, sheetType);

            // 全体の外枠
            string allBodyCells = ColConvertUtil.GetCellRangeAlphabeth(1 , _MaxColNum - 1, WtAnalysisConst._HEADER_ROW_NUM - 2 , recordCount + WtAnalysisConst._HEADER_ROW_NUM);
            sheet.Cells[allBodyCells].Style.Border.BorderAround(ExcelBorderStyle.Thin);

            // タイトル
            sheet.Cells[1, 1].Value = WorkTimeAnalysisUtil.GetTitle(_SumStartDate, _SumEndDate, sheetType);
            sheet.Cells[1, 1].Style.Font.Size = WtAnalysisConst._TITLE_FONT_SIZE;
            sheet.Cells[1, 1].Style.Font.Bold = true;
            string cellRangeTitle = ColConvertUtil.GetCellRangeAlphabeth(1, 3, 1, 1);
            sheet.Cells[cellRangeTitle].Merge = true;

            // 出力日時
            sheet.Cells[2, 1].Value = string.Format(WtAnalysisConst._OUTPUT_DATE, DateTime.Now.ToString(FormatConst._FORMAT_DATE_YYYYMMDDHHMMSS_SLASH));
            sheet.Cells[2, 1].Style.Font.Size = WtAnalysisConst._BODY_FONT_SIZE;
            string cellRangeDate = ColConvertUtil.GetCellRangeAlphabeth(1, 1, 2, 2);
            sheet.Cells[cellRangeDate].Merge = true;
        }
        /// <summary>
        /// ヘッダー列マージ
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="bodyColStartIdx"></param>
        /// <param name="bodyColcount"></param>
        /// <param name="sheetType"></param>
        private void MergeHeader_Common(ExcelWorksheet sheet,int bodyColStartIdx, int bodyColcount, WtAnalysisConst.SheetType sheetType) {
            for (int i = bodyColStartIdx; i < _MaxColNum; i += bodyColcount) {
                int tgtRowNum = WtAnalysisConst._HEADER_ROW_NUM - 2;
                string cellRange = ColConvertUtil.GetCellRangeAlphabeth(i, bodyColcount - 1, tgtRowNum, tgtRowNum);
                sheet.Cells[cellRange].Merge = true;
            }
            for (int i = bodyColStartIdx; i < _MaxColNum; i += (bodyColcount / 2)) {
                int tgtRowStartNum = WtAnalysisConst._HEADER_ROW_NUM - 1;
                string cellRange = ColConvertUtil.GetCellRangeAlphabeth(i, (bodyColcount / 2) - 1, tgtRowStartNum, tgtRowStartNum);
                sheet.Cells[cellRange].Merge = true;
            }
        }
        /// <summary>
        /// 列幅調整
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="bodyColStartIdx"></param>
        /// <param name="sheetType"></param>
        private void EditColWidth_Common(ExcelWorksheet sheet, int bodyColStartIdx, WtAnalysisConst.SheetType sheetType) {
            sheet.Cells[sheet.Dimension.Address].AutoFitColumns();
            for (int colIdx = bodyColStartIdx; colIdx < _MaxColNum + 1; colIdx++) {
                string HeaderTxt = sheet.Cells[WtAnalysisConst._HEADER_ROW_NUM, colIdx].Value.ToString();
                switch (HeaderTxt) {
                    case WtAnalysisConst._HEADER_COL_WORK_TIME_H:
                    case WtAnalysisConst._HEADER_COL_WORK_TIME_M:
                        sheet.Column(colIdx).Width = WtAnalysisConst._BODY_WORK_TIME_HM_WIDTH;
                        break;
                    case WtAnalysisConst._HEADER_COL_WORK_TIME:
                        if (sheetType == WtAnalysisConst.SheetType.COST) {
                            sheet.Cells[WtAnalysisConst._HEADER_ROW_NUM, colIdx].Value += WtAnalysisConst._UNIT_HEADER_WORK_TIME;
                            sheet.Column(colIdx).Width = WtAnalysisConst._BODY_PER_MONTH_WIDTH + 3.5;
                            break;
                        }
                        sheet.Column(colIdx).Width = WtAnalysisConst._BODY_PER_MONTH_WIDTH + 2;
                        break;
                    case WtAnalysisConst._HEADER_COL_COST:
                        sheet.Column(colIdx).Width = WtAnalysisConst._BODY_PER_MONTH_WIDTH + 2;
                        break;
                    case WtAnalysisConst._HEADER_COL_COST_1000:
                        sheet.Column(colIdx).Width = WtAnalysisConst._BODY_PER_MONTH_WIDTH + 3.5;
                        if (sheetType == WtAnalysisConst.SheetType.COST) {
                            sheet.Cells[WtAnalysisConst._HEADER_ROW_NUM, colIdx].Value += WtAnalysisConst._UNIT_HEADER_COST_1000;
                        }
                        break;
                    default:
                        sheet.Column(colIdx).Width = WtAnalysisConst._BODY_PER_MONTH_WIDTH;
                        break;
                }
            }
        }
        /// <summary>
        /// 列グループ化
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="bodyColStartIdx"></param>
        /// <param name="sheetType"></param>
        private void GroupingCols_Common(ExcelWorksheet sheet, int bodyColStartIdx, WtAnalysisConst.SheetType sheetType) {
            if (sheetType == WtAnalysisConst.SheetType.COST) {
                return;
            }
            for (int colIdx = bodyColStartIdx; colIdx < _MaxColNum + 1; colIdx++) {
                string HeaderTxt = sheet.Cells[WtAnalysisConst._HEADER_ROW_NUM, colIdx].Value.ToString();
                if (bodyColStartIdx <= colIdx && HeaderTxt != WtAnalysisConst._HEADER_COL_COST) {
                    sheet.Column(colIdx).OutlineLevel = 1;
                    sheet.Column(colIdx).Collapsed = true;
                }
            }
        }
        /// <summary>
        /// ピボット専用の編集
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="recordCount"></param>
        /// <param name="sheetType"></param>
        private void EditLayout_Pibot(ExcelWorksheet sheet, int recordCount, WtAnalysisConst.SheetType sheetType) {

            if (sheetType != WtAnalysisConst.SheetType.MEMBER && sheetType != WtAnalysisConst.SheetType.PROJECT) {
                // 要員別ピボット・作業別ピボット以外は処理しない
                return;
            }
            // 行グループ化
            sheet.OutLineSummaryBelow = false;
            for (int rowIdx = WtAnalysisConst._HEADER_ROW_NUM + 2; rowIdx < recordCount + WtAnalysisConst._HEADER_ROW_NUM + 1; rowIdx++) {
                switch (sheetType) {
                    case WtAnalysisConst.SheetType.MEMBER: // 要員別ピボット
                        if (sheet.Cells[rowIdx, 1].Value == null) {

                            // 行グループ化１
                            sheet.Row(rowIdx).OutlineLevel = 1;
                            sheet.Row(rowIdx).Collapsed = false;
                            
                            if (sheet.Cells[rowIdx, 2].Value == null) {
                                // 行グループ化２
                                sheet.Row(rowIdx).OutlineLevel = 2;
                                sheet.Row(rowIdx).Collapsed = true;
                            }
                        }
                        break;
                    case WtAnalysisConst.SheetType.PROJECT: // 作業別ピボット
                        if (sheet.Cells[rowIdx, 1].Value == null) {

                            // 行グループ化１
                            sheet.Row(rowIdx).OutlineLevel = 1;
                            sheet.Row(rowIdx).Collapsed = true;
                        }
                        break;
                }
                // 合計欄の編集
                EditLayout_PibotSum(sheet, rowIdx);
            }
        }
        /// <summary>
        /// ピボット専用の編集（合計欄）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowIdx"></param>
        private void EditLayout_PibotSum(ExcelWorksheet sheet, int rowIdx) {
            for (int colIdx = 1; colIdx < _MaxColNum + 1; colIdx++) {
                if (sheet.Cells[rowIdx, colIdx].Value != null && sheet.Cells[rowIdx, colIdx].Value.ToString() == WtAnalysisConst._SUM_VALUE) {

                    // 合計は右寄せにする
                    sheet.Cells[rowIdx, colIdx].Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                }
                if (WtAnalysisConst._BODY_COL_START_PER_MONTH - 2 <= colIdx) {
                    var cellValueWorkName = sheet.Cells[rowIdx, WtAnalysisConst._BODY_COL_START_PER_MONTH - 1].Value;
                    if (cellValueWorkName != null && cellValueWorkName.ToString() == WtAnalysisConst._SUM_VALUE) {

                        // 合計欄の上部を二重線にする
                        sheet.Cells[rowIdx, colIdx].Style.Border.Top.Style = ExcelBorderStyle.Double;
                    }
                }
            }
        }
        /// <summary>
        /// 各セルごとの編集
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="recordCount"></param>
        /// <param name="sheetName"></param>
        /// <param name="bodyColStartIdx"></param>
        /// <param name="bodyColcount"></param>
        /// <param name="sheetType"></param>
        private void EditExcelLayout_EachCells(ExcelWorksheet sheet, int recordCount, string sheetName,
            int bodyColStartIdx, int bodyColcount, WtAnalysisConst.SheetType sheetType) {
            for (int colIdx = 1; colIdx < _MaxColNum + 1; colIdx++) {
                for (int rowIdx = WtAnalysisConst._HEADER_ROW_NUM - 2; rowIdx < recordCount + WtAnalysisConst._HEADER_ROW_NUM + 1; rowIdx++) {
                    var valCrrtCell = sheet.Cells[rowIdx, colIdx];

                    // ヘッダーテキスト
                    int HeaderPAColIdx = colIdx - (bodyColcount / 2) + 1;

                    // 枠線
                    valCrrtCell.Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    if (WtAnalysisConst._HEADER_ROW_NUM <= rowIdx || bodyColStartIdx <= colIdx) {
                        valCrrtCell.Style.Border.Bottom.Style = ExcelBorderStyle.Hair;
                    }
                    // 各セルの背景色判定（灰色塗りつぶし）
                    if (JudgeIsEachCellBackColor_Nothing(sheetName, rowIdx, colIdx, valCrrtCell)) {
                        WorkTimeAnalysisUtil.SetCellColor(sheet, rowIdx, colIdx, Color.LightGray);
                    }
                    if (bodyColStartIdx <= colIdx && WtAnalysisConst._HEADER_ROW_NUM < rowIdx) {

                        // 工数欄の空白セルは灰色とする
                        if ( sheet.Cells[rowIdx, colIdx].Value == null || sheet.Cells[rowIdx, colIdx].Value.ToString() == string.Empty) {
                            WorkTimeAnalysisUtil.SetCellColor(sheet, rowIdx, colIdx, Color.LightGray);
                            continue;
                        }
                        // 各セルごとの編集（工数関係のセル）
                        EditEachCell_WorkTimeCols(sheet, sheetType, valCrrtCell, rowIdx, colIdx, HeaderPAColIdx);
                    }
                }
            }
        }
        /// <summary>
        /// 各セルの背景色判定（灰色塗りつぶし）
        /// </summary>
        /// <param name="sheetName"></param>
        /// <param name="rowIdx"></param>
        /// <param name="colIdx"></param>
        /// <param name="valCrrtCell"></param>
        /// <returns></returns>
        private bool JudgeIsEachCellBackColor_Nothing(string sheetName, int rowIdx, int colIdx, ExcelRange valCrrtCell) {
            if (sheetName == WtAnalysisConst._SHEET_NAME_PIBOT_USER && rowIdx == WtAnalysisConst._HEADER_ROW_NUM + 1) {
                // 要員名（要員別ピボット限定）
                return true;
            }
            if (colIdx <= 2) {
                // 列キー項目
                return true;
            }
            if (colIdx <= 4 && valCrrtCell.Value == null) {
                // 工数がセットされていない
                return true;
            }
            return false;
        }
        /// <summary>
        /// 各セルごとの編集（工数関係のセル）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="sheetType"></param>
        /// <param name="valCrrtCell"></param>
        /// <param name="rowIdx"></param>
        /// <param name="colIdx"></param>
        /// <param name="HeaderPAColIdx"></param>
        private void EditEachCell_WorkTimeCols(ExcelWorksheet sheet, WtAnalysisConst.SheetType sheetType, ExcelRange valCrrtCell, int rowIdx, int colIdx, int HeaderPAColIdx) {
            // セルの表示形式
            string HeaderTxt = sheet.Cells[WtAnalysisConst._HEADER_ROW_NUM, colIdx].Value.ToString();
            switch (HeaderTxt) {
                case WtAnalysisConst._HEADER_COL_WORK_TIME_M:   // 作業時間（m）
                    valCrrtCell.Style.Numberformat.Format = FormatConst._FORMAT_ZERO_2;
                    break;
                case WtAnalysisConst._HEADER_COL_WORK_TIME:     // 工数
                    if (sheetType == WtAnalysisConst.SheetType.COST) {
                        valCrrtCell.Style.Numberformat.Format = FormatConst._FORMAT_FLOAT_2;
                        break;
                    }
                    valCrrtCell.Style.Numberformat.Format = FormatConst._FORMAT_FLOAT_2_HUMANMOON;
                    break;
                case WtAnalysisConst._HEADER_COL_WORK_TIME_PCT: // 割合
                    valCrrtCell.Style.Numberformat.Format = FormatConst._FORMAT_FLOAT_1_PCT;
                    break;
                case WtAnalysisConst._HEADER_COL_MAN_MOON:      // 人月
                    valCrrtCell.Style.Numberformat.Format = FormatConst._FORMAT_FLOAT_2;
                    break;
                case WtAnalysisConst._HEADER_COL_COST:          // コスト
                    valCrrtCell.Style.Numberformat.Format = FormatConst._FORMAT_FLOAT_MONEY;
                    // セル背景色編集
                    EditEachCell_CostMoney(sheet, valCrrtCell, rowIdx, colIdx, HeaderPAColIdx);
                    break;
                case WtAnalysisConst._HEADER_COL_COST_1000:     // 金額
                    if (sheetType == WtAnalysisConst.SheetType.COST) {
                        valCrrtCell.Style.Numberformat.Format = FormatConst._FORMAT_FLOAT_MONEY_1000;
                        // セル背景色編集
                        EditEachCell_CostMoney(sheet, valCrrtCell, rowIdx, colIdx, HeaderPAColIdx);
                    }
                    break;
            }
        }
        /// <summary>
        /// 各セルごとの編集（コスト・金額）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="valCrrtCell"></param>
        /// <param name="rowIdx"></param>
        /// <param name="colIdx"></param>
        /// <param name="HeaderPAColIdx"></param>
        private void EditEachCell_CostMoney(ExcelWorksheet sheet, ExcelRange valCrrtCell, int rowIdx, int colIdx, int HeaderPAColIdx) {
            // セル背景色編集
            WorkTimeAnalysisUtil.SetCellColor(sheet, rowIdx, colIdx, Color.Yellow);

            string HeaderPATxt = string.Empty;
            if (0 < HeaderPAColIdx) {
                HeaderPATxt = sheet.Cells[WtAnalysisConst._HEADER_ROW_NUM - 1, HeaderPAColIdx].Value.ToString();
            }
            if (HeaderPATxt == WtAnalysisConst._HEADER_COL_ACTUAL) {

                // 実績工数と計画工数を比較
                if (sheet.Cells[rowIdx, HeaderPAColIdx - 1].Value != null && !string.IsNullOrEmpty(sheet.Cells[rowIdx, HeaderPAColIdx - 1].Value.ToString())) {
                    decimal decPlannedCost = decimal.Parse(sheet.Cells[rowIdx, HeaderPAColIdx - 1].Value.ToString());
                    decimal decActualCost = decimal.Parse(valCrrtCell.Value.ToString());
                    if (decPlannedCost < decActualCost) {
                        // 計画工数オーバー
                        valCrrtCell.Style.Font.Color.SetColor(Color.Red);
                    }
                    if (decActualCost < decPlannedCost) {
                        // 計画工数内
                        valCrrtCell.Style.Font.Color.SetColor(Color.Blue);
                    }
                }
            }
        }
        #endregion

        #region シート（プロジェクト別ピボット）
        /// <summary>
        /// シート作成（プロジェクト別ピボット）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="AnalysisRecords"></param>
        /// <param name="dicSumMonth"></param>
        private void CreateSheet_PibotProject(ExcelWorksheet sheet, List<WorkTimeAnalysisEntity> AnalysisRecords, Dictionary<int, string> dicSumMonth) {

            // Excel出力用オブジェクト作成
            var dtoList = WorkTimeAnalysisUtil.GetExcelDtoListKeyProject(AnalysisRecords);

            // ヘッダクリア
            string clearCellsAlphabet = ColConvertUtil.GetCellRangeAlphabeth(1, _MaxColNum, 1, WtAnalysisConst._HEADER_ROW_NUM + 1);
            sheet.Cells[clearCellsAlphabet].Value = string.Empty;

            // ソート
            dtoList = dtoList.OrderBy(n => n.projectCode)
                              .ThenBy(n => n.projectName)
                              .ThenBy(n => n.rowType)
                              .ThenBy(n => n.organization)
                              .ThenBy(n => n.userName)
                              .ToList();

            // 転記
            PostingAnalysis_PibotProject(sheet, dtoList, dicSumMonth);

            // レイアウト編集
            EditExcelLayout_Common(sheet, dtoList.Count, WtAnalysisConst._SHEET_NAME_PIBOT_USER, WtAnalysisConst.SheetType.PROJECT);
        }
        /// <summary>
        /// Excelに転記（プロジェクト別ピボット）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="dtoList"></param>
        /// <param name="dicSumMonth"></param>
        private void PostingAnalysis_PibotProject(ExcelWorksheet sheet, List<WorkTimeAnalysisDto> dtoList, Dictionary<int, string> dicSumMonth) {
            int rowIdx = WtAnalysisConst._HEADER_ROW_NUM + 1;
            foreach (var dto in dtoList) {
                // ヘッダキー項目
                PostingHeaderKeys_Common(sheet, WtAnalysisConst.SumType.PROJECT);

                // 明細キー項目
                switch (dto.rowType) {
                    case WtAnalysisConst.ExcelRowType.PROJECT_HEADER:
                        sheet.Cells[ColConvertUtil.ToAlphabet(1) + rowIdx].Value = dto.projectCode;
                        sheet.Cells[ColConvertUtil.ToAlphabet(2) + rowIdx].Value = dto.projectName;
                        break;
                }
                // シート作成（共通）
                PostingAnalysis_Common(sheet, dto, dicSumMonth, WtAnalysisConst.SumType.PROJECT, rowIdx);

                // キー項目が変わったら次の列に移行
                rowIdx++;
            }
        }
        #endregion

        #region シート（要員別ピボット）
        /// <summary>
        /// シート作成（要員別ピボット）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="AnalysisRecords"></param>
        /// <param name="dicSumMonth"></param>
        private void CreateSheet_PibotMember(ExcelWorksheet sheet, List<WorkTimeAnalysisEntity> AnalysisRecords, Dictionary<int, string> dicSumMonth) {

            // Excel出力用オブジェクト作成
            var dtoList = WorkTimeAnalysisUtil.GetExcelDtoListKeyUser(AnalysisRecords);

            // ヘッダクリア
            string clearCellsAlphabet = ColConvertUtil.GetCellRangeAlphabeth(1, _MaxColNum, 1, WtAnalysisConst._HEADER_ROW_NUM + 1);
            sheet.Cells[clearCellsAlphabet].Value = string.Empty;

            // ソート
            dtoList = dtoList.OrderBy(n => n.organization)
                              .ThenBy(n => n.userName)
                              .ThenBy(n => n.rowType)
                              .ThenBy(n => n.projectCode)
                              .ThenBy(n => n.projectName)
                              .ToList();

            // 転記
            PostingAnalysis_PibotMember(sheet, dtoList, dicSumMonth);

            // レイアウト編集
            EditExcelLayout_Common(sheet, dtoList.Count, WtAnalysisConst._SHEET_NAME_PIBOT_USER, WtAnalysisConst.SheetType.MEMBER);
        }
        /// <summary>
        /// Excelに転記（要員別ピボット）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="dtoList"></param>
        /// <param name="dicSumMonth"></param>
        private void PostingAnalysis_PibotMember(ExcelWorksheet sheet, List<WorkTimeAnalysisDto> dtoList, Dictionary<int, string> dicSumMonth) {
            int rowIdx = WtAnalysisConst._HEADER_ROW_NUM + 1;
            foreach (var dto in dtoList) {
                // ヘッダキー項目
                PostingHeaderKeys_Common(sheet, WtAnalysisConst.SumType.MEMBER);

                // 明細キー項目
                switch (dto.rowType) {
                    case WtAnalysisConst.ExcelRowType.ORGANIZATION_HEADER:
                        sheet.Cells[ColConvertUtil.ToAlphabet(1) + rowIdx].Value = dto.organization;
                        break;
                    case WtAnalysisConst.ExcelRowType.MEMBER_HEADER:
                        sheet.Cells[ColConvertUtil.ToAlphabet(2) + rowIdx].Value = dto.userName;
                        break;
                }
                // シート作成（共通）
                PostingAnalysis_Common(sheet, dto, dicSumMonth, WtAnalysisConst.SumType.MEMBER, rowIdx);

                // キー項目が変わったら次の列に移行
                rowIdx++;
            }
        }
        #endregion

        #region シート（コスト総括）
        /// <summary>
        /// シート作成（コスト総括）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="AnalysisRecords"></param>
        /// <param name="dicSumMonth"></param>
        private void CreateSheet_Cost(ExcelWorksheet sheet, List<WorkTimeAnalysisEntity> AnalysisRecords, Dictionary<int, string> dicSumMonth) {

            // Excel出力用オブジェクト作成
            var dtoList = WorkTimeAnalysisUtil.GetExcelDtoListKeyProject(AnalysisRecords);
            dtoList = dtoList.Where(n => n.rowType == WtAnalysisConst.ExcelRowType.SUMMARY).ToList();

            // ヘッダクリア
            string clearCellsAlphabet = ColConvertUtil.GetCellRangeAlphabeth(1, _MaxColNum, 1, WtAnalysisConst._HEADER_ROW_NUM + 1);
            sheet.Cells[clearCellsAlphabet].Value = string.Empty;

            // ソート
            dtoList = dtoList.OrderBy(n => n.projectCode)
                              .ThenBy(n => n.projectName)
                              .ToList();

            // 転記
            PostingAnalysis_Cost(sheet, dtoList, dicSumMonth);

            // レイアウト編集
            EditExcelLayout_Common(sheet, dtoList.Count, WtAnalysisConst._SHEET_NAME_SUM_COST, WtAnalysisConst.SheetType.COST);
        }
        /// <summary>
        /// Excelに転記（コスト総括）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="dtoList"></param>
        /// <param name="dicSumMonth"></param>
        private void PostingAnalysis_Cost(ExcelWorksheet sheet, List<WorkTimeAnalysisDto> dtoList, Dictionary<int, string> dicSumMonth) {
            int rowIdx = WtAnalysisConst._HEADER_ROW_NUM + 1;
            foreach (var dto in dtoList) {
                if (dto.rowType != WtAnalysisConst.ExcelRowType.SUMMARY) {
                    continue;
                }
                // ヘッダキー項目
                PostingHeaderKeys_Common(sheet, WtAnalysisConst.SumType.COST);

                // 明細キー項目
                switch (dto.rowType) {
                    case WtAnalysisConst.ExcelRowType.ORGANIZATION_HEADER:
                        sheet.Cells[ColConvertUtil.ToAlphabet(1) + rowIdx].Value = dto.projectCode;
                        sheet.Cells[ColConvertUtil.ToAlphabet(2) + rowIdx].Value = dto.projectName;
                        break;
                }
                // シート作成（コスト総括）
                PostingAnalysis_Cost(sheet, dto, dicSumMonth,  rowIdx);

                // キー項目が変わったら次の列に移行
                rowIdx++;
            }
        }
        /// <summary>
        /// シート作成（コスト総括）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="dto"></param>
        /// <param name="dicSumMonth"></param>
        /// <param name="rowIdx"></param>
        private void PostingAnalysis_Cost(ExcelWorksheet sheet, WorkTimeAnalysisDto dto, Dictionary<int, string> dicSumMonth, int rowIdx) {
            foreach (var colsDto in dto.workTimeDatas) {
                // =========================================================
                // ヘッダ
                int entityIdx = dto.workTimeDatas.IndexOf(colsDto);
                var tgtDic = dicSumMonth.Where(n => n.Value == colsDto.date).FirstOrDefault();
                if (tgtDic.Equals(null)) {
                    continue;
                }
                int tgtColNum = ((tgtDic.Key) * WtAnalysisConst._BODY_COL_COUNT_COST) + WtAnalysisConst._BODY_COL_START_COST;

                // 集計月
                var valHeaderMonth = sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 0) + (WtAnalysisConst._HEADER_ROW_NUM - 2)].Value;
                if (valHeaderMonth == null || string.IsNullOrEmpty(valHeaderMonth.ToString())) {
                    string range =  ColConvertUtil.ToAlphabet(tgtColNum + 0) + (WtAnalysisConst._HEADER_ROW_NUM - 2);
                    sheet.Cells[range].Value = WorkTimeAnalysisUtil.GetHeaderMonth(_ConfigInfo.ConnStrTimeEntryDB, DateTime.Parse(colsDto.date));
                }

                var valheaderPA = sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 0) + (WtAnalysisConst._HEADER_ROW_NUM - 1)].Value;
                if (valheaderPA == null || string.IsNullOrEmpty(valheaderPA.ToString())) {
                    // 計画コスト
                    sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 0) + (WtAnalysisConst._HEADER_ROW_NUM - 1)].Value = WtAnalysisConst._HEADER_COL_PLANNED;
                    sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 0) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_TIME;
                    sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 1) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_COST_1000;

                    // 実績工数
                    string rangeJ = ColConvertUtil.ToAlphabet(tgtColNum + (WtAnalysisConst._BODY_COL_COUNT_COST / 2)) + (WtAnalysisConst._HEADER_ROW_NUM - 1);
                    sheet.Cells[rangeJ].Value = WtAnalysisConst._HEADER_COL_ACTUAL;
                    sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 2) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_WORK_TIME;
                    sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 3) + WtAnalysisConst._HEADER_ROW_NUM].Value = WtAnalysisConst._HEADER_COL_COST_1000;
                }
                // =========================================================
                // 明細
                sheet.Cells[ColConvertUtil.ToAlphabet(1) + rowIdx].Value = dto.projectCode;
                sheet.Cells[ColConvertUtil.ToAlphabet(2) + rowIdx].Value = dto.projectName;

                // 計画工数
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 0) + rowIdx].Value = colsDto.decPlannedManHour;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 1) + rowIdx].Value = Math.Round(colsDto.decPlannedCost / 1000, 1, MidpointRounding.AwayFromZero);

                // 実績工数
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 2) + rowIdx].Value = colsDto.decActualManHour;
                sheet.Cells[ColConvertUtil.ToAlphabet(tgtColNum + 3) + rowIdx].Value = Math.Round(colsDto.decActualCost / 1000, 1, MidpointRounding.AwayFromZero);
            }
        }
        #endregion

        #region シート（一覧）
        /// <summary>
        /// シート作成（一覧）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="AnalysisRecords"></param>
        /// <param name="dicSumMonth"></param>
        private void CreateSheet_Looks(ExcelWorksheet sheet, List<WorkTimeAnalysisEntity> AnalysisRecords, Dictionary<int, string> dicSumMonth) {

            // Excel出力用オブジェクト作成
            var dtoList = WorkTimeAnalysisUtil.GetExcelDtoListKeyUser(AnalysisRecords);

            // ヘッダクリア
            string clearCellsAlphabet = ColConvertUtil.GetCellRangeAlphabeth(1, _MaxColNum, 1, WtAnalysisConst._HEADER_ROW_NUM + 1);
            sheet.Cells[clearCellsAlphabet].Value = string.Empty;

            // 行データのみ抽出
            dtoList = dtoList.Where(n => n.rowType == WtAnalysisConst.ExcelRowType.BODY_ROW).ToList();

            // ソート
            dtoList = dtoList.OrderBy(n => n.organization)
                              .ThenBy(n => n.userName)
                              .ThenBy(n => n.rowType)
                              .ThenBy(n => n.projectCode)
                              .ThenBy(n => n.projectCode)
                              .ToList();

            // 転記
            PostingAnalysis_Looks(sheet, dtoList, dicSumMonth);

            // レイアウト編集
            EditExcelLayout_Common(sheet, dtoList.Count, WtAnalysisConst._SHEET_NAME_LOOKS, WtAnalysisConst.SheetType.LOOKS);
        }
        /// <summary>
        /// Excelに転記（一覧）
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="dtoList"></param>
        /// <param name="dicSumMonth"></param>
        private void PostingAnalysis_Looks(ExcelWorksheet sheet, List<WorkTimeAnalysisDto> dtoList, Dictionary<int, string> dicSumMonth) {
            int rowIdx = WtAnalysisConst._HEADER_ROW_NUM + 1;
            foreach (var dto in dtoList) {
                if (dto.rowType != WtAnalysisConst.ExcelRowType.BODY_ROW) {
                    continue;
                }
                // ヘッダキー項目
                PostingHeaderKeys_Common(sheet, WtAnalysisConst.SumType.MEMBER);

                // 明細キー項目
                sheet.Cells[ColConvertUtil.ToAlphabet(1) + rowIdx].Value = dto.organization;
                sheet.Cells[ColConvertUtil.ToAlphabet(2) + rowIdx].Value = dto.userName;

                // シート作成（共通）
                PostingAnalysis_Common(sheet, dto, dicSumMonth, WtAnalysisConst.SumType.MEMBER, rowIdx);

                // キー項目が変わったら次の列に移行
                rowIdx++;
            }
        }
        #endregion
    }
}
