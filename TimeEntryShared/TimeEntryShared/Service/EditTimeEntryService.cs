﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using OfficeOpenXml;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TimeEntryShared.Const;
using TimeEntryShared.Dto;
using TimeEntryShared.Entity;
using TimeEntryShared.Util;

namespace TimeEntryShared.Service {
    public class EditTimeEntryService {
        /// <summary>
        /// Config情報
        /// </summary>
        private TimeEntryConfigEntity _ConfigInfo = new TimeEntryConfigEntity();
        /// <summary>
        /// TimeTracker出力情報
        /// </summary>
        private List<UserInfoDto> _TimeTrackerUserInfos = new List<UserInfoDto>();
        /// <summary>
        /// 従業員情報
        /// </summary>
        private List<MstEmploeeEntity> _EmploeeInfos = new List<MstEmploeeEntity>();
        /// <summary>
        /// グループマスタ
        /// </summary>
        private List<MstGroupEntity> _GroupList = new List<MstGroupEntity>();
        /// <summary>
        /// 部署マスタ
        /// </summary>
        private List<MstBuEntity> _BuList = new List<MstBuEntity>();
        /// <summary>
        /// MHSCマスタ
        /// </summary>
        private List<MhscMstEntity> _MhscMstInfos = new List<MhscMstEntity>();
        private const int _TDS_LENGTH_EXIST_TIME = 3;

        /// <summary>
        /// メイン処理
        /// </summary>
        /// <param name="propaties"></param>
        /// <param name="tgtEmploeeNum"></param>
        /// <param name="isUsingRPA"></param>
        /// <returns></returns>
        public string Main(ServicePropaties propaties, string tgtEmploeeNum = "", bool isUsingRPA = false) {
            LogAndConsoleUtil.ShowLogAndConsoleInfo(SharedMessages._MSG_OPERATION_START, LogAndConsoleUtil.StartEndLineType.Start);

            // TimeEntry.config
            _ConfigInfo = propaties.ConfigInfo;

            // TimeTrackerユーザ情報
            _TimeTrackerUserInfos = propaties.TimeTrackerUserInfos;

            // 従業員情報ロード
            _EmploeeInfos = propaties.EmploeeInfos;

            // 部署マスタロード
            _BuList = propaties.BuList;

            // グループマスタロード
            _GroupList = propaties.GroupList;

            // MHSCマスタロード
            _MhscMstInfos = propaties.MhscMstInfos;

            // 集計月
            var sumDt = GetSumMonth();

            // 出力対象
            if (!string.IsNullOrEmpty(tgtEmploeeNum)) {
                _EmploeeInfos = _EmploeeInfos.Where(n => n.EmploeeNum == tgtEmploeeNum).ToList();
            }
            // ソート
            _EmploeeInfos = _EmploeeInfos.OrderBy(n => n.BuCode)
                                          .ThenBy(n => n.GroupCode)
                                          .ThenBy(n => n.EmploeeNum).ToList();

            // デフォルト勤務表の存在チェック
            string defaultKinmuhyoPath = Path.Combine(_ConfigInfo.DefaultKinmuhyoPath, _ConfigInfo.DefaultKinmuhyoName);
            if (!File.Exists(defaultKinmuhyoPath)) {
                var exNotExistDef = new Exception(SharedMessages._MSG_ERROR_NOT_EXIST_DEF);
                LogAndConsoleUtil.ShowLogAndConsoleErr(exNotExistDef.Message, exNotExistDef.StackTrace);
                return exNotExistDef.Message;
            }
            // 勤務実績表作成
            foreach (var emp in _EmploeeInfos) {
                try {
                    // チェック処理
                    if (!ValidateCreateKinmuhyo(emp, sumDt)) {
                        continue;
                    }
                    // 強制再作成フラグ=ONならば、既存の勤務表を削除
                    if (propaties.ConfigInfo.ForcingReCreateFlg == FlgValConst._INT_FLG_ON) {
                        string KinmuhyoPath = GetKinmuhyoPath(emp, sumDt);
                        CommonUtil.DeleteFile(KinmuhyoPath);
                    }
                    // 初期化
                    string kinmuhyoPath = GetKinmuhyoPath(emp, sumDt);
                    InitKinmuhyo(kinmuhyoPath, emp.Name);

                    // 勤怠時刻転記
                    PostingWorkTimesProxy(emp, sumDt, isUsingRPA);

                    // 作業内容欄転記
                    PostingWorkAreaProxy(emp, sumDt);

                } catch (Exception e) {
                    LogAndConsoleUtil.ShowLogAndConsoleErr(e.Message, e.StackTrace);

                    // chromeDriver強制終了
                    WebDriverUtil.KillChromeDriver();
                    return e.Message;
                }
            }
            LogAndConsoleUtil.ShowLogAndConsoleInfo(SharedMessages._MSG_OPERATION_END, LogAndConsoleUtil.StartEndLineType.End);
            return string.Empty;
        }
        /// <summary>
        /// Validate（勤務表作成）
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="sumDt"></param>
        /// <returns></returns>
        private bool ValidateCreateKinmuhyo(MstEmploeeEntity emp, DateTime sumDt) {
            if (emp.DeleteFlg == FlgValConst._STR_FLG_ON) {
                // 削除データは処理しない
                return false;
            }
            if (!KinmuhyoUtil.ValidateIsKinmuhyoOpened(_ConfigInfo, _BuList, _GroupList, emp, sumDt)) {
                // 勤務表を開きっぱなしの場合はログ出力して処理終了
                LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Format(SharedMessages._MSG_ERROR_KINMUHYO_OPEND, emp.Name));
                return false;
            }
            return true;
        }
        /// <summary>
        /// 集計月取得
        /// </summary>
        /// <param name="configInfo"></param>
        /// <returns></returns>
        public DateTime GetSumMonth() {
            DateTime dt = DateTime.Parse(_ConfigInfo.SumStartDate);
            dt = new DateTime(dt.Year, dt.Month, 1);
            return dt;
        }
        /// <summary>
        /// 勤怠時刻欄ユーザタイプ取得
        /// </summary>
        /// <param name="emp"></param>
        /// <returns></returns>
        public KinmuhyoConst.UserType GetUserType(MstEmploeeEntity emp) {
            if (emp.DefaultWorkFlg == FlgValConst._STR_FLG_ON) {
                var tgtEmpInfo = _EmploeeInfos.Where(n => n.EmploeeNum == emp.EmploeeNum).FirstOrDefault();
                if (tgtEmpInfo != null) {
                    return KinmuhyoConst.UserType.DEFAULT_WORK;
                }
            }
            var ttInfo = _TimeTrackerUserInfos.Where(n => CommonUtil.CutSpace(n.UserName) == CommonUtil.CutSpace(emp.Name)).FirstOrDefault();
            if (ttInfo != null) {
                return KinmuhyoConst.UserType.TIME_TRACKER;
            }
            return KinmuhyoConst.UserType.NONE;
        }
        /// <summary>
        /// 勤務表のパス取得
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="sumDt"></param>
        /// <returns></returns>
        public string GetKinmuhyoPath(MstEmploeeEntity emp, DateTime sumDt) {
            string kinmuhyoPath = KinmuhyoUtil.GetKinmuhyoPath(_ConfigInfo, _BuList, _GroupList, emp, sumDt);
            string kinmuhyoName = KinmuhyoUtil.GetKinmuhyoName(_ConfigInfo, emp.Name, sumDt);
            string KinmuhyoPath = Path.Combine(kinmuhyoPath, kinmuhyoName);
            return KinmuhyoPath;
        }
        /// <summary>
        /// 勤務実績表作成（初期化）
        /// </summary>
        /// <param name="KinmuhyoPath"></param>
        /// <param name="name"></param>
        public void InitKinmuhyo(string KinmuhyoPath, string name) {
            LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Format(SharedMessages._MSG_KINMUHYO_TGT, name));
            
            if (!File.Exists(KinmuhyoPath)) {
                string defaultKinmuhyoPath = Path.Combine(_ConfigInfo.DefaultKinmuhyoPath, _ConfigInfo.DefaultKinmuhyoName);
                string CopyKinmuhyoPath = KinmuhyoPath + CommonLiteral._MARK_STAR;
                CommonUtil.DeleteFile(CopyKinmuhyoPath);
                File.Copy(defaultKinmuhyoPath, CopyKinmuhyoPath);

                var xlsxFile = File.OpenRead(CopyKinmuhyoPath);
                using (var package = new ExcelPackage(new FileInfo(CopyKinmuhyoPath))) {
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    var sheet = package.Workbook.Worksheets[KinmuhyoConst._SHEET_NAME_KINMUHYO];
                    package.SaveAs(new FileInfo(KinmuhyoPath));
                }
                xlsxFile.Close();
                LogAndConsoleUtil.ShowLogAndConsoleInfo(SharedMessages._MSG_INIT_KINMUHYO_END);
                CommonUtil.DeleteFile(CopyKinmuhyoPath);
            }
        }

        #region 勤怠時刻欄転記
        /// <summary>
        /// 勤怠時刻転記（Proxy）
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="sumDt"></param>
        public void PostingWorkTimesProxy(MstEmploeeEntity emp, DateTime sumDt, bool isUsingRPA) {
            var wrokTimeDtoList = new List<WorkTimeDto>();
            if (isUsingRPA) {
                // RPAで取得
                WebDriverUtil.UpdateChromeDriver();
                wrokTimeDtoList = GetWtDtoList_RPA(emp, sumDt);
            } else {
                // SQLで取得
                wrokTimeDtoList = ModelUtil.GetWtDtoList_SQL(_ConfigInfo, emp, sumDt);
            }
            // 勤怠時刻転記
            string commonPath = KinmuhyoUtil.GetKinmuhyoPath(_ConfigInfo, _BuList, _GroupList, emp, sumDt);
            string kinmuhyoName = KinmuhyoUtil.GetKinmuhyoName(_ConfigInfo, emp.Name, sumDt);
            string KinmuhyoPath = GetKinmuhyoPath(emp, sumDt);
            string tempPath = Path.Combine(commonPath, CommonLiteral._MARK_STAR + kinmuhyoName);
            if (File.Exists(KinmuhyoPath)) {
                PostingWorkTimes(emp, KinmuhyoPath, tempPath, wrokTimeDtoList);
            }
        }
        /// <summary>
        /// 勤怠情報取得（RPA）
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="sumDt"></param>
        /// <returns></returns>
        private List<WorkTimeDto> GetWtDtoList_RPA(MstEmploeeEntity emp, DateTime sumDt) {
            var ret = new List<WorkTimeDto>();

            // 接続情報作成
            string WbUrl = KinmuhyoUtil.GetWhiteBordURL(_ConfigInfo, emp);
            var options = new ChromeOptions();
            options.AddArgument(KinmuhyoConst._OPT_ARG_HEADLESS);
            options.AddArgument(KinmuhyoConst._OPT_ARG_NO_SAND_BOX);
            var driverService = ChromeDriverService.CreateDefaultService();
            driverService.HideCommandPromptWindow = true;
            using (var driver = new ChromeDriver(driverService, options)) {

                // Chromeでホワイトボードを開く
                driver.Navigate().GoToUrl(WbUrl);

                // ホワイトボードの勤務表を開き、情報を抽出
                var rows = driver.FindElements(By.TagName(KinmuhyoConst._TAG_NAME_TEXT_ROW));
                foreach (var row in rows) {
                    var dto = new WorkTimeDto();
                    string plainTxt = row.Text.Replace(Environment.NewLine, CommonLiteral._LITERAL_SPACE_HALF);
                    var tds = plainTxt.Split(CommonLiteral._LITERAL_SPACE_HALF.ToCharArray()[0]);
                    int intDay;
                    if (!int.TryParse(tds[0], out intDay)) {
                        continue;
                    }
                    dto.Day = intDay;     // 日付
                    dto.WeekDay = tds[1]; // 曜日

                    if (_TDS_LENGTH_EXIST_TIME < tds.Length) {
                        string[] timeSplit = GetTimeSplit(plainTxt);
                        dto.StartHH = GetTdRine_ByIdx(timeSplit, 0); // 出勤(h)
                        dto.StartMM = GetTdRine_ByIdx(timeSplit, 1); // 出勤(m)
                        dto.EndHH   = GetTdRine_ByIdx(timeSplit, 2); // 退勤(h)
                        dto.EndMM   = GetTdRine_ByIdx(timeSplit, 3); // 退勤(m)
                    }                    
                    if (tds.Contains(KinmuhyoConst._MARK_REST)) {
                        dto.RestMark = KinmuhyoConst._MARK_REST;
                    }
                    // 時刻丸め
                    bool isMHSC = emp.MhscFlg == FlgValConst._STR_FLG_ON;
                    bool isShiftB = emp.ForcingShiftBFlg == FlgValConst._STR_FLG_ON;
                    var newDto = KinmuhyoUtil.RoundWorkTimeProxy(_ConfigInfo.DefaultKinmuhyoFormat, sumDt, dto, isMHSC, isShiftB);
                    ret.Add(newDto);
                }
            }
            return ret;
        }
        /// <summary>
        /// td取得
        /// </summary>
        /// <param name="td"></param>
        /// <param name="idx"></param>
        /// <returns></returns>
        private string GetTdRine_ByIdx(string [] td, int idx) {
            if (td.Length <= idx) {
                return string.Empty;
            }
            return td[idx];
        }
        /// <summary>
        /// 時刻欄開始インデックス取得
        /// </summary>
        /// <param name="plainTxt"></param>
        /// <returns></returns>
        private int GetTimeStartIdx(string plainTxt) {
            bool isFirstSpace = true;
            int idxTgtSpace = 0;
            foreach (var c in plainTxt) {
                if (isFirstSpace && c == CommonLiteral._LITERAL_SPACE_HALF.ToCharArray()[0]) {
                    isFirstSpace = false;
                }
                if (!isFirstSpace && c == CommonLiteral._LITERAL_SPACE_HALF.ToCharArray()[0]) {
                    idxTgtSpace++;
                    break;
                }
                idxTgtSpace++;
            }
            idxTgtSpace++;
            int ret = idxTgtSpace + 1;
            return ret;
        }
        /// <summary>
        /// 時刻欄分割配列取得
        /// </summary>
        /// <param name="plainTxt"></param>
        /// <returns></returns>
        private string[] GetTimeSplit(string plainTxt) {

            // 時刻欄開始インデックス取得
            int timeStartIdx = GetTimeStartIdx(plainTxt);

            // 時刻欄の純粋なテキストを取得
            string timePlainTxt = plainTxt.Substring(timeStartIdx, plainTxt.Length - timeStartIdx);

            // テキストに含まれるスペースの数に応じて処理分岐
            int spaceCount = timePlainTxt.Where(n => n == CommonLiteral._LITERAL_SPACE_HALF.ToCharArray()[0]).ToList().Count;
            if (spaceCount == 1) {
                // 退勤時刻未入力（スペース1個のみ）
                timePlainTxt = string.Concat(timePlainTxt, string.Concat(CommonLiteral._LITERAL_SPACE_HALF, CommonLiteral._LITERAL_SPACE_HALF));
            }
            if (spaceCount == 2) {
                // 出勤時刻未入力（スペース2個のみ）
                timePlainTxt = string.Concat(CommonLiteral._LITERAL_SPACE_HALF, timePlainTxt);
            }
            // スペースで分割して時刻を配列に格納
            string[] timeSplit = timePlainTxt.Split(CommonLiteral._LITERAL_SPACE_HALF.ToCharArray()[0]);
            return timeSplit;
        }
        /// <summary>
        /// 勤怠時刻転記
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="OriginXlsxPath"></param>
        /// <param name="tempPath"></param>
        /// <param name="dtoList"></param>
        public void PostingWorkTimes(MstEmploeeEntity emp, string OriginXlsxPath, string tempPath, List<WorkTimeDto> dtoList) {
            var xlsxFile = File.OpenRead(OriginXlsxPath);
            using (var package = new ExcelPackage(new FileInfo(OriginXlsxPath))) {

                // 作業内容シート作成
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var sheet = package.Workbook.Worksheets[KinmuhyoConst._SHEET_NAME_KINMUHYO];
                sheet.Cells[KinmuhyoConst._CELL_NAME_NAME].Value = emp.Name;

                // 編集
                foreach (var dto in dtoList) {
                    if (!string.IsNullOrEmpty(dto.StartHH) && !string.IsNullOrEmpty(dto.StartMM)) {
                        sheet.Cells[KinmuhyoConst._COL_SHUKKIN_HH + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = int.Parse(dto.StartHH);
                        sheet.Cells[KinmuhyoConst._COL_SHUKKIN_MM + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = int.Parse(dto.StartMM);
                    }
                    if (!string.IsNullOrEmpty(dto.EndHH) && !string.IsNullOrEmpty(dto.EndMM)) {
                        sheet.Cells[KinmuhyoConst._COL_TAIKIN_HH + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = int.Parse(dto.EndHH);
                        sheet.Cells[KinmuhyoConst._COL_TAIKIN_MM + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = int.Parse(dto.EndMM);
                    }
                    switch (dto.RestType) {
                        case TimeConst.RestType.PAID:
                            sheet.Cells[KinmuhyoConst._COL_WORK_STATUS + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = KinmuhyoConst._MARK_PAID;
                            break;

                            #region 午前休暇・午後休暇（制度廃止によりコメントアウト）
                            //case TimeConst.RestType.AM:
                            //    sheet.Cells[KinmuhyoConst._COL_AM_REST + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = KinmuhyoConst._MARK_REST;
                            //    sheet.Cells[KinmuhyoConst._COL_JITSUDOU_HH + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = TimeConst._DEFAULT_WORK_TIME_PER_DAY;
                            //    sheet.Cells[KinmuhyoConst._COL_JITSUDOU_MM + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = 0;
                            //    PostingZangyoTime(sheet, dto);
                            //    break;
                            //case TimeConst.RestType.PM:
                            //    sheet.Cells[KinmuhyoConst._COL_PM_REST + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = KinmuhyoConst._MARK_REST;
                            //    sheet.Cells[KinmuhyoConst._COL_JITSUDOU_HH + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = TimeConst._DEFAULT_WORK_TIME_PER_DAY;
                            //    sheet.Cells[KinmuhyoConst._COL_JITSUDOU_MM + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = 0;
                            //    sheet.Cells[KinmuhyoConst._COL_ZANGYO_HH + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = null;
                            //    sheet.Cells[KinmuhyoConst._COL_ZANGYO_MM + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = null;
                            //    break;
                            #endregion

                            #region 部分休暇（A・B・C勤務を考慮すると判定不可能なためコメントアウト）
                            //case TimeConst.RestType.ONE_QUARTER:
                            //    sheet.Cells[KinmuhyoConst._COL_AM_REST + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = KinmuhyoConst._MARK_ONE_QUARTER;
                            //    sheet.Cells[KinmuhyoConst._COL_JITSUDOU_HH + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = TimeConst._DEFAULT_WORK_TIME_PER_DAY;
                            //    sheet.Cells[KinmuhyoConst._COL_JITSUDOU_MM + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = 0;
                            //    break;
                            //case TimeConst.RestType.TWO_QUARTER:
                            //    sheet.Cells[KinmuhyoConst._COL_AM_REST + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = KinmuhyoConst._MARK_TWO_QUARTER;
                            //    sheet.Cells[KinmuhyoConst._COL_JITSUDOU_HH + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = TimeConst._DEFAULT_WORK_TIME_PER_DAY;
                            //    sheet.Cells[KinmuhyoConst._COL_JITSUDOU_MM + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = 0;
                            //    break;
                            //case TimeConst.RestType.THREE_QUARTER:
                            //    sheet.Cells[KinmuhyoConst._COL_AM_REST + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = KinmuhyoConst._MARK_THREE_QUARTER;
                            //    sheet.Cells[KinmuhyoConst._COL_JITSUDOU_HH + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = TimeConst._DEFAULT_WORK_TIME_PER_DAY;
                            //    sheet.Cells[KinmuhyoConst._COL_JITSUDOU_MM + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = 0;
                            //    if (int.Parse(dto.StartHH) < TimeConst._DEFAULT_NOON_REST_START_HH && int.Parse(dto.EndHH) <= TimeConst._DEFAULT_NOON_REST_START_HH) {
                            //        sheet.Cells[KinmuhyoConst._COL_PM_REST + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = KinmuhyoConst._MARK_CONTAINS_REST;
                            //    }
                            //break;
                            #endregion
                    }
                }
                // 保存
                package.SaveAs(new FileInfo(tempPath));
            }
            xlsxFile.Close();
            CommonUtil.DeleteFile(OriginXlsxPath);
            File.Copy(tempPath, OriginXlsxPath);
            CommonUtil.DeleteFile(tempPath);
        }
        /// <summary>
        /// 残業時間欄の転記
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="dto"></param>
        public void PostingZangyoTime(ExcelWorksheet sheet, WorkTimeDto dto) {
            if (string.IsNullOrEmpty(dto.EndHH) || string.IsNullOrEmpty(dto.EndMM)) {
                return;
            }
            decimal decEndTime = int.Parse(dto.EndHH) * TimeConst._MINUTE_AN_HOUR + int.Parse(dto.EndMM);
            decimal zangyoTime = decEndTime - (TimeConst._DEFAULT_WORK_END_TYPE_B_HH * TimeConst._MINUTE_AN_HOUR);
            if (decimal.Zero < zangyoTime) {
                sheet.Cells[KinmuhyoConst._COL_ZANGYO_HH + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = (int)(zangyoTime / TimeConst._MINUTE_AN_HOUR);
                sheet.Cells[KinmuhyoConst._COL_ZANGYO_MM + (dto.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER)].Value = (int)(zangyoTime % TimeConst._MINUTE_AN_HOUR);
            }
        }

        /// <summary>
        /// 部分休暇時刻編集（前休暇）
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="workStartTime"></param>
        /// <param name="oneQ"></param>
        /// <param name="twoQ"></param>
        /// <param name="threeQ"></param>
        /// <param name="fourQ"></param>
        /// <returns></returns>
        public WorkTimeDto EditQuarterWork_Front(WorkTimeDto ret, decimal workStartTime, int oneQ, int twoQ, int threeQ, int fourQ) {
            if (workStartTime < twoQ * TimeConst._MINUTE_AN_HOUR) {
                ret.RestType = TimeConst.RestType.ONE_QUARTER;
                ret.StartHH = oneQ.ToString();
                ret.StartMM = decimal.Zero.ToString(FormatConst._FORMAT_PRE_ZERO_2);
                return ret;
            }
            if (workStartTime < threeQ * TimeConst._MINUTE_AN_HOUR) {
                ret.RestType = TimeConst.RestType.TWO_QUARTER;
                ret.StartHH = twoQ.ToString();
                ret.StartMM = decimal.Zero.ToString(FormatConst._FORMAT_PRE_ZERO_2);
                return ret;
            }
            if (workStartTime < fourQ * TimeConst._MINUTE_AN_HOUR) {
                ret.RestType = TimeConst.RestType.THREE_QUARTER;
                ret.StartHH = threeQ.ToString();
                ret.StartMM = decimal.Zero.ToString(FormatConst._FORMAT_PRE_ZERO_2);
                return ret;
            }
            return ret;
        }
        /// <summary>
        /// 部分休暇時刻編集（後休暇）
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="workEndTime"></param>
        /// <param name="oneQ"></param>
        /// <param name="twoQ"></param>
        /// <param name="threeQ"></param>
        /// <param name="fourQ"></param>
        /// <returns></returns>
        public WorkTimeDto EditQuarterWork_Back(WorkTimeDto ret, decimal workEndTime, int oneQ, int twoQ, int threeQ, int fourQ) {
            if (workEndTime < twoQ * TimeConst._MINUTE_AN_HOUR) {
                ret.RestType = TimeConst.RestType.THREE_QUARTER;
                ret.EndHH = oneQ.ToString();
                ret.EndMM = decimal.Zero.ToString(FormatConst._FORMAT_PRE_ZERO_2);
                return ret;
            }
            if (workEndTime < threeQ * TimeConst._MINUTE_AN_HOUR) {
                ret.RestType = TimeConst.RestType.TWO_QUARTER;
                ret.EndHH = twoQ.ToString();
                ret.EndMM = decimal.Zero.ToString(FormatConst._FORMAT_PRE_ZERO_2);
                return ret;
            }
            if (workEndTime < fourQ * TimeConst._MINUTE_AN_HOUR) {
                ret.RestType = TimeConst.RestType.ONE_QUARTER;
                ret.EndHH = threeQ.ToString();
                ret.EndMM = decimal.Zero.ToString(FormatConst._FORMAT_PRE_ZERO_2);
                return ret;
            }
            return ret;
        }
        /// <summary>
        /// 始業～終業の時間（分）を取得
        /// </summary>
        /// <param name="dtTimeStart"></param>
        /// <param name="dtTimeEnd"></param>
        /// <returns></returns>
        public decimal GetWorkTimeLength(decimal dtTimeStart, decimal dtTimeEnd) {
            decimal WorkTimeLength = dtTimeEnd - dtTimeStart;
            if (dtTimeStart < TimeConst._DEFAULT_NOON_REST_START_HH * TimeConst._MINUTE_AN_HOUR
            && TimeConst._DEFAULT_NOON_REST_START_HH * TimeConst._MINUTE_AN_HOUR <= dtTimeEnd) {
                // 勤務時間から昼休憩を省く
                WorkTimeLength -= TimeConst._MINUTE_AN_HOUR;
            }
            return WorkTimeLength;
        }

        #endregion

        #region 作業内容転記
        /// <summary>
        /// 作業内容/一覧転記（Proxy）
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="sumD"></param>
        public void PostingWorkAreaProxy(MstEmploeeEntity emp, DateTime sumDt) {
            var userType = GetUserType(emp);
            switch (userType) {
                case KinmuhyoConst.UserType.TIME_TRACKER:
                    PostingWorkArea_TimeTracker(emp, sumDt);
                    break;
                case KinmuhyoConst.UserType.DEFAULT_WORK:
                    PostingWorkArea_DefaultWork(emp, sumDt);
                    break;
            }
        }
        /// <summary>
        /// 作業内容/一覧転記（TimeTraclerユーザ）
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="sumDt"></param>
        public void PostingWorkArea_TimeTracker(MstEmploeeEntity emp, DateTime sumDt) {
            // 実績CSV出力
            TimeTrackerUtil.OutputTimeEntry(_ConfigInfo, _EmploeeInfos, _TimeTrackerUserInfos, emp.EmploeeNum);

            // 出力内容取得
            string csvPath = Environment.CurrentDirectory;
            var ttInfo = _TimeTrackerUserInfos.Where(n => CommonUtil.CutSpace(n.UserName) == CommonUtil.CutSpace(emp.Name)).FirstOrDefault();
            csvPath = Path.Combine(csvPath, TimeTrackerUtil.GetTimeEntryCsvName(ttInfo));
            if (!File.Exists(csvPath)) {
                return;
            }
            var csvDatas = TimeTrackerUtil.LoadTimeEntryCsv(_ConfigInfo, csvPath);
            if (csvDatas.Count == 0) {
                return;
            }
            csvDatas = TimeTrackerUtil.SortTimeEntryCsv(csvDatas);

            // 転記
            PostingWorkArea(emp, csvDatas, sumDt);
        }
        /// <summary>
        /// 作業内容/一覧転記（デフォ作業ユーザ）
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="sumDt"></param>
        public void PostingWorkArea_DefaultWork( MstEmploeeEntity emp, DateTime sumDt) {
            string kinmuhyoPath = GetKinmuhyoPath(emp, sumDt);
            var KadoDates = GetDefaultWorkJudgeDtos(kinmuhyoPath
                                                  , sumDt
                                                  , emp.MhscFlg == FlgValConst._STR_FLG_ON
                                                  , emp.ForcingShiftBFlg == FlgValConst._STR_FLG_ON
                                                  , emp.NightShiftFlg == FlgValConst._STR_FLG_ON);

            // デフォ作業データ取得
            var csvDatas = new List<EditTimeEntryDto>();
            KadoDates.ForEach(dto => csvDatas = AddDefaultWorkDto(csvDatas, emp, dto));

            // 件数チェック
            if (csvDatas.Count == 0) {
                return ;
            }
            // 転記
            PostingWorkArea(emp, csvDatas, sumDt);
        }
        /// <summary>
        /// 稼働日の作業データ判定dtoを取得
        /// </summary>
        /// <param name="kinmuhyoXlsxPath"></param>
        /// <param name="KijunDate"></param>
        /// <param name="isMHSC"></param>
        /// <param name="isForcingBshift"></param>
        /// <param name="isNightShiftFlg"></param>
        /// <returns></returns>
        public List<DefaultWorkJudgeDto> GetDefaultWorkJudgeDtos(string kinmuhyoXlsxPath, DateTime KijunDate, bool isMHSC, bool isForcingBshift, bool isNightShiftFlg) {
            var ret = new List<DefaultWorkJudgeDto>();
            var xlsxFile = File.OpenRead(kinmuhyoXlsxPath);
            using (var package = new ExcelPackage(new FileInfo(kinmuhyoXlsxPath))) {

                // 作業内容シート作成
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var sheet = package.Workbook.Worksheets[KinmuhyoConst._SHEET_NAME_KINMUHYO];

                // 稼働日をリストアップ
                for (int i = 1; i <= TimeConst._MAX_DAY_COUNT_MONTH; i++) {
                    DateTime dt;
                    string strDt = KijunDate.Year + CommonLiteral._LITERAL_HYPHEN + KijunDate.Month + CommonLiteral._LITERAL_HYPHEN + i;
                    if (!DateTime.TryParse(strDt, out dt)) {
                        // 日付変換できない場合はスルー
                        continue;
                    }
                    string tgtStartHHCell = KinmuhyoConst._COL_SHUKKIN_HH + (i + KinmuhyoConst._ROW_IDX_SHEET_HEADER);
                    string tgtStartMMCell = KinmuhyoConst._COL_SHUKKIN_MM + (i + KinmuhyoConst._ROW_IDX_SHEET_HEADER);
                    string tgtEndHHCell = KinmuhyoConst._COL_TAIKIN_HH + (i + KinmuhyoConst._ROW_IDX_SHEET_HEADER);
                    string tgtEndMMCell = KinmuhyoConst._COL_TAIKIN_MM + (i + KinmuhyoConst._ROW_IDX_SHEET_HEADER);
                    string tgtRestVal = KinmuhyoConst._COL_WORK_STATUS + (i + KinmuhyoConst._ROW_IDX_SHEET_HEADER);
                    var tgtStartHHVal = sheet.Cells[tgtStartHHCell].Value;
                    var tgtStartMMVal = sheet.Cells[tgtStartMMCell].Value;
                    var tgtEndHHVal = sheet.Cells[tgtEndHHCell].Value;
                    var tgtEndMMVal = sheet.Cells[tgtEndMMCell].Value;
                    var tgtDayRestVal = sheet.Cells[tgtRestVal].Value;
                    if (tgtStartHHVal == null && tgtEndHHVal == null && tgtDayRestVal == null) {

                        // 勤怠データが取得できない場合はスルー
                        continue;
                    }
                    // 作業データdto作成
                    var wtDto = new WorkTimeDto();
                    wtDto.Day = dt.Day;
                    wtDto.StartHH = CommonUtil.ToStringForbidNull(tgtStartHHVal);
                    wtDto.StartMM = CommonUtil.ToStringForbidNull(tgtStartMMVal);
                    wtDto.EndHH = CommonUtil.ToStringForbidNull(tgtEndHHVal);
                    wtDto.EndMM = CommonUtil.ToStringForbidNull(tgtEndMMVal);
                    wtDto.RestMark = tgtDayRestVal == null ? string.Empty : KinmuhyoConst._MARK_REST;

                    // 作業データ判定dto作成
                    var judgeDto = CreateDefaultWorkJudgeDto(sheet, wtDto, dt, isMHSC, isForcingBshift, isNightShiftFlg);
                    ret.Add(judgeDto);
                }
            }
            return ret;
        }
        /// <summary>
        /// 作業データ判定dto作成
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="wtDto"></param>
        /// <param name="dt"></param>
        /// <param name="isMHSC"></param>
        /// <param name="isForcingBshift"></param>
        /// <param name="isNightShiftFlg"></param>
        /// <returns></returns>
        private DefaultWorkJudgeDto CreateDefaultWorkJudgeDto(ExcelWorksheet sheet, WorkTimeDto wtDto, DateTime dt,
            bool isMHSC, bool isForcingBshift, bool isNightShiftFlg) {

            // 稼働日と休暇タイプをセット
            var ret = new DefaultWorkJudgeDto() {
                WorkDate = dt,
                RestType = KinmuhyoUtil.RoundWorkTimeProxy(_ConfigInfo.DefaultKinmuhyoFormat, dt, wtDto, isMHSC, isForcingBshift).RestType,
            };
            if (string.IsNullOrEmpty(wtDto.StartMM) || string.IsNullOrEmpty(wtDto.StartMM)
                || string.IsNullOrEmpty(wtDto.EndHH) || string.IsNullOrEmpty(wtDto.EndMM)) {

                // 出退勤時刻がすべて取得できていない場合はスルー
                return ret;
            }
            // 稼働時間の取得
            decimal KadoTime = 0;
            decimal StartHHVal = decimal.Parse(wtDto.StartHH);
            decimal StartMMVal = decimal.Parse(wtDto.StartMM);
            decimal EndHHVal = decimal.Parse(wtDto.EndHH);
            decimal EndMMVal = decimal.Parse(wtDto.EndMM);
            KadoTime = (EndHHVal * TimeConst._MINUTE_AN_HOUR + EndMMVal) - (StartHHVal * TimeConst._MINUTE_AN_HOUR + StartMMVal);

            // 夜勤の場合、休憩時間を2時間とる
            decimal decStartVal = StartHHVal * TimeConst._MINUTE_AN_HOUR + StartMMVal;
            decimal decEndVal = EndHHVal * TimeConst._MINUTE_AN_HOUR + EndMMVal;
            if (isNightShiftFlg && ((KinmuhyoConst._NIGHT_SHIFT_START_HH - 1) * TimeConst._MINUTE_AN_HOUR) <= decStartVal) {
                KadoTime -= TimeConst._MINUTE_AN_HOUR * 2;
            } else {
                // 稼働時間から昼休憩を抜く
                KadoTime -= GetNoonRestTime(decStartVal, decEndVal);
            }
            ret.WorkTime = KadoTime;
            ret.StartTime = StartHHVal * TimeConst._MINUTE_AN_HOUR + StartMMVal;
            ret.EndTime = EndHHVal * TimeConst._MINUTE_AN_HOUR + EndMMVal;

            // 深夜休憩時間の算出
            if (ret.StartTime < (KinmuhyoConst._NIGHT_SHIFT_START_HH - 1) * TimeConst._MINUTE_AN_HOUR
            && TimeConst._NIGHT_RESTSTART_HH * TimeConst._MINUTE_AN_HOUR < ret.EndTime) {
                // 深夜の休憩時間を抜く
                decimal delMidNightRest = ret.EndTime - (TimeConst._NIGHT_RESTSTART_HH * TimeConst._MINUTE_AN_HOUR);
                if ((TimeConst._MINUTE_AN_HOUR / 2) < delMidNightRest) {
                    delMidNightRest = TimeConst._MINUTE_AN_HOUR / 2;
                }
                ret.WorkTime -= delMidNightRest;
            }
            return ret;
        }
        /// <summary>
        /// 昼休憩時間取得
        /// </summary>
        /// <param name="decStartVal"></param>
        /// <param name="decEndVal"></param>
        /// <returns></returns>
        public decimal GetNoonRestTime(decimal decStartVal, decimal decEndVal) {
            if ( TimeConst._DEFAULT_NOON_REST_START_HH * TimeConst._MINUTE_AN_HOUR < decStartVal) {
                return decimal.Zero;
            }
            if (decEndVal < TimeConst._DEFAULT_NOON_REST_END_HH * TimeConst._MINUTE_AN_HOUR) {
                return decimal.Zero;
            }
            return TimeConst._MINUTE_AN_HOUR;
        }
        /// <summary>
        /// 作業内容/一覧クリア
        /// </summary>
        /// <param name="sheet"></param>
        public void ClearWorkArea(ExcelWorksheet sheet) {
            // 作業内容欄
            for (int rowIdx = KinmuhyoConst._ROW_IDX_SHEET_HEADER + 1; rowIdx <= KinmuhyoConst._ROW_IDX_SHEET_HEADER + TimeConst._MAX_DAY_COUNT_MONTH; rowIdx++) {
                for (int workIdx = 0; workIdx < KinmuhyoConst._MAX_WORK_CNT_WORK_DETAILS; workIdx++) {
                    int colIdx = KinmuhyoConst._COL_IDX_FIRST_WORK_NAME + (workIdx * KinmuhyoConst._COL_INTERVAL_WORK_DETAILS_WORK);
                    sheet.Cells[rowIdx, colIdx].Value = null;
                    sheet.Cells[rowIdx, colIdx + 1].Value = null;
                    sheet.Cells[rowIdx, colIdx + 2].Value = null;
                }
            }
            // 作業一覧
            int curIdx = 0;
            for (int rowIdx = KinmuhyoConst._ROW_IDX_MST_START; curIdx < KinmuhyoConst._MAX_ROW_CNT_WORK_MASTER; rowIdx++, curIdx++) {
                sheet.Cells[KinmuhyoConst._COL_MST_CHARGE_NO + rowIdx].Value = null;
                sheet.Cells[KinmuhyoConst._COL_MST_NAME + rowIdx].Value      = null;
            }
        }
        /// <summary>
        /// 作業内容/一覧転記
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="csvDatas"></param>
        /// <param name="sumDt"></param>
        public void PostingWorkArea(MstEmploeeEntity emp, List<EditTimeEntryDto> csvDatas, DateTime sumDt) {
            if (csvDatas == null || csvDatas.Count == 0) {
                return;
            }
            string KinmuhyoPath = GetKinmuhyoPath(emp, sumDt);
            string kinmuhyoName = KinmuhyoUtil.GetKinmuhyoName(_ConfigInfo, emp.Name, sumDt);
            string commonPath = KinmuhyoUtil.GetKinmuhyoPath(_ConfigInfo, _BuList, _GroupList, emp, sumDt);
            string tempPath = Path.Combine(commonPath, CommonLiteral._MARK_STAR + kinmuhyoName);
            var xlsxFile = File.OpenRead(KinmuhyoPath);
            using (var package = new ExcelPackage(new FileInfo(KinmuhyoPath))) {

                // 作業名からチャージ№を除く
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                foreach (var data in csvDatas) {
                    var dto = KinmuhyoUtil.RemoveChargeNo(data);
                    data.ProjectCode = dto.ProjectCode;
                    data.ProjectName = dto.ProjectName;
                }
                var sheet = package.Workbook.Worksheets[KinmuhyoConst._SHEET_NAME_KINMUHYO];

                // 作業内容/一覧クリア
                if (csvDatas.Count() != 0) {
                    ClearWorkArea(sheet);
                }
                // 作業内容シート作成
                CreateWorkDetails(sheet, xlsxFile, csvDatas);

                // 作業マスタシート作成
                CreateWorkMaster(sheet, csvDatas, emp);

                // 保存
                package.SaveAs(new FileInfo(tempPath));
            }
            xlsxFile.Close();
            CommonUtil.DeleteFile(KinmuhyoPath);
            File.Copy(tempPath, KinmuhyoPath);
            CommonUtil.DeleteFile(tempPath);
        }
        /// <summary>
        /// 作業内容欄作成
        /// </summary>
        /// <param name="package"></param>
        /// <param name="xlsxFile"></param>
        /// <param name="csvDatas"></param>
        /// <returns></returns>
        public void CreateWorkDetails(ExcelWorksheet Sheet, FileStream xlsxFile, List<EditTimeEntryDto> csvDatas) {
            DateTime dtOld = DateTime.MinValue;
            int tgtWorkNameColIndex = KinmuhyoConst._COL_IDX_FIRST_WORK_NAME;
            foreach (var data in csvDatas) {
                DateTime dtDate;
                if (!DateTime.TryParse(data.WorkDate, out dtDate)) {
                    continue;
                }
                int tgtRowIndex = dtDate.Day + KinmuhyoConst._ROW_IDX_SHEET_HEADER;
                if (dtOld == dtDate) {
                    tgtWorkNameColIndex += KinmuhyoConst._COL_INTERVAL_WORK_DETAILS_WORK;
                } else {
                    tgtWorkNameColIndex = KinmuhyoConst._COL_IDX_FIRST_WORK_NAME;
                    dtOld = dtDate;
                }
                Sheet.Cells[tgtRowIndex, tgtWorkNameColIndex    ].Value = data.ProjectName;
                Sheet.Cells[tgtRowIndex, tgtWorkNameColIndex + 1].Value = data.ManHourH;
                Sheet.Cells[tgtRowIndex, tgtWorkNameColIndex + 2].Value = data.ManHourM;
            }
        }
        /// <summary>
        /// 作業マスタ作成
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="csvDatas"></param>
        /// <param name="emp"></param>
        public void CreateWorkMaster(ExcelWorksheet sheet, List<EditTimeEntryDto> csvDatas, MstEmploeeEntity emp) {

            // チャージ№リスト作成
            List<KeyValuePair<string, string>> ChargeNumList = new List<KeyValuePair<string, string>>();
            if (emp.MhscFlg == FlgValConst._STR_FLG_ON) {
                _MhscMstInfos.ForEach(m => ChargeNumList.Add(new KeyValuePair<string, string>(m.ChargeNo, m.WorkName)));
            } else {
                csvDatas.ForEach(v => ChargeNumList.Add(new KeyValuePair<string, string>(v.ProjectCode, v.ProjectName)));
                ChargeNumList = ChargeNumList.Distinct().ToList();
            }
            ChargeNumList = ChargeNumList.OrderBy(n => n.Key).ToList();

            // 明細
            int masterCount = 0;
            foreach (var master in ChargeNumList) {
                if (masterCount < KinmuhyoConst._MAX_ROW_CNT_WORK_MASTER) {
                    sheet.Cells[KinmuhyoConst._COL_MST_CHARGE_NO + (masterCount + KinmuhyoConst._ROW_IDX_MST_START)].Value = master.Key;
                    sheet.Cells[KinmuhyoConst._COL_MST_NAME      + (masterCount + KinmuhyoConst._ROW_IDX_MST_START)].Value = master.Value;
                    masterCount++;
                }
            }
        }
        #endregion

        #region デフォ作業Dto
        /// <summary>
        /// デフォ作業Dto取得
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="emp"></param>
        /// <param name="judgeDto"></param>
        /// <returns></returns>
        public List<EditTimeEntryDto> AddDefaultWorkDto(List<EditTimeEntryDto> ret, MstEmploeeEntity emp, DefaultWorkJudgeDto judgeDto) {
            var work1 = GetDefaultWorkDto(judgeDto.WorkDate
                                        , emp.DefaultWorkChargeNo1
                                        , emp.DefaultWorkName1
                                        , int.Parse(emp.DefaultWorkTimeH1)
                                        , int.Parse(emp.DefaultWorkTimeM1)
                                        , int.Parse(emp.MhscFlg));

            var work2 = GetDefaultWorkDto(judgeDto.WorkDate
                                        , emp.DefaultWorkChargeNo2
                                        , emp.DefaultWorkName2
                                        , int.Parse(emp.DefaultWorkTimeH2)
                                        , int.Parse(emp.DefaultWorkTimeM2)
                                        , int.Parse(emp.MhscFlg));

            var work3 = GetDefaultWorkDto(judgeDto.WorkDate
                                        , emp.DefaultWorkChargeNo3
                                        , emp.DefaultWorkName3
                                        , int.Parse(emp.DefaultWorkTimeH3)
                                        , int.Parse(emp.DefaultWorkTimeM3)
                                        , int.Parse(emp.MhscFlg));

            var workRest = GetDefaultWorkDto_Rest(judgeDto, int.Parse(emp.MhscFlg));

            // 稼働時間≠作業時間の場合、差分を作業１に含める
            decimal totalWorkTime = work1.ManHour + work2.ManHour + work3.ManHour;
            if (judgeDto.RestType != TimeConst.RestType.PAID && judgeDto.WorkTime != 0 && totalWorkTime != judgeDto.WorkTime) {
                work1.ManHour += (judgeDto.WorkTime - totalWorkTime);
                work1.ManHourH = (int)(work1.ManHour / TimeConst._MINUTE_AN_HOUR);
                work1.ManHourM = (int)(work1.ManHour % TimeConst._MINUTE_AN_HOUR);
            }
            if (judgeDto.RestType != TimeConst.RestType.PAID && work1.ManHour != 0) {
                ret.Add(work1);
            }
            if (judgeDto.RestType != TimeConst.RestType.PAID && work2.ManHour != 0) {
                ret.Add(work2);
            }
            if (judgeDto.RestType != TimeConst.RestType.PAID && work3.ManHour != 0) {
                ret.Add(work3);
            }
            if (!string.IsNullOrEmpty(workRest.ProjectCode) && workRest.ManHour != 0) {
                ret.Add(workRest);
            }
            return ret;
        }
        /// <summary>
        /// デフォルト作業Dto作成
        /// </summary>
        /// <param name="workDate"></param>
        /// <param name="chargeNo"></param>
        /// <param name="workName"></param>
        /// <param name="workTimeHH"></param>
        /// <param name="workTimeMM"></param>
        /// <param name="MhscFlg"></param>
        /// <returns></returns>
        public EditTimeEntryDto GetDefaultWorkDto(DateTime workDate, string chargeNo, string workName, int workTimeHH, int workTimeMM, int MhscFlg) {
            var ret = new EditTimeEntryDto();
            if (!string.IsNullOrEmpty(chargeNo)) {
                ret = new EditTimeEntryDto() {
                    ProjectCode = chargeNo,
                    ProjectName = workName,
                    ManHour = (workTimeHH * TimeConst._MINUTE_AN_HOUR) + workTimeMM,
                    ManHourH = workTimeHH,
                    ManHourM = workTimeMM,
                    WorkDate = workDate.ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH),
                };
                if (MhscFlg == 1) {
                    var tgtWork = _MhscMstInfos.Where(n => n.ChargeNo == chargeNo).FirstOrDefault();
                    if (tgtWork != null) {
                        ret.ProjectName = tgtWork.WorkName;
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// デフォルト作業Dto作成（休暇分）
        /// </summary>
        /// <param name="judgeDto"></param>
        /// <param name="MhscFlg"></param>
        /// <returns></returns>
        public EditTimeEntryDto GetDefaultWorkDto_Rest(DefaultWorkJudgeDto judgeDto, int MhscFlg) {
            var workRest = new EditTimeEntryDto();
            decimal manHour = decimal.Zero;
            string chargeNoRest = KinmuhyoConst._CHARGE_NO_REST;
            string workNameRest = KinmuhyoConst._WORK_NAME_REST;
            if (MhscFlg == 1) {
                var tgtRestWork = _MhscMstInfos.Where(n => n.RestFlg != 0).FirstOrDefault();
                if (tgtRestWork != null) {
                    chargeNoRest = tgtRestWork.ChargeNo;
                    workNameRest = tgtRestWork.WorkName;
                }
            }
            workRest = new EditTimeEntryDto() {
                ProjectCode = chargeNoRest,
                ProjectName = workNameRest,
                WorkDate = judgeDto.WorkDate.ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH),
            };
            switch (judgeDto.RestType) {
                case TimeConst.RestType.PAID:
                    workRest.ManHour = (TimeConst._DEFAULT_WORK_TIME_PER_DAY * TimeConst._MINUTE_AN_HOUR);
                    workRest.ManHourH = TimeConst._DEFAULT_WORK_TIME_PER_DAY;
                    workRest.ManHourM = (int)decimal.Zero;
                    break;
                case TimeConst.RestType.AM:
                    manHour = judgeDto.StartTime - (TimeConst._DEFAULT_WORK_START_TYPE_B_HH * TimeConst._MINUTE_AN_HOUR);
                    workRest.ManHour = manHour;
                    workRest.ManHourH = (int)(manHour / TimeConst._MINUTE_AN_HOUR);
                    workRest.ManHourM = (int)(manHour % TimeConst._MINUTE_AN_HOUR);
                    break;
                case TimeConst.RestType.PM:
                    manHour = (TimeConst._DEFAULT_WORK_END_TYPE_B_HH * TimeConst._MINUTE_AN_HOUR) - judgeDto.EndTime;
                    workRest.ManHour = manHour;
                    workRest.ManHourH = (int)(manHour / TimeConst._MINUTE_AN_HOUR);
                    workRest.ManHourM = (int)(manHour % TimeConst._MINUTE_AN_HOUR);
                    break;
                case TimeConst.RestType.ONE_QUARTER:
                    manHour = TimeConst._MINUTE_AN_HOUR * 2;
                    workRest.ManHour = manHour;
                    workRest.ManHourH = (int)(manHour / TimeConst._MINUTE_AN_HOUR);
                    workRest.ManHourM = (int)(manHour % TimeConst._MINUTE_AN_HOUR);
                    break;
                case TimeConst.RestType.TWO_QUARTER:
                    manHour = TimeConst._MINUTE_AN_HOUR * 4;
                    workRest.ManHour = manHour;
                    workRest.ManHourH = (int)(manHour / TimeConst._MINUTE_AN_HOUR);
                    workRest.ManHourM = (int)(manHour % TimeConst._MINUTE_AN_HOUR);
                    break;
                case TimeConst.RestType.THREE_QUARTER:
                    decimal WorkTimeLength = GetWorkTimeLength(judgeDto.StartTime, judgeDto.EndTime);
                    manHour = TimeConst._MINUTE_AN_HOUR * 6;
                    workRest.ManHour = manHour;
                    workRest.ManHourH = (int)(manHour / TimeConst._MINUTE_AN_HOUR);
                    workRest.ManHourM = (int)(manHour % TimeConst._MINUTE_AN_HOUR);
                    break;
            }
            return workRest;
        }
        #endregion
    }
}
