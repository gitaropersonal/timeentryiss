﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity {
    public class MhscMstEntity {
        public string Nendo { get; set; }
        public string ChargeNo { get; set; }
        public string WorkName { get; set; }
        public int RestFlg { get; set; }
        public int DelFlg { get; set; }
    }
}
