﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity {
    public class WorkTimeAnalysisEntity {
        public WorkTimeAnalysisEntity() {
            CalcDatas = new WTECalcDatas();
        }
        public string date { get; set; }
        public string projectId { get; set; }
        public string projectCode { get; set; }
        public string projectName { get; set; }
        public string userId { get; set; }
        public string userName { get; set; }
        public string plannedTime { get; set; }
        public string actualTime { get; set; }
        public string plannedCost { get; set; }
        public string actualCost { get; set; }
        public string organization { get; set; }
        public WTECalcDatas CalcDatas { get; set; }
    }
    public class WTECalcDatas {
        public decimal decPlannedManHour { get; set; }
        public decimal decActualManHour { get; set; }
        public decimal decPlannedPct { get; set; }
        public decimal decActualPct { get; set; }
        public decimal decPlannedHumanMoon { get; set; }
        public decimal decActualHumanMoon { get; set; }
        public decimal decPlannedCost { get; set; }
        public decimal decActualCost { get; set; }
        public decimal totalPlannedTime { get; set; }
        public decimal totalActualTime { get; set; }
        public decimal totalPlannedManHour { get; set; }
        public decimal totalActualManHour { get; set; }
        public decimal totalPlannedPct { get; set; }
        public decimal totalActualPct { get; set; }
        public decimal totalPlannedHumanMoon { get; set; }
        public decimal totalActualHumanMoon { get; set; }
        public decimal totalPlannedCost { get; set; }
        public decimal totalActualCost { get; set; }
    }
}
