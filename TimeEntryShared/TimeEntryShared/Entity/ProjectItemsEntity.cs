﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity {
    public class ProjectItemsEntity {
        public string name { get; set; }
        public string code { get; set; }
        public string description { get; set; }
        public string managerId { get; set; }
        public string managerName { get; set; }
        public string plannedStartDate { get; set; }
        public string plannedFinishDate { get; set; }
        public string isFinished { get; set; }
        public string workItemRootFolderId { get; set; }
        public string documentItemRootFolderId { get; set; }
        public string canEdit { get; set; }
        public string profileId { get; set; }
        public string isLocked { get; set; }
        public string lockedAt { get; set; }
        public string lockedBy { get; set; }
        public string organizationId { get; set; }
        public string organizationName { get; set; }
        public string lockedApp { get; set; }
        public string isDeleted { get; set; }
        public string aclId { get; set; }
        public string permission { get; set; }
        public string standardUnitCost { get; set; }
        public string projectCategories { get; set; }
        public string id { get; set; }
        public string createdAt { get; set; }
        public string createdBy { get; set; }
        public string updatedAt { get; set; }
        public string updatedBy { get; set; }

    }
}
