﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity {
    public class WorkTimesConfigEntity {
        public string ComponentsPath { get; set; }
        public string TxtJsonWorkTimeName { get; set; }
        public string BatName { get; set; }
        public string WorkTimesCsv { get; set; }
        public string CsvWorkTimesCols_UserName { get; set; }
        public string CsvWorkTimesCols_ProjectCode { get; set; }
        public string CsvWorkTimesCols_ProjectName { get; set; }
        public string CsvWorkTimesCols_WorkDate { get; set; }
        public string CsvWorkTimesCols_StartTime { get; set; }
        public string CsvWorkTimesCols_FinishName { get; set; }
    }
}
