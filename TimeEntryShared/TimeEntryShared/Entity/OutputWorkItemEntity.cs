﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity {
    public class OutputWorkItemEntity {
        public string Id { get; set; }
        public string Name { get; set; }
        public string PlannedStartDate { get; set; }
        public string PlannedFinishDate { get; set; }
        public decimal PlannedTime { get; set; }
        public decimal ActualTime { get; set; }
        public bool IsAclInherited { get; set; }
        public bool CanEdit { get; set; }
        public string ParentId { get; set; }
    }
}
