﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity {
    public class OutputManHourCsvEntity {
        public string UserName { get; set; }
        public string UserCode { get; set; }
        public string CurrentTeam { get; set; }
        public string CurrentTeamCode { get; set; }
        public string TgtMonthTeam { get; set; }
        public string TgtMonthTeamCode { get; set; }
        public string ProjectName { get; set; }
        public string ProjectCode { get; set; }
        public string Client { get; set; }
        public string WorkField { get; set; }
        public string WorkType { get; set; }
        public string Priority { get; set; }
        public string WorkDate { get; set; }
        public string ActualManHour { get; set; }
        public string ActualManHourMinute { get; set; }
        public string ActualCost { get; set; }

    }
}
