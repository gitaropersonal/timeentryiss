﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity{
    public class UserInfoEntity{
        public string name { get; set; }
        public string englishName { get; set; }
        public string loginName { get; set; }
        public string email { get; set; }
        public string code { get; set; }
        public string organizationName { get; set; }
        public string language { get; set; }
        public string timeZone { get; set; }
        public string isDeleted { get; set; }
        public string unitCost { get; set; }
        public string organizationId { get; set; }
        public string timeEntryLockedDate { get; set; }
        public string description { get; set; }
        public string ldapLoginName { get; set; }
        public string systemRoleId { get; set; }
        public string systemRoleName { get; set; }
        public string canEdit { get; set; }
        public string aclId { get; set; }
        public string permission { get; set; }
        public string id { get; set; }
        public string createdAt { get; set; }
        public string createdBy { get; set; }
        public string updatedAt { get; set; }
        public string updatedBy { get; set; }
    }
}
