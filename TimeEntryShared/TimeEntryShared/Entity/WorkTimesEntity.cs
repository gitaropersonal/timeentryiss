﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity {
    public class WorkTimesEntity {
        public string projectId { get; set; }
        public string projectName { get; set; }
        public string projectCode { get; set; }
        public string workDate { get; set; }
        public string startTime { get; set; }
        public string finishTime { get; set; }
        public string time { get; set; }
        public string memo { get; set; }
        public string workItemId { get; set; }
        public string workItemName { get; set; }
        public string workItemNumber { get; set; }
        public string workItemTypeId { get; set; }
        public string userId { get; set; }
        public string userName { get; set; }
        public string timeEntryCategoryId { get; set; }
        public string timeEntryCategory { get; set; }
        public string processCategoryId { get; set; }
        public string processCategory { get; set; }
        public string isLocked { get; set; }
        public string isDeleted { get; set; }
        public string id { get; set; }
        public string createdAt { get; set; }
        public string createdBy { get; set; }
        public string updatedAt { get; set; }
        public string updatedBy { get; set; }
    }
}
