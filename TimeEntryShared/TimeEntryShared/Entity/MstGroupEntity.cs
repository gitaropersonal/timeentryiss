﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity{
    public class MstGroupEntity {
        public string BuCode { get; set; }
        public string GroupCode { get; set; }
        public string GroupName { get; set; }
        public string DelFlg { get; set; }
    }
}
