﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity{
    public class MstBuEntity {
        public string BuCode { get; set; }
        public string BuName { get; set; }
        public string DelFlg { get; set; }
    }
}
