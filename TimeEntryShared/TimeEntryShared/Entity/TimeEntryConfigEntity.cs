﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeEntryShared.Entity {
    public class TimeEntryConfigEntity {
        public string EmploeePath { get; set; }
        public string URL_WHITE_BORD { get; set; }
        public string SumStartDate { get; set; }
        public string SumEndDate { get; set; }
        public string BackDaysCount { get; set; }
        public int WbLookPreMonthCount { get; set; }
        public int UsingCsvMstFlg { get; set; }
        public string KinmuhyoPath { get; set; }
        public string KinmuhyoName { get; set; }
        public string DefaultKinmuhyoPath { get; set; }
        public string DefaultKinmuhyoName { get; set; }
        public string DefaultKinmuhyoFormat { get; set; }
        public int ForcingReCreateFlg { get; set; }
        public string WtAnalysisWorkFolder { get; set; }
        public string ConnStrWBDB { get; set; }
        public string ConnStrTimeEntryDB { get; set; }
    }
}
