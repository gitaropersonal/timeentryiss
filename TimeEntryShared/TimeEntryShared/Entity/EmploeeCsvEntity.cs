﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity{
    public class MstEmploeeEntity {
        public string BuCode { get; set; }
        public string GroupCode { get; set; }
        public string Name { get; set; }
        public string EmploeeNum { get; set; }
        public string LoginPass { get; set; }
        public string NightShiftFlg { get; set; }
        public string DefaultWorkFlg { get; set; }
        public string DefaultWorkChargeNo1 { get; set; }
        public string DefaultWorkName1 { get; set; }
        public string DefaultWorkTimeH1 { get; set; }
        public string DefaultWorkTimeM1 { get; set; }
        public string DefaultWorkChargeNo2 { get; set; }
        public string DefaultWorkName2 { get; set; }
        public string DefaultWorkTimeH2 { get; set; }
        public string DefaultWorkTimeM2 { get; set; }
        public string DefaultWorkChargeNo3 { get; set; }
        public string DefaultWorkName3 { get; set; }
        public string DefaultWorkTimeH3 { get; set; }
        public string DefaultWorkTimeM3 { get; set; }
        public string MhscFlg { get; set; }
        public string ForcingShiftBFlg { get; set; }
        public string DeleteFlg { get; set; }
    }
}
