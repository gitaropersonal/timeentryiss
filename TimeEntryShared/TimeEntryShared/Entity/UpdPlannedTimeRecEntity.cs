﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Entity {
    public class UpdPlannedTimeRecEntity {
        public string ProjectId { get; set; }
        public string UserId { get; set; }
        public DateTime YearMonth { get; set; }
        public string InitHostName { get; set; }
        public decimal PlannedTime { get; set; }
        public DateTime InitDate { get; set; }
    }
}
