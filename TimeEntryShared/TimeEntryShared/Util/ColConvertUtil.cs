﻿using System;
using System.Collections.Generic;
using TimeEntryShared.Const;

namespace TimeEntryShared.Util {
    static class ColConvertUtil {
        /// <summary>
        /// Excelのカラム名的なアルファベット文字列へ変換
        /// </summary>
        /// <param name="self"></param>
        /// <returns></returns>
        public static string ToAlphabet(this int self) {
            if (self <= 0) {
                return string.Empty;
            }
            int n = self % 26;
            n = (n == 0) ? 26 : n;
            string s = ((char)(n + 64)).ToString();
            if (self == n) {
                return s;
            }
            return ((self - n) / 26).ToAlphabet() + s;
        }
        /// <summary>
        /// セル範囲アルファベット取得
        /// </summary>
        /// <param name="startCellNum"></param>
        /// <param name="Delta"></param>
        /// <param name="tgtRowNumStart"></param>
        /// <param name="tgtRowNumEnd"></param>
        /// <returns></returns>
        public static string GetCellRangeAlphabeth(int startCellNum, int Delta, int tgtRowNumStart, int tgtRowNumEnd) {
            string minColAlphabet = ToAlphabet(startCellNum);
            string maxColAlphabet = ToAlphabet(startCellNum + Delta);
            return string.Concat(minColAlphabet, tgtRowNumStart) + CommonLiteral._LITERAL_COLON + string.Concat(maxColAlphabet, tgtRowNumEnd);
        }
        /// <summary>
        /// 列範囲アルファベット取得
        /// </summary>
        /// <param name="startCellNum"></param>
        /// <param name="Delta"></param>
        /// <returns></returns>
        public static string GetColRangeAlphabeth(int startCellNum, int Delta) {
            string minColAlphabet = ToAlphabet(startCellNum);
            string maxColAlphabet = ToAlphabet(startCellNum + Delta);
            return string.Concat(minColAlphabet, CommonLiteral._LITERAL_COLON, maxColAlphabet);
        }
    }
}