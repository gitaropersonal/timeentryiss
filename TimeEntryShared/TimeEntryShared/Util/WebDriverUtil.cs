﻿using System;
using System.Linq;
using System.Diagnostics;
using System.IO;
using System.Net;
using Microsoft.Win32;
using TimeEntryShared.Const;
using WebDriverManager;
using WebDriverManager.DriverConfigs.Impl;

namespace TimeEntryShared.Util {
    public static class WebDriverUtil {
        /// <summary>
        /// ChromeDriver更新
        /// </summary>
        public static void UpdateChromeDriver() {
            // ローカルChromeバージョン
            string localChromeVersion = GetChromeVersion();

            // ローカルChromeに対応した最新版マイナーバージョンを取得
            var client = new WebClient();
            string address = string.Concat(WebDriverConst._URL_LATEST_CHROME_DRIVER, localChromeVersion.Substring(0, localChromeVersion.LastIndexOf(CommonLiteral._CHAR_DOT)));

            // ドライバーのバージョンと種別を取得
            var chromeConfig = new ChromeConfig();
            string driverVersion = client.DownloadString(address);
            string driverType = chromeConfig.GetName();

            // ダウンロードフォルダ作成
            string downLoadDriverPath = Path.Combine(Environment.CurrentDirectory, driverType);
            downLoadDriverPath = Path.Combine(downLoadDriverPath, driverVersion);
            downLoadDriverPath = Path.Combine(downLoadDriverPath, WebDriverConst._BIT_TYPE64);
            CommonUtil.CreateFolder(downLoadDriverPath);

            // Chromeドライバーをダウンロード
            downLoadDriverPath = Path.Combine(downLoadDriverPath, WebDriverConst._DRIVER_NAME_CHROME);
            LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Format(WebDriverMessages._LATEST_CHROME_BROWSER_VER, localChromeVersion));
            LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Format(WebDriverMessages._LATEST_CHROME_DRIVER_VER, driverVersion));
            var driverManeger = new DriverManager();
            driverManeger.SetUpDriver(chromeConfig, driverVersion);

            // ドライバーをローカルにコピー
            string environmentDriverPath = Path.Combine(Environment.CurrentDirectory, WebDriverConst._DRIVER_NAME_CHROME);
            File.Copy(downLoadDriverPath, environmentDriverPath, true);
            LogAndConsoleUtil.ShowLogAndConsoleInfo(WebDriverMessages._UPD_CHROME_DRIVER_FINISH);
        }
        /// <summary>
        /// Chromeバージョン取得
        /// </summary>
        /// <returns></returns>
        public static string GetChromeVersion() {
            string wowNode = string.Empty;
            if (Environment.Is64BitOperatingSystem) wowNode = @"Wow6432Node\";

            RegistryKey regKey = Registry.LocalMachine;
            RegistryKey keyPath = regKey.OpenSubKey(@"Software\" + wowNode + @"Google\Update\Clients");

            if (keyPath == null) {
                regKey = Registry.CurrentUser;
                keyPath = regKey.OpenSubKey(@"Software\" + wowNode + @"Google\Update\Clients");
            }

            if (keyPath == null) {
                regKey = Registry.LocalMachine;
                keyPath = regKey.OpenSubKey(@"Software\Google\Update\Clients");
            }

            if (keyPath == null) {
                regKey = Registry.CurrentUser;
                keyPath = regKey.OpenSubKey(@"Software\Google\Update\Clients");
            }

            if (keyPath != null) {
                string[] subKeys = keyPath.GetSubKeyNames();
                foreach (string subKey in subKeys) {
                    object value = keyPath.OpenSubKey(subKey).GetValue("name");
                    bool found = false;
                    if (value != null) {
                        found = value.ToString().Equals("Google Chrome", StringComparison.InvariantCultureIgnoreCase);
                    }
                    if (found) {
                        return keyPath.OpenSubKey(subKey).GetValue("pv").ToString();
                    }
                }
            }
            return "不明";
        }
        /// <summary>
        /// chromeDriver強制終了
        /// </summary>
        public static void KillChromeDriver() {
            var ps = Process.GetProcesses().Where(pr => pr.ProcessName == "chromedriver");
            foreach (var p in ps) {
                string defaultDriverPath = Path.Combine(Environment.CurrentDirectory, WebDriverConst._DRIVER_NAME_CHROME);
                if (!p.HasExited && p.MainWindowTitle == defaultDriverPath) {
                    // 起動中かつTimeEntryClientの実行環境で起動しているドライバーのみ強制終了
                    p.Kill();
                }
            }
        }
    }
}
