﻿using System.IO;
using System.Collections.Generic;
using System.Globalization;

namespace TimeEntryShared.Util
{
    /// <summary>
    /// CSV作成クラス
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class CsvUtil<T> {
        /// <summary>
        /// CSV出力
        /// </summary>
        /// <param name="datas"></param>
        /// <param name="fileName"></param>
        /// <param name="Encoding"></param>
        public void OutPutCsv(List<T> datas, string fileName, string Encoding) {
            var enc = System.Text.Encoding.GetEncoding(Encoding);
            using (var textWriter = new StreamWriter(fileName, false, enc)) {
                using (var writer = new CsvHelper.CsvWriter(textWriter, CultureInfo.CurrentCulture)) {
                    // パラメータ設定
                    writer.Configuration.HasHeaderRecord = false;

                    // CSV出力
                    writer.WriteRecords(datas);
                }
            }
        }
    }
}
