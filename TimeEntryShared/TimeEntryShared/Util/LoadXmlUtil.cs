﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;

namespace TimeEntryShared.Util {
    public static class LoadXmlUtil {
        /// <summary>
        /// Configファイル読み取り（セクション）
        /// </summary>
        /// <param name="ConfigFileName"></param>
        /// <param name="SectionName"></param>
        /// <returns></returns>
        public static IEnumerable<XElement> GetConfigSection(string ConfigFileName, string SectionName) {
            //configファイルを読み取る
            XElement xml = XElement.Load(ConfigFileName);

            // 指定されたセクションを取得
            IEnumerable<XElement> ret = from item in xml.Elements(SectionName)
                                        select item;
            return ret;
        }
        /// <summary>
        /// Configファイル読み取り（単一要素）
        /// </summary>
        /// <param name="ConfigFileName"></param>
        /// <param name="SectionName"></param>
        /// <param name="ElementName"></param>
        /// <returns></returns>
        public static string GetConfigElement(string ConfigFileName, string SectionName, string ElementName) {
            //configファイルを読み取る
            XElement xml = XElement.Load(ConfigFileName);

            // 指定されたセクションを取得
            IEnumerable<XElement> Section = from item in xml.Elements(SectionName)
                                            select item;
            // 指定された要素を取得
            return GetElementFromSection(Section, ElementName);
        }
        /// <summary>
        /// セクションから単一要素取得
        /// </summary>
        /// <param name="Section"></param>
        /// <param name="ElementName"></param>
        /// <returns></returns>
        public static string GetElementFromSection(IEnumerable<XElement> Section, string ElementName) {
            // 指定された要素を取得
            var tgtList = new List<string>();
            foreach (XElement Elements in Section) {
                var tgtElement = Elements.Element(ElementName);

                // 要素が見つかった場合のみリストイン
                if (tgtElement != null) {
                    tgtList.Add(tgtElement.Value);
                }
            }
            string ret = string.Empty;
            if (tgtList.Count != 0) {
                ret = tgtList.First();
            }
            return ret;

        }
    }
}
