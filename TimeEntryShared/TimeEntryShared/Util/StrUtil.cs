﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TimeEntryShared.Const;

namespace TimeEntryShared.Util {
    public static class StrUtil {
        /// <summary>
        /// 文字列に含まれる半角記号を除外
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string CutHalfMarkChars(string text) {
            // Nullまたは空文字の場合はそのまま返却
            if (string.IsNullOrWhiteSpace(text)) return text;

            string result = string.Empty;
            string[] str = new string[text.Length];
            for (int i = 0; i < text.Length; i++) {
                str[i] = text.Substring(i, 1);
            }
            foreach (var item in str) {
                if (Regex.IsMatch(item, "^[!-/:-@¥[-`{-~]+$")) {
                    continue;
                }
                result += item;
            }
            return result;
        }
        /// <summary>
        /// 文字列に含まれる半角記号を除外（除外の例外を引数で設定）
        /// </summary>
        /// <param name="text"></param>
        /// <param name="exStr"></param>
        /// <returns></returns>
        public static string CutHalfMarkCharsEx(string text, string exStr) {
            // Nullまたは空文字の場合はそのまま返却
            if (string.IsNullOrWhiteSpace(text)) return text;

            string result = string.Empty;
            string[] str = new string[text.Length];
            for (int i = 0; i < text.Length; i++) {
                str[i] = text.Substring(i, 1);
            }
            foreach (var item in str) {
                if (Regex.IsMatch(item, "^[!-/:-@¥[-`{-~]+$") && item != exStr) {
                    continue;
                }
                result += item;
            }
            return result;
        }
        /// <summary>
        /// 文字列→数値→文字列変換
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string strToIntToString(string str) {
            int intVal;
            if (!int.TryParse(str, out intVal)) {
                return string.Empty;
            }
            return intVal.ToString();
        }
        /// <summary>
        /// 文字列型の数値に前ゼロをつける（2桁）
        /// </summary>
        /// <param name="strInt"></param>
        /// <returns></returns>
        public static string StrIntToStrPreZero2(string strInt) {
            if (string.IsNullOrEmpty(strInt)) {
                return decimal.Zero.ToString(FormatConst._FORMAT_PRE_ZERO_2);
            }
            var ret = int.Parse(strInt).ToString(FormatConst._FORMAT_PRE_ZERO_2);
            return ret;
        }
        /// <summary>
        /// 文字列から数字のみ抜き出す
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string SelNumericFromStr(string str) {
            return Regex.Replace(str, @"[^0-9]", "");
        }
        /// <summary>
        /// Tostring（null非許容）
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static string TostringNullForbid(object o) {
            if (o == null) {
                return string.Empty;
            }
            return o.ToString();
        }
        /// <summary>
        /// Nullチェック付きSubstring
        /// </summary>
        /// <param name="o"></param>
        /// <param name="startIdx"></param>
        /// <param name="endIdx"></param>
        /// <param name="defaultVal"></param>
        /// <returns></returns>
        public static string SubstringCheckNull(object o, int startIdx, int endIdx, string defaultVal = "") {
            if (o == null) {
                return defaultVal;
            }
            if (string.IsNullOrEmpty(o.ToString())) {
                return defaultVal;
            }
            return o.ToString().Substring(startIdx, endIdx);
        }
    }
}
