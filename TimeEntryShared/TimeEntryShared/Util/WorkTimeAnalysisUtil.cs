﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using TimeEntryShared.Const;
using TimeEntryShared.Dto;
using TimeEntryShared.Entity;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace TimeEntryShared.Util {
    public static class WorkTimeAnalysisUtil {
        /// <summary>
        /// 工数分析Excelフルパス取得
        /// </summary>
        /// <param name="dtStart"></param>
        /// <param name="dtEnd"></param>
        /// <param name="outputExcelPath"></param>
        /// <returns></returns>
        public static string GetWTAnalysisExcel(DateTime dtStart, DateTime dtEnd, string outputExcelPath) {
            string tgtMonthStart = dtStart.ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI);
            string tgtMonthEnd = dtEnd.ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI);
            string fileName = string.Format(WtAnalysisConst._FILE_NAME_ANALYSIS_TITLE, tgtMonthStart, tgtMonthEnd, string.Empty);
            string outputExcelName = string.Concat(fileName, WtAnalysisConst._EX_XLSX);
            string ret = Path.Combine(outputExcelPath, outputExcelName);
            return ret;
        }

        #region 分析データの編集
        /// <summary>
        /// 分析データの編集
        /// </summary>
        /// <param name="ConfigInfo"></param>
        /// <param name="material"></param>
        /// <param name="ttInfos"></param>
        /// <param name="sumStartDate"></param>
        /// <param name="sumEndDate"></param>
        /// <param name="tgtTTGroupName"></param>
        /// <param name="using160h"></param>
        /// <returns></returns>
        public static List<WorkTimeAnalysisEntity> EditAnalysisEntities(TimeEntryConfigEntity ConfigInfo, List<WorkTimeAnalysisEntity> material, List<UserInfoDto> ttInfos,
            DateTime sumStartDate, DateTime sumEndDate, string tgtTTGroupName = "", bool using160h = true) {

            // 月間稼働時間取得
            var WorkMonthTimeList = GetWorkMonthTimeList(ConfigInfo.DefaultKinmuhyoPath, sumStartDate, sumEndDate, TimeConst._MAX_DAY_COUNT_MONTH, using160h);

            // 標的の所属のみに絞る
            var ret = NarrowTgtEntites(material, ttInfos, tgtTTGroupName);

            // 計画工数更新履歴に該当レコードがある場合、データの計画工数を上書きする
            ret = UpdatePlannedTimeByRec(ConfigInfo.ConnStrTimeEntryDB, ttInfos, ret);

            // 作業名・所属・工数割り当て
            ret = SetManHourToEntities(ret, ttInfos, WorkMonthTimeList);

            // 集計単位の工数合計割り当て
            ret = SetTotalManHourToEntities(ret);

            // 割合・人月割り当て
            ret = SetAverageFromTotal(ret);

            // ソート
            ret = ret.OrderBy(n => n.organization)
                            .ThenBy(n => n.userId)
                            .ThenBy(n => n.projectCode)
                            .ThenBy(n => n.date)
                            .ToList();
            return ret;
        }
        /// <summary>
        /// 標的の所属のみに絞る（分析データの編集）
        /// </summary>
        /// <param name="material"></param>
        /// <param name="ttInfos"></param>
        /// <param name="tgtTTGroupName"></param>
        /// <returns></returns>
        private static List<WorkTimeAnalysisEntity> NarrowTgtEntites(List<WorkTimeAnalysisEntity> material, List<UserInfoDto> ttInfos, string tgtTTGroupName) {
            var ret = new List<WorkTimeAnalysisEntity>();
            if (string.IsNullOrEmpty(tgtTTGroupName)) {
                ret.AddRange(material);
                return ret;
            }
            foreach (var Analysis in material) {
                // 所属の取得
                var tgtTTInfo = ttInfos.Where(n => n.UserId == Analysis.userId).FirstOrDefault();
                if (tgtTTInfo == null) {
                    continue;
                }
                if (tgtTTInfo.OrganizationName != tgtTTGroupName) {
                    continue;
                }
                // 標的の所属のみ抽出
                ret.Add(Analysis);
            }
            return ret;
        }
        /// <summary>
        /// 計画工数更新履歴に該当レコードがある場合、データの計画工数を上書きする
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="ttInfos"></param>
        /// <param name="ret"></param>
        /// <returns></returns>
        private static List<WorkTimeAnalysisEntity> UpdatePlannedTimeByRec(string connString, List<UserInfoDto> ttInfos, List<WorkTimeAnalysisEntity> ret) {

            // 一定時間以上前のレコードを削除（工数分析に更新結果が反映されるまで時間がかかるため）
            ModelUtil.DelUpdPlannedTimeRec(connString, DateTime.Now);

            // レコード単位でループ
            foreach (var entity in ret) {

                // 計画工数更新レコード取得
                var dt = DateTime.Parse(entity.date);
                var con = new UpdPlannedTimeRecEntity() {
                    ProjectId = entity.projectId,
                    UserId = entity.userId,
                    YearMonth = new DateTime(dt.Year, dt.Month, 1),
                    InitDate = DateTime.Now,
                };
                var updRec = ModelUtil.LoadUpdPlanndeTimeRec(connString, con);
                if (updRec.Count == 0) {
                    continue;
                }
                string strUpdPlannedTime = updRec[0].PlannedTime.ToString();
                if (strUpdPlannedTime == entity.plannedTime) {
                    // Entityとレコードの計画工数が一致していたら、レコードを削除
                    ModelUtil.DelUpdPlannedTimeRec(connString, con);
                    continue;
                }
                // 一致していなければレコードの計画工数で上書き
                entity.plannedTime = strUpdPlannedTime;
                entity.plannedCost = GetUpdPlannedCost(ttInfos, entity).ToString();
            }
            return ret;
        }
        /// <summary>
        /// 計画コストの更新値取得
        /// </summary>
        /// <param name="ttInfos"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        private static decimal GetUpdPlannedCost(List<UserInfoDto> ttInfos, WorkTimeAnalysisEntity entity) {
            var decPlannedTime = decimal.Parse(entity.plannedTime);
            var decUnitCost = decimal.Parse(ttInfos.Where(n => n.UserId == entity.userId).First().UnitCost);
            return decUnitCost * (decPlannedTime / TimeConst._MINUTE_AN_HOUR);
        }
        /// <summary>
        /// 作業名・所属・工数割り当て（分析データの編集）
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="ttInfos"></param>
        /// <param name="WorkMonthTimeList"></param>
        /// <returns></returns>
        private static List<WorkTimeAnalysisEntity> SetManHourToEntities(List<WorkTimeAnalysisEntity> ret, List<UserInfoDto> ttInfos
            , Dictionary<DateTime, int> WorkMonthTimeList) {

            foreach (var Analysis in ret) {

                // 作業名からチャージ№抽出
                var dto = new EditTimeEntryDto();
                dto.ProjectCode = WtAnalysisConst._ESCAPE_PROJECT_CODE;
                dto.ProjectName = Analysis.projectName;
                dto = KinmuhyoUtil.RemoveChargeNo(dto);
                Analysis.projectCode = dto.ProjectCode;
                Analysis.projectName = dto.ProjectName;

                // 所属の取得
                var tgtTTInfo = ttInfos.Where(n => n.UserId == Analysis.userId).FirstOrDefault();
                if (tgtTTInfo != null) {
                    Analysis.organization = tgtTTInfo.OrganizationName;
                }
                // 計画工数
                int MonthWorkTime = TimeConst._DEFAULT_WORK_MONTH_TIME;
                var tgtWorkTimeMonth = WorkMonthTimeList.Where(n => n.Key == DateTime.Parse(Analysis.date)).FirstOrDefault();
                if (!tgtWorkTimeMonth.Equals(null)) {
                    MonthWorkTime = tgtWorkTimeMonth.Value;
                }
                decimal decPlanned = decimal.Parse(Analysis.plannedTime);
                Analysis.CalcDatas.decPlannedManHour = ConvertWtToManHour(decPlanned, MonthWorkTime);
                Analysis.CalcDatas.decPlannedCost = decimal.Parse(Analysis.plannedCost);

                // 実績工数
                decimal decActual = decimal.Parse(Analysis.actualTime);
                Analysis.CalcDatas.decActualManHour = ConvertWtToManHour(decActual, MonthWorkTime);
                Analysis.CalcDatas.decActualCost = decimal.Parse(Analysis.actualCost);
            }
            return ret;
        }
        /// <summary>
        /// 集計単位の工数合計割り当て（分析データの編集）
        /// </summary>
        /// <param name="ret"></param>
        /// <returns></returns>
        private static List<WorkTimeAnalysisEntity> SetTotalManHourToEntities(List<WorkTimeAnalysisEntity> ret) {
            foreach (var Analysis in ret) {
                var tgtList = ret.Where(n => n.organization == Analysis.organization && n.userId == Analysis.userId && n.date == Analysis.date).ToList();
                Analysis.CalcDatas.totalPlannedTime = tgtList.Sum(n => decimal.Parse(n.plannedTime));
                Analysis.CalcDatas.totalActualTime = tgtList.Sum(n => decimal.Parse(n.actualTime));
                Analysis.CalcDatas.totalPlannedManHour = tgtList.Sum(n => n.CalcDatas.decPlannedManHour);
                Analysis.CalcDatas.totalActualManHour = tgtList.Sum(n => n.CalcDatas.decActualManHour);
                Analysis.CalcDatas.totalPlannedCost = tgtList.Sum(n => n.CalcDatas.decPlannedCost);
                Analysis.CalcDatas.totalActualCost = tgtList.Sum(n => n.CalcDatas.decActualCost);

                // 計画
                decimal decPlanned = decimal.Parse(Analysis.plannedTime);
                if (Analysis.CalcDatas.totalPlannedManHour != 0) {
                    // 割合
                    Analysis.CalcDatas.decPlannedPct = CalcPct(Analysis.CalcDatas.decPlannedManHour, Analysis.CalcDatas.totalPlannedManHour);
                    // 人月
                    Analysis.CalcDatas.decPlannedHumanMoon = CalcHumanMoon(Analysis.CalcDatas.decPlannedManHour, Analysis.CalcDatas.totalPlannedManHour);
                }
                // 実績
                if (Analysis.CalcDatas.totalActualManHour != 0) {
                    // 割合
                    Analysis.CalcDatas.decActualPct = CalcPct(Analysis.CalcDatas.decActualManHour, Analysis.CalcDatas.totalActualManHour);
                    // 人月
                    Analysis.CalcDatas.decActualHumanMoon = CalcHumanMoon(Analysis.CalcDatas.decActualManHour, Analysis.CalcDatas.totalActualManHour);
                }
            }
            return ret;
        }
        /// <summary>
        /// 割合・人月割り当て（分析データの編集）
        /// </summary>
        /// <param name="ret"></param>
        /// <returns></returns>
        private static List<WorkTimeAnalysisEntity> SetAverageFromTotal(List<WorkTimeAnalysisEntity> ret) {
            foreach (var Analysis in ret) {
                var tgtList = ret.Where(n => n.organization == Analysis.organization && n.userId == Analysis.userId && n.date == Analysis.date).ToList();
                // 割合（合計）
                Analysis.CalcDatas.totalPlannedPct = tgtList.Sum(n => n.CalcDatas.decPlannedPct);
                Analysis.CalcDatas.totalActualPct = tgtList.Sum(n => n.CalcDatas.decActualPct);
                // 人月（合計）
                Analysis.CalcDatas.totalPlannedHumanMoon = tgtList.Sum(n => n.CalcDatas.decPlannedHumanMoon);
                Analysis.CalcDatas.totalActualHumanMoon = tgtList.Sum(n => n.CalcDatas.decActualHumanMoon);
            }
            return ret;
        }
        #endregion

        #region 集計
        /// <summary>
        /// 作業時間→工数変換
        /// </summary>
        /// <param name="decWorkTime"></param>
        /// <param name="intMonthWorkTime"></param>
        /// <returns></returns>
        public static decimal ConvertWtToManHour(decimal decWorkTime, int intMonthWorkTime) {
            return Math.Round(decWorkTime / TimeConst._MINUTE_AN_HOUR / intMonthWorkTime, 2, MidpointRounding.AwayFromZero);
        }
        /// <summary>
        /// 割合計算
        /// </summary>
        /// <param name="decManHour"></param>
        /// <param name="totalManHour"></param>
        /// <returns></returns>
        public static decimal CalcPct(decimal decManHour, decimal totalManHour) {
            return Math.Round(decManHour * 100 / totalManHour, 1, MidpointRounding.AwayFromZero) / 100;
        }
        /// <summary>
        /// 人月計算
        /// </summary>
        /// <param name="decManHour"></param>
        /// <param name="totalManHour"></param>
        /// <returns></returns>
        public static decimal CalcHumanMoon(decimal decManHour, decimal totalManHour) {
            return Math.Round(decManHour / totalManHour, 2, MidpointRounding.AwayFromZero);
        }
        /// <summary>
        /// プロジェクト単位の集計
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <returns></returns>
        public static List<WorkTimeColsDto> SumWorkTime_PerProject(List<WorkTimeAnalysisEntity> AnalysisRecords) {
            var ret = new List<WorkTimeColsDto>();
            foreach (var Analysis in AnalysisRecords) {
                var tgtList = ret.Where(n => n.projectCode == Analysis.projectCode && n.projectName == Analysis.projectName && n.date == Analysis.date).ToList();
                if (tgtList.Count != 0) {
                    continue;
                }
                // 集計対象をリスト化
                var sumTgtAnalysis = AnalysisRecords.Where(n => n.projectCode == Analysis.projectCode
                                                             && n.projectName == Analysis.projectName
                                                             && n.date == Analysis.date).ToList();
                
                // プロジェクト単位の集計Dto作成
                var dto = CreateDto_SumWorkTime_PerProject(Analysis, sumTgtAnalysis);
                ret.Add(dto);
            }
            return ret;
        }
        /// <summary>
        /// プロジェクト単位の集計Dto作成
        /// </summary>
        /// <param name="Analysis"></param>
        /// <param name="sumTgtAnalysis"></param>
        /// <returns></returns>
        private static WorkTimeColsDto CreateDto_SumWorkTime_PerProject(WorkTimeAnalysisEntity Analysis, List<WorkTimeAnalysisEntity> sumTgtAnalysis) {
            var ret = new WorkTimeColsDto();
            // チャージ№
            ret.projectCode = Analysis.projectCode;
            // 作業名
            ret.projectName = Analysis.projectName;
            // 集計月
            ret.date = Analysis.date;
            // 作業時間
            ret.decPlannedWorkTime = sumTgtAnalysis.Sum(n => decimal.Parse(n.plannedTime));
            ret.decActualWorkTime = sumTgtAnalysis.Sum(n => decimal.Parse(n.actualTime));
            // 工数
            ret.decPlannedManHour = sumTgtAnalysis.Sum(n => n.CalcDatas.decPlannedManHour);
            ret.decActualManHour = sumTgtAnalysis.Sum(n => n.CalcDatas.decActualManHour);
            // 割合
            ret.decPlannedPct = sumTgtAnalysis.Sum(n => n.CalcDatas.decPlannedPct);
            ret.decActualPct = sumTgtAnalysis.Sum(n => n.CalcDatas.decActualPct);
            // 人月
            ret.decPlannedHumanMoon = sumTgtAnalysis.Sum(n => n.CalcDatas.decPlannedHumanMoon);
            ret.decActualHumanMoon = sumTgtAnalysis.Sum(n => n.CalcDatas.decActualHumanMoon);
            // 人月
            ret.decPlannedCost = sumTgtAnalysis.Sum(n => n.CalcDatas.decPlannedCost);
            ret.decActualCost = sumTgtAnalysis.Sum(n => n.CalcDatas.decActualCost);

            return ret;
        }
        #endregion

        #region 月間労働時間
        /// <summary>
        /// 月間稼働時間リスト取得
        /// </summary>
        /// <param name="defaultKinmuhyoPath"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="tgtDay"></param>
        /// <param name="using160h"></param>
        /// <returns></returns>
        public static Dictionary<DateTime, int> GetWorkMonthTimeList(string defaultKinmuhyoPath, DateTime startDate, DateTime endDate
            , int tgtDay = 31, bool using160h = true) {
            var startIdxDate = new DateTime(startDate.Year, startDate.Month, 1);
            var endIdxDate = new DateTime(endDate.Year, endDate.Month, 1);
            var ret = new Dictionary<DateTime, int>();
            for (DateTime dt = startIdxDate; dt <= endIdxDate; dt = dt.AddMonths(1)) {

                // 月間労働時間取得（勤務実績表より）
                ret.Add(dt, GetWorkTimeMonth(defaultKinmuhyoPath, dt, tgtDay, using160h));
            }
            return ret;
        }
        /// <summary>
        /// 月間労働時間取得（勤務実績表より）
        /// </summary>
        /// <param name="defaultKinmuhyoPath"></param>
        /// <param name="KijunDate"></param>
        /// <param name="tgtDay"></param>
        /// <param name="using160h"></param>
        /// <returns></returns>
        public static int GetWorkTimeMonth(string defaultKinmuhyoPath, DateTime KijunDate, int tgtDay = 31, bool using160h = true) {
            if (using160h) {
                // デフォルト値をreturn
                return TimeConst._DEFAULT_WORK_MONTH_TIME;
            }
            // 対象月のデフォルト勤務表取得
            var dummyConfig = new TimeEntryConfigEntity() {
                DefaultKinmuhyoName = TimeEntryConfigConst._DEFAULT_KINMUHYO_FORMAT_NAME,
            };
            dummyConfig = KinmuhyoUtil.GetKinmuhyoFormatName(dummyConfig, KijunDate);
            defaultKinmuhyoPath = Path.Combine(defaultKinmuhyoPath, dummyConfig.DefaultKinmuhyoName);
            if (!File.Exists(defaultKinmuhyoPath)) {

                // ヒットしなければデフォルト値をreturn
                return TimeConst._DEFAULT_WORK_MONTH_TIME;
            }
            int ret = 0;
            var xlsxFile = File.OpenRead(defaultKinmuhyoPath);
            using (var package = new ExcelPackage(new FileInfo(defaultKinmuhyoPath))) {

                // 作業内容シート作成
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                var sheet = package.Workbook.Worksheets[KinmuhyoConst._SHEET_NAME_KINMUHYO];

                // 労働日をリストアップ
                for (int i = 1; i <= tgtDay; i++) {
                    string cellWeekDay = KinmuhyoConst._COL_WEEK_DAY + (i + KinmuhyoConst._ROW_IDX_SHEET_HEADER);
                    string cellHoliDay = KinmuhyoConst._COL_HOLI_DAY + (i + KinmuhyoConst._ROW_IDX_SHEET_HEADER);
                    string valWeekDay = string.Empty;
                    if (sheet.Cells[cellWeekDay].Value != null) {
                        valWeekDay = sheet.Cells[cellWeekDay].Value.ToString();
                    }
                    string valHoliDay = string.Empty;
                    if (sheet.Cells[cellHoliDay].Value != null) {
                        valHoliDay = sheet.Cells[cellHoliDay].Value.ToString();
                    }
                    DateTime dt;
                    string strDt = KijunDate.Year + CommonLiteral._LITERAL_HYPHEN + KijunDate.Month + CommonLiteral._LITERAL_HYPHEN + i;
                    if (!DateTime.TryParse(strDt, out dt)) {
                        // 不正な日付はスルー
                        continue;
                    }
                    if (KinmuhyoConst._WEEK_END_DAYS.Contains(valWeekDay)) {
                        // 土日はスルー
                        continue;
                    }
                    if (valHoliDay == KinmuhyoConst._HOLIDAY_MARK) {
                        // 祝日はスルー
                        continue;
                    }
                    ret++;
                }
            }
            return ret * TimeConst._DEFAULT_WORK_TIME_PER_DAY;
        }
        /// <summary>
        /// 集計月の取得
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <returns></returns>
        public static Dictionary<int, string> GetSumMonth(List<WorkTimeAnalysisEntity> AnalysisRecords) {
            var sumMonthList = new List<string>();
            foreach (var entity in AnalysisRecords) {
                if (!sumMonthList.Contains(entity.date)) {
                    sumMonthList.Add(entity.date);
                }
            }
            sumMonthList.Sort();
            var ret = new Dictionary<int, string>();
            int dicIdx = 0;
            foreach (var date in sumMonthList) {
                if (!ret.Values.Contains(date)) {
                    ret.Add(dicIdx, date);
                    dicIdx++;
                }
            }
            return ret;
        }
        /// <summary>
        /// ヘッダーに表示する月を取得
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="tgtDate"></param>
        /// <returns></returns>
        public static string GetHeaderMonth(string ConnStrTimeEntryDB, DateTime tgtDate) {
            string ret = tgtDate.ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI);
            ret += string.Format(WtAnalysisConst._HEADER_FORMAT_MONTH_WORK_TIME, GetWorkTimeMonth(ConnStrTimeEntryDB, tgtDate));
            return ret;
        }
        #endregion

        #region Excel出力用オブジェクト作成（ユーザ）
        /// <summary>
        /// Excel出力用オブジェクト作成（ユーザ）
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <returns></returns>
        public static List<WorkTimeAnalysisDto> GetExcelDtoListKeyUser(List<WorkTimeAnalysisEntity> AnalysisRecords) {

            // キー項目セット
            var ret = new List<WorkTimeAnalysisDto>();
            ret = SetKeysUser(AnalysisRecords, ret);

            // 工数セット
            ret = SetManHourUser(AnalysisRecords, ret);

            return ret;
        }
        /// <summary>
        /// キー項目セット（Excel出力用オブジェクト作成（ユーザ））
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <param name="ret"></param>
        private static List<WorkTimeAnalysisDto> SetKeysUser(List<WorkTimeAnalysisEntity> AnalysisRecords, List<WorkTimeAnalysisDto> ret) {
            // キー項目セット
            foreach (var Analysis in AnalysisRecords) {
                // 所属ヘッダ
                if (!ret.Exists(n => n.organization == Analysis.organization && n.rowType == WtAnalysisConst.ExcelRowType.ORGANIZATION_HEADER)) {

                    var OrganizationHeader = new WorkTimeAnalysisDto() {
                        organization = Analysis.organization,
                        userId = string.Empty,
                        userName = string.Empty,
                        projectCode = string.Empty,
                        rowType = WtAnalysisConst.ExcelRowType.ORGANIZATION_HEADER,
                        workTimeDatas = new List<WorkTimeColsDto>(),
                    };
                    ret.Add(OrganizationHeader);
                }
                // ユーザヘッダ
                if (!ret.Exists(n => n.organization == Analysis.organization
                                  && n.userId == Analysis.userId
                                  && n.rowType == WtAnalysisConst.ExcelRowType.MEMBER_HEADER)) {

                    var UserHeader = new WorkTimeAnalysisDto() {
                        organization = Analysis.organization,
                        userId = Analysis.userId,
                        userName = Analysis.userName,
                        projectCode = string.Empty,
                        rowType = WtAnalysisConst.ExcelRowType.MEMBER_HEADER,
                        workTimeDatas = new List<WorkTimeColsDto>(),
                    };
                    ret.Add(UserHeader);
                }
                // 明細
                if (!ret.Exists(n => n.organization == Analysis.organization
                                  && n.projectCode == Analysis.projectCode
                                  && n.projectName == Analysis.projectName
                                  && n.userId == Analysis.userId)) {

                    var bodyRow = new WorkTimeAnalysisDto() {
                        organization = Analysis.organization,
                        userId = Analysis.userId,
                        userName = Analysis.userName,
                        projectCode = Analysis.projectCode,
                        projectName = Analysis.projectName,
                        rowType = WtAnalysisConst.ExcelRowType.BODY_ROW,
                        workTimeDatas = new List<WorkTimeColsDto>(),
                    };
                    ret.Add(bodyRow);
                }
                // 集計
                if (!ret.Exists(n => n.organization == Analysis.organization
                                  && n.userId == Analysis.userId
                                  && n.rowType == WtAnalysisConst.ExcelRowType.SUMMARY)) {

                    var summmary = new WorkTimeAnalysisDto() {
                        organization = Analysis.organization,
                        userId = Analysis.userId,
                        userName = Analysis.userName,
                        projectCode = Analysis.projectCode,
                        projectName = Analysis.projectName,
                        totalPlannedTime = Analysis.CalcDatas.totalPlannedTime,
                        totalActualTime = Analysis.CalcDatas.totalActualTime,
                        totalPlannedManHour = Analysis.CalcDatas.totalPlannedManHour,
                        totalActualManHour = Analysis.CalcDatas.totalActualManHour,
                        totalPlannedPct = Analysis.CalcDatas.totalPlannedPct,
                        totalActualPct = Analysis.CalcDatas.totalActualPct,
                        totalPlannedHumanMoon = Analysis.CalcDatas.totalPlannedManHour,
                        totalActualHumanMoon = Analysis.CalcDatas.totalActualManHour,
                        totalPlannedCost = Analysis.CalcDatas.totalPlannedCost,
                        totalActualCost = Analysis.CalcDatas.totalActualCost,
                        rowType = WtAnalysisConst.ExcelRowType.SUMMARY,
                        workTimeDatas = new List<WorkTimeColsDto>(),
                    };
                    ret.Add(summmary);
                }
            }
            return ret;
        }
        /// <summary>
        /// 工数セット（Excel出力用オブジェクト作成（ユーザ））
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <param name="ret"></param>
        /// <returns></returns>
        private static List<WorkTimeAnalysisDto> SetManHourUser(List<WorkTimeAnalysisEntity> AnalysisRecords, List<WorkTimeAnalysisDto> ret) {
            foreach (var Analysis in AnalysisRecords) {
                var tgtBodyRow = ret.Where(n => n.organization == Analysis.organization
                                             && n.projectCode == Analysis.projectCode
                                             && n.projectName == Analysis.projectName
                                             && n.userId == Analysis.userId
                                             && n.rowType == WtAnalysisConst.ExcelRowType.BODY_ROW).FirstOrDefault();
                // 明細行
                if (tgtBodyRow != null) {
                    var bodyRowEntity = new WorkTimeColsDto() {
                        date = Analysis.date,
                        projectCode = Analysis.projectCode,
                        projectName = Analysis.projectName,
                        decPlannedWorkTime = decimal.Parse(Analysis.plannedTime),
                        decActualWorkTime = decimal.Parse(Analysis.actualTime),
                        decPlannedManHour = Analysis.CalcDatas.decPlannedManHour,
                        decActualManHour = Analysis.CalcDatas.decActualManHour,
                        decPlannedPct = Analysis.CalcDatas.decPlannedPct,
                        decActualPct = Analysis.CalcDatas.decActualPct,
                        decPlannedHumanMoon = Analysis.CalcDatas.decPlannedHumanMoon,
                        decActualHumanMoon = Analysis.CalcDatas.decActualHumanMoon,
                        decPlannedCost = Analysis.CalcDatas.decPlannedCost,
                        decActualCost = Analysis.CalcDatas.decActualCost,
                    };
                    tgtBodyRow.workTimeDatas.Add(bodyRowEntity);
                }
                // 集計行
                var tgtSummary = ret.Where(n => n.organization == Analysis.organization
                                             && n.userId == Analysis.userId
                                             && n.rowType == WtAnalysisConst.ExcelRowType.SUMMARY).FirstOrDefault();
                if (tgtSummary != null) {
                    var summaryEntity = new WorkTimeColsDto() {
                        date = Analysis.date,
                        projectCode = Analysis.projectCode,
                        projectName = Analysis.projectName,
                        decPlannedWorkTime = Analysis.CalcDatas.totalPlannedTime,
                        decActualWorkTime = Analysis.CalcDatas.totalActualTime,
                        decPlannedManHour = Analysis.CalcDatas.totalPlannedManHour,
                        decActualManHour = Analysis.CalcDatas.totalActualManHour,
                        decPlannedPct = Analysis.CalcDatas.totalPlannedPct,
                        decActualPct = Analysis.CalcDatas.totalActualPct,
                        decPlannedHumanMoon = Analysis.CalcDatas.totalPlannedHumanMoon,
                        decActualHumanMoon = Analysis.CalcDatas.totalActualHumanMoon,
                        decPlannedCost = Analysis.CalcDatas.totalPlannedCost,
                        decActualCost = Analysis.CalcDatas.totalActualCost,
                    };
                    tgtSummary.workTimeDatas.Add(summaryEntity);
                }
            }
            return ret;
        }
        #endregion

        #region Excel出力用オブジェクト作成（作業）
        /// <summary>
        /// Excel出力用オブジェクト作成（作業）
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <returns></returns>
        public static List<WorkTimeAnalysisDto> GetExcelDtoListKeyProject(List<WorkTimeAnalysisEntity> AnalysisRecords) {
            // プロジェクト単位の集計
            var sumDatasPerProject = SumWorkTime_PerProject(AnalysisRecords);

            // キー項目セット
            var ret = new List<WorkTimeAnalysisDto>();
            ret = SetKeysProject(AnalysisRecords, sumDatasPerProject, ret);

            // 工数セット
            ret = SetManHourProject(AnalysisRecords, sumDatasPerProject, ret);

            return ret;
        }
        /// <summary>
        /// キー項目セット（Excel出力用オブジェクト作成（作業））
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <param name="sumDatasPerProject"></param>
        /// <param name="ret"></param>
        private static List<WorkTimeAnalysisDto> SetKeysProject(List<WorkTimeAnalysisEntity> AnalysisRecords, List<WorkTimeColsDto> sumDatasPerProject, List<WorkTimeAnalysisDto> ret) {
            foreach (var Analysis in AnalysisRecords) {
                // 作業ヘッダ
                if (!ret.Exists(n => n.projectCode == Analysis.projectCode
                                  && n.projectName == Analysis.projectName
                                  && n.rowType == WtAnalysisConst.ExcelRowType.PROJECT_HEADER)) {

                    var ProjectHeader = new WorkTimeAnalysisDto() {
                        projectCode = Analysis.projectCode,
                        projectName = Analysis.projectName,
                        rowType = WtAnalysisConst.ExcelRowType.PROJECT_HEADER,
                        workTimeDatas = new List<WorkTimeColsDto>(),
                    };
                    ret.Add(ProjectHeader);
                }
                // 明細
                if (!ret.Exists(n => n.organization == Analysis.organization
                                  && n.projectCode == Analysis.projectCode
                                  && n.projectName == Analysis.projectName
                                  && n.userId == Analysis.userId)) {

                    var bodyRow = new WorkTimeAnalysisDto() {
                        organization = Analysis.organization,
                        userId = Analysis.userId,
                        userName = Analysis.userName,
                        projectCode = Analysis.projectCode,
                        projectName = Analysis.projectName,
                        rowType = WtAnalysisConst.ExcelRowType.BODY_ROW,
                        workTimeDatas = new List<WorkTimeColsDto>(),
                    };
                    ret.Add(bodyRow);
                }
                // 集計
                if (!ret.Exists(n => n.projectCode == Analysis.projectCode
                                  && n.projectName == Analysis.projectName
                                  && n.rowType == WtAnalysisConst.ExcelRowType.SUMMARY)) {

                    var tgtSumDatas = sumDatasPerProject.Where(n => n.projectCode == Analysis.projectCode
                                                                 && n.projectName == Analysis.projectName
                                                                 && n.date == Analysis.date).FirstOrDefault();
                    if (tgtSumDatas != null) {
                        var summmary = new WorkTimeAnalysisDto() {
                            organization = Analysis.organization,
                            userId = Analysis.userId,
                            userName = Analysis.userName,
                            projectCode = Analysis.projectCode,
                            projectName = Analysis.projectName,
                            totalPlannedTime = tgtSumDatas.decPlannedWorkTime,
                            totalActualTime = tgtSumDatas.decActualWorkTime,
                            totalPlannedManHour = tgtSumDatas.decPlannedManHour,
                            totalActualManHour = tgtSumDatas.decActualManHour,
                            totalPlannedPct = tgtSumDatas.decPlannedPct,
                            totalActualPct = tgtSumDatas.decActualPct,
                            totalPlannedHumanMoon = tgtSumDatas.decPlannedHumanMoon,
                            totalActualHumanMoon = tgtSumDatas.decActualHumanMoon,
                            totalPlannedCost = tgtSumDatas.decPlannedCost,
                            totalActualCost = tgtSumDatas.decActualCost,
                            rowType = WtAnalysisConst.ExcelRowType.SUMMARY,
                            workTimeDatas = new List<WorkTimeColsDto>(),
                        };
                        ret.Add(summmary);
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 工数セット（Excel出力用オブジェクト作成（作業））
        /// </summary>
        /// <param name="AnalysisRecords"></param>
        /// <param name="sumDatasPerProject"></param>
        /// <param name="ret"></param>
        /// <returns></returns>
        private static List<WorkTimeAnalysisDto> SetManHourProject(List<WorkTimeAnalysisEntity> AnalysisRecords, List<WorkTimeColsDto> sumDatasPerProject, List<WorkTimeAnalysisDto> ret) {
            foreach (var Analysis in AnalysisRecords) {
                var tgtBodyRow = ret.Where(n => n.organization == Analysis.organization
                                             && n.projectCode == Analysis.projectCode
                                             && n.projectName == Analysis.projectName
                                             && n.userId == Analysis.userId
                                             && n.rowType == WtAnalysisConst.ExcelRowType.BODY_ROW).FirstOrDefault();
                // 明細行
                if (tgtBodyRow != null) {
                    var bodyRowEntity = new WorkTimeColsDto() {
                        date = Analysis.date,
                        projectCode = Analysis.projectCode,
                        projectName = Analysis.projectName,
                        decPlannedWorkTime = decimal.Parse(Analysis.plannedTime),
                        decActualWorkTime = decimal.Parse(Analysis.actualTime),
                        decPlannedManHour = Analysis.CalcDatas.decPlannedManHour,
                        decActualManHour = Analysis.CalcDatas.decActualManHour,
                        decPlannedPct = Analysis.CalcDatas.decPlannedPct,
                        decActualPct = Analysis.CalcDatas.decActualPct,
                        decPlannedHumanMoon = Analysis.CalcDatas.decPlannedHumanMoon,
                        decActualHumanMoon = Analysis.CalcDatas.decActualHumanMoon,
                        decPlannedCost = Analysis.CalcDatas.decPlannedCost,
                        decActualCost = Analysis.CalcDatas.decActualCost,
                    };
                    tgtBodyRow.workTimeDatas.Add(bodyRowEntity);
                }
                // 集計行
                var tgtSummary = ret.Where(n => n.projectCode == Analysis.projectCode
                                             && n.projectName == Analysis.projectName
                                             && n.rowType == WtAnalysisConst.ExcelRowType.SUMMARY).FirstOrDefault();
                var tgtSumDatas = sumDatasPerProject.Where(n => n.projectCode == Analysis.projectCode
                                                             && n.projectName == Analysis.projectName
                                                             && n.date == Analysis.date).FirstOrDefault();
                if (tgtSummary != null && tgtSumDatas != null) {
                    var summaryEntity = new WorkTimeColsDto() {
                        date = Analysis.date,
                        projectCode = Analysis.projectCode,
                        projectName = Analysis.projectName,
                        decPlannedWorkTime = tgtSumDatas.decPlannedWorkTime,
                        decActualWorkTime = tgtSumDatas.decActualWorkTime,
                        decPlannedManHour = tgtSumDatas.decPlannedManHour,
                        decActualManHour = tgtSumDatas.decActualManHour,
                        decPlannedPct = tgtSumDatas.decPlannedPct,
                        decActualPct = tgtSumDatas.decActualPct,
                        decPlannedHumanMoon = tgtSumDatas.decPlannedHumanMoon,
                        decActualHumanMoon = tgtSumDatas.decActualHumanMoon,
                        decPlannedCost = tgtSumDatas.decPlannedCost,
                        decActualCost = tgtSumDatas.decActualCost,
                    };
                    tgtSummary.workTimeDatas.Add(summaryEntity);
                }
            }
            return ret;
        }
        #endregion

        #region Excel編集
        /// <summary>
        /// タイトル取得
        /// </summary>
        /// <param name="tgtMonthStart"></param>
        /// <param name="tgtMonthEnd"></param>
        /// <param name="sheetType"></param>
        /// <returns></returns>
        public static string GetTitle(string tgtMonthStart, string tgtMonthEnd, WtAnalysisConst.SheetType sheetType) {
            string strSheetType = string.Empty;
            switch (sheetType) {
                case WtAnalysisConst.SheetType.LOOKS:
                    strSheetType = WtAnalysisConst._SHEET_NAME_LOOKS;
                    break;
                case WtAnalysisConst.SheetType.PROJECT:
                    strSheetType = WtAnalysisConst._SHEET_NAME_PIBOT_PROJECT;
                    break;
                case WtAnalysisConst.SheetType.MEMBER:
                    strSheetType = WtAnalysisConst._SHEET_NAME_PIBOT_USER;
                    break;
                case WtAnalysisConst.SheetType.COST:
                    strSheetType = WtAnalysisConst._SHEET_NAME_SUM_COST;
                    break;
            }
            tgtMonthStart = DateTime.Parse(tgtMonthStart).ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI);
            tgtMonthEnd   = DateTime.Parse(tgtMonthEnd).ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI);
            return string.Format(WtAnalysisConst._PIBOT_TITLE, tgtMonthStart, tgtMonthEnd, strSheetType);
        }
        /// <summary>
        /// 記載欄の最大列番号取得
        /// </summary>
        /// <param name="dicSumMonth"></param>
        /// <param name="sheetType"></param>
        /// <returns></returns>
        public static int GetMaxColNum(Dictionary<int, string> dicSumMonth, WtAnalysisConst.SheetType sheetType) {
            int bodyColStartIdx = WtAnalysisConst._BODY_COL_START_PER_MONTH;
            int bodyColcount = WtAnalysisConst._BODY_COL_COUNT_PER_MONTH;
            if (sheetType == WtAnalysisConst.SheetType.COST) {
                bodyColStartIdx = WtAnalysisConst._BODY_COL_START_COST;
                bodyColcount = WtAnalysisConst._BODY_COL_COUNT_COST;
            }
            int maxColNum = ((dicSumMonth.Max(n => n.Key) + 1) * bodyColcount) + bodyColStartIdx - 1;
            return maxColNum;
        }
        /// <summary>
        /// セルの着色
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="rowIdx"></param>
        /// <param name="colIdx"></param>
        /// <param name="tgtColor"></param>
        public static void SetCellColor(ExcelWorksheet sheet, int rowIdx, int colIdx, Color tgtColor) {
            sheet.Cells[rowIdx, colIdx].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[rowIdx, colIdx].Style.Fill.BackgroundColor.SetColor(tgtColor);
        }
        /// <summary>
        /// セルの着色
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="cellRange"></param>
        /// <param name="tgtColor"></param>
        public static void SetCellColor(ExcelWorksheet sheet, string cellRange, Color tgtColor) {
            sheet.Cells[cellRange].Style.Fill.PatternType = ExcelFillStyle.Solid;
            sheet.Cells[cellRange].Style.Fill.BackgroundColor.SetColor(tgtColor);
        }
        #endregion
    }
}
