﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using TimeEntryShared.Const;
using TimeEntryShared.Dto;
using TimeEntryShared.Entity;

namespace TimeEntryShared.Util {
    public static class ModelUtil {
        private const int _COL_COUNT_CSV_EMPLOEE = 22;
        private const int _COL_COUNT_CSV_BU = 3;
        private const int _COL_COUNT_CSV_GROUP = 4;
        private const int _COL_COUNT_CSV_MHSC = 3;

        #region Config
        /// <summary>
        /// Configロード
        /// </summary>
        /// <returns></returns>
        public static TimeEntryConfigEntity LoadConfig() {
            string configPath = Path.Combine(Environment.CurrentDirectory, TimeEntryConfigConst._CONFIG_FILE_NAME);
            var exeFileMap    = new ExeConfigurationFileMap { ExeConfigFilename = configPath };
            var config        = ConfigurationManager.OpenMappedExeConfiguration(exeFileMap, ConfigurationUserLevel.None);
            var ret = new TimeEntryConfigEntity();
            string ComponentsPath             = config.AppSettings.Settings["ComponentsPath"].Value.ToString();
            ret.EmploeePath                   = ComponentsPath;
            ret.URL_WHITE_BORD                = config.AppSettings.Settings["UrlWhiteBord"].Value.ToString();
            ret.BackDaysCount                 = config.AppSettings.Settings["SumRange_BackDaysCount"].Value.ToString();
            ret.KinmuhyoPath                  = config.AppSettings.Settings["XlsxKinmuhyoPath"].Value.ToString();
            ret.KinmuhyoName                  = config.AppSettings.Settings["XlsxKinmuhyoName"].Value.ToString();
            ret.ForcingReCreateFlg            = int.Parse(config.AppSettings.Settings["ForcingReCreateFlg"].Value.ToString());
            ret.DefaultKinmuhyoPath           = config.AppSettings.Settings["XlsxDefaultKinmuhyoPath"].Value.ToString();
            ret.WtAnalysisWorkFolder          = config.AppSettings.Settings["_WORK_FOLDER_PATH"].Value.ToString();
            ret.DefaultKinmuhyoName           = TimeEntryConfigConst._DEFAULT_KINMUHYO_FORMAT_NAME;
            ret.ConnStrWBDB                   = config.ConnectionStrings.ConnectionStrings["WBDB"].ConnectionString;
            ret.ConnStrTimeEntryDB            = config.ConnectionStrings.ConnectionStrings["TimeEntryDB"].ConnectionString;
            ret.UsingCsvMstFlg                = int.Parse(config.AppSettings.Settings["UsingCsvMstFlg"].Value.ToString());

            // 締日と設定値から集計期間を取得
            int intShiteiDay = int.Parse(ret.BackDaysCount);
            var Today = DateTime.Now;
            DateTime dt = Today.AddDays(intShiteiDay * -1);
            DateTime dtStart = new DateTime(dt.Year, dt.Month, 1);
            DateTime dtEnd = dtStart.AddMonths(1).AddDays(-1);
            ret.SumStartDate = dtStart.ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH);
            ret.SumEndDate = dtEnd.ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH);

            // 何か月前のホワイトボードを開くか
            ret.WbLookPreMonthCount = GetWBBackMonthCount(dtStart);

            // デフォルト勤務表のファイル名
            ret= KinmuhyoUtil.GetKinmuhyoFormatName(ret, dtStart);
            string defaultKinmuhyoPath = Path.Combine(ret.DefaultKinmuhyoPath, ret.DefaultKinmuhyoName);
            if (!File.Exists(defaultKinmuhyoPath)) {
                throw new Exception(SharedMessages._MSG_NOT_EXIST_DEF_KIN);
            }
            return ret;
        }
        /// <summary>
        /// 何か月前のホワイトボードを開くか
        /// </summary>
        /// <param name="dtSumStart"></param>
        /// <returns></returns>
        public static int GetWBBackMonthCount(DateTime dtSumStart) {
            var Today = DateTime.Now;
            var dtFirstDate = new DateTime(Today.Year, Today.Month, 1);
            int WbLookPreMonthCount = 0;
            if (dtSumStart < dtFirstDate) {
                while (dtSumStart < dtFirstDate) {
                    WbLookPreMonthCount++;
                    dtFirstDate = dtFirstDate.AddMonths(-1);
                }
            }
            return WbLookPreMonthCount;
        }
        #endregion

        #region DB
        /// <summary>
        /// 社員マスタロード
        /// </summary>
        /// <param name="ConfigInfo"></param>
        /// <returns></returns>
        public static List<MstEmploeeEntity> LoadEmploeeMst(TimeEntryConfigEntity ConfigInfo) {
            if (ConfigInfo.UsingCsvMstFlg == FlgValConst._INT_FLG_ON) {
                return LoadEmploeeMstFromCsv(ConfigInfo.EmploeePath);
            }
            return LoadEmploeeMstFromDB(ConfigInfo.ConnStrTimeEntryDB);
        }
        /// <summary>
        /// 社員マスタロード（CSV）
        /// </summary>
        /// <param name="EmploeePath"></param>
        /// <returns></returns>
        public static List<MstEmploeeEntity> LoadEmploeeMstFromCsv(string EmploeePath) {
            var ret = new List<MstEmploeeEntity>();
            string csvPath = Path.Combine(EmploeePath, TimeEntryConfigConst._CSV_NAME_MST_EMPLOEE);
            using (var sr = new StreamReader(csvPath, Encoding.GetEncoding(EncodingConst._ENCODING_SJIS))) {
                bool isFirdtLine = true;
                while (!sr.EndOfStream) {
                    var line = sr.ReadLine();
                    if (isFirdtLine) {
                        // １行目はヘッダーなのでスルー
                        isFirdtLine = false;
                        continue;
                    }
                    var values = line.Split(CommonLiteral._CSV_SEPALATOR);
                    if (values.Length != _COL_COUNT_CSV_EMPLOEE) {
                        // 既定の行数でなければスルー
                        continue;
                    }
                    var emp = new MstEmploeeEntity() {
                        BuCode               = values[0],
                        GroupCode            = values[1],
                        Name                 = values[2],
                        EmploeeNum           = values[3],
                        LoginPass            = values[4],
                        DefaultWorkChargeNo1 = values[7],
                        DefaultWorkName1     = values[8],
                        DefaultWorkChargeNo2 = values[11],
                        DefaultWorkName2     = values[12],
                        DefaultWorkChargeNo3 = values[15],
                        DefaultWorkName3     = values[16],
                    };
                    emp.NightShiftFlg     = GetIntValue(values[5]).ToString();  // 夜勤フラグ
                    emp.DefaultWorkFlg    = GetIntValue(values[6]).ToString();  // デフォ作業フラグ
                    emp.DefaultWorkTimeH1 = GetIntValue(values[9]).ToString();  // 作業時間1（時間）
                    emp.DefaultWorkTimeM1 = GetIntValue(values[10]).ToString(); // 作業時間1（分）
                    emp.DefaultWorkTimeH2 = GetIntValue(values[13]).ToString(); // 作業時間2（時間）
                    emp.DefaultWorkTimeM2 = GetIntValue(values[14]).ToString(); // 作業時間2（分）
                    emp.DefaultWorkTimeH3 = GetIntValue(values[17]).ToString(); // 作業時間3（時間）
                    emp.DefaultWorkTimeM3 = GetIntValue(values[18]).ToString(); // 作業時間3（分）
                    emp.MhscFlg           = GetIntValue(values[19]).ToString(); // MHSCフラグ
                    emp.ForcingShiftBFlg  = GetIntValue(values[20]).ToString(); // 強制Bシフトフラグ
                    emp.DeleteFlg         = GetIntValue(values[21]).ToString(); // 削除フラグ
                    ret.Add(emp);
                }
            }
            return ret;
        }
        /// <summary>
        /// 社員マスタロード（DB）
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <returns></returns>
        public static List<MstEmploeeEntity> LoadEmploeeMstFromDB(string ConnStrTimeEntryDB) {
            var ret = new List<MstEmploeeEntity>();

            // SQL文の作成
            string sql = $@"
            SELECT DISTINCT
                ME.EmploeeNum
            ,   ME.BuCode
            ,   ME.GroupCode
            ,   ME.Name
            ,   ME.LoginPass
            ,   ME.NightShiftFlg
            ,   ME.DefaultWorkFlg
            ,   ME.MhscFlg
            ,   ME.ForcingShiftBFlg
            ,   ME.DeleteFlg
            ,   MDW.DefaultWorkChargeNo1
            ,   MDW.DefaultWorkName1
            ,   MDW.DefaultWorkTimeH1
            ,   MDW.DefaultWorkTimeM1
            ,   MDW.DefaultWorkChargeNo2
            ,   MDW.DefaultWorkName2
            ,   MDW.DefaultWorkTimeH2
            ,   MDW.DefaultWorkTimeM2
            ,   MDW.DefaultWorkChargeNo3
            ,   MDW.DefaultWorkName3
            ,   MDW.DefaultWorkTimeH3
            ,   MDW.DefaultWorkTimeM3
            FROM
                TM_EMPLOEE AS ME
            LEFT OUTER JOIN
                TM_DEFAULT_WORK AS MDW
            ON
                ME.EmploeeNum = MDW.EmploeeNum
            LEFT OUTER JOIN
                TM_BU AS MB
            ON
                ME.BuCode = MB.BuCode
            LEFT OUTER JOIN
                TM_GROUP AS MG
            ON
                ME.GroupCode = MG.GroupCode
            WHERE
                MB.DelFlg = 0
            AND (ME.GroupCode = '' OR (ME.GroupCode <> '' AND MG.DelFlg = 0))
            ORDER BY EmploeeNum
            ";
            using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                using (var cmd = cn.CreateCommand()) {
                    try {
                        cn.Open();
                        cmd.CommandText = sql;
                        using (var dr = cmd.ExecuteReader()) {
                            while (dr.Read()) {
                                var entity = new MstEmploeeEntity();
                                entity.BuCode                = CommonUtil.ToStringForbidNull(dr[nameof(entity.BuCode)]);
                                entity.GroupCode             = CommonUtil.ToStringForbidNull(dr[nameof(entity.GroupCode)]);
                                entity.Name                  = CommonUtil.ToStringForbidNull(dr[nameof(entity.Name)]);
                                entity.EmploeeNum            = CommonUtil.ToStringForbidNull(dr[nameof(entity.EmploeeNum)]);
                                entity.LoginPass             = CommonUtil.ToStringForbidNull(dr[nameof(entity.LoginPass)]);
                                entity.NightShiftFlg         = CommonUtil.ToStringForbidNull(dr[nameof(entity.NightShiftFlg)]);
                                entity.DefaultWorkFlg        = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkFlg)]);
                                entity.DefaultWorkChargeNo1  = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkChargeNo1)]);
                                entity.DefaultWorkName1      = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkName1)]);
                                entity.DefaultWorkTimeH1     = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkTimeH1)]);
                                entity.DefaultWorkTimeM1     = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkTimeM1)]);
                                entity.DefaultWorkChargeNo2  = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkChargeNo2)]);
                                entity.DefaultWorkName2      = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkName2)]);
                                entity.DefaultWorkTimeH2     = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkTimeH2)]);
                                entity.DefaultWorkTimeM2     = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkTimeM2)]);
                                entity.DefaultWorkChargeNo3  = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkChargeNo3)]);
                                entity.DefaultWorkName3      = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkName3)]);
                                entity.DefaultWorkTimeH3     = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkTimeH3)]);
                                entity.DefaultWorkTimeM3     = CommonUtil.ToStringForbidNull(dr[nameof(entity.DefaultWorkTimeM3)]);
                                entity.MhscFlg               = CommonUtil.ToStringForbidNull(dr[nameof(entity.MhscFlg)]);
                                entity.ForcingShiftBFlg      = CommonUtil.ToStringForbidNull(dr[nameof(entity.ForcingShiftBFlg)]);
                                entity.DeleteFlg             = CommonUtil.ToStringForbidNull(dr[nameof(entity.DeleteFlg)]);

                                ret.Add(entity);
                            }
                        }
                    } catch (Exception e) {
                        throw e;
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// 社員マスタ登録
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="InsertEntity"></param>
        /// <returns></returns>
        public static void InsertEmploeeMst(string ConnStrTimeEntryDB, MstEmploeeEntity InsertEntity) {

            // SQL文の作成
            string sql = $@"
            INSERT INTO
                TM_EMPLOEE(
                EmploeeNum
            ,   Name
            ,   BuCode
            ,   GroupCode
            ,   LoginPass
            ,   NightShiftFlg
            ,   DefaultWorkFlg
            ,   MhscFlg
            ,   ForcingShiftBFlg
            ,   DeleteFlg)VALUES(
                '{InsertEntity.EmploeeNum}'
            ,   '{InsertEntity.Name}'
            ,   '{InsertEntity.BuCode}'
            ,   '{InsertEntity.GroupCode}'
            ,   '{InsertEntity.LoginPass}'
            ,   '{InsertEntity.NightShiftFlg}'
            ,   '{InsertEntity.DefaultWorkFlg}'
            ,   '{InsertEntity.MhscFlg}'
            ,   '{InsertEntity.ForcingShiftBFlg}'
            ,   '{InsertEntity.DeleteFlg}'
            )";
            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {
                        cn.Open();
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// 社員マスタ更新
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="UpdEntity"></param>
        /// <returns></returns>
        public static void UpdEmploeeMst(string ConnStrTimeEntryDB, MstEmploeeEntity UpdEntity) {

            // SQL文の作成
            string sql = $@"
            UPDATE
                TM_EMPLOEE
            SET
                Name                 = '{UpdEntity.Name}'
            ,   BuCode               = '{UpdEntity.BuCode}'
            ,   GroupCode            = '{UpdEntity.GroupCode}'
            ,   LoginPass            = '{UpdEntity.LoginPass}'
            ,   NightShiftFlg        = '{UpdEntity.NightShiftFlg}'
            ,   DefaultWorkFlg       = '{UpdEntity.DefaultWorkFlg}'
            ,   MhscFlg              = '{UpdEntity.MhscFlg}'
            ,   ForcingShiftBFlg     = '{UpdEntity.ForcingShiftBFlg}'
            ,   DeleteFlg            = '{UpdEntity.DeleteFlg}'
            WHERE
                EmploeeNum = '{UpdEntity.EmploeeNum}'
            ";
            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {
                        cn.Open();
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// デフォルト作業マスタ登録
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="InsertEntity"></param>
        /// <returns></returns>
        public static void InsertDefaultWorkMst(string ConnStrTimeEntryDB, MstEmploeeEntity InsertEntity) {

            // SQL文の作成
            string sql = $@"
            INSERT INTO
                TM_DEFAULT_WORK(
                EmploeeNum
            ,   DefaultWorkChargeNo1
            ,   DefaultWorkName1
            ,   DefaultWorkTimeH1
            ,   DefaultWorkTimeM1
            ,   DefaultWorkChargeNo2
            ,   DefaultWorkName2
            ,   DefaultWorkTimeH2
            ,   DefaultWorkTimeM2
            ,   DefaultWorkChargeNo3
            ,   DefaultWorkName3 
            ,   DefaultWorkTimeH3
            ,   DefaultWorkTimeM3)VALUES(
                '{InsertEntity.EmploeeNum}'
            ,   '{InsertEntity.DefaultWorkChargeNo1}'
            ,   '{InsertEntity.DefaultWorkName1}'
            ,   '{InsertEntity.DefaultWorkTimeH1}'
            ,   '{InsertEntity.DefaultWorkTimeM1}'
            ,   '{InsertEntity.DefaultWorkChargeNo2}'
            ,   '{InsertEntity.DefaultWorkName2}'
            ,   '{InsertEntity.DefaultWorkTimeH2}'
            ,   '{InsertEntity.DefaultWorkTimeM2}'
            ,   '{InsertEntity.DefaultWorkChargeNo3}'
            ,   '{InsertEntity.DefaultWorkName3}'
            ,   '{InsertEntity.DefaultWorkTimeH3}'
            ,   '{InsertEntity.DefaultWorkTimeM3}'
            )";

            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {
                        cn.Open();
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// デフォルト作業マスタ更新
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="UpdEntity"></param>
        /// <returns></returns>
        public static void UpdDefaultWorkMst(string ConnStrTimeEntryDB, MstEmploeeEntity UpdEntity) {

            // SQL文の作成
            string sql = $@"
            UPDATE
                TM_DEFAULT_WORK
            SET
                DefaultWorkChargeNo1 = '{UpdEntity.DefaultWorkChargeNo1}'
            ,   DefaultWorkName1     = '{UpdEntity.DefaultWorkName1}'
            ,   DefaultWorkTimeH1    = '{UpdEntity.DefaultWorkTimeH1}'
            ,   DefaultWorkTimeM1    = '{UpdEntity.DefaultWorkTimeM1}'
            ,   DefaultWorkChargeNo2 = '{UpdEntity.DefaultWorkChargeNo2}'
            ,   DefaultWorkName2     = '{UpdEntity.DefaultWorkName2}'
            ,   DefaultWorkTimeH2    = '{UpdEntity.DefaultWorkTimeH2}'
            ,   DefaultWorkTimeM2    = '{UpdEntity.DefaultWorkTimeM2}'
            ,   DefaultWorkChargeNo3 = '{UpdEntity.DefaultWorkChargeNo3}'
            ,   DefaultWorkName3     = '{UpdEntity.DefaultWorkName3}'
            ,   DefaultWorkTimeH3    = '{UpdEntity.DefaultWorkTimeH3}'
            ,   DefaultWorkTimeM3    = '{UpdEntity.DefaultWorkTimeM3}'
            WHERE
                EmploeeNum = '{UpdEntity.EmploeeNum}'
            ";

            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {
                        cn.Open();
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// 部署マスタロード
        /// </summary>
        /// <param name="ConfigInfo"></param>
        /// <returns></returns>
        public static List<MstBuEntity> LoadBuMst(TimeEntryConfigEntity ConfigInfo) {
            if (ConfigInfo.UsingCsvMstFlg == FlgValConst._INT_FLG_ON) {
                return LoadBuCsv(ConfigInfo.EmploeePath);
            }
            return LoadBuDB(ConfigInfo.ConnStrTimeEntryDB);
        }
        /// <summary>
        /// 部署マスタロード（DB）
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <returns></returns>
        public static List<MstBuEntity> LoadBuDB(string ConnStrTimeEntryDB) {
            var ret = new List<MstBuEntity>();

            // SQL文の作成
            string sql = $@"
            SELECT
                BU.*
            FROM
                TM_BU AS BU
            ";
            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {
                        cn.Open();
                        cmd.CommandText = sql;
                        using (var dr = cmd.ExecuteReader()) {
                            while (dr.Read()) {
                                var entity = new MstBuEntity();
                                entity.BuCode = CommonUtil.ToStringForbidNull(dr[nameof(entity.BuCode)]);
                                entity.BuName = CommonUtil.ToStringForbidNull(dr[nameof(entity.BuName)]);
                                entity.DelFlg = CommonUtil.ToStringForbidNull(dr[nameof(entity.DelFlg)]);
                                ret.Add(entity);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                throw e;
            }
            return ret;
        }
        /// <summary>
        /// 部署マスタロード（CSV）
        /// </summary>
        /// <param name="ConfigInfo"></param>
        /// <returns></returns>
        public static List<MstBuEntity> LoadBuCsv(string EmploeePath) {
            string csvPath = Path.Combine(EmploeePath, TimeEntryConfigConst._CSV_NAME_MST_BU);
            var ret = new List<MstBuEntity>();
            using (var sr = new StreamReader(csvPath, Encoding.GetEncoding(EncodingConst._ENCODING_SJIS))) {
                bool isFirdtLine = true;
                while (!sr.EndOfStream) {
                    var line = sr.ReadLine();
                    if (isFirdtLine) {
                        // １行目はヘッダーなのでスルー
                        isFirdtLine = false;
                        continue;
                    }
                    var values = line.Split(CommonLiteral._CSV_SEPALATOR);
                    if (values.Length != _COL_COUNT_CSV_BU) {
                        // 既定の行数でなければスルー
                        continue;
                    }
                    var bu = new MstBuEntity() {
                        BuCode = values[0],
                        BuName = values[1],
                        DelFlg = values[2],
                    };
                    ret.Add(bu);
                }
            }
            return ret;
        }
        /// <summary>
        /// 部署マスタ登録（DB）
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="InsEntities"></param>
        public static void InsBuMst(string ConnStrTimeEntryDB, List<MstBuEntity> InsEntities) {
            if (InsEntities.Count == 0) {
                return;
            }
            try {
                foreach (var InsEntity in InsEntities) {
                    using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                        using (var cmd = cn.CreateCommand()) {
                            // SQL文の作成
                            string sql = $@"
                            INSERT INTO TM_BU(
                                BuCode
                            ,   BuName
                            ,   DelFlg
                            )VALUES(
                                '{InsEntity.BuCode}'
                            ,   '{InsEntity.BuName}'
                            ,   '{InsEntity.DelFlg}'
                            );
                            ";
                            cn.Open();
                            cmd.CommandText = sql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// 部署マスタ更新（DB）
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="UpdEntities"></param>
        public static void UpdBuMst(string ConnStrTimeEntryDB, List<MstBuEntity> UpdEntities) {
            if (UpdEntities.Count == 0) {
                return;
            }
            try {
                foreach (var UpdEntity in UpdEntities) {
                    using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                        using (var cmd = cn.CreateCommand()) {
                            // SQL文の作成
                            string sql = $@"
                            UPDATE
                                TM_BU
                            SET
                                BuName = '{UpdEntity.BuName}'
                            ,   DelFlg = '{UpdEntity.DelFlg}'
                            WHERE
                                BuCode = '{UpdEntity.BuCode}'
                            ";
                            cn.Open();
                            cmd.CommandText = sql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// グループマスタロード
        /// </summary>
        /// <param name="ConfigInfo"></param>
        /// <returns></returns>
        public static List<MstGroupEntity> LoadGroupMst(TimeEntryConfigEntity ConfigInfo) {
            if (ConfigInfo.UsingCsvMstFlg == FlgValConst._INT_FLG_ON) {
                return LoadGroupCsv(ConfigInfo.EmploeePath);
            }
            return LoadGroupDB(ConfigInfo.ConnStrTimeEntryDB);
        }
        /// <summary>
        /// グループマスタロード（DB）
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <returns></returns>
        public static List<MstGroupEntity> LoadGroupDB(string ConnStrTimeEntryDB) {
            var ret = new List<MstGroupEntity>();

            // SQL文の作成
            string sql = $@"
            SELECT
                GR.*
            FROM
                TM_GROUP AS GR
            INNER JOIN
                TM_BU AS BU
            ON
                BU.BuCode = GR.BuCode
            ";
            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {
                        cn.Open();
                        cmd.CommandText = sql;
                        using (var dr = cmd.ExecuteReader()) {
                            while (dr.Read()) {
                                var entity = new MstGroupEntity();
                                entity.BuCode    = CommonUtil.ToStringForbidNull(dr[nameof(entity.BuCode)]);
                                entity.GroupCode = CommonUtil.ToStringForbidNull(dr[nameof(entity.GroupCode)]);
                                entity.GroupName = CommonUtil.ToStringForbidNull(dr[nameof(entity.GroupName)]);
                                entity.DelFlg    = CommonUtil.ToStringForbidNull(dr[nameof(entity.DelFlg)]);
                                ret.Add(entity);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                throw e;
            }
            return ret;
        }
        /// <summary>
        /// グループマスタロード（CSV）
        /// </summary>
        /// <param name="EmploeePath"></param>
        /// <returns></returns>
        public static List<MstGroupEntity> LoadGroupCsv(string EmploeePath) {
            var ret = new List<MstGroupEntity>();
            string csvPathGroup = Path.Combine(EmploeePath, TimeEntryConfigConst._CSV_NAME_MST_GROUP);
            string csvPathBu = Path.Combine(EmploeePath, TimeEntryConfigConst._CSV_NAME_MST_BU);
            var BuEntities = LoadBuCsv(EmploeePath);
            using (var sr = new StreamReader(csvPathGroup, Encoding.GetEncoding(EncodingConst._ENCODING_SJIS))) {
                bool isFirdtLine = true;
                while (!sr.EndOfStream) {
                    var line = sr.ReadLine();
                    if (isFirdtLine) {
                        // １行目はヘッダーなのでスルー
                        isFirdtLine = false;
                        continue;
                    }
                    var values = line.Split(CommonLiteral._CSV_SEPALATOR);
                    if (values.Length != _COL_COUNT_CSV_GROUP) {
                        // 既定の行数でなければスルー
                        continue;
                    }
                    var group = new MstGroupEntity() {
                        BuCode = values[0],
                        GroupCode = values[1],
                        GroupName = values[2],
                        DelFlg = values[3],
                    };
                    if (BuEntities.Where(n => n.BuCode == group.BuCode && n.DelFlg == FlgValConst._STR_FLG_OFF).FirstOrDefault() != null) {
                        ret.Add(group);
                    }
                }
            }
            return ret;
        }
        /// <summary>
        /// グループマスタ登録（DB）
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="InsEntities"></param>
        public static void InsGroupMst(string ConnStrTimeEntryDB, List<MstGroupEntity> InsEntities) {
            if (InsEntities.Count == 0) {
                return;
            }
            try {
                foreach (var InsEntity in InsEntities) {
                    using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                        using (var cmd = cn.CreateCommand()) {

                            // SQL文の作成
                            string sql = $@"
                            INSERT INTO TM_GROUP(
                                BuCode
                            ,   GroupCode
                            ,   GroupName
                            ,   DelFlg
                            )VALUES(
                                '{InsEntity.BuCode}'
                            ,   '{InsEntity.GroupCode}'
                            ,   '{InsEntity.GroupName}'
                            ,   '{InsEntity.DelFlg}'
                            );
                            ";
                            cn.Open();
                            cmd.CommandText = sql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            } catch (Exception e) {
                throw e;
            }

        }
        /// <summary>
        /// グループマスタ更新（DB）
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="UpdEntities"></param>
        public static void UpdGroupMst(string ConnStrTimeEntryDB, List<MstGroupEntity> UpdEntities) {
            if (UpdEntities.Count == 0) {
                return;
            }
            try {
                foreach (var UpdEntity in UpdEntities) {
                    using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                        using (var cmd = cn.CreateCommand()) {
                            // SQL文の作成
                            string sql = $@"
                            UPDATE
                                TM_GROUP
                            SET
                                GroupName = '{UpdEntity.GroupName}'
                            ,   DelFlg    = '{UpdEntity.DelFlg}'
                            WHERE
                                BuCode    = '{UpdEntity.BuCode}'
                            AND GroupCode = '{UpdEntity.GroupCode}'
                            ";
                            cn.Open();
                            cmd.CommandText = sql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// MHSC作業マスタロード（DB）
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="tgtYear"></param>
        /// <param name="DelContain"></param>
        /// <returns></returns>
        public static List<MhscMstEntity> LoadMhscMst(string ConnStrTimeEntryDB, int tgtYear = 0,  bool DelContain = false) {
            var ret = new List<MhscMstEntity>();
            // コメントアウト
            string cmOutNendo = string.Empty;
            if (tgtYear == 0) {
                cmOutNendo = "--";
            }
            string cmOutDel = string.Empty;
            if (DelContain) {
                cmOutDel = "--";
            }
            // SQL文の作成
            string sql = $@"
            SELECT
                MHSC.*
            FROM
                TM_MHSC_WORK AS MHSC
            WHERE
                0 = 0
            {cmOutNendo}AND MHSC.Nendo = '{tgtYear}'
            {cmOutDel  }AND MHSC.DelFlg= 0
            ";
            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {
                        cn.Open();
                        cmd.CommandText = sql;
                        using (var dr = cmd.ExecuteReader()) {
                            while (dr.Read()) {
                                var entity = new MhscMstEntity();
                                entity.Nendo = CommonUtil.ToStringForbidNull(dr[nameof(entity.Nendo)]);
                                entity.ChargeNo = CommonUtil.ToStringForbidNull(dr[nameof(entity.ChargeNo)]);
                                entity.WorkName = CommonUtil.ToStringForbidNull(dr[nameof(entity.WorkName)]);
                                entity.RestFlg  = int.Parse(CommonUtil.ToStringForbidNull(dr[nameof(entity.RestFlg)]));
                                entity.DelFlg = int.Parse(CommonUtil.ToStringForbidNull(dr[nameof(entity.DelFlg)]));
                                ret.Add(entity);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                throw e;
            }
            return ret;
        }
        /// <summary>
        /// MHSC作業マスタ登録（DB）
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="InsEntities"></param>
        public static void InsMhscMst(string ConnStrTimeEntryDB, List<MhscMstEntity> InsEntities) {
            if (InsEntities.Count == 0) {
                return;
            }
            try {
                foreach (var InsEntity in InsEntities) {
                    using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                        using (var cmd = cn.CreateCommand()) {

                            // SQL文の作成
                            string sql = $@"
                            INSERT INTO TM_MHSC_WORK(
                                Nendo
                            ,   ChargeNo
                            ,   WorkName
                            ,   RestFlg
                            ,   DelFlg
                            )VALUES(
                                '{InsEntity.Nendo}'
                            ,   '{InsEntity.ChargeNo}'
                            ,   '{InsEntity.WorkName}'
                            ,   '{InsEntity.RestFlg}'
                            ,   '{InsEntity.DelFlg}'
                            );
                            ";
                            cn.Open();
                            cmd.CommandText = sql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            } catch (Exception e) {
                throw e;
            }

        }
        /// <summary>
        /// MHSC作業マスタ更新（DB）
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="UpdEntities"></param>
        public static void UpdMhscMst(string ConnStrTimeEntryDB, List<MhscMstEntity> UpdEntities) {
            if (UpdEntities.Count == 0) {
                return;
            }
            try {
                foreach (var UpdEntity in UpdEntities) {
                    using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                        using (var cmd = cn.CreateCommand()) {
                            // SQL文の作成
                            string sql = $@"
                            UPDATE
                                TM_MHSC_WORK
                            SET
                                WorkName  = '{UpdEntity.WorkName}'
                            ,   RestFlg   = '{UpdEntity.RestFlg}'
                            ,   DelFlg    = '{UpdEntity.DelFlg}'
                            WHERE
                                Nendo     = '{UpdEntity.Nendo}'
                            AND ChargeNo  = '{UpdEntity.ChargeNo}'
                            ";
                            cn.Open();
                            cmd.CommandText = sql;
                            cmd.ExecuteNonQuery();
                        }
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// 年度取得
        /// </summary>
        /// <param name="tgtDate"></param>
        /// <returns></returns>
        public static int GetNendo(DateTime tgtDate) {
            int ret = tgtDate.Year;
            if (tgtDate.Month < 4) {
                ret--;
            }
            return ret;
        }
        /// <summary>
        /// 計画工数更新レコードロード
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="con"></param>
        /// <returns></returns>
        public static List<UpdPlannedTimeRecEntity> LoadUpdPlanndeTimeRec(string ConnStrTimeEntryDB, UpdPlannedTimeRecEntity con) {
            var ret = new List<UpdPlannedTimeRecEntity>();

            // SQL文の作成
            string sql = $@"
            SELECT DISTINCT ProjectId, UserId, YearMonth, PlannedTime FROM TT_UPD_PLANNED_TIME_REC
            WHERE
                ProjectId = '{con.ProjectId}'
            AND UserId    = '{con.UserId}'
            AND YearMonth = '{con.YearMonth}'
            ";
            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {
                        cn.Open();
                        cmd.CommandText = sql;
                        using (var dr = cmd.ExecuteReader()) {
                            while (dr.Read()) {
                                var entity = new UpdPlannedTimeRecEntity();
                                entity.ProjectId = CommonUtil.ToStringForbidNull(dr[nameof(entity.ProjectId)]);
                                entity.UserId = CommonUtil.ToStringForbidNull(dr[nameof(entity.UserId)]);
                                entity.YearMonth = DateTime.Parse(CommonUtil.ToStringForbidNull(dr[nameof(entity.YearMonth)]));
                                entity.PlannedTime = decimal.Parse(CommonUtil.ToStringForbidNull(dr[nameof(entity.PlannedTime)]));
                                ret.Add(entity);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                throw e;
            }
            return ret;
        }
        /// <summary>
        /// 計画工数更新レコード存在チェック
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="con"></param>
        /// <returns></returns>
        public static bool IsExistUpdPlanndeTimeRec(string ConnStrTimeEntryDB, UpdPlannedTimeRecEntity con) {

            // SQL文の作成
            string sql = $@"
            SELECT COUNT(*) FROM TT_UPD_PLANNED_TIME_REC
            WHERE
                ProjectId = '{con.ProjectId}'
            AND UserId    = '{con.UserId}'
            AND YearMonth = '{con.YearMonth}'
            ";
            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {
                        cn.Open();
                        cmd.CommandText = sql;
                        int count = int.Parse(cmd.ExecuteScalar().ToString());
                        if (count == 0) {
                            return false;
                        }
                        return true;
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// 計画工数更新レコード登録
        /// </summary>
        /// <param name="ConfigInfo"></param>
        /// <param name="InsEntity"></param>
        public static void InsertUpdPlannedTimeRec(string ConnStrTimeEntryDB, UpdPlannedTimeRecEntity InsEntity) {
            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {

                        // SQL文の作成
                        string sql = $@"
                            INSERT INTO TT_UPD_PLANNED_TIME_REC(
                                UserId
                            ,   ProjectId
                            ,   YearMonth
                            ,   InitHostName
                            ,   InitDate
                            ,   PlannedTime
                            )VALUES(
                                '{InsEntity.UserId}'
                            ,   '{InsEntity.ProjectId}'
                            ,   '{InsEntity.YearMonth}'
                            ,   '{InsEntity.InitHostName}'
                            ,   '{InsEntity.InitDate}'
                            ,   '{InsEntity.PlannedTime}'
                            );
                            ";
                        cn.Open();
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// 計画工数更新レコード更新
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="InsEntity"></param>
        public static void UpdateUpdPlannedTimeRec(string ConnStrTimeEntryDB, UpdPlannedTimeRecEntity InsEntity) {
            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {

                        // SQL文の作成
                        string sql = $@"
                            UPDATE TT_UPD_PLANNED_TIME_REC
                            SET 
                                InitHostName = '{InsEntity.InitHostName}'
                            ,   PlannedTime  = '{InsEntity.PlannedTime}'
                            ,   InitDate     = '{InsEntity.InitDate}'
                            WHERE
                                UserId       = '{InsEntity.UserId}'
                            AND ProjectId    = '{InsEntity.ProjectId}'
                            AND YearMonth    = '{InsEntity.YearMonth.ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH)}'
                            ;
                            ";
                        cn.Open();
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// 計画工数更新レコード削除
        /// </summary>
        /// <param name="InsEntities"></param>
        public static void DelUpdPlannedTimeRec(string ConnStrTimeEntryDB, UpdPlannedTimeRecEntity InsEntity) {
            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {

                        // SQL文の作成
                        string sql = $@"
                            DELETE FROM TT_UPD_PLANNED_TIME_REC
                            WHERE
                                UserId       = '{InsEntity.UserId}'
                            AND ProjectId    = '{InsEntity.ProjectId}'
                            AND YearMonth    = '{InsEntity.YearMonth.ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH)}'
                            ;
                            ";
                        cn.Open();
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// 計画工数更新レコード削除（時間）
        /// </summary>
        /// <param name="ConnStrTimeEntryDB"></param>
        /// <param name="SysDate"></param>
        public static void DelUpdPlannedTimeRec(string ConnStrTimeEntryDB, DateTime SysDate) {
            try {
                using (var cn = new SqlConnection(ConnStrTimeEntryDB)) {
                    using (var cmd = cn.CreateCommand()) {

                        // SQL文の作成
                        string sql = $@"
                            DELETE FROM TT_UPD_PLANNED_TIME_REC
                            WHERE
                                InitDate <= '{SysDate.AddMinutes(-3)}'
                            ;
                            ";
                        cn.Open();
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                    }
                }
            } catch (Exception e) {
                throw e;
            }
        }
        /// <summary>
        /// 勤怠情報取得（SQL）
        /// </summary>
        /// <param name="ConfigInfo"></param>
        /// <param name="emp"></param>
        /// <param name="sumDt"></param>
        /// <returns></returns>
        public static List<WorkTimeDto> GetWtDtoList_SQL(TimeEntryConfigEntity ConfigInfo, MstEmploeeEntity emp, DateTime sumDt) {
            var ret = new List<WorkTimeDto>();
            var rows = new List<string[]>();
            var _Conn = new MySqlConnection(ConfigInfo.ConnStrWBDB);
            var _Command = new MySqlCommand();
            try {
                _Conn.Open();
                _Command.CommandTimeout = 600;
                _Command.Connection = _Conn;
                _Command.CommandText = GetSqlSelWBWorkTimes(emp, sumDt);
                using (MySqlDataReader sdr = _Command.ExecuteReader()) {
                    while (sdr.Read()) {
                        var dto = new WorkTimeDto();
                        dto.Day = int.Parse(sdr[0].ToString());
                        dto.WeekDay = ConvertWBWeekDay(sumDt, dto.Day);
                        dto.StartHH = ConvertWBTime(sdr[1].ToString(), true);
                        dto.StartMM = ConvertWBTime(sdr[1].ToString(), false);
                        dto.EndHH = ConvertWBTime(sdr[2].ToString(), true);
                        dto.EndMM = ConvertWBTime(sdr[2].ToString(), false);
                        dto.RestMark = ConvertWBRestFlg(sdr[3].ToString(), dto);

                        // 時刻丸め
                        bool isMHSC = emp.MhscFlg == FlgValConst._STR_FLG_ON;
                        bool isShiftB = emp.ForcingShiftBFlg == FlgValConst._STR_FLG_ON;
                        var newDto = KinmuhyoUtil.RoundWorkTimeProxy(ConfigInfo.DefaultKinmuhyoFormat, sumDt, dto, isMHSC, isShiftB);
                        ret.Add(newDto);
                    }
                }
            } catch (Exception e) {
                throw e;
            } finally {
                _Conn.Close();
            }
            return ret;
        }
        /// <summary>
        /// SQL文の作成（WB出退勤時刻取得）
        /// </summary>
        /// <param name="emp"></param>
        /// <param name="sumDt"></param>
        /// <returns></returns>
        private static string GetSqlSelWBWorkTimes(MstEmploeeEntity emp, DateTime sumDt) {
            string Sql = $@"
            SELECT
                T_Shuttai.day
            ,   T_Shuttai.stime
            ,   T_Shuttai.etime
            ,   T_Shuttai.flag
            FROM
                jmis.Shuttaikin AS T_Shuttai
            WHERE
                T_Shuttai.id    = '{emp.EmploeeNum}'
            AND T_Shuttai.year  = '{sumDt.Year}'
            AND T_Shuttai.month = '{sumDt.Month}'
            ORDER BY
                T_Shuttai.day
            ,   T_Shuttai.stime
            ,   T_Shuttai.etime
            ";
            return Sql;
        }
        /// <summary>
        /// WB勤怠時刻を文字列に変換
        /// </summary>
        /// <param name="wbTime"></param>
        /// <param name="isHH"></param>
        /// <returns></returns>
        private static string ConvertWBTime(string wbTime, bool isHH) {
            if (wbTime == decimal.Zero.ToString()) {
                // 0なら空文字を返す（出勤していない）
                return string.Empty;
            }
            int intWbTime;
            if (!int.TryParse(wbTime, out intWbTime)) {
                // 不正値なら空文字を返す（出勤していない）
                return string.Empty;
            }
            var intWbHH = Math.Truncate((decimal)(intWbTime / 100));
            if (isHH) {
                // 時刻（時）
                return intWbHH.ToString();
            }
            // 時刻（分）
            var decWbMM = intWbTime - (intWbHH * 100);
            return decWbMM.ToString(FormatConst._FORMAT_PRE_ZERO_2);
        }
        /// <summary>
        /// WBのフラグから休暇文字列に変換
        /// </summary>
        /// <param name="wbFlg"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        private static string ConvertWBRestFlg(string wbFlg, WorkTimeDto dto) {
            if (string.IsNullOrEmpty(wbFlg)) {
                return string.Empty;
            }
            int intWbFlg;
            if (!int.TryParse(wbFlg, out intWbFlg)) {
                return string.Empty;
            }
            switch (intWbFlg) {
                case (int)KinmuhyoConst.WBShuttaiFlag.NORMAL:
                    return string.Empty;
                case (int)KinmuhyoConst.WBShuttaiFlag.REST:
                    if (string.IsNullOrEmpty(dto.StartHH) && string.IsNullOrEmpty(dto.EndHH)) {
                        // 出退勤時刻が登録されていなければ有休扱い
                        return KinmuhyoConst._MARK_REST;
                    }
                    return string.Empty;
            }
            return string.Empty;
        }
        /// <summary>
        /// WBの日付から曜日に変換
        /// </summary>
        /// <param name="sumDt"></param>
        /// <param name="tgtDay"></param>
        /// <returns></returns>
        private static string ConvertWBWeekDay(DateTime sumDt, int tgtDay) {
            var tgtWeekDay = new DateTime(sumDt.Year, sumDt.Month, tgtDay).DayOfWeek;
            switch (tgtWeekDay) {
                case DayOfWeek.Sunday:
                    return "日";
                case DayOfWeek.Monday:
                    return "月";
                case DayOfWeek.Tuesday:
                    return "火";
                case DayOfWeek.Wednesday:
                    return "水";
                case DayOfWeek.Thursday:
                    return "木";
                case DayOfWeek.Friday:
                    return "金";
                case DayOfWeek.Saturday:
                    return "土";
            }
            return string.Empty;
        }
        /// <summary>
        /// 文字列→数値変換
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        private static int GetIntValue(string val) {
            int intVal = 0;
            if (!int.TryParse(val, out intVal)) {
                return 0;
            }
            return intVal;
        }
        #endregion
    }
}
