﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using TimeEntryShared.Const;
using TimeEntryShared.Dto;
using TimeEntryShared.Entity;

namespace TimeEntryShared.Util {
    public static class KinmuhyoUtil {
        private static TimeSpan _TS_15 = TimeSpan.FromMinutes(15);
        private static TimeSpan _TS_30 = TimeSpan.FromMinutes(30);
        /// <summary>
        /// ホワイトボードURL取得
        /// </summary>
        /// <param name="configInfo"></param>
        /// <param name="hitEmploee"></param>
        /// <returns></returns>
        public static string GetWhiteBordURL(TimeEntryConfigEntity configInfo, MstEmploeeEntity hitEmploee) {
            string ret = configInfo.URL_WHITE_BORD;
            ret += hitEmploee.EmploeeNum;
            if (0 < configInfo.WbLookPreMonthCount) {
                ret += string.Concat(CommonLiteral._MARK_PRE_MONTH, configInfo.WbLookPreMonthCount);
            }
            return ret;
        }
        /// <summary>
        /// 勤務表パス取得
        /// </summary>
        /// <param name="configInfo"></param>
        /// <param name="buInfos"></param>
        /// <param name="groupInfos"></param>
        /// <param name="emp"></param>
        /// <param name="TgtYYYYMM"></param>
        /// <returns></returns>
        public static string GetKinmuhyoPath(TimeEntryConfigEntity configInfo, List<MstBuEntity> buInfos, List<MstGroupEntity> groupInfos, MstEmploeeEntity emp, DateTime TgtYYYYMM) {
            string position = buInfos.Where(n => n.BuCode == emp.BuCode).FirstOrDefault().BuName;
            string group = string.Empty;
            if (groupInfos.Where(n => n.GroupCode == emp.GroupCode).FirstOrDefault() != null) {
                group = groupInfos.Where(n => n.GroupCode == emp.GroupCode).FirstOrDefault().GroupName;
                position = string.Concat(position, CommonLiteral._LITERAL_YEN_MARK, group);
            }
            string kinmuhyoPath = Path.Combine(configInfo.KinmuhyoPath, position);
            kinmuhyoPath = Path.Combine(kinmuhyoPath, TgtYYYYMM.ToString(FormatConst._FORMAT_DATE_YYYYMM_NENTSUKI));
            CommonUtil.CreateFolder(kinmuhyoPath);
            return kinmuhyoPath;
        }
        /// <summary>
        /// 勤務表ファイル名取得
        /// </summary>
        /// <param name="configInfo"></param>
        /// <param name="Busyo"></param>
        /// <param name="user"></param>
        /// <param name="Dt"></param>
        /// <returns></returns>
        public static string GetKinmuhyoName(TimeEntryConfigEntity configInfo, string user, DateTime Dt) {
            string kinmuhyoName = string.Format(configInfo.KinmuhyoName, Dt.Year, Dt.Month.ToString(FormatConst._FORMAT_PRE_ZERO_2), CommonUtil.CutSpace(user));
            return kinmuhyoName;
        }
        /// <summary>
        /// 勤務表のフォーマット名取得
        /// </summary>
        /// <param name="configInfo"></param>
        /// <param name="Dt"></param>
        /// <returns></returns>
        public static TimeEntryConfigEntity GetKinmuhyoFormatName(TimeEntryConfigEntity configInfo, DateTime Dt) {
            var dtFirstDate = new DateTime(Dt.Year, Dt.Month, 1);
            var tgtKey = string.Empty;
            var tgtFormat = string.Empty;
            foreach (var dc in KinmuhyoFormatConst.KinmuhyoFormatDc) {
                var dtDc = DateTime.Parse(dc.Key);
                if (dtFirstDate < dtDc) {
                    break;
                }
                tgtKey = dc.Key;
                tgtFormat = dc.Value;
            }
            configInfo.DefaultKinmuhyoName = string.Format(configInfo.DefaultKinmuhyoName, Dt.Year, Dt.Month, tgtFormat);
            configInfo.DefaultKinmuhyoFormat = tgtKey;
            return configInfo;
        }
        /// <summary>
        /// 作業名からチャージ№を除く
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static EditTimeEntryDto RemoveChargeNo(EditTimeEntryDto data) {
            if (data.ProjectName.Contains(data.ProjectCode)) {
                data.ProjectName = data.ProjectName.Replace(data.ProjectCode, string.Empty).TrimStart().TrimEnd();
                return data;
            }
            if (data.ProjectName.Contains(CommonLiteral._CHAR_HYPHEN)) {

                // チャージ№がハイフン２つ区切りの場合
                if (data.ProjectName.IndexOf(CommonLiteral._CHAR_HYPHEN) != data.ProjectName.LastIndexOf(CommonLiteral._CHAR_HYPHEN)) {

                    // 想定される基本形に整形（###-##-##、予###-##-##）
                    data.ProjectCode = data.ProjectName.Substring(0, data.ProjectName.IndexOf(CommonLiteral._CHAR_HYPHEN) + 6);
                    data.ProjectName = data.ProjectName.Replace(data.ProjectCode, string.Empty).TrimStart();

                    // チャージ№の最後にXが3つ並んでいるパターン（###-##-XXX）
                    string chargeNoLast2 = data.ProjectCode.Substring(data.ProjectCode.Length - 2, 2);
                    string chargeNameTop1 = data.ProjectName.Substring(0, 1);
                    if (string.Concat(chargeNoLast2, chargeNameTop1) == KinmuhyoConst._CHARGE_NO_LAST_XXX) {
                        data.ProjectCode += chargeNameTop1;
                        data.ProjectName = data.ProjectName.Substring(1, data.ProjectName.Length - 2).TrimStart();
                        return data;
                    }
                    // 新宿協力会社のパターン（SJK-###-00）
                    string chargeNoTop3 = data.ProjectCode.Substring(0, 3);
                    if (chargeNoTop3 == KinmuhyoConst._CHARGE_NO_SJK_TOP3 && string.Concat(chargeNoLast2, chargeNameTop1) == KinmuhyoConst._CHARGE_NO_SJK_LAST3) {
                        data.ProjectCode += chargeNameTop1;
                        data.ProjectName = data.ProjectName.Substring(1, data.ProjectName.Length - 2).TrimStart();
                        return data;
                    }
                    return data;
                }
                // チャージ№のハイフンが1つかつ頭文字が「予」の場合（予YYYYMMDD-####）
                int intChargeNo = 0;
                if (data.ProjectName.Substring(0, 1) == KinmuhyoConst._CHARGE_NO_TOP_YOTEI && int.TryParse(data.ProjectName.Substring(1, 7), out intChargeNo)){
                    data.ProjectCode = data.ProjectName.Substring(0, data.ProjectName.IndexOf(CommonLiteral._CHAR_HYPHEN) + 5);
                    data.ProjectName = data.ProjectName.Replace(data.ProjectCode, string.Empty).TrimStart();
                    return data;
                }
            }
            return data;
        }
        /// <summary>
        /// 勤怠時刻丸め（Proxy）
        /// </summary>
        /// <param name="tgtFormat"></param>
        /// <param name="tgtDt"></param>
        /// <param name="dto"></param>
        /// <param name="isMHSC"></param>
        /// <param name="isShiftB"></param>
        /// <returns></returns>
        public static WorkTimeDto RoundWorkTimeProxy(string tgtFormat, DateTime tgtDt, WorkTimeDto dto, bool isMHSC, bool isShiftB) {
            // 有休
            var ret = new WorkTimeDto();
            if (dto.RestMark == KinmuhyoConst._MARK_REST) {
                ret.RestMark = KinmuhyoConst._MARK_PAID;
                ret.RestType = TimeConst.RestType.PAID;
                ret.Day = dto.Day;
                ret.WeekDay = dto.WeekDay;
                return ret;
            }
            // 開始時刻
            if (!string.IsNullOrEmpty(dto.StartHH) && !string.IsNullOrEmpty(dto.StartMM)) {
                var startDt = GetWorkStartTime(tgtDt, dto);
                if (isShiftB && startDt.Hour < TimeConst._DEFAULT_NOON_REST_END_HH) {
                    // 強制BシフトONかつ午前中の出勤なら9:00に編集
                    ret.StartHH = TimeConst._DEFAULT_WORK_START_TYPE_B_HH.ToString();
                    ret.StartMM = decimal.Zero.ToString();
                } else {
                    ret.StartHH = startDt.Hour.ToString();
                    ret.StartMM = startDt.Minute.ToString();
                }
            }
            // 終了時刻
            bool IsDaySpan = (!string.IsNullOrEmpty(dto.EndHH) && TimeConst._HOURS_A_DAY <= int.Parse(dto.EndHH));
            if (!string.IsNullOrEmpty(dto.EndHH) && !string.IsNullOrEmpty(dto.EndMM)) {
                var EndDt = GetWorkEndTime(tgtDt, dto, IsDaySpan);
                ret.EndHH = IsDaySpan ? (EndDt.Hour + TimeConst._HOURS_A_DAY).ToString() : EndDt.Hour.ToString();
                ret.EndMM = EndDt.Minute.ToString();
                decimal decEndHH;
                decimal decEndMM;
                if (decimal.TryParse(ret.EndHH, out decEndHH) && decimal.TryParse(ret.EndMM, out decEndMM)) {
                    decimal decEndTime = (decEndHH * TimeConst._MINUTE_AN_HOUR) + decEndMM;
                    if (isShiftB && decEndTime < (TimeConst._DEFAULT_WORK_END_TYPE_B_HH * TimeConst._MINUTE_AN_HOUR)) {
                        // 強制BシフトONかつ定時前の退勤なら18:00に編集
                        ret.EndHH = TimeConst._DEFAULT_WORK_END_TYPE_B_HH.ToString();
                        ret.EndMM = decimal.Zero.ToString();
                    }
                }
            }
            ret.Day = dto.Day;
            ret.WeekDay = dto.WeekDay;

            // 出勤・退勤のどちらか一つでも未入力の場合は休暇判定しない
            if (string.IsNullOrEmpty(ret.StartHH) || string.IsNullOrEmpty(ret.EndHH)) {
                return ret;
            }
            switch (tgtFormat) {
                case KinmuhyoFormatConst._FORMAT_KINMUHYO_START_DATE_20190301:
                    ret = EditRestType_201903(ret, IsDaySpan);
                    break;
                case KinmuhyoFormatConst._FORMAT_KINMUHYO_START_DATE_20200501:
                    ret = EditRestType_202005(ret, IsDaySpan, isMHSC);
                    break;
            }
            return ret;
        }
        /// <summary>
        /// 休日種別取得（2019.03版）
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="NightShiftFlg"></param>
        /// <returns></returns>
        public static WorkTimeDto EditRestType_201903(WorkTimeDto ret, bool NightShiftFlg) {
            if (NightShiftFlg) {
                return ret;
            }
            // 午前休
            decimal dtTimeStart = (int.Parse(ret.StartHH) * TimeConst._MINUTE_AN_HOUR) + int.Parse(ret.StartMM);
            if (dtTimeStart >= (TimeConst._DEFAULT_NOON_REST_START_HH) * TimeConst._MINUTE_AN_HOUR
             && dtTimeStart <= (TimeConst._DEFAULT_NOON_REST_END_HH + 1) * TimeConst._MINUTE_AN_HOUR) {
                ret.RestType = TimeConst.RestType.AM;
            }
            // 午後休
            decimal dtTimeEnd = (int.Parse(ret.EndHH) * TimeConst._MINUTE_AN_HOUR) + int.Parse(ret.EndMM);
            if (dtTimeEnd >= (TimeConst._DEFAULT_NOON_REST_START_HH) * TimeConst._MINUTE_AN_HOUR
             && dtTimeEnd <= (TimeConst._DEFAULT_NOON_REST_END_HH + 1) * TimeConst._MINUTE_AN_HOUR) {
                ret.RestType = TimeConst.RestType.PM;
            }
            return ret;
        }
        /// <summary>
        /// 休日種別取得（2020.05版）
        /// </summary>
        /// <param name="ret"></param>
        /// <param name="NightShiftFlg"></param>
        /// <param name="isMHSC"></param>
        /// <returns></returns>
        public static WorkTimeDto EditRestType_202005(WorkTimeDto ret, bool NightShiftFlg, bool isMHSC) {
            if (NightShiftFlg || ret.RestType != TimeConst.RestType.NONE || isMHSC) {
                return ret;
            }
            decimal workStartTime = (int.Parse(ret.StartHH) * TimeConst._MINUTE_AN_HOUR) + int.Parse(ret.StartMM);
            decimal workEndTime = (int.Parse(ret.EndHH) * TimeConst._MINUTE_AN_HOUR) + int.Parse(ret.EndMM);
            if (workEndTime < workStartTime) {
                ret.RestType = TimeConst.RestType.NONE;
                return ret;
            }
            ret.ShiftType = JudgeShiftType(workStartTime, workEndTime);
            //switch (ret.ShiftType) {
            //    case TimeConst.WorkShiftType.A:
            //        if (TimeConst._DEFAULT_WORK_START_TYPE_A_HH * TimeConst._MINUTE_AN_HOUR < workStartTime) {
            //            ret = EditQuarterWork_Front(ret, workStartTime, 10, 12, 15, 17);
            //        } else {
            //            ret = EditQuarterWork_Back(ret, workEndTime, 10, 12, 15, 17);
            //        }
            //        break;
            //    case TimeConst.WorkShiftType.B:
            //        if (TimeConst._DEFAULT_WORK_START_TYPE_B_HH * TimeConst._MINUTE_AN_HOUR < workStartTime) {
            //            ret = EditQuarterWork_Front(ret, workStartTime, 11, 14, 16, 18);
            //        } else {
            //            ret = EditQuarterWork_Back(ret, workEndTime, 11, 14, 16, 18);
            //        }
            //        break;
            //    case TimeConst.WorkShiftType.C:
            //        if (TimeConst._DEFAULT_WORK_START_TYPE_C_HH * TimeConst._MINUTE_AN_HOUR < workStartTime) {
            //            ret = EditQuarterWork_Front(ret, workStartTime, 13, 15, 17, 19);
            //        } else {
            //            ret = EditQuarterWork_Back(ret, workEndTime, 13, 15, 17, 19);
            //        }
            //        break;
            //}
            return ret;
        }
        /// <summary>
        /// 出勤シフト判定
        /// </summary>
        /// <param name="workStartTime"></param>
        /// <param name="workEndTime"></param>
        /// <returns></returns>
        public static TimeConst.WorkShiftType JudgeShiftType(decimal workStartTime, decimal workEndTime) {
            // 出勤時刻から判定
            if (workStartTime == TimeConst._DEFAULT_WORK_START_TYPE_A_HH * TimeConst._MINUTE_AN_HOUR) {
                return TimeConst.WorkShiftType.A;
            }
            if (workStartTime == TimeConst._DEFAULT_WORK_START_TYPE_B_HH * TimeConst._MINUTE_AN_HOUR) {
                return TimeConst.WorkShiftType.B;
            }
            if (workStartTime == TimeConst._DEFAULT_WORK_START_TYPE_C_HH * TimeConst._MINUTE_AN_HOUR) {
                return TimeConst.WorkShiftType.C;
            }
            // 退勤時刻から判定（現状困難）

            return TimeConst.WorkShiftType.UNKNOWN;
        }
        /// <summary>
        /// 始業時刻取得
        /// </summary>
        /// <param name="tgtDt"></param>
        /// <param name="dto"></param>
        /// <returns></returns>
        public static DateTime GetWorkStartTime(DateTime tgtDt, WorkTimeDto dto) {
            int intStartHH = int.Parse(dto.StartHH);
            int intStartMM = int.Parse(dto.StartMM);
            var dt = new DateTime(tgtDt.Year, tgtDt.Month, dto.Day, intStartHH, intStartMM, 0);
            if (dt.Hour < TimeConst._DEFAULT_WORK_START_TYPE_A_HH || (dt.Hour == TimeConst._DEFAULT_WORK_START_TYPE_A_HH && dt.Minute == 0)) {
                // A出勤
                return new DateTime(tgtDt.Year, tgtDt.Month, dto.Day, TimeConst._DEFAULT_WORK_START_TYPE_A_HH, 0, 0);
            }
            if (dt.Hour < TimeConst._DEFAULT_WORK_START_TYPE_B_HH || (dt.Hour == TimeConst._DEFAULT_WORK_START_TYPE_B_HH && dt.Minute == 0)) {
                // B出勤
                return new DateTime(tgtDt.Year, tgtDt.Month, dto.Day, TimeConst._DEFAULT_WORK_START_TYPE_B_HH, 0, 0);
            }
            if (dt.Hour < TimeConst._DEFAULT_WORK_START_TYPE_C_HH || (dt.Hour == TimeConst._DEFAULT_WORK_START_TYPE_C_HH && dt.Minute == 0)) {
                // C出勤
                return new DateTime(tgtDt.Year, tgtDt.Month, dto.Day, TimeConst._DEFAULT_WORK_START_TYPE_C_HH, 0, 0);
            }
            return EditTimeUtil.RoundUp(dt, _TS_30);
        }
        /// <summary>
        /// 終業時刻取得
        /// </summary>
        /// <param name="tgtDt"></param>
        /// <param name="dto"></param>
        /// <param name="isDaySpan"></param>
        /// <returns></returns>
        public static DateTime GetWorkEndTime(DateTime tgtDt, WorkTimeDto dto, bool isDaySpan) {
            int intEndHH = int.Parse(dto.EndHH);
            int intEndMM = int.Parse(dto.EndMM);
            if (isDaySpan) {
                intEndHH -= TimeConst._HOURS_A_DAY;
            }
            var dt = new DateTime(tgtDt.Year, tgtDt.Month, dto.Day, intEndHH, intEndMM, 0);
            return EditTimeUtil.RoundDown(dt, _TS_15);
        }
        /// <summary>
        /// 勤務表を開きっぱなしにしていないかチェック
        /// </summary>
        /// <param name="ConfigInfo"></param>
        /// <param name="Bulist"></param>
        /// <param name="GroupList"></param>
        /// <param name="emp"></param>
        /// <param name="sumDt"></param>
        /// <returns></returns>
        public static bool ValidateIsKinmuhyoOpened(TimeEntryConfigEntity ConfigInfo, List<MstBuEntity> Bulist, List<MstGroupEntity> GroupList,
            MstEmploeeEntity emp, DateTime sumDt) {
            try {
                string kinmuhyoPath = GetKinmuhyoPath(ConfigInfo, Bulist, GroupList, emp, sumDt);
                string kinmuhyoName = GetKinmuhyoName(ConfigInfo, emp.Name, sumDt);
                string fullPath = Path.Combine(kinmuhyoPath, kinmuhyoName);
                if (!File.Exists(fullPath)) {
                    return true;
                }
                using (var fs = File.OpenWrite(fullPath)) {
                }
            } catch {
                return false;
            }
            return true;
        }
    }
}
