﻿using System;
using System.IO;
using TimeEntryShared.Const;
using TimeEntryShared.Dto;

namespace TimeEntryShared.Util {
    public static class CommonUtil {
        /// <summary>
        /// ToString（null非許容）
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ToStringForbidNull(object obj) {
            if (obj == null) {
                return string.Empty;
            }
            return obj.ToString();
        }
        /// <summary>
        /// 空文字削除
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string CutSpace(string str) {
            return str.Replace(CommonLiteral._LITERAL_SPACE_ALL, string.Empty).Replace(CommonLiteral._LITERAL_SPACE_HALF, string.Empty);
        }
        /// <summary>
        /// 既存ファイル削除
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteFile(string path) {
            if (File.Exists(path)) {
                File.Delete(path);
            }
        }
        /// <summary>
        /// フォルダ作成
        /// </summary>
        public static void CreateFolder(string path) {
            if (!Directory.Exists(path)) {
                Directory.CreateDirectory(path);
            }
        }
        /// <summary>
        /// フォルダ削除
        /// </summary>
        public static void DeleteFolder(string path) {
            if (Directory.Exists(path)) {
                Directory.Delete(path);
            }
        }
        /// <summary>
        /// サービスプロパティ取得
        /// </summary>
        /// <returns></returns>
        public static ServicePropaties GetPropaties() {
            var ret = new ServicePropaties();
            ret.ConfigInfo = ModelUtil.LoadConfig();
            ret.TimeTrackerUserInfos = TimeTrackerUtil.LoadTimeTrackerUserInfo();
            ret.BuList = ModelUtil.LoadBuMst(ret.ConfigInfo);
            ret.GroupList = ModelUtil.LoadGroupMst(ret.ConfigInfo);
            ret.EmploeeInfos = ModelUtil.LoadEmploeeMst(ret.ConfigInfo);
            int tgtNendo = ModelUtil.GetNendo(DateTime.Parse(ret.ConfigInfo.SumStartDate));
            ret.MhscMstInfos = ModelUtil.LoadMhscMst(ret.ConfigInfo.ConnStrTimeEntryDB, tgtNendo);
            return ret;
        }
        /// <summary>
        /// 稼働時間（分）を文字列型の時間（HH:MM）に変換
        /// </summary>
        /// <param name="decWorkTime"></param>
        /// <returns></returns>
        public static string ConvertWorkTimeToHHMMColon(decimal decWorkTime) {
            int intH = (int)(decWorkTime / TimeConst._MINUTE_AN_HOUR);
            int intM = (int)(decWorkTime % TimeConst._MINUTE_AN_HOUR);
            return string.Concat(intH.ToString(), CommonLiteral._LITERAL_COLON, intM.ToString(FormatConst._FORMAT_PRE_ZERO_2));
        }
        /// <summary>
        /// 文字列型の時間（HH:MM）を稼働時間（分）に変換
        /// </summary>
        /// <param name="strHHMM_Colon"></param>
        /// <returns></returns>
        public static decimal ConvertStrToWorkTimeMinutes(string strHHMM_Colon) {
            var spl = strHHMM_Colon.Split(CommonLiteral._LITERAL_COLON.ToCharArray()[0]);
            int intH = int.Parse(spl[0]);
            int intM = int.Parse(spl[1]);
            return intH * TimeConst._MINUTE_AN_HOUR + intM;
        }
        /// <summary>
        /// 入力値変換（obj→hh:mm）
        /// </summary>
        /// <param name="val"></param>
        /// <param name="defaultRetStr"></param>
        /// <returns></returns>
        public static string ConvertCellValToHHMM(object val, string defaultRetStr) {
            if (val == null) {
                return defaultRetStr;
            }
            string strVal = val.ToString();

            // 入力値が有効文字列の場合（hh:mm）
            if (strVal.Contains(CommonLiteral._LITERAL_COLON)) {
                var split = strVal.Split(CommonLiteral._LITERAL_COLON.ToCharArray()[0]);
                if (2 < split.Length) {
                    return defaultRetStr;
                }
                string spl1 = split[0];
                string spl2 = split[1];
                int intSpl1;
                int intSpl2;
                if (!int.TryParse(spl1, out intSpl1) || !int.TryParse(spl2, out intSpl2)) {
                    // 異常値（数値変換不可）
                    return defaultRetStr;
                }
                if (intSpl1 < 0 || intSpl2 < 0) {
                    // 異常値（マイナス）
                    return defaultRetStr;
                }
                if (999 < intSpl1 || TimeConst._MINUTE_AN_HOUR < intSpl2) {
                    // 異常値（時間変換不可）
                    return defaultRetStr;
                }
                if (intSpl2 != 15 && intSpl2 != 30 && intSpl2 != 45 && intSpl2 != 0) {
                    // 異常値（15分単位でない）
                    intSpl2 = 0;
                }
                return string.Concat(intSpl1.ToString(), CommonLiteral._LITERAL_COLON, intSpl2.ToString(FormatConst._FORMAT_PRE_ZERO_2));
            }
            int intVal;
            if (!int.TryParse(strVal, out intVal)) {
                // 異常値（数値変換不可）
                return defaultRetStr;
            }
            if (intVal < 0) {
                // 異常値（マイナス）
                return defaultRetStr;
            }
            if (999 < intVal) {
                // 異常値（時間変換不可）
                return defaultRetStr;
            }
            return string.Concat(intVal.ToString(), CommonLiteral._LITERAL_COLON, decimal.Zero.ToString(FormatConst._FORMAT_PRE_ZERO_2));
        }
    }
}
