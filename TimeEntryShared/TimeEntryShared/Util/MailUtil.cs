﻿using System;
using System.IO;
using System.Configuration;
using System.Net.Mail;

namespace TimeEntryShared.Util {
    public class MailUtil {
        public string _MailFromAddress = string.Empty;
        public string _MailToAddress = string.Empty;
        public string _SmtpHost = string.Empty;
        public int _SmtpPort = 0;
        private const string _CONFIG_FILE_NAME = "MailSettings.config";
        private string _SUBJECT_ERR            = "[{0}] システムエラー";
        private const string _BODY_ERR         = "エラーが発生しました。";
        private const char _MARK_COMMA         = ',';

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public MailUtil() {
            string configPath = Path.Combine(Environment.CurrentDirectory, _CONFIG_FILE_NAME);
            var exeFileMap = new ExeConfigurationFileMap { ExeConfigFilename = configPath };
            var config = ConfigurationManager.OpenMappedExeConfiguration(exeFileMap, ConfigurationUserLevel.None);
            _MailFromAddress = config.AppSettings.Settings["MailFromAddress"].Value.ToString();
            _MailToAddress   = config.AppSettings.Settings["MailToAddress"].Value.ToString();
            _SmtpHost        = config.AppSettings.Settings["SmtpHost"].Value.ToString();
            _SmtpPort        = int.Parse(config.AppSettings.Settings["SmtpPort"].Value.ToString());
        }
        /// <summary>
        /// エラーメール送信
        /// </summary>
        /// <param name="errMsg"></param>
        public void SendDefaultErrMail(string errMsg) {
            // 件名、本文の編集
            string programID = System.Reflection.Assembly.GetEntryAssembly().GetName().Name;
            string mailSubject = string.Format(_SUBJECT_ERR, programID);
            string mailBody = string.Concat(_BODY_ERR, Environment.NewLine, Environment.NewLine, errMsg);

            // メール送信
            SendMail(mailSubject, mailBody, string.Empty);
        }

        /// <summary>
        /// メール送信
        /// </summary>
        /// <param name="mailSubject"></param>
        /// <param name="mailBody"></param>
        /// <param name="tempFiles"></param>
        public void SendMail(string mailSubject, string mailBody, string tempFiles = null) {
            using (var sc = new SmtpClient()) {
                // SMTPの設定
                sc.Host = _SmtpHost;
                sc.Port = _SmtpPort;
                sc.DeliveryMethod = SmtpDeliveryMethod.Network;

                using (var mm = new MailMessage()) {

                    // FROMアドレスの設定
                    mm.From = new MailAddress(_MailFromAddress);

                    // TOアドレスの設定（カンマ区切り）
                    foreach (string address in _MailToAddress.Split(_MARK_COMMA)) {
                        if (address != string.Empty) {
                            mm.To.Add(new MailAddress(address));
                        }
                    }
                    // 件名の設定
                    mm.Subject = mailSubject;

                    // 本文の設定
                    mm.Body = mailBody;

                    // 添付ファイルの設定
                    foreach (string file in tempFiles.Split(_MARK_COMMA)) {
                        if (file != string.Empty) {
                            var at = new Attachment(file);
                            mm.Attachments.Add(at);
                        }
                    }
                    // メール送信
                    sc.Send(mm);
                }
            }
        }
    }
}
