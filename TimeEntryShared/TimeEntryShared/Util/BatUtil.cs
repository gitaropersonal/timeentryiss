﻿using System;
using System.Diagnostics;
using System.IO;
using TimeEntryShared.Const;
using TimeEntryShared.Dto;

namespace TimeEntryShared.Util {
    public static class BatUtil {
        /// <summary>
        /// バッチ起動（ユーザ情報JSONファイル出力）
        /// </summary>
        public static void ExecBat_OutputUserInfosJs() {
            string batPath = Path.Combine(Environment.CurrentDirectory, BatConst._BAT_NAME_API0001);

            var process = new Process();
            try {
                var pInfo = new ProcessStartInfo();
                pInfo.FileName = batPath;
                pInfo.CreateNoWindow = true;
                pInfo.UseShellExecute = false;
                pInfo.RedirectStandardInput = true;
                process = Process.Start(pInfo);
                process.WaitForExit();
            } catch (Exception e) {
                throw e;
            } finally {
                process.Close();
                process.Dispose();
            }
        }
        /// <summary>
        /// バッチ起動（実績工数CSV出力）
        /// </summary>
        /// <param name="ttInfo"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public static void ExecBat_OutputTimeEntryCsv(UserInfoDto ttInfo, string startDate, string endDate) {
            string batPath = Path.Combine(Environment.CurrentDirectory, BatConst._BAT_NAME_API0002);

            var process = new Process();
            try {
                // パラメータ作成
                string arg = string.Concat(CommonLiteral._LITERAL_SPACE_HALF, ttInfo.UserId);
                arg += string.Concat(CommonLiteral._LITERAL_SPACE_HALF, startDate);
                arg += string.Concat(CommonLiteral._LITERAL_SPACE_HALF, endDate);

                var pInfo = new ProcessStartInfo();
                pInfo.FileName = batPath;
                pInfo.Arguments = arg;
                pInfo.CreateNoWindow = true;
                pInfo.UseShellExecute = false;
                pInfo.RedirectStandardInput = true;
                process = Process.Start(pInfo);
                process.WaitForExit();

            } catch (Exception e) {
                throw e;
            } finally {
                process.Close();
                process.Dispose();
            }
        }
        /// <summary>
        /// バッチ起動（工数分析）
        /// </summary>
        /// <param name="UserNames"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public static void ExecBat_WorkTimeAnalysis(string UserNames, string startDate, string endDate) {
            string batPath = Path.Combine(Environment.CurrentDirectory, BatConst._BAT_NAME_API0005);

            var process = new Process();
            try {
                // パラメータ作成
                string arg = string.Concat(CommonLiteral._LITERAL_SPACE_HALF, startDate);
                arg += string.Concat(CommonLiteral._LITERAL_SPACE_HALF, endDate);

                var pInfo = new ProcessStartInfo();
                pInfo.FileName = batPath;
                pInfo.Arguments = arg;
                pInfo.CreateNoWindow = true;
                pInfo.UseShellExecute = false;
                pInfo.RedirectStandardInput = true;
                process = Process.Start(pInfo);
                process.WaitForExit();

                string msg = string.Format(WorkTimeAnalysisMessages._MSG_OUTPUT_INFO, UserNames, startDate, endDate);
                LogAndConsoleUtil.ShowLogAndConsoleInfo(msg);

            } catch (Exception e) {
                throw e;
            } finally {
                process.Close();
                process.Dispose();
            }
        }
        /// <summary>
        /// バッチ起動（プロジェクトのフィールド一覧取得）
        /// </summary>
        /// <param name="ProjectId"></param>
        public static void ExecBat_OutputProjectItems(string ProjectId) {
            string batPath = Path.Combine(Environment.CurrentDirectory, BatConst._BAT_NAME_API0003);

            var process = new Process();
            try {
                // パラメータ作成
                string arg = string.Concat(CommonLiteral._LITERAL_SPACE_HALF, ProjectId);

                var pInfo = new ProcessStartInfo();
                pInfo.FileName = batPath;
                pInfo.Arguments = arg;
                pInfo.CreateNoWindow = true;
                pInfo.UseShellExecute = false;
                pInfo.RedirectStandardInput = true;
                process = Process.Start(pInfo);
                process.WaitForExit();

                string msg = string.Format(WorkTimeAnalysisMessages._MSG_OUTPUT_WORKITEMS, ProjectId);
                LogAndConsoleUtil.ShowLogAndConsoleInfo(msg);

            } catch (Exception e) {
                throw e;
            } finally {
                process.Close();
                process.Dispose();
            }
        }
        /// <summary>
        /// バッチ起動（ワークアイテム一覧取得）
        /// </summary>
        /// <param name="WorkItemId"></param>
        /// <param name="UserId"></param>
        /// <param name="PlannedTime"></param>
        public static void ExecBat_OutputWorkItems(string WorkItemId) {
            string batPath = Path.Combine(Environment.CurrentDirectory, BatConst._BAT_NAME_API0004);

            var process = new Process();
            try {
                // パラメータ作成
                string arg = string.Concat(CommonLiteral._LITERAL_SPACE_HALF, WorkItemId);

                var pInfo = new ProcessStartInfo();
                pInfo.FileName = batPath;
                pInfo.Arguments = arg;
                pInfo.CreateNoWindow = true;
                pInfo.UseShellExecute = false;
                pInfo.RedirectStandardInput = true;
                process = Process.Start(pInfo);
                process.WaitForExit();

                string msg = string.Format(WorkTimeAnalysisMessages._MSG_OUTPUT_WORKITEMS, WorkItemId);
                LogAndConsoleUtil.ShowLogAndConsoleInfo(msg);

            } catch (Exception e) {
                throw e;
            } finally {
                process.Close();
                process.Dispose();
            }
        }
        /// <summary>
        /// バッチ起動（計画工数更新）
        /// </summary>
        /// <param name="ProjectId"></param>
        /// <param name="UserId"></param>
        /// <param name="PlannedTime"></param>
        public static void ExecBat_UpdatePlannedTime(string WorkItemId, string UserId, decimal PlannedTime) {
            string batPath = Path.Combine(Environment.CurrentDirectory, CommonLiteral._FOLDER_NAME_BAT);
            batPath = Path.Combine(batPath, BatConst._FOLDER_NAME_API0006);
            batPath = Path.Combine(batPath, BatConst._BAT_NAME_API0006);

            var process = new Process();
            try {
                // パラメータ作成
                string arg = string.Concat(CommonLiteral._LITERAL_SPACE_HALF, WorkItemId);
                arg += string.Concat(CommonLiteral._LITERAL_SPACE_HALF, UserId);
                arg += string.Concat(CommonLiteral._LITERAL_SPACE_HALF, PlannedTime);

                var pInfo = new ProcessStartInfo();
                pInfo.FileName = batPath;
                pInfo.Arguments = arg;
                pInfo.CreateNoWindow = true;
                pInfo.UseShellExecute = false;
                pInfo.RedirectStandardInput = true;
                //process.StartInfo.Verb = "RunAs";
                //process.StartInfo.UseShellExecute = true;
                process = Process.Start(pInfo);
                process.WaitForExit();

                string msg = string.Format(WorkTimeAnalysisMessages._MSG_UPDATE_PLANNED_TIME, UserId, WorkItemId, PlannedTime);
                LogAndConsoleUtil.ShowLogAndConsoleInfo(msg);

            } catch (Exception e) {
                throw e;
            } finally {
                process.Close();
                process.Dispose();
            }
        }
    }
}
