﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeEntryShared.Util {
    public static class EditTimeUtil {
        /// <summary>
        /// 時刻切上
        /// </summary>
        /// <param name="input"></param>
        /// <param name="interval"></param>
        /// <returns></returns>
        public static DateTime RoundUp(DateTime input, TimeSpan interval) {
            return new DateTime(((input.Ticks + interval.Ticks - 1) / interval.Ticks) * interval.Ticks, input.Kind);
        }
        /// <summary>
        /// 時刻切捨
        /// </summary>
        /// <param name="input"></param>
        /// <param name="interval"></param>
        /// <returns></returns>
        public static DateTime RoundDown(DateTime input, TimeSpan interval) {
            return new DateTime((((input.Ticks + interval.Ticks) / interval.Ticks) - 1) * interval.Ticks, input.Kind);
        }
    }
}
