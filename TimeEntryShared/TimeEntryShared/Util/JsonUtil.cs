﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using Newtonsoft.Json;
using TimeEntryShared.Const;
using TimeEntryShared.Entity;

namespace TimeEntryShared.Util {
    /// <summary>
    /// JSONオブジェクトを指定のCSVオブジェクトに変換
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class JsonUtil<T> {
        public T JsonToCsvConverter(string jsonPath) {
            using (FileStream fs = new FileStream(jsonPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)) {
                using (var sr = new StreamReader(fs, Encoding.GetEncoding(EncodingConst._ENCODING_UTF8))) {

                    // JSONテキスト読取
                    string json = EditJsonStr(sr.ReadToEnd());

                    // オブジェクトにして返す
                    return JsonConvert.DeserializeObject<T>(json);
                }
            }
        }
        /// <summary>
        /// JSONデータ読取（工数分析）
        /// </summary>
        /// <param name="jsonPath"></param>
        /// <returns></returns>
        public List<WorkTimeAnalysisEntity> JsonConverter_WorkTimeAnalysis(string jsonPath) {
            using (var sr = new StreamReader(jsonPath, Encoding.GetEncoding(EncodingConst._ENCODING_UTF8))) {
                string json = EditJsonStr(sr.ReadToEnd());
                dynamic o = JsonConvert.DeserializeObject(json);
                var ret = new List<WorkTimeAnalysisEntity>();

                // ファクトデータを読取
                foreach (var data in o.factData) {
                    var entity = ConvertJsonColToEntity(data);
                    ret.Add(entity);
                }
                return ret;
            }
        }
        /// <summary>
        /// カラム読取（工数分析）
        /// </summary>
        /// <param name="data"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        private static WorkTimeAnalysisEntity ConvertJsonColToEntity(dynamic data) {
            var ret = new WorkTimeAnalysisEntity();

            // レコードを1行ずつ読取
            var cols = new List<string>();
            foreach (var row in data.values) {
                foreach (var col in row) {
                    cols.Add(col.ToString());
                }
            }
            ret.date        = cols[0];
            ret.projectId   = cols[1];
            ret.projectName = cols[2];
            ret.userId      = cols[3];
            ret.userName    = cols[4];
            ret.plannedTime = cols[5];
            ret.actualTime  = cols[6];
            ret.plannedCost = cols[7];
            ret.actualCost  = cols[8];
            return ret;
        }
        /// <summary>
        /// JSONデータ読取（ワークアイテム）
        /// </summary>
        /// <param name="jsonPath"></param>
        /// <returns></returns>
        public List<OutputWorkItemEntity> JsonConverter_WorkItem(string jsonPath) {
            using (var sr = new StreamReader(jsonPath, Encoding.GetEncoding(EncodingConst._ENCODING_UTF8))) {
                string jsTxt = EditJsonStr(sr.ReadToEnd());
                jsTxt.Trim('[').Trim(']');
                var spl = new string[] { "{\"fields\":" };
                var split = jsTxt.Split(spl, StringSplitOptions.None);
                int idx = 0;
                var ret = new List<OutputWorkItemEntity>();
                string parentId = string.Empty;
                foreach (var js in split) {
                    if (1 < idx) {
                        bool isHavingSub = false;
                        string JsDash = js;
                        if (js.Contains("SubItems")){
                            JsDash = js.Substring(0, js.LastIndexOf(',')) + "}";
                            isHavingSub = true;
                        }
                        string newJs = JsDash.Substring(0, JsDash.IndexOf('}') + 1);
                        var data = JsonConvert.DeserializeObject<OutputWorkItemEntity>(newJs);
                        if (data != null) {
                            if (isHavingSub) {
                                parentId = data.Id;
                                data.ParentId = string.Empty;
                            } else {
                                data.ParentId = parentId;
                            }
                            ret.Add(data);
                        }
                    }
                    idx++;
                }
                return ret;
            }
        }
        /// <summary>
        /// JSONデータ読取（プロジェクト）
        /// </summary>
        /// <param name="jsonPath"></param>
        /// <returns></returns>
        public List<ProjectItemsEntity> JsonConverter_Project(string jsonPath) {
            using (var sr = new StreamReader(jsonPath, Encoding.GetEncoding(EncodingConst._ENCODING_UTF8))) {
                string json = EditJsonStr(sr.ReadToEnd());
                dynamic o = JsonConvert.DeserializeObject(json);
                var ret = new List<ProjectItemsEntity>();

                // ファクトデータを読取
                foreach (var data in o) {
                    var entity = new ProjectItemsEntity();

                    // 不要な文字列を削除
                    string jsTxt = json.Trim('[').Trim(']');
                    jsTxt = jsTxt.Replace("\"metadata\":{},", string.Empty);
                    entity = JsonConvert.DeserializeObject<ProjectItemsEntity>(jsTxt);
                    ret.Add(entity);
                }
                return ret;
            }
        }
        /// <summary>
        /// JSON文字列整形
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        private string EditJsonStr(string json) {
            json = json.Replace(@"\""", @"""");
            if (json[0] == '"') {
                json = json.Remove(0, 1);
            }
            if (json[json.Length - 1] == '"') {
                json = json.Remove(json.Length - 1, 1);
            }
            return json;
        }
    }
}
