﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using TimeEntryShared.Const;
using TimeEntryShared.Dto;
using TimeEntryShared.Entity;

namespace TimeEntryShared.Util {
    public static class TimeTrackerUtil {
        /// <summary>
        /// 実績CSVの名称取得
        /// </summary>
        /// <param name="ttInfo"></param>
        /// <returns></returns>
        public static string GetTimeEntryCsvName(UserInfoDto ttInfo) {
            string ret = string.Format(BatConst._CSV_NAME_TIME_ENTRY, string.Concat(Environment.MachineName, ttInfo.UserId));
            return ret;
        }
        /// <summary>
        /// TimeTrackerユーザ情報ロード
        /// </summary>
        /// <returns></returns>
        public static List<UserInfoDto> LoadTimeTrackerUserInfo() {
            var ret = new List<UserInfoDto>();
            // ユーザ情報一覧出力バッチ起動
            BatUtil.ExecBat_OutputUserInfosJs();

            // JSONパス取得
            string currJsPath = Path.Combine(Environment.CurrentDirectory, string.Format(BatConst._TXT_NAME_JS_USER_INFOS, Environment.MachineName));

            // JSONファイル読み込み
            var json = new JsonUtil<JsUserInfoRoot>();
            var root = json.JsonToCsvConverter(currJsPath);
            CommonUtil.DeleteFile(currJsPath);
            if (root == null) {
                return ret;
            }
            foreach (var entitiy in root.Data) {
                if (entitiy.loginName == BatConst._LOGIN_ID_ADMIN || entitiy.loginName == BatConst._LOGIN_ID_KANRI) {
                    // 不要データならスルー
                    continue;
                }
                var ttInfo = new UserInfoDto() {
                    LoginName = entitiy.loginName,
                    Pass = BatConst._LOGIN_PASS,
                    UserId = entitiy.id,
                    UserName = entitiy.name,
                    OrganizationName = entitiy.organizationName,
                    UnitCost = entitiy.unitCost,
                };
                ret.Add(ttInfo);
            }
            return ret;
        }
        /// <summary>
        /// TimeTracker実績CSV出力
        /// </summary>
        /// <param name="configInfo"></param>
        /// <param name="EmploeeInfos"></param>
        /// <param name="ttInfos"></param>
        /// <param name="tgtEmploeeNum"></param>
        public static void OutputTimeEntry(TimeEntryConfigEntity configInfo, List<MstEmploeeEntity> EmploeeInfos, List<UserInfoDto> ttInfos, string tgtEmploeeNum = "") {
            if (!string.IsNullOrEmpty(tgtEmploeeNum)) {
                var emp = EmploeeInfos.Where(n => n.EmploeeNum == tgtEmploeeNum).FirstOrDefault();
                var tgtTTinfo = ttInfos.Where(n => CommonUtil.CutSpace(n.UserName) == CommonUtil.CutSpace(emp.Name)).FirstOrDefault();
                ttInfos = new List<UserInfoDto>();
                if (tgtTTinfo != null) {
                    ttInfos.Add(tgtTTinfo);
                }
            }
            // CSV出力バッチ起動
            ttInfos.ForEach(ttInfo => BatUtil.ExecBat_OutputTimeEntryCsv(ttInfo, configInfo.SumStartDate, configInfo.SumEndDate));

            LogAndConsoleUtil.ShowLogAndConsoleInfo(SharedMessages._MSG_OUTPUT_CSV_END);
        }
        /// <summary>
        /// TimeTracker実績CSV読取処理
        /// </summary>
        /// <param name="ConfigInfo"></param>
        /// <param name="csvPath"></param>
        /// <returns></returns>
        public static List<EditTimeEntryDto> LoadTimeEntryCsv(TimeEntryConfigEntity ConfigInfo, string csvPath) {
            bool isFirstLine = true;
            var ret = new List<EditTimeEntryDto>();
            using (var sr = new StreamReader(csvPath, Encoding.GetEncoding(EncodingConst._ENCODING_SJIS))) {
                while (!sr.EndOfStream) {
                    var line = sr.ReadLine();
                    var values = line.Split(CommonLiteral._CSV_SEPALATOR);
                    if (isFirstLine) {
                        // 列インデックスの決定
                        for (int i = 0; i < values.Length; i++) {
                            var val = values[i];
                            if (val == TimeEntryConfigConst._CSV_TIME_ENTRY_COS_USER_NAME) {
                                TimeEntryConfigConst._COL_INDEX_USER_NAME = i;
                            }
                            if (val == TimeEntryConfigConst._CSV_TIME_ENTRY_COS_PROJECT_CODE) {
                                TimeEntryConfigConst._COL_INDEX_PROJECT_CODE = i;
                            }
                            if (val == TimeEntryConfigConst._CSV_TIME_ENTRY_COS_PROJECT_NAME) {
                                TimeEntryConfigConst._COL_INDEX_PROJECT_NAME = i;
                            }
                            if (val == TimeEntryConfigConst._CSV_TIME_ENTRY_COS_WORK_DATE) {
                                TimeEntryConfigConst._COL_INDEX_WORK_DATE = i;
                            }
                            if (val == TimeEntryConfigConst._CSV_TIME_ENTRY_COS_MAN_HOUR) {
                                TimeEntryConfigConst._COL_INDEX_MAN_HOUR = i;
                            }
                        }
                        // １行目はヘッダーなのでスルー
                        isFirstLine = false;
                        continue;
                    }
                    string strManHour = values[TimeEntryConfigConst._COL_INDEX_MAN_HOUR];       // 工数
                    decimal manHour = 0;
                    if (decimal.TryParse(strManHour, out manHour)) {
                        var dto = new EditTimeEntryDto {
                            UserName = values[TimeEntryConfigConst._COL_INDEX_USER_NAME],       // ユーザー名
                            ProjectName = values[TimeEntryConfigConst._COL_INDEX_PROJECT_NAME], // プロジェクト名
                            ProjectCode = values[TimeEntryConfigConst._COL_INDEX_PROJECT_CODE], // プロジェクトコード
                            WorkDate = values[TimeEntryConfigConst._COL_INDEX_WORK_DATE],       // 作業日時
                            ManHour = manHour,
                        };
                        dto.ManHourH = (int)Math.Truncate(manHour);
                        dto.ManHourM = GetManHourM(manHour);
                        dto.ProjectName = dto.ProjectName.TrimEnd();

                        // CSVデータを追加
                        ret.Add(dto);
                    }
                }
            }
            CommonUtil.DeleteFile(csvPath);
            return ret;
        }
        /// <summary>
        /// TimeTracker実績CSVソート
        /// </summary>
        /// <param name="ret"></param>
        /// <returns></returns>
        public static List<EditTimeEntryDto> SortTimeEntryCsv(List<EditTimeEntryDto> ret) {
            return ret.OrderBy(n => n.UserName)
                      .ThenBy(n => n.WorkDate).ToList();
        }
        /// <summary>
        /// 工数（分）取得
        /// </summary>
        /// <param name="manHour"></param>
        /// <returns></returns>
        private static int GetManHourM(decimal manHour) {
            manHour -= (int)Math.Truncate(manHour);
            return (int)(manHour * TimeConst._MINUTE_AN_HOUR);
        }
    }
}
