﻿using System;
using log4net;

namespace TimeEntryShared.Util {
    public static class LogAndConsoleUtil {
        private static readonly ILog LOG = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private const string _ERR_TITLE                    = "エラー内容           ：{0}";
        private const string _FROMAT_CONTENT_HOST          = "ホスト名               ：{0}";
        private const string _FROMAT_CONTENT_IP            = "IPアドレス           ：{0}";
        private const string _MSG_CONSOLE_FROMAT           = "{0}  {1}";
        private const string _FORMAT_DATETIME_YYMMDDHHmmss = "yyyy/MM/dd HH:mm:ss";
        private const string _MSG_STRAT_END_LINE           = "===========================================";

        /// <summary>
        /// 開始終了線のタイプ
        /// </summary>
        public enum StartEndLineType {
            None = 0,
            Start = 1,
            End = 2
        }

        /// <summary>
        /// ログとコンソールにメッセージ表示（Info）
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="lineType"></param>
        public static void ShowLogAndConsoleInfo(string msg, StartEndLineType lineType = StartEndLineType.None) {

            // 開始線
            if (lineType == StartEndLineType.Start) {
                LOG.Info(_MSG_STRAT_END_LINE);
            }
            // ログ出力
            LOG.Info(msg);

            // 終了線
            if (lineType == StartEndLineType.End) {
                LOG.Info(_MSG_STRAT_END_LINE);
            }
            // コンソール出力
            msg = string.Format(_MSG_CONSOLE_FROMAT, DateTime.Now.ToString(_FORMAT_DATETIME_YYMMDDHHmmss), msg);
            Console.Write(msg);
            Console.WriteLine();
        }
        /// <summary>
        /// ログとコンソールにメッセージ表示（Error）
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="stackTrace"></param>
        /// <param name="sendMailFlg"></param>
        public static void ShowLogAndConsoleErr(string msg, string stackTrace, bool sendMailFlg = true) {

            // メッセージ作成
            string eMessage = Environment.NewLine
                            + Environment.NewLine + msg
                            + Environment.NewLine
                            + Environment.NewLine
                            + Environment.NewLine + stackTrace
                            + Environment.NewLine;

            string host = Environment.NewLine + string.Format(_FROMAT_CONTENT_HOST, Environment.MachineName);
            string ipa  = Environment.NewLine + string.Format(_FROMAT_CONTENT_IP, NetworkUtil.GetIpAddress());
            string err  = Environment.NewLine + string.Format(_ERR_TITLE, eMessage);
            eMessage = string.Concat(host, ipa, err);

            // ログ出力
            LOG.Error(eMessage);

            // コンソール出力
            Console.Write(string.Format(_MSG_CONSOLE_FROMAT, DateTime.Now.ToString(_FORMAT_DATETIME_YYMMDDHHmmss), eMessage));
            Console.WriteLine();

            // メール送付
            if (sendMailFlg) {
                try {
                    var mailService = new MailUtil();
                    mailService.SendDefaultErrMail(eMessage);
                } catch {
                }
            }
        }
    }
}
