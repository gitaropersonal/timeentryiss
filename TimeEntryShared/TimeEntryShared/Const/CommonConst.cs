﻿namespace TimeEntryShared.Const {
    /// <summary>
    /// 特定文字列（共通）
    /// </summary>
    public static class CommonLiteral {
        public const char _CSV_SEPALATOR          = ',';
        public const char _CHAR_DOT               = '.';
        public const char _CHAR_HYPHEN             = '-';
        public const char _CHAR_PAD_ZERO          = '0';
        public const string _CSV_SEPALATOR_STR    = ",";
        public const string _LITERAL_COLON        = ":";
        public const string _LITERAL_SPACE_ALL    = "　";
        public const string _LITERAL_SPACE_HALF   = " ";
        public const string _LITERAL_UNEDER_BAR   = "_";
        public const string _LITERAL_HYPHEN       = "-";
        public const string _MARK_PRE_MONTH       = "&df=";
        public const string _MARK_STAR            = "★";
        public const string _LITERAL_YEN_MARK     = @"\";
        public const string _FOLDER_NAME_EXCEL    = "Excel";
        public const string _FOLDER_NAME_BAT      = "Bat";
        public const string _LITERAL_PUNCTUATION  = "、";
        public const string _LITERAL_DOT          = ".";
        public const string _DEFAULT_HHMM         = "0:00";
    }
}
