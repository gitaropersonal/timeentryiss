﻿namespace TimeEntryShared.Const {
    /// <summary>
    /// バッチ関連
    /// </summary>
    public static class BatConst {
        public const string _LOGIN_ID_ADMIN = "administrator";
        public const string _LOGIN_ID_KANRI = "kanriuser";

        public const string _BAT_NAME_OUTPUT_USER_INFO_64       = "outputUserInfoJs64.bat";
        public const string _BAT_NAME_OUTPUT_TIME_ENTRY_64      = "outputTimeEntryCsv64.bat";
        public const string _BAT_NAME_OUTPUT_TIME_ENTRY_32      = "outputTimeEntryCsv32.bat";
        public const string _BAT_NAME_OUTPUT_WT_ANALYSIS_64     = "outputWorkTimeAnalysis64.bat";
        public const string _BAT_NAME_OUTPUT_WORK_ITEMS_64      = "outputWorkItem64.bat";
        public const string _BAT_NAME_OUTPUT_PROJECT_64         = "outputProject64.bat";
        public const string _BAT_NAME_UPDATE_PLANNED_TIME_64    = "updatePlannedTime64.bat";
        public const string _LOGIN_PASS                         = "Isstokyo155";
        public const string _CSV_NAME_TIME_ENTRY                = "timeEntry{0}.csv";
        public const string _TXT_NAME_JS_USER_INFOS             = "JsUserInfo{0}.txt";
        public const string _TXT_NAME_WORKTIME_ITEMS            = "workTimeAnalysis{0}.txt";
        public const string _TXT_NAME_JS_WORK_ITEMS             = "jsWorkItems{0}.txt";
        public const string _TXT_NAME_JS_PROJECT_ITEMS          = "jsProject{0}.txt";
        public const string _OUTPUT_TGT_ALL_TT_USERS            = "TimeTracker登録者全員";

        public const string _BAT_NAME_API0001                   = "API0001_OutputUserInfos.exe";
        public const string _BAT_NAME_API0002                   = "API0002_OutputTimeEntry.exe";
        public const string _BAT_NAME_API0003                   = "API0003_OutputProject.exe";
        public const string _BAT_NAME_API0004                   = "API0004_OutputWorkItem.exe";
        public const string _BAT_NAME_API0005                   = "API0005_OutputWorkTimeAnalysis.exe";
        public const string _BAT_NAME_API0006                   = "API0006_UpdatePlannedTime.exe";
        public const string _FOLDER_NAME_API0006                = "API0006_UpdatePlannedTime";
    }
}
