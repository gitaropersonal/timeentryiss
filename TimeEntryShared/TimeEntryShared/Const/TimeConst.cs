﻿namespace TimeEntryShared.Const {
    /// <summary>
    /// 時間
    /// </summary>
    public static class TimeConst {
        public enum WorkShiftType {
            UNKNOWN = 0,
            A = 1,
            B = 3,
            C = 5,
        }
        public enum RestType {
            NONE = 0,
            PAID = 1,
            AM = 3,
            PM = 5,
            ONE_QUARTER = 14,
            TWO_QUARTER = 24,
            THREE_QUARTER = 34,
        }
        public const int _DEFAULT_WORK_TIME_PER_DAY    = 8;
        public const int _DEFAULT_NOON_REST_START_HH   = 12;
        public const int _DEFAULT_NOON_REST_END_HH     = 13;
        public const int _DEFAULT_WORK_START_TYPE_A_HH = 8;
        public const int _DEFAULT_WORK_START_TYPE_B_HH = 9;
        public const int _DEFAULT_WORK_START_TYPE_C_HH = 10;
        public const int _DEFAULT_WORK_END_TYPE_A_HH   = 17;
        public const int _DEFAULT_WORK_END_TYPE_B_HH   = 18;
        public const int _DEFAULT_WORK_END_TYPE_C_HH   = 19;
        public const int _NIGHT_RESTSTART_HH           = 22;
        public const int _MAX_DAY_COUNT_MONTH          = 31;
        public const int _MINUTE_AN_HOUR               = 60;
        public const int _HOURS_A_DAY                  = 24;
        public const int _DEFAULT_WORK_MONTH_TIME      = 160;
    }
}
