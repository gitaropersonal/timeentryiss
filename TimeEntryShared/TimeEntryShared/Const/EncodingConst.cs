﻿namespace TimeEntryShared.Const {
    /// <summary>
    /// エンコーディング
    /// </summary>
    public static class EncodingConst {
        public const string _ENCODING_SJIS = "Shift_JIS";
        public const string _ENCODING_UTF8 = "UTF-8";
    }
}
