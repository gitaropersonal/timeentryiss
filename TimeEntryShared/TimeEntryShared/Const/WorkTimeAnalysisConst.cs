﻿namespace TimeEntryShared.Const {
    public static class WtAnalysisConst {
        public enum ExcelRowType {
            PROJECT_HEADER = 1,
            ORGANIZATION_HEADER = 5,
            MEMBER_HEADER = 10,
            BODY_ROW = 100,
            SUMMARY = 1000,
        }
        public enum SumType {
            PROJECT = 1,
            MEMBER = 2,
            COST = 3,
        }
        public enum SheetType {
            PROJECT = 0,
            MEMBER = 1,
            LOOKS = 2,
            COST = 3,
        }
        // ヘッダ見出し
        public const string _HEADER_COL_ORGANIZATION  = "所属";
        public const string _HEADER_COL_USER_NAME     = "要員名";
        public const string _HEADER_COL_CHARGE_NO     = "チャージ№";
        public const string _HEADER_COL_WORK_NAME     = "プロジェクト名";
        public const string _HEADER_COL_TIME          = "時間";
        public const string _HEADER_COL_WORK_TIME_H   = "h";
        public const string _HEADER_COL_WORK_TIME_M   = "m";
        public const string _HEADER_COL_WORK_TIME     = "工数";
        public const string _HEADER_COL_WORK_TIME_PCT = "割合";
        public const string _HEADER_COL_MAN_MOON      = "人月";
        public const string _HEADER_COL_PLANNED       = "計画";
        public const string _HEADER_COL_ACTUAL        = "実績";
        public const string _HEADER_COL_COST          = "コスト";
        public const string _HEADER_COL_COST_1000     = "金額";
        public const string _HEADER_FORMAT_MONTH_WORK_TIME = @"({0}h)";

        // 定数
        public const string _FILE_NAME_EXCEL          = "工数分析";
        public const string _SHEET_NAME_PIBOT_USER    = "ピボット（要員別）";
        public const string _SHEET_NAME_PIBOT_PROJECT = "ピボット（プロジェクト別）";
        public const string _SHEET_NAME_LOOKS         = "一覧";
        public const string _SHEET_NAME_SUM_COST      = "コスト総括";
        public const string _FILE_NAME_ANALYSIS_TITLE = @"{0}～{1}_工数分析{2}";
        public const string _PIBOT_TITLE              = @"{0}～{1} {2}";
        public const string _EX_XLSX                  = ".xlsx";
        public const string _OUTPUT_DATE              = @"出力日時：{0}";
        public const string _SUM_VALUE                = "合計";
        public const string _ESCAPE_PROJECT_CODE      = "***-**-**";
        public const string _FIRST_HALF               = "①";
        public const string _SECOND_HALF              = "②";
        public const string _PATH_DEFAULT_FOLDER_PATH = @"C:\Windows";
        public const string _UNIT_HEADER_WORK_TIME    = "（人月）";
        public const string _UNIT_HEADER_COST_1000    = "（千円）";

        // 数値
        public const int _MAN_HOUR_CSV_COL_COUNT      = 16;
        public const int _HEADER_ROW_NUM              = 5;
        public const int _BODY_COL_COUNT_PER_MONTH    = 12;
        public const int _BODY_COL_START_PER_MONTH    = 5;
        public const int _BODY_COL_COUNT_COST         = 4;
        public const int _BODY_COL_START_COST         = 3;
        public const int _HALF_DIVIDE_DAY             = 15;
        public const float _BODY_FONT_SIZE            = 9;
        public const float _TITLE_FONT_SIZE           = 16;
        public const double _BODY_WORK_TIME_HM_WIDTH  = 3.63;
        public const double _BODY_PER_MONTH_WIDTH     = 6.5;
    }
    /// <summary>
    /// メッセージ
    /// </summary>
    public static class WorkTimeAnalysisMessages {
        public const string _MSG_OPERATION_START     = "工数分析Excel出力              ：開始";
        public const string _MSG_OPERATION_END       = "工数分析Excel出力              ：終了";
        public const string _MSG_OUTPUT_INFO         = "    出力内容                   ：ユーザ：{0} 集計期間：{1}～{2}";
        public const string _MSG_OUTPUT_EXCEL_PATH   = "    Excel出力先                ：{0}";
        public const string _MSG_UPDATE_PLANNED_TIME = "    計画工数更新               ：ユーザ：{0} ワークアイテムID：{1} 計画工数：{2}";
        public const string _MSG_OUTPUT_WORKITEMS    = "    ワークアイテム一覧         ：ID：{0}";
    }
}
