﻿namespace TimeEntryShared.Const {
    public static class WebDriverConst {
        public const string _URL_LATEST_CHROME_DRIVER  = "https://chromedriver.storage.googleapis.com/LATEST_RELEASE_";
        public const string _DRIVER_NAME_CHROME        = "chromedriver.exe";
        public const string _BIT_TYPE64                = "X64";
    }
    public static class WebDriverMessages {
        public const string _LATEST_CHROME_DRIVER_VER  = "ChromeDriverバージョン         ：{0}";
        public const string _LATEST_CHROME_BROWSER_VER = "Chromeブラウザバージョン       ：{0}";
        public const string _UPD_CHROME_DRIVER_FINISH  = "ChromeDriver更新";
        public const string _UPD_BY_WD_FAILED          = "WebDriverManagerによる更新失敗 {0}{1}";
    }
}
