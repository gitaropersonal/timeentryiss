﻿namespace TimeEntryShared.Const {
    /// <summary>
    /// 定数定義クラス（勤務表）
    /// </summary>
    public static class KinmuhyoConst {
        
        public enum UserType {
            NONE = 0,
            TIME_TRACKER = 1,
            DEFAULT_WORK = 3,
        }
        public enum WBShuttaiFlag {
            NORMAL = 0,
            REST = 8,
        }
        public const string _CELL_NAME_NAME                  = "I5";
        public const string _SHEET_NAME_KINMUHYO             = "作業報告書";
        public const string _CHARGE_NO_LAST_XXX              = "XXX";
        public const string _CHARGE_NO_SJK_TOP3              = "SJK";
        public const string _CHARGE_NO_SJK_LAST3             = "-00";
        public const string _CHARGE_NO_TOP_YOTEI             = "予";

        // 勤務表Excelセル
        public const string _COL_WEEK_DAY                    = "C";
        public const string _COL_HOLI_DAY                    = "D";
        public const string _COL_WORK_STATUS                 = "E";
        public const string _COL_AM_REST                     = "F";
        public const string _COL_PM_REST                     = "G";
        public const string _COL_SHUKKIN_HH                  = "H";
        public const string _COL_SHUKKIN_MM                  = "I";
        public const string _COL_TAIKIN_HH                   = "J";
        public const string _COL_TAIKIN_MM                   = "K";
        public const string _COL_JITSUDOU_HH                 = "L";
        public const string _COL_JITSUDOU_MM                 = "M";
        public const string _COL_KYUKEI_HH                   = "N";
        public const string _COL_KYUKEI_MM                   = "O";
        public const string _COL_ZANGYO_HH                   = "P";
        public const string _COL_ZANGYO_MM                   = "Q";
        public const string _COL_WARIMASHI_HH                = "T";
        public const string _COL_WARIMASHI_MM                = "U";
        public const string _COL_SHINYA_HH                   = "V";
        public const string _COL_SHINYA_MM                   = "W";
        public const string _COL_KADO_HH                     = "X";
        public const string _COL_KADO_MM                     = "Y";
        public const string _COL_MST_CHARGE_NO               = "AO";
        public const string _COL_MST_NAME                    = "AR";
        public const string _COL_MST_HIDDEN_WORK_TIME        = "BD";
        public const int _COL_IDX_FIRST_WORK_NAME            = 26;
        public const int _COL_INTERVAL_WORK_DETAILS_WORK     = 3;
        public const int _MAX_COL_IDX_WORK_NAME              = 55;
        public const int _MAX_ROW_CNT_WORK_MASTER            = 18;
        public const int _MAX_WORK_CNT_WORK_DETAILS          = 10;
        public const int _ROW_IDX_HIDDEN_WORK_BUF            = 21;
        public const int _ROW_IDX_MST_END                    = 81;
        public const int _ROW_IDX_MST_START                  = 64;
        public const int _ROW_IDX_SHEET_HEADER               = 7;

        // 勤怠時刻転記用
        public const string _OPT_ARG_HEADLESS                = "headless";
        public const string _OPT_ARG_NO_SAND_BOX             = "no-sandbox";
        public const string _TAG_NAME_TABLE                  = "table";
        public const string _TAG_NAME_TEXT_ROW               = "tr";
        public const string _TAG_NAME_TEXT_DATA              = "td";
        public const string _MARK_REST                       = "休";
        public const string _MARK_PAID                       = "有";
        public const string _MARK_ONE_QUARTER                = "1/4";
        public const string _MARK_TWO_QUARTER                = "2/4";
        public const string _MARK_THREE_QUARTER              = "3/4";
        public const string _MARK_CONTAINS_REST              = "あり";
        public const string _FROMAR_TIME                     = "{0}:{1}";
        public const string _CHARGE_NO_REST                  = "990-99-01";
        public const string _WORK_NAME_REST                  = "休暇";
        public const char _PADDING_MARK_SPACE                = ' ';

        // 夜勤用
        public const string _MSG_END_OF_NIGHT_SHIFT          = "夜勤明け";
        public static string[] _WEEK_END_DAYS                = { "土", "日" };
        public static string _HOLIDAY_MARK                   = "祝";
        public const int _NIGHT_SHIFT_START_HH               = 21;
        public const int _NIGHT_SHIFT_END_HH                 = 9;
        public const int _NIGHT_SHIFT_KYUKEI_TIME            = 2;
        public const int _NIGHT_SHIFT_WARIMASHI_TIME         = 4;
        public const int _NIGHT_SHIFT_SHINYA_TIME            = 6;
    }
}
