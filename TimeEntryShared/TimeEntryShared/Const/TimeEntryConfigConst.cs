﻿namespace TimeEntryShared.Const {
    /// <summary>
    /// Config関連
    /// </summary>
    public static class TimeEntryConfigConst {
        public const string _CONFIG_FILE_NAME                   = "TimeEntry.config";
        public const string _CSV_NAME_MST_EMPLOEE               = "EmploeeNum.csv";
        public const string _CSV_NAME_MST_GROUP                 = "MST_GROUP.csv";
        public const string _CSV_NAME_MST_BU                    = "MST_BU.csv";
        public const string _DEFAULT_KINMUHYO_FORMAT_NAME       = @"{0}年{1}月作業時間外報告書{2}.xlsx";
        public const string _CSV_TIME_ENTRY_COS_USER_NAME       = "ユーザー名";
        public const string _CSV_TIME_ENTRY_COS_PROJECT_NAME    = "プロジェクト名";
        public const string _CSV_TIME_ENTRY_COS_PROJECT_CODE    = "プロジェクトコード";
        public const string _CSV_TIME_ENTRY_COS_WORK_DATE       = "日付";
        public const string _CSV_TIME_ENTRY_COS_MAN_HOUR        = "実績工数";
        public static int _COL_INDEX_USER_NAME                  = 0;
        public static int _COL_INDEX_PROJECT_CODE               = 0;
        public static int _COL_INDEX_PROJECT_NAME               = 0;
        public static int _COL_INDEX_WORK_DATE                  = 0;
        public static int _COL_INDEX_MAN_HOUR                   = 0;
    }
}
