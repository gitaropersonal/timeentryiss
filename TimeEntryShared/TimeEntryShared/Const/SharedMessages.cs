﻿namespace TimeEntryShared.Const {
    /// <summary>
    /// メッセージ定義クラス
    /// </summary>
    public static class SharedMessages {

        // 勤務表作成用
        public const string _MSG_OPERATION_START        = "勤務表作成処理                 ：開始";
        public const string _MSG_OPERATION_END          = "勤務表作成処理                 ：終了";
        public const string _MSG_NOT_EXIST_DEF_KIN      = "デフォルト勤務表がありません（捜索ファイル名：{0}）";
        public const string _MSG_NOT_CREATE_KIN_DAY     = "勤務表作成日でないため、処理しません";
        public const string _MSG_KINMUHYO_TGT           = "  作成対象                     ：名前：{0}";
        public const string _MSG_INIT_KINMUHYO_END      = "                               ：初期化完了";
        public const string _MSG_KINMUHYO_TGT_END       = "                               ：完了";
        public const string _MSG_OUTPUT_CSV_END         = "    timeEntry.csv出力          ：完了";
        public const string _MSG_CONTINUE_ERR           = "※エラー発生したが処理継続";
        public const string _MSG_SELECT_FOLDER          = "出力フォルダを指定してください。\r\n（何も選択しなければデスクトップに出力）";
        public const string _MSG_ERROR_NOT_EXIST_DEF    = "デフォルト勤務表が存在しません。";
        public const string _MSG_ERROR_KINMUHYO_OPEND   = "勤務表が閉じていません         ：名前：{0}";

        // 夜勤用                                       
        public const string _MSG_OPERATION_START_N      = "夜勤処理                       ：開始";
        public const string _MSG_OPERATION_END_N        = "夜勤処理                       ：終了";
        public const string _MSG_NIGHT_SHIFT_TARGET     = "  処理対象                     ：名前：{0}";
        public const string _MSG_NIGHT_SHIFT_END        = "                               ：完了";
        public const string _ERR_MSG_NO_CSV_DATAS       = "CSVデータが取得出来ていません";
    }
}
