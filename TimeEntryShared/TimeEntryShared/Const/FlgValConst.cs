﻿namespace TimeEntryShared.Const {
    public static class FlgValConst {
        public const string _STR_FLG_OFF = "0";
        public const string _STR_FLG_ON = "1";
        public const int _INT_FLG_OFF = 0;
        public const int _INT_FLG_ON = 1;
    }
}
