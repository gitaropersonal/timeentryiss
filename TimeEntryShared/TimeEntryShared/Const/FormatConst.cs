﻿using System.Collections.Generic;

namespace TimeEntryShared.Const {
    /// <summary>
    /// フォーマット
    /// </summary>
    public static class FormatConst {
        public const string _FORMAT_DATE_YYYYMMDD_SLASH       = "yyyy/MM/dd";
        public const string _FORMAT_DATE_YYYYMM_NENTSUKI      = "yyyy年MM月";
        public const string _FORMAT_DATE_YYMM_NENTSUKI        = "yy年MM月";
        public const string _FORMAT_DATE_M_TSUKI              = "M月";
        public const string _FORMAT_PRE_ZERO_2                = "0#";
        public const string _FORMAT_ZERO_2                    = "00";
        public const string _FORMAT_FLOAT_1                   = "#0.0";
        public const string _FORMAT_FLOAT_2                   = "#0.00";
        public const string _FORMAT_FLOAT_1_PCT               = "#0.0%";
        public const string _FORMAT_FLOAT_2_HUMANMOON         = "#0.00人月";
        public const string _FORMAT_FLOAT_MONEY               = "¥ ###,##0";
        public const string _FORMAT_FLOAT_MONEY_1000          = "###,##0.0";
        public const string _FORMAT_DATE_YYYYMMDDHHMMSS       = "yyyyMMddhhmmss";
        public const string _FORMAT_DATE_YYYYMMDDHHMMSS_SLASH = "yyyy/MM/dd HH:mm:ss";
        public const string _FORMAT_CMB_BU_FORMAT             = "{0}----{1}";
        public const string _FORMAT_CMB_GROUP_FORMAT          = "{0}-{1}";
    }
    /// <summary>
    /// 勤務表フォーマット
    /// </summary>
    public static class KinmuhyoFormatConst {
        public const string _FORMAT_KINMUHYO_START_DATE_20190301 = "2019-03-01";
        public const string _FORMAT_KINMUHYO_START_DATE_20200501 = "2020-05-01";
        /// <summary>
        /// 勤務表フォーマットの名称
        /// </summary>
        public static Dictionary<string, string> KinmuhyoFormatDc = new Dictionary<string, string>() {
            {_FORMAT_KINMUHYO_START_DATE_20190301 , "（2019.03版）"},
            {_FORMAT_KINMUHYO_START_DATE_20200501 , "（2020.05版）"},
        };
    }
}
