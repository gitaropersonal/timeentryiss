﻿using System;
using TimeEntryShared.Util;
using TimeEntryShared.Const;
using TimeEntryShared.Service;

namespace TEB02100_WorkTimeAnalysis {
    class Program {
        static void Main(string[] args) {
            LogAndConsoleUtil.ShowLogAndConsoleInfo(WorkTimeAnalysisMessages._MSG_OPERATION_START, LogAndConsoleUtil.StartEndLineType.Start);

            try {
                // Configロード
                var configInfo = ModelUtil.LoadConfig();

                // Serviceの実行
                var service = new WorkTimeAnalysisService();
                string sumStartDate = configInfo.SumStartDate;
                string sumEndDate = DateTime.Parse(configInfo.SumStartDate).AddMonths(3).AddDays(-1).ToString(FormatConst._FORMAT_DATE_YYYYMMDD_SLASH);
                service.Main(configInfo, sumStartDate, sumEndDate, configInfo.WtAnalysisWorkFolder);

            } catch (Exception e) {

                LogAndConsoleUtil.ShowLogAndConsoleErr(e.Message, e.StackTrace);

            } finally {

                LogAndConsoleUtil.ShowLogAndConsoleInfo(WorkTimeAnalysisMessages._MSG_OPERATION_END, LogAndConsoleUtil.StartEndLineType.End);

            }
        }
    }
}
