@echo off
setlocal
cd /d %~dp0

REM "ユーザ名:パスワード"をBase64に変換
REM 参考→ https://qiita.com/Jinshichi/items/9aba17a2a06bf96c0122
set /p<NUL="%~1" > "%~dp0.tmp"
certutil -encode "%~dp0.tmp" "%~dp0.out" >NUL
for /f %%i in ('findstr /b /c:"-" /v "%~dp0.out"') do set authEnc=%%i
del "%~dp0.tmp" "%~dp0.out"
echo %authEnc%
C:\Windows\WinSxS\amd64_curl_31bf3856ad364e35_10.0.17134.982_none_5fe7cd847360a601\curl.exe -H "Authorization: Basic %authEnc%" "http://timetracker01sv/TimeTrackerNX/api/system/users/%~2/timeEntries?startDate=%~3&finishDate=%~4" > JsWorkTime.txt
