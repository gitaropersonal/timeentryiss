﻿using System;
using System.Configuration;
using TimeEntryShared.Entity;
using TimeEntryShared.Util;
using TimeEntryShared.Const;
using TimeEntryShared.Service;

namespace TEB02000_OutputWorkTime {
    class Program {
        static void Main(string[] args) {
            LogAndConsoleUtil.ShowLogAndConsoleInfo(OutputTimeEntryMessages._MSG_OPERATION_START, LogAndConsoleUtil.StartEndLineType.Start);

            try {
                var appConfig = LoadAppConfig();
                var service = new OutputWorkTimeService();
                service.Main(appConfig);

            } catch (Exception e) {

                LogAndConsoleUtil.ShowLogAndConsoleErr(e.Message, e.StackTrace);

            } finally {

                LogAndConsoleUtil.ShowLogAndConsoleInfo(OutputTimeEntryMessages._MSG_OPERATION_END, LogAndConsoleUtil.StartEndLineType.End);

            }
        }
        /// <summary>
        /// AppConfigロード
        /// </summary>
        /// <returns></returns>
        private static WorkTimesConfigEntity LoadAppConfig() {
            var ret = new WorkTimesConfigEntity();
            ret.ComponentsPath               = ConfigurationManager.AppSettings[nameof(ret.ComponentsPath)].ToString();
            ret.TxtJsonWorkTimeName          = ConfigurationManager.AppSettings[nameof(ret.TxtJsonWorkTimeName)].ToString();
            ret.BatName                      = ConfigurationManager.AppSettings[nameof(ret.BatName)].ToString();
            ret.WorkTimesCsv                 = ConfigurationManager.AppSettings[nameof(ret.WorkTimesCsv)].ToString();
            ret.CsvWorkTimesCols_UserName    = ConfigurationManager.AppSettings[nameof(ret.CsvWorkTimesCols_UserName)].ToString();
            ret.CsvWorkTimesCols_ProjectCode = ConfigurationManager.AppSettings[nameof(ret.CsvWorkTimesCols_ProjectCode)].ToString();
            ret.CsvWorkTimesCols_ProjectName = ConfigurationManager.AppSettings[nameof(ret.CsvWorkTimesCols_ProjectName)].ToString();
            ret.CsvWorkTimesCols_WorkDate    = ConfigurationManager.AppSettings[nameof(ret.CsvWorkTimesCols_WorkDate)].ToString();
            ret.CsvWorkTimesCols_StartTime   = ConfigurationManager.AppSettings[nameof(ret.CsvWorkTimesCols_StartTime)].ToString();
            ret.CsvWorkTimesCols_FinishName  = ConfigurationManager.AppSettings[nameof(ret.CsvWorkTimesCols_FinishName)].ToString();
            return ret;
        }
    }
}
