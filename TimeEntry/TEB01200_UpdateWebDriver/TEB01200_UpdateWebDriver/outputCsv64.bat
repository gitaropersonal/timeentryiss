@echo off
setlocal
cd /d %~dp0

set /p<NUL="%~1" > "%~dp0.tmp"
certutil -encode "%~dp0.tmp" "%~dp0.out" >NUL
for /f %%i in ('findstr /b /c:"-" /v "%~dp0.out"') do set authEnc=%%i
del "%~dp0.tmp" "%~dp0.out"
echo %authEnc%
C:\Windows\WinSxS\amd64_curl_31bf3856ad364e35_10.0.17134.982_none_5fe7cd847360a601\curl.exe -H "Authorization: Basic %authEnc%" "http://timetracker01sv/TimeTrackerNX/api/workItem/workItems/timeEntries/export?filterBy=User&filterValues=%~2&startDate=%~3&finishDate=%~4&scale=day&groupBy=project&fileType=csv" > timeEntry.csv
