﻿using System;
using System.IO;
using TimeEntryShared.Util;
using TimeEntryShared.Const;

namespace TEB01200_UpdateWebDriver {
    class Program {

        static void Main(string[] args) {
            try {
                string configPath = Path.Combine(Environment.CurrentDirectory, TimeEntryConfigConst._CONFIG_FILE_NAME);
                var configInfo = CommonUtil.LoadConfig(configPath);
                CommonUtil.UpdateChromeDriver(configInfo.WebDrivertComponentsPath);

            } catch (Exception e) {

                LogAndConsoleUtil.ShowLogAndConsoleErr(e.Message, e.StackTrace);

            }
        }
    }
}
