﻿using System;
using System.IO;
using TimeTrackerAPILib.Logic;
using TimeTrackerAPILib.Util;

namespace API0001_OutputUserInfos {
    class Program {
        static void Main(string[] args) {
            try {
                // HTTPクエリ実行
                var ret = new API0001_OutputUserInfosLogic().GetInfos();

                // 出力パス取得
                string currJsPath = Path.Combine(Environment.CurrentDirectory, string.Format("JsUserInfo{0}.txt", Environment.MachineName));

                // JSONファイルを書きだす
                JsonUtil.SaveFile(ret, currJsPath);

                // ログ出力
                LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Concat("出力：", currJsPath));

            } catch (Exception e) {
                LogAndConsoleUtil.ShowLogAndConsoleErr(e.Message, e.StackTrace);
            }
        }
    }
}
