﻿using System;
using TimeTrackerAPILib.Logic;
using TimeTrackerAPILib.Util;

namespace API0006_UpdatePlannedTime {
    class Program {
        static void Main(string[] args) {
            try {
                if (args.Length < 3) {
                    return;
                }
                // HTTPクエリ実行
                var ret = new API0006_UpdatePlannedTimeLogic().Update(args[0], args[1], args[2]);

                // ログ出力
                LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Format("計画工数更新     ワークアイテムID：{0}  ユーザID：{1}  計画工数：{2}", args));

            } catch (Exception e) {
                LogAndConsoleUtil.ShowLogAndConsoleErr(e.Message, e.StackTrace);
            }
        }
    }
}
