@echo off
setlocal
cd /d %~dp0

set /p<NUL="administrator:Isstokyo155" > "%~dp0.tmp"
certutil -encode "%~dp0.tmp" "%~dp0.out" >NUL
for /f %%i in ('findstr /b /c:"-" /v "%~dp0.out"') do set authEnc=%%i
del "%~dp0.tmp" "%~dp0.out"
echo %authEnc%

REM curlでAPI発行する
REM   -XでPUT電文を指定
REM   実行ディレクトリにupdateResult.txtを生成

REM バッチファイル内でシングルクオートを使うとうまく動かないので
REM json部分はダブルクオートで括ったうえ、内側のダブルクオートたちを
REM バックスラッシュでエスケープしている。
C:\Windows\WinSxS\amd64_curl_31bf3856ad364e35_10.0.17134.982_none_5fe7cd847360a601\curl.exe -X PUT -H "Authorization: Basic %authEnc%" -H "Content-Type: application/json" -d "{ \"assignmentChange\":{ \"updates\":[ {\"user\":\"kasai\",\"PlannedTime\":\"480\"} ] , \"recalculatePlannedTimeByAssignment\":\"true\", } }" "http://timetracker01sv/TimeTrackerNX/api/workitem/workItems/15249"
