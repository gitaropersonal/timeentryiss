﻿using System;
using System.Net.Http;

namespace TimeTrackerAPILib.Logic {
    public class API0003_OutputProjectLogic : ApiBase {

        /// <summary>
        /// プロジェクト情報取得
        /// </summary>
        /// <param name="projectId"></param>
        public string GetInfos(string projectId) {
            if (string.IsNullOrEmpty(projectId)) {
                return string.Empty;
            }
            // HTTPクエリ実行（プロジェクト情報出力）
            ExecHttpQuery(projectId);

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            return result.ToString();
        }
        /// <summary>
        /// HTTPクエリ実行（プロジェクト情報出力）
        /// </summary>
        /// <param name="projectId"></param>
        public async void ExecHttpQuery(string projectId) {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(string.Format(TimeTrackerURL._URL_OUTPUT_PROJECT_INFO, projectId));
            using (var request = new HttpRequestMessage(HttpMethod.Get, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
    }
}