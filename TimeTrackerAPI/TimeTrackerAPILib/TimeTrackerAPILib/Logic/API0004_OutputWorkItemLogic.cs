﻿using System;
using System.Net.Http;

namespace TimeTrackerAPILib.Logic {
    public class API0004_OutputWorkItemLogic : ApiBase {
        
        /// <summary>
        /// ワークアイテム情報一覧取得
        /// </summary>
        /// <param name="projectId"></param>
        public string GetInfos(string workItemId) {
            if (string.IsNullOrEmpty(workItemId)) {
                return string.Empty;
            }
            // HTTPクエリ実行（ワークアイテム取得）
            ExecHttpQuery(workItemId);

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            return result.ToString();
        }
        /// <summary>
        /// HTTPクエリ実行（ワークアイテム取得）
        /// </summary>
        /// <param name="workItemId"></param>
        public async void ExecHttpQuery(string workItemId) {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(string.Format(TimeTrackerURL._URL_OUTPUT_WORK_ITEM, workItemId));
            using (var request = new HttpRequestMessage(HttpMethod.Get, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
    }
}