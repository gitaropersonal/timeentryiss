﻿using System;
using System.Net.Http;

namespace TimeTrackerAPILib.Logic {
    public class API0001_OutputUserInfosLogic : ApiBase {

        /// <summary>
        /// ユーザ情報一覧取得
        /// </summary>
        /// <param name="args"></param>
        public string GetInfos() {
            // HTTPクエリ実行（ユーザ情報一覧）
            ExecHttpQuery();

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            return result.ToString();
        }
        /// <summary>
        /// HTTPクエリ実行（ユーザ情報一覧）
        /// </summary>
        public async void ExecHttpQuery() {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(TimeTrackerURL._URL_OUTPUT_USER_INFOS);
            using (var request = new HttpRequestMessage(HttpMethod.Get, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
    }
}