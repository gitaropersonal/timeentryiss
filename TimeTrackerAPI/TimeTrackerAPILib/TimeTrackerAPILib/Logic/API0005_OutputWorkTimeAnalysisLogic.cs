﻿using System;
using System.Net.Http;

namespace TimeTrackerAPILib.Logic {
    public class API0005_OutputWorkTimeAnalysisLogic : ApiBase {

        /// <summary>
        /// 工数分析情報一覧取得
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public string GetInfos(string startDate, string endDate) {
            if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate)) {
                return string.Empty;
            }
            // HTTPクエリ実行（工数分析情報取得）
            ExecHttpQuery(startDate, endDate);

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            return result.ToString();
        }
        /// <summary>
        /// HTTPクエリ実行（工数分析情報取得）
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public  async void ExecHttpQuery(string startDate, string endDate) {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(TimeTrackerURL._URL_OUTPUT_WORK_TIME_ANALYSIS);
            using (var request = new HttpRequestMessage(HttpMethod.Post, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);
                request.Content = new StringContent(TimeTrackerURL.CreateBody_OutputWorkTimeAnalysis(startDate, endDate));

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
    }
}