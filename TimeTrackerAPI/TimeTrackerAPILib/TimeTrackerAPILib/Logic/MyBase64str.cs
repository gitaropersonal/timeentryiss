﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTrackerAPILib.Logic {
    /// <summary>
    /// ID・パスワードの変換クラス（Toヘッダー情報）
    /// </summary>
    public class MyBase64str {
        private Encoding enc;

        public MyBase64str(string encStr) {
            enc = Encoding.GetEncoding(encStr);
        }

        public string Encode(string str) {
            return Convert.ToBase64String(enc.GetBytes(str));
        }

        public string Decode(string str) {
            return enc.GetString(Convert.FromBase64String(str));
        }
    }
}
