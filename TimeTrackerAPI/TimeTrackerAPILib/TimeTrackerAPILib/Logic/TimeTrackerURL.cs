﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TimeTrackerAPILib.Logic {
    public static class TimeTrackerURL {
        public const string _AUTHORIZATION                 = "Authorization";
        public const string _APPLICATION_JSON              = "application/json";
        public const string _DEFAULT_ID_PASS               = "administrator:Isstokyo155";
        public const string _AUTHORIZATION_SCHEMA          = "Basic ";
        public const string _URL_OUTPUT_USER_INFOS         = "http://timetracker01sv/TimeTrackerNX/api/system/users";
        public const string _URL_OUTPUT_TIME_ENTRY         = "http://timetracker01sv/TimeTrackerNX/api/workItem/workItems/timeEntries/export?filterBy=User&filterValues={0}&startDate={1}&finishDate={2}&scale=day&groupBy=project&fileType=csv";
        public const string _URL_OUTPUT_PROJECT_INFO       = "http://timetracker01sv/TimeTrackerNX/api/project/projects/{0}";
        public const string _URL_OUTPUT_WORK_ITEM          = "http://timetracker01sv/TimeTrackerNX/api/workitem/workItems/{0}/subItems?fields=Id,Name,PlannedStartDate,PlannedFinishDate,PlannedTime,ActualTime";
        public const string _URL_OUTPUT_WORK_TIME_ANALYSIS = "http://timetracker01sv/TimeTrackerNX/api/analytics/timeEntities";
        public const string _URL_UPDATE_PLANNED_TIME       = "http://timetracker01sv/TimeTrackerNX/api/workitem/workItems/{0}";

        /// <summary>
        /// BODY取得（工数分析）
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static string CreateBody_OutputWorkTimeAnalysis(string startDate, string endDate) {
            string str = string.Empty;
            str += "{  \"fields\": [    {      \"name\": \"plannedTime\"";
            str += ",      \"calcBy\": \"sum\"    }";
            str += ",    {      \"name\": \"actualTime\"";
            str += ",      \"calcBy\": \"sum\"    }";
            str += ",    {      \"name\": \"plannedCost\"";
            str += ",      \"calcBy\": \"sum\"    }";
            str += ",    {      \"name\": \"actualCost\"";
            str += ",      \"calcBy\": \"sum\"    }  ]";
            str += ",  \"groups\": [    {      \"name\": \"date\"";
            str += ",      \"granularity\": \"month\"    }";
            str += ",    {      \"name\": \"project\"";
            str += ",        \"granularity\": \"project\"    }";
            str += ",    {      \"name\": \"user\"";
            str += ",      \"granularity\": \"user\"    }  ]";
            str += ",  \"filterBy\": {    \"type\": \"And\"";
            str += ",    \"children\": [    ]  }";
            str += ",    \"filterBy\": {        \"type\": \"And\"";
            str += ",        \"children\": [            {                \"type\": \"Ge\"";
            str += ",                \"left\": {                    \"type\": \"field\"";
            str += ",                    \"name\": \"date\"                }";
            str += ",                \"right\": {                    \"type\": \"value\"";
            str += ",                    \"value\": \"" + startDate + "\"                }            }";
            str += ",            {                \"type\": \"Le\"";
            str += ",                \"left\": {                    \"type\": \"field\"";
            str += ",                    \"name\": \"date\"                }";
            str += ",                \"right\": {                    \"type\": \"value\"";
            str += ",                    \"value\": \""+ endDate + "\"                }            }        ]    }}";
            return str;
        }
        /// <summary>
        /// BODY取得（計画工数更新）
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="plannedTime"></param>
        /// <returns></returns>
        public static string CreateBody_UpdatePlannedTime(string userId, string plannedTime) {
            string str = string.Empty;
            str += "{ \"assignmentChange\":{ \"updates\":[ {\"user\":\""+ userId + "\"";
            str += ", \"PlannedTime\":\""+ plannedTime + "\"} ] ";
            str += ", \"recalculatePlannedTimeByAssignment\":\"true\"";
            str += ", } }";
            return str;
        }
    }
}
