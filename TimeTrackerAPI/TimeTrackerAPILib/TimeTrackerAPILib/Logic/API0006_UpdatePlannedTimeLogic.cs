﻿using System;
using System.Net.Http;
using System.Text;

namespace TimeTrackerAPILib.Logic {
    public class API0006_UpdatePlannedTimeLogic : ApiBase {

        /// <summary>
        /// 計画工数更新
        /// </summary>
        /// <param name="workItemId"></param>
        /// <param name="userId"></param>
        /// <param name="plannedTime"></param>
        /// <returns></returns>
        public string Update(string workItemId, string userId, string plannedTime) {
            if (string.IsNullOrEmpty(workItemId) || string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(plannedTime)) {
                return string.Empty;
            }
            // HTTPクエリ実行（工数分析情報取得）
            ExecHttpQuery(workItemId, userId, plannedTime);

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            return result.ToString();
        }
        /// <summary>
        /// HTTPクエリ実行（計画工数更新）
        /// </summary>
        /// <param name="workItemId"></param>
        /// <param name="userId"></param>
        /// <param name="plannedTime"></param>
        public async void ExecHttpQuery(string workItemId, string userId, string plannedTime) {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(string.Format(TimeTrackerURL._URL_UPDATE_PLANNED_TIME, workItemId));
            using (var request = new HttpRequestMessage(HttpMethod.Put, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);
                request.Content = new StringContent(TimeTrackerURL.CreateBody_UpdatePlannedTime(userId, plannedTime), Encoding.UTF8, TimeTrackerURL._APPLICATION_JSON);

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
    }
}