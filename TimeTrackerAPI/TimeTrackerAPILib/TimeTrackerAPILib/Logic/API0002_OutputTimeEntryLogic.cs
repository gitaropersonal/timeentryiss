﻿using System;
using System.Net.Http;

namespace TimeTrackerAPILib.Logic {
    public class API0002_OutputTimeEntryLogic : ApiBase {

        /// <summary>
        /// ユーザ情報一覧取得
        /// </summary>
        /// <param name="parameters"></param>
        public string[] GetInfos(string[] parameters) {
            if (parameters.Length < 3) {
                return new string[] { };
            }
            // HTTPクエリ実行（ユーザ情報一覧）
            ExecHttpQuery(parameters);

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            if (result == null || string.IsNullOrEmpty(result.ToString())) {
                return new string[] { };
            }
            string[] del = { "\r\n" };
            var ret = result.ToString().Split(del, StringSplitOptions.None);
            return ret;
        }
        /// <summary>
        /// HTTPクエリ実行（実績工数CSV出力）
        /// </summary>
        /// <param name="parameters"></param>
        public async void ExecHttpQuery(string[] parameters) {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(string.Format(TimeTrackerURL._URL_OUTPUT_TIME_ENTRY, parameters));
            using (var request = new HttpRequestMessage(HttpMethod.Get, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
    }
}