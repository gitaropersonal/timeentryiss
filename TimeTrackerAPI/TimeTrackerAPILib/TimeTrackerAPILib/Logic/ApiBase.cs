﻿using System.Text;
using System.IO;
using System.Threading;
using System.Net.Http;

namespace TimeTrackerAPILib.Logic {
    public abstract class ApiBase {
        protected static HttpClient _HTTP_CLIENT = new HttpClient();
        protected static HttpResponseMessage _RESPONSE = new HttpResponseMessage();
        protected const int _MAX_RETRY_COUNT = 120;
        protected const int _SLEEP_TIME_500 = 500;
        protected const string _MSG_ERROR_AWAIT_FAILED = "TimeTrackerとの通信がタイムアウトしました({0}ミリ秒)。\r\nしばらく時間がたってからリトライしてください。";

        /// <summary>
        /// レスポンス待機
        /// </summary>
        /// <returns></returns>
        protected bool AwaitResponse() {
            int reTryCount = 0;
            while (_RESPONSE.Content == null && reTryCount <= _MAX_RETRY_COUNT) {
                Thread.Sleep(_SLEEP_TIME_500);
                reTryCount++;
            }
            if (_RESPONSE.Content == null || _MAX_RETRY_COUNT < reTryCount) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// レスポンスコンテンツ取得
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        protected object Extract(HttpContent content) {
            var read = content.ReadAsStringAsync();
            read.Wait();
            //reset the internal stream position to allow the WebAPI pipeline to read it again.
            content.ReadAsStreamAsync()
                .ContinueWith(t => {
                    if (t.Result.CanSeek) {
                        t.Result.Seek(0, SeekOrigin.Begin);
                    }
                })
                .Wait();

            return read.Result;
        }
        /// <summary>
        /// ID・パスワードをヘッダー情報に変換
        /// </summary>
        /// <returns></returns>_DEFAULT_ID_PASS
        protected string ConvetIdPassToHeader() {
            var base64 = new MyBase64str(Encoding.UTF8.BodyName);
            return string.Concat(TimeTrackerURL._AUTHORIZATION_SCHEMA, base64.Encode(TimeTrackerURL._DEFAULT_ID_PASS));
        }
        /// <summary>
        /// メッセージ作成（通信タイムアウト）
        /// </summary>
        /// <returns></returns>
        protected string CreateMessage_ErrAwait() {
            return string.Format(_MSG_ERROR_AWAIT_FAILED, _SLEEP_TIME_500 * _MAX_RETRY_COUNT);
        }
    }
}
