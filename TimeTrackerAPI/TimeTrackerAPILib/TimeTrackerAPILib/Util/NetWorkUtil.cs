﻿using System;
using System.Net;

namespace TimeTrackerAPILib.Util {
    static class NetworkUtil {
        /// <summary> IPアドレスを取得する</summary>
        static public String GetIpAddress() {
            IPAddress[] adrList = Dns.GetHostAddresses(Environment.MachineName);
            foreach (IPAddress address in adrList) {
                // IPv4 を返却する
                if (address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork) {
                    return address.ToString();
                }
            }
            return null;
        }
    }
}
