﻿using System.IO;
using System.Text;

namespace TimeTrackerAPILib.Util {
    /// <summary>
    /// CSV出力
    /// </summary>
    public static class CsvUtil {
        public static void SaveFile(string[] lines, string fileName) {
            using (var sw = new StreamWriter(fileName, false, Encoding.GetEncoding("Shift_JIS"))) {
                foreach (var row in lines) {
                    if (string.IsNullOrEmpty(row)) {
                        continue;
                    }
                    sw.WriteLine(row);
                }
            }
        }
    }
}
