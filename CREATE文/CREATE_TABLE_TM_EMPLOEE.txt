--=================================
USE TimeEntry
GO
----------------------------
    CREATE TABLE TM_EMPLOEE
    (
        [EmploeeNum]        nvarchar(6)   NOT NULL   DEFAULT '',
        [BuCode]            nvarchar(3)   NOT NULL   DEFAULT '',
        [GroupCode]         nvarchar(6)              DEFAULT '',
        [Name]              nvarchar(40)  NOT NULL   DEFAULT '',
        [LoginPass]         nvarchar(16)  NOT NULL   DEFAULT 'Isstokyo155',
        [NightShiftFlg]     tinyint       NOT NULL   DEFAULT 0,
        [DefaultWorkFlg]    tinyint       NOT NULL   DEFAULT 0,
        [MhscFlg]           tinyint       NOT NULL   DEFAULT 0,
        [ForcingShiftBFlg]  tinyint       NOT NULL   DEFAULT 0,
        [DeleteFlg]         tinyint       NOT NULL   DEFAULT 0

        CONSTRAINT PK_TM_EMPLOEE PRIMARY KEY CLUSTERED
        (
            [EmploeeNum]
        )
    ) 

----------------------------------------------
EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'社員マスタ'                ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'TM_EMPLOEE'
EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'社員番号'                  ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'TM_EMPLOEE',@level2type=N'COLUMN',@level2name=N'EmploeeNum'
EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'部署コード'                ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'TM_EMPLOEE',@level2type=N'COLUMN',@level2name=N'BuCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'グループコード'            ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'TM_EMPLOEE',@level2type=N'COLUMN',@level2name=N'GroupCode'
EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'社員名'                    ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'TM_EMPLOEE',@level2type=N'COLUMN',@level2name=N'Name'
EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'ログインパスワード'        ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'TM_EMPLOEE',@level2type=N'COLUMN',@level2name=N'LoginPass'
EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'夜勤担当フラグ'            ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'TM_EMPLOEE',@level2type=N'COLUMN',@level2name=N'NightShiftFlg'
EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'デフォルト作業使用フラグ'  ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'TM_EMPLOEE',@level2type=N'COLUMN',@level2name=N'DefaultWorkFlg'
EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'MHSC所属フラグ'            ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'TM_EMPLOEE',@level2type=N'COLUMN',@level2name=N'MhscFlg'
EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'強制Bシフトフラグ'         ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'TM_EMPLOEE',@level2type=N'COLUMN',@level2name=N'ForcingShiftBFlg'
EXEC sys.sp_addextendedproperty @name=N'MS_Description',@value=N'削除フラグ'                ,@level0type=N'SCHEMA',@level0name=N'dbo',@level1type=N'TABLE',@level1name=N'TM_EMPLOEE',@level2type=N'COLUMN',@level2name=N'DeleteFlg'
