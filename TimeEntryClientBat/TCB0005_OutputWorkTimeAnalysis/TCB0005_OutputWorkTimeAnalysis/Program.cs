﻿using System;
using System.IO;
using ClientBatShared.TimeTracker;
using ClientBatShared.Util;

namespace TCB0005_OutputWorkTimeAnalysis {
    class Program {
        static void Main(string[] args) {
            try {
                if (args.Length < 2) {
                    return;
                }
                // HTTPクエリ実行
                var ret = TimeTrackerAPI.Get_WorkTimeAnalysis(args[0], args[1]);

                // 出力パス取得
                string currJsonPath = Path.Combine(Environment.CurrentDirectory, string.Format("workTimeAnalysis{0}.txt", Environment.MachineName));

                // JSONファイルを書きだす
                JsonUtil.SaveFile(ret, currJsonPath);

                // ログ出力
                LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Concat("出力：", currJsonPath));

            } catch (Exception e) {
                LogAndConsoleUtil.ShowLogAndConsoleErr(e.Message, e.StackTrace);
            }
        }
    }
}
