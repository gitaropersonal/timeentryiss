﻿using System;
using System.IO;
using ClientBatShared.TimeTracker;
using ClientBatShared.Util;

namespace TCB0004_OutputWorkItem {
    class Program {
        static void Main(string[] args) {
            try {
                // HTTPクエリ実行
                var ret = TimeTrackerAPI.Get_WorkItemInfo(args[0]);
                if (string.IsNullOrEmpty(ret)) {
                    return;
                }
                // 出力パス取得
                string currJsonPath = Path.Combine(Environment.CurrentDirectory, string.Format("jsWorkItems{0}.txt", Environment.MachineName));

                // JSONファイルを書きだす
                JsonUtil.SaveFile(ret, currJsonPath);

                // ログ出力
                LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Concat("出力：", currJsonPath));

            } catch (Exception e) {
                LogAndConsoleUtil.ShowLogAndConsoleErr(e.Message, e.StackTrace);
            }
        }
    }
}
