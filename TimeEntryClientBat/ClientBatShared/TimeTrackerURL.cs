﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientBatShared {
    public static class TimeTrackerURL {
        public const string _AUTHORIZATION = "Authorization";
        public const string _DEFAULT_ID_PASS = "administrator:Isstokyo155";
        public const string _AUTHORIZATION_SCHEMA = "Basic ";
        public const string _URL_OUTPUT_USER_INFOS = "http://timetracker01sv/TimeTrackerNX/api/system/users";
    }
}
