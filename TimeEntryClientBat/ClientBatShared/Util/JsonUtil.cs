﻿using System.IO;
using System.Text;
using Newtonsoft.Json;

namespace ClientBatShared.Util {
    /// <summary>
    /// JSONファイル出力
    /// </summary>
    public static class JsonUtil {
        public static void SaveFile(object obj, string fileName) {
            string targetListOnJSON = JsonConvert.SerializeObject(obj, Formatting.Indented);
            using (var sw = new StreamWriter(fileName, false, Encoding.GetEncoding("UTF-8"))) {
                sw.Write(targetListOnJSON);
            }
        }
    }
}
