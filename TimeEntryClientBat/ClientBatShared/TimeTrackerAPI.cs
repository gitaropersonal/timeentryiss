﻿using System;
using System.Text;
using System.IO;
using System.Threading;
using System.Net.Http;
using System.Net.Http.Headers;

namespace ClientBatShared {
    public static class TimeTrackerAPI {
        private static HttpClient _HTTP_CLIENT = new HttpClient();
        private static HttpResponseMessage _RESPONSE = new HttpResponseMessage();
        private const int _MAX_RETRY_COUNT = 10;
        private const int _SLEEP_TIME_500 = 500;

        #region ユーザ情報一覧
        /// <summary>
        /// ユーザ情報一覧取得
        /// </summary>
        /// <param name="args"></param>
        public static string Get_TimeTrackerUseInfos() {
            // HTTPクエリ実行（ユーザ情報一覧）
            ExecHttpQuery_OutputUseInfos();

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception();
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            return result.ToString();
        }
        /// <summary>
        /// HTTPクエリ実行（ユーザ情報一覧）
        /// </summary>
        public static async void ExecHttpQuery_OutputUseInfos() {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(TimeTrackerURL._URL_OUTPUT_USER_INFOS);
            using (var request = new HttpRequestMessage(HttpMethod.Get, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
        #endregion

        /// <summary>
        /// HTTPクエリ実行（計画工数更新）
        /// </summary>
        public static void ExecHttpQuery_UpdatePlanndeTime() {
            using (var httpClient = new HttpClient()) {
                using (var request = new HttpRequestMessage(new HttpMethod("POST"), "http://timetracker01sv/TimeTrackerNX/api/analytics/timeEntities")) {
                    request.Headers.TryAddWithoutValidation("Authorization", "Basic %authEnc%");

                    request.Content = new StringContent("{  \"fields\": [    {      \"name\": \"plannedTime\",      \"calcBy\": \"sum\"    },    {      \"name\": \"actualTime\",      \"calcBy\": \"sum\"    },   {      \"name\": \"plannedCost\",      \"calcBy\": \"sum\"    },    {      \"name\": \"actualCost\",      \"calcBy\": \"sum\"    }  ],  \"groups\": [    {      \"name\": \"date\",      \"granularity\": \"month\"    },    {      \"name\": \"project\",        \"granularity\": \"project\"    },    {      \"name\": \"user\",      \"granularity\": \"user\"    }  ],  \"filterBy\": {    \"type\": \"And\",    \"children\": [    ]  },    \"filterBy\": {        \"type\": \"And\",        \"children\": [            {                \"type\": \"Ge\",                \"left\": {                    \"type\": \"field\",                    \"name\": \"date\"                },                \"right\": {                    \"type\": \"value\",                    \"value\": \"%~2\"                }            },            {                \"type\": \"Le\",                \"left\": {                    \"type\": \"field\",                    \"name\": \"date\"                },                \"right\": {                    \"type\": \"value\",                    \"value\": \"%~3\"                }            }        ]    }}");
                    request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/x-www-form-urlencoded");

                    var response = httpClient.SendAsync(request);
                }
            }
        }

        #region 共通
        /// <summary>
        /// レスポンス待機
        /// </summary>
        /// <returns></returns>
        private static bool AwaitResponse() {
            int reTryCount = 0;
            while (_RESPONSE.Content == null && reTryCount <= _MAX_RETRY_COUNT) {
                Thread.Sleep(_SLEEP_TIME_500);
                reTryCount++;
            }
            if (_RESPONSE.Content == null || _MAX_RETRY_COUNT < reTryCount) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// レスポンスコンテンツ取得
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static object Extract(HttpContent content) {
            var read = content.ReadAsStringAsync();
            read.Wait();
            //reset the internal stream position to allow the WebAPI pipeline to read it again.
            content.ReadAsStreamAsync()
                .ContinueWith(t => {
                    if (t.Result.CanSeek) {
                        t.Result.Seek(0, SeekOrigin.Begin);
                    }
                })
                .Wait();

            return read.Result;
        }
        /// <summary>
        /// ID・パスワードをヘッダー情報に変換
        /// </summary>
        /// <returns></returns>_DEFAULT_ID_PASS
        private static string ConvetIdPassToHeader() {
            var base64 = new MyBase64str(Encoding.UTF8.BodyName);
            return string.Concat(TimeTrackerURL._AUTHORIZATION_SCHEMA, base64.Encode(TimeTrackerURL._DEFAULT_ID_PASS));
        }
        #endregion
    }
    /// <summary>
    /// ID・パスワードの変換クラス（Toヘッダー情報）
    /// </summary>
    public class MyBase64str {
        private Encoding enc;

        public MyBase64str(string encStr) {
            enc = Encoding.GetEncoding(encStr);
        }

        public string Encode(string str) {
            return Convert.ToBase64String(enc.GetBytes(str));
        }

        public string Decode(string str) {
            return enc.GetString(Convert.FromBase64String(str));
        }
    }
}