﻿using System;
using System.Text;
using System.IO;
using System.Threading;
using System.Net.Http;

namespace ClientBatShared.TimeTracker {
    public static class TimeTrackerAPI {
        private static HttpClient _HTTP_CLIENT = new HttpClient();
        private static HttpResponseMessage _RESPONSE = new HttpResponseMessage();
        private const int _MAX_RETRY_COUNT = 120;
        private const int _SLEEP_TIME_500 = 500;
        private const string _MSG_ERROR_AWAIT_FAILED = "TimeTrackerとの通信がタイムアウトしました({0}ミリ秒)。\r\nしばらく時間がたってからリトライしてください。";

        #region ユーザ情報一覧
        /// <summary>
        /// ユーザ情報一覧取得
        /// </summary>
        /// <param name="args"></param>
        public static string Get_TimeTrackerUseInfos() {
            // HTTPクエリ実行（ユーザ情報一覧）
            ExecHttpQuery_OutputUseInfos();

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            return result.ToString();
        }
        /// <summary>
        /// HTTPクエリ実行（ユーザ情報一覧）
        /// </summary>
        public static async void ExecHttpQuery_OutputUseInfos() {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(TimeTrackerURL._URL_OUTPUT_USER_INFOS);
            using (var request = new HttpRequestMessage(HttpMethod.Get, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
        #endregion

        #region 実績工数出力
        /// <summary>
        /// ユーザ情報一覧取得
        /// </summary>
        /// <param name="parameters"></param>
        public static string[] Get_TimeEntryCsv(string[] parameters) {
            if (parameters.Length < 3) {
                return new string[] { };
            }
            // HTTPクエリ実行（ユーザ情報一覧）
            ExecHttpQuery_TimeEntryCsv(parameters);

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            if (result == null || string.IsNullOrEmpty(result.ToString())) {
                return new string[] { };
            }
            string[] del = { "\r\n" };
            var ret = result.ToString().Split(del, StringSplitOptions.None);
            return ret;
        }
        /// <summary>
        /// HTTPクエリ実行（実績工数CSV出力）
        /// </summary>
        /// <param name="parameters"></param>
        public static async void ExecHttpQuery_TimeEntryCsv(string[] parameters) {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(string.Format(TimeTrackerURL._URL_OUTPUT_TIME_ENTRY, parameters));
            using (var request = new HttpRequestMessage(HttpMethod.Get, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
        #endregion

        #region プロジェクト情報出力
        /// <summary>
        /// プロジェクト情報取得
        /// </summary>
        /// <param name="projectId"></param>
        public static string Get_PrijectInfo(string projectId) {
            if (string.IsNullOrEmpty(projectId)) {
                return string.Empty;
            }
            // HTTPクエリ実行（プロジェクト情報出力）
            ExecHttpQuery_ProjectInfo(projectId);

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            return result.ToString();
        }
        /// <summary>
        /// HTTPクエリ実行（プロジェクト情報出力）
        /// </summary>
        /// <param name="projectId"></param>
        public static async void ExecHttpQuery_ProjectInfo(string projectId) {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(string.Format(TimeTrackerURL._URL_OUTPUT_PROJECT_INFO, projectId));
            using (var request = new HttpRequestMessage(HttpMethod.Get, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
        #endregion

        #region ワークアイテム情報出力
        /// <summary>
        /// ワークアイテム情報一覧取得
        /// </summary>
        /// <param name="projectId"></param>
        public static string Get_WorkItemInfo(string workItemId) {
            if (string.IsNullOrEmpty(workItemId)) {
                return string.Empty;
            }
            // HTTPクエリ実行（ワークアイテム取得）
            ExecHttpQuery_WorkItemtInfo(workItemId);

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            return result.ToString();
        }
        /// <summary>
        /// HTTPクエリ実行（ワークアイテム取得）
        /// </summary>
        /// <param name="workItemId"></param>
        public static async void ExecHttpQuery_WorkItemtInfo(string workItemId) {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(string.Format(TimeTrackerURL._URL_OUTPUT_WORK_ITEM, workItemId));
            using (var request = new HttpRequestMessage(HttpMethod.Get, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
        #endregion

        #region 工数分析情報出力
        /// <summary>
        /// 工数分析情報一覧取得
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public static string Get_WorkTimeAnalysis(string startDate, string endDate) {
            if (string.IsNullOrEmpty(startDate) || string.IsNullOrEmpty(endDate)) {
                return string.Empty;
            }
            // HTTPクエリ実行（工数分析情報取得）
            ExecHttpQuery_WorkTimeAnalysis(startDate, endDate);

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            return result.ToString();
        }
        /// <summary>
        /// HTTPクエリ実行（工数分析情報取得）
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public static async void ExecHttpQuery_WorkTimeAnalysis(string startDate, string endDate) {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(TimeTrackerURL._URL_OUTPUT_WORK_TIME_ANALYSIS);
            using (var request = new HttpRequestMessage(HttpMethod.Post, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);
                request.Content = new StringContent(TimeTrackerURL.CreateBody_OutputWorkTimeAnalysis(startDate, endDate));

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
        #endregion

        #region 計画工数更新
        /// <summary>
        /// 計画工数更新
        /// </summary>
        /// <param name="workItemId"></param>
        /// <param name="userId"></param>
        /// <param name="plannedTime"></param>
        /// <returns></returns>
        public static string Update_PlannedTime(string workItemId, string userId, string plannedTime) {
            if (string.IsNullOrEmpty(workItemId) || string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(plannedTime)) {
                return string.Empty;
            }
            // HTTPクエリ実行（工数分析情報取得）
            ExecHttpQuery_UpdatePlannedTime(workItemId, userId, plannedTime);

            // レスポンス待ち
            if (!AwaitResponse()) {
                throw new Exception(CreateMessage_ErrAwait());
            }
            // レスポンスコンテンツ取得
            var result = Extract(_RESPONSE.Content);
            return result.ToString();
        }
        /// <summary>
        /// HTTPクエリ実行（計画工数更新）
        /// </summary>
        /// <param name="workItemId"></param>
        /// <param name="userId"></param>
        /// <param name="plannedTime"></param>
        public static async void ExecHttpQuery_UpdatePlannedTime(string workItemId, string userId, string plannedTime) {
            _HTTP_CLIENT = new HttpClient();
            var uri = new Uri(string.Format(TimeTrackerURL._URL_UPDATE_PLANNED_TIME, workItemId));
            using (var request = new HttpRequestMessage(HttpMethod.Put, uri)) {

                // ID・パスワードをヘッダー情報に変換
                string cnvStr = ConvetIdPassToHeader();
                request.Headers.Add(TimeTrackerURL._AUTHORIZATION, cnvStr);
                request.Content = new StringContent(TimeTrackerURL.CreateBody_UpdatePlannedTime(userId, plannedTime), Encoding.UTF8, TimeTrackerURL._APPLICATION_JSON);

                // 実行
                _RESPONSE = await _HTTP_CLIENT.SendAsync(request);
            }
        }
        #endregion

        #region 共通
        /// <summary>
        /// レスポンス待機
        /// </summary>
        /// <returns></returns>
        private static bool AwaitResponse() {
            int reTryCount = 0;
            while (_RESPONSE.Content == null && reTryCount <= _MAX_RETRY_COUNT) {
                Thread.Sleep(_SLEEP_TIME_500);
                reTryCount++;
            }
            if (_RESPONSE.Content == null || _MAX_RETRY_COUNT < reTryCount) {
                return false;
            }
            return true;
        }
        /// <summary>
        /// レスポンスコンテンツ取得
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static object Extract(HttpContent content) {
            var read = content.ReadAsStringAsync();
            read.Wait();
            //reset the internal stream position to allow the WebAPI pipeline to read it again.
            content.ReadAsStreamAsync()
                .ContinueWith(t => {
                    if (t.Result.CanSeek) {
                        t.Result.Seek(0, SeekOrigin.Begin);
                    }
                })
                .Wait();

            return read.Result;
        }
        /// <summary>
        /// ID・パスワードをヘッダー情報に変換
        /// </summary>
        /// <returns></returns>_DEFAULT_ID_PASS
        private static string ConvetIdPassToHeader() {
            var base64 = new MyBase64str(Encoding.UTF8.BodyName);
            return string.Concat(TimeTrackerURL._AUTHORIZATION_SCHEMA, base64.Encode(TimeTrackerURL._DEFAULT_ID_PASS));
        }
        /// <summary>
        /// メッセージ作成（通信タイムアウト）
        /// </summary>
        /// <returns></returns>
        private static string CreateMessage_ErrAwait() {
            return string.Format(_MSG_ERROR_AWAIT_FAILED, _SLEEP_TIME_500 * _MAX_RETRY_COUNT);
        }
        #endregion
    }
    /// <summary>
    /// ID・パスワードの変換クラス（Toヘッダー情報）
    /// </summary>
    public class MyBase64str {
        private Encoding enc;

        public MyBase64str(string encStr) {
            enc = Encoding.GetEncoding(encStr);
        }

        public string Encode(string str) {
            return Convert.ToBase64String(enc.GetBytes(str));
        }

        public string Decode(string str) {
            return enc.GetString(Convert.FromBase64String(str));
        }
    }
}