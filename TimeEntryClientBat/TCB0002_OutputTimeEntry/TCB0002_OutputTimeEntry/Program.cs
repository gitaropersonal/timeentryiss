﻿using System;
using System.IO;
using ClientBatShared.TimeTracker;
using ClientBatShared.Util;

namespace TCB0002_OutputTimeEntry {
    class Program {
        static void Main(string[] args) {
            try {
                // HTTPクエリ実行
                var ret = TimeTrackerAPI.Get_TimeEntryCsv(args);
                if (ret.Length < 3) {
                    return;
                }
                // 出力パス取得
                string currCsvPath = Path.Combine(Environment.CurrentDirectory, string.Format("TimeEntry{0}.csv", string.Concat(Environment.MachineName, args[0])));

                // JSONファイルを書きだす
                CsvUtil.SaveFile(ret, currCsvPath);

                // ログ出力
                LogAndConsoleUtil.ShowLogAndConsoleInfo(string.Concat("出力：", currCsvPath));

            } catch (Exception e) {
                LogAndConsoleUtil.ShowLogAndConsoleErr(e.Message, e.StackTrace);
            }
        }
    }
}
